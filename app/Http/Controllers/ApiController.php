<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ItenaryOrder;
use App\ItenaryLeg;
use App\PassengerInformation;
use App\LegDetail;
use App\UserIpDetails;
use App\Country;
use App\City;
use App\CitiesLatLong;
use App\SavedTrips;
use App\Domain;
use App\DomainFrontend;
use App\FlightMeal;
use App\AgentNotification;

class ApiController extends Controller
{
    public function getItineraryDetail($order_id) {
    	$itinerary_order = ItenaryOrder::where('order_id',$order_id)->first();
    	$itinerary_leg = ItenaryLeg::where('itenary_order_id',$order_id)->get();

    	$passenger_information = PassengerInformation::where('itenary_order_id',$order_id)->get();

    	$response['itinerary_order'] = $itinerary_order;
    	$response['passenger_information'] = $passenger_information;

    	foreach ($itinerary_leg as $leg) {
    		$country = Country::getCountryByCode($leg['country_code']);
    		$city = City::getCityDetail($leg['from_city_id']);
            $cityLatLong = CitiesLatLong::where('city_id',$leg['from_city_id'])->first();
    		if(!empty($country['name'])) {
    			$leg['country_name'] = $country['name'];
    		} else {
    			$leg['country_name'] = '';
    		}

            if(!empty($city['description'])) {
                $leg['city_description'] = $city['description'];
            } else {
                $leg['city_description'] = '';
            }
            if(!empty($city['small'])) {
                $leg['city_image'] = 'https://cms.eroam.com/'.$city['small'];
            } else {
                $leg['city_image'] = 'http://dev.eroam.com/assets/images/no-image-2.jpg';
            }

    		$response['itinerary_leg'][] = $leg;
            $response['city_detail'][] = ['lat'=>$cityLatLong['lat'],'lng'=>$cityLatLong['lng'],'name'=> $city['name']];    		
            $leg['leg_detail'] = LegDetail::where('itenary_order_id',$order_id)->where('itenary_leg_id',$leg['itenary_leg_id'])->get();

       	}
    	return response()->json([
				'successful' => true, 'response' => $response
				]);
    }

    public function addUserIpDetail() {
    	$user_ip_detail = new UserIpDetails;
        
        $user_ip_detail->search_type = request()->search_type;
    	$user_ip_detail->ip_address = request()->ip_address;
        $user_ip_detail->city = request()->city;
        $user_ip_detail->country_code = request()->country_code;
    	$user_ip_detail->country_name = request()->country_name;
        $user_ip_detail->interestlist = request()->interestlist;
        $user_ip_detail->nationality = request()->nationality;
        $user_ip_detail->latitude = request()->latitude;
        $user_ip_detail->longitude = request()->longitude;
        $user_ip_detail->region = request()->region;
        $user_ip_detail->accommodation = request()->accommodation;
        $user_ip_detail->room = request()->room;
        $user_ip_detail->transport = request()->transport;
        $user_ip_detail->age_group = request()->age_group;
        $user_ip_detail->gender = request()->gender;
        $user_ip_detail->interest_list_id = request()->interest_list_id;
        $user_ip_detail->tour_type = request()->tour_type;
        $user_ip_detail->project = request()->project;
        $user_ip_detail->type_option = request()->type_option;
        $user_ip_detail->package_option = request()->package_option;
        $user_ip_detail->start_location = request()->start_location;
        $user_ip_detail->end_location = request()->end_location;
        $user_ip_detail->start_date = request()->start_date;
        $user_ip_detail->save();

    	return response()->json(['success' => true,'data'=>$user_ip_detail]);
    }

    public function saveTripDetail() {

        $trip_detail = new SavedTrips;

        $trip_detail->user_id = request()->user_id;
        $trip_detail->trip_data = request()->trip_data;
        $trip_detail->map_data = request()->map_data;
        $trip_detail->search_input = request()->search_input;
        $trip_detail->save();
        if(!empty(request()->agent_id)){
            $timeSevenDays = strtotime("+7 day",time());
            $AgentNotification = AgentNotification::create([
                'agent_id'=> request()->agent_id,
                'read'=> request()->agent_read,
                'booking_id'=> 0,
                'notification'=> 'Pending Booking Follow-Up',
                'showing_date'=> date('Y-m-d', $timeSevenDays),
                'type'=> 'Itenary Booking Follow-Up',
            ]);
        }
        return response()->json(['success'=> true,'data'=>$trip_detail]);
    }

    public function getSavedTrips($user_id) {
        $response = SavedTrips::where('user_id',$user_id)->get();
        return response()->json([
            'successful' => true, 'response' => $response
        ]);
    }

    public function getSavedTripDetail($trip_id) {
        $response = SavedTrips::where('id',$trip_id)->first();
        return response()->json([
            'successful' => true, 'response'=> $response
        ]);
    }

    public function getDomainLogo() {
        $domain = request()->domain;
        $frontend = Domain::where('name',$domain)->with('frontend')->first()['frontend'];
        return ['logo'=>'https://eroam-dev.s3.us-west-2.amazonaws.com/onboardingLogo/'.$frontend['logo'],'header_color'=>$frontend['header_color'],'footer_color'=>$frontend['footer_color']];
    }

    public function getSocialLinks(){
        $domain = request()->domain;
        $frontend = Domain::where('name',$domain)->with('frontend')->first()['frontend'];
        return $frontend;
    }
    
    public function getDomainProduct() {

        $domain = request()->domain;

        $products = Domain::where('name',$domain)->with('products')->first();
        return ['products' => $products['products']];
    }

    // for testing purpose
    public function getDomainProductDhara() {
        $domain = request()->header('domain');

        $products = Domain::where('name',$domain)->with('products')->first();
        return ['products' => $products['products']];
    }
    

    public function getLicenseeAccount() {
        $domain = request()->domain;
        $licensee_account = Domain::where('name',$domain)->with('licenseeAccount')->first();
        return ['licensee_account' => $licensee_account];
    }

    public function getInventoryData() {
        $domain = request()->domain;
        $inventory = Domain::where('name',$domain)->with('inventory')->first();

        return ['inventory' => $inventory['inventory']];
    }

    public function allCountryForSelect()
    {
        $data = Country::where('show_on_eroam',1)
                        ->pluck('name','code');
        return ['country' => $data];
    }
     public function allCountryForSelectTemp()
    {
        $data = Country::where('show_on_eroam',1)
                        ->select('name','code','country_code')->orderBy('name','asc')->get();
        return ['country' => $data];
    }
    public function mealTypeForSelect()
    {
        $data = FlightMeal::pluck('display_name','name');
        return ['meal' => $data];
    }
}
