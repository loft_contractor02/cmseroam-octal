<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use App\Country;
use App\City;
use DB;
use Session;
use Response;
use App\HotelMapping;
use App\ActivityPropertyList;

class HotelMappingController extends Controller 
{

	public function callExpediaHotelMapping(Request $oRequest)
	{
		session(['page_name' => 'expedia_hotel_mapping']);
		$propertyList 	= [];
		$country 		= '';
		$city 			= '';
		$hotel_ids 		= [];
		$sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : 'Name';
    	$sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : 'asc';
		if (Input::has('country_id'))
		{	
			$data 			= Input::all();
			$country 		= $data['country_id'];
			$city 			= $data['city'];
			$countryInfo 	= Country::where('id',$country)->first();
			$country_code 	= $countryInfo['code'];
			$sCity = City::where('id',$data['city'])->value('name');
			//$db_ext 		= \DB::connection('mariadb');
/*			$query = "select * from activepropertylist where Country='".$country_code."'";
			if($sCity != '')
				$query .= "and City='".$sCity."'";*/
			
			$propertyList 	= ActivityPropertyList::geActivityPropertyList($country_code,$sCity,$sOrderField,$sOrderBy);

			$hotel_ids 		= HotelMapping::select('EANHotelID')->where('country_id',$country)->get();
			//print_r($propertyList);exit;
		}
		$countries 	 = ['' => 'Select Country'] + Country::orderBy('name','asc')->pluck('name','id')->toArray();
		$cities 	 = City::orderBy('name','asc')->where('country_id',$country)->pluck('name','id');		
		if($oRequest->isMethod('POST'))
            $oViewName =  'WebView::hotel_mapping._more_hotel_mapping';
        else
            $oViewName = 'WebView::hotel_mapping.index';
        
       
		return \View::make($oViewName ,compact('countries','cities','propertyList','country','city','hotel_ids','sOrderField','sOrderBy'));	
	}

	public function addHotelMapping()
	{
		$data 			= Input::all();
		$country_id 	= $data['country_id'];
		$city_id 		= $data['city_id'];
		$hotels_id 		= $data['hotel_id'];
		$insertData = [];
		$hotelIDs   = '';

		$hotel_ids 		= HotelMapping::select('EANHotelID')->where('country_id',$country_id)->get();
	    foreach ($hotel_ids as $hotelID) {
	        $hotelIDs = $hotelID->EANHotelID.",".$hotelIDs;
	    }
	    $hotelIDs   = explode(",",$hotelIDs);
	    $aIds = array_diff($hotels_id , $hotelIDs);
	   //echo "<pre>";print_r($hotelIDs);
	    //echo "<pre>";print_r($hotels_id);
	    //echo "<pre>";print_r($aIds);exit;
		foreach ($aIds as $hotelId) {
			$data = [
					'country_id' => $country_id,
					'city_id' 	 => $city_id,
					'EANHotelID' => $hotelId
				];	
			$oHotelMapping = HotelMapping::firstOrNew($data);
			$oHotelMapping->created_at= date("Y-m-d H:i:s");
			$oHotelMapping->save();
		}
		Session::flash('message', "Hotel Mapping Changes are Done Successfully!");
		
		return Response::json(array(
	        'status' => 200));
	}

}