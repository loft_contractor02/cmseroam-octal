<?php
//priya
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

use App\Activity;
use App\Currency;
use App\ActivityOperator;
use App\ActivitySupplier;
use App\City;
use App\Label;
use App\ActivityLabelPivot;
use App\ActivityMarkup;
use App\ActivityBasePrice;
use App\ActivityPrice;
use App\ActivityMarkupPercentage;
use App\ActivityMarkupAgentCommission;
use App\ActivityMarkupSupplierCommission;
use App\Country;
use App\ActivityImage;
use App\Domain;
use Session;
use File;
use Image;
use DB;
use Auth;

class ActivityController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function callActivityList(Request $oRequest) 
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'activity' || $oRequest->query('isreset') == 1)
            $oRequest->session()->forget('activity');

        session(['page_name' => 'activity']);
        $aData = session('activity') ? session('activity') : array();

        $oRequest->session()->forget('activity');
        
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sSearchBy = ($oRequest->has('search_by')) ? $oRequest->search_by : ((count($aData)) ? $aData['search_by'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'a.id');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'desc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        if(count($aData) &&  $sSearchStr != $aData['search_str'])
            $nPage = 1;
        
        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });
        

        $oActivityList = Activity::getActivityList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);
        
        setSession($sSearchStr,$sSearchBy,$sOrderField,$sOrderBy,$nShowRecord,$oActivityList->currentPage(),'activity');
        
        if($oRequest->page > 1)
            $oViewName =  'WebView::activity._activity_list_ajax';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::activity.activity_list' : 'WebView::activity._activity_list_ajax';
        
        return \View::make($oViewName, compact('oActivityList','sSearchStr','sOrderField','sOrderBy','nShowRecord','sSearchBy'));  
    }
    
    public function callActivityDelete($nIdActivity) 
    {
        $oActivity = Activity::find($nIdActivity);
        $oActivity->delete();
        return 1;
    }

    public function callActivityPublish($nIdActivity) 
    {        
        $oActivity = Activity::find($nIdActivity);
        $data1= new Activity();
        $is_publish=$oActivity->is_publish;
        if(($is_publish)=='1')
        {  
            \DB::table('zactivity')
            ->where('id', $nIdActivity)
            ->update(['is_publish' => "0"]);     
        }
        else
        {     
            \DB::table('zactivity')
            ->where('id', $nIdActivity)
            ->update(['is_publish' => "1"]);         
        }
        return 1;   
        
    }
    public function callActivityCreate(Request $oRequest,$nIdActivity='') 
    {
        $oActivityImages = [];
        if ($oRequest->isMethod('post'))
        {
            //print_r(Input::all());exit;
            $random_activity_id = substr($oRequest->random_activity_id,5);
            $oValidator = Validator::make($oRequest->all(), [
                                    'name'                         => 'required',
                                    'city_id'                      => 'required',
                                    'has_accomodation'             => 'required',
                                    'default'                      => 'required',
                                    'pickup'                       => 'required',
                                    'dropoff'                      => 'required',
                                    'has_transport'                => 'required',
                                    'default_activity_supplier_id' => 'required',
                                    'activity_operator_id'         => 'required',
                                    'currency_id'                  => 'required',
                                    'ranking'                      => 'required',
                                    'description'                  => 'required',
                                    'labels'                       => 'required',
                                    //'reception_phone'              =>'numeric',
                                    //'website'                      =>'url',
                                    'reception_email'              =>'email'
                                    ]);
            if($oValidator->fails()) 
            {
                return Redirect::back()->withErrors($oValidator)->withInput();
            }
            //dd($oRequest->all());
            $aLabels = $oRequest->labels;
            //dd($aLabels);

            $aData = $oRequest->except([ 'has_destination_city','labels','video_url' ]);   
            $domains = '';
			if($oRequest->domains){
				$domains = implode(',',$oRequest->domains);
            }

            //dd($domains);
            $aData['domain_ids'] = $domains;
			$aData['licensee_id'] = Auth::user()->licensee_id;
            $aData['user_id'] = Auth::user()->id;
                  
            $oActivity = Activity::updateOrCreate(['id' => $oRequest->id_activity],$aData);
            if(Input::hasFile('activity_image')){
                //echo "test"; exit();
                if(isset($oRequest->id_activity)){
                    $activity_id = $oRequest->id_activity;
                }else{
                    $activity_id = $oActivity->id;
                }
                $image   = Input::file('activity_image');
                $image_validator = image_validator($image);
                // check if image is valid
                if( $image_validator['fails'] ){
                    //$error = ['message' => $image_validator['message']];
                }else{
                    $imageName = time().'.'.$image->getClientOriginalExtension(); 
                    $filePath = 'activities/' . $imageName;
                    $t = \Storage::disk('s3')->put($filePath, file_get_contents($image), 'public');                   
                    $update['mapfile'] = $imageName;
                    $update_data = array('original' => $imageName,'activity_id'=>$activity_id);
                    $ActivityTypeLogoUpdate = ActivityImage::updateOrCreate(['activity_id' => $oRequest->id_activity], $update_data);
                }

            }
            
            if(isset($aLabels)){
                foreach ($aLabels as $label) {
                        $pivot_add = [
                                        'label_id' => $label,
                                        'activity_id' => $oActivity->id
                        ];
                        $pivot = ActivityLabelPivot::create($pivot_add);
                }
            }
            $aArray['from_activity'] = 1;
            $aArray['id'] = $oActivity->id;
            
            if($oRequest->activity_id == '')
            {
                $images = ActivityImage::where('random_activity_id','=',$random_activity_id)->get();
                if(count($images) > 0){    
                    foreach ($images as $image) {
                       
                        $value = ['activity_id' => $oActivity->id];

                        ActivityImage::where('id','=',$image->id)->update( $value );
                    }
                }
            }

            ActivityImage::where('activity_id',$oActivity->id)
                        ->where('type','video')
                        ->update(['is_active'=>0]);
            $aVideoUrl = $oRequest->video_url;
            foreach ($aVideoUrl as $key => $value) {
                $oVideo = ActivityImage::firstOrNew(['activity_id' => $oActivity->id, 'type' => 'video', 'original' => $value]);
                $oVideo->is_active =1;
                $oVideo->save();
            }
            ActivityImage::where('activity_id',$oActivity->id)
                        ->where('is_active',0)
                        ->delete();

            if($oRequest->id_activity != '')
                Session::flash('message', trans('messages.success_updated'));
            else
                Session::flash('message', trans('messages.success_inserted'));
            
            if($oRequest->id_activity == '')
            {
                return redirect()->route('activity.activity-season-create',[$aArray['id'],$aArray['from_activity']]);
            }
            return redirect()->route('activity.activity-list');
        }
        $aLable=array();
        if($nIdActivity != '')
        {
            $oActivity = Activity::find($nIdActivity);
            $aLable = ActivityLabelPivot::where('activity_id','=',$nIdActivity)->pluck('label_id')->toArray();
            $activity_image_name  = ActivityImage::where('activity_id',$nIdActivity)->pluck('original');
            $oActivityImages    = ActivityImage::where('activity_id','=', $nIdActivity)->where('type','image')->orderBy('sort_order','asc')->get();
            $oVideos  = ActivityImage::where('activity_id','=', $nIdActivity)->where('type','video')->get();
        }
        
        $currencies =  ['' => 'Select Currency'] + Currency::orderBy('code','asc')->pluck('code','id')->toArray();
        $labels   =  Label::orderBy('name','asc')->pluck('name','id');
        $cities  = ['' => 'Select City'] + City::orderBy('name','asc')->pluck('name','id')->toArray();
        $suppliers = ['' => 'Select Supplier'] + ActivitySupplier::orderBy('name','asc')->pluck('name','id')->toArray();
        $operators = ['' => 'Select Operator'] + ActivityOperator::orderBy('name','asc')->pluck('name','id')->toArray();
        $DomainList=Domain::join('user_domains', 'user_domains.domain_id', '=', 'domains.id')
        ->where('user_domains.user_id', Auth::user()->id)  
        ->select(
           'domains.name as name',
           'user_domains.domain_id as id'
       )->orderBy('user_domains.domain_id')->get();      
        return \View::make('WebView::activity.activity_create', compact('oActivity','aLable','cities','activity_types','suppliers','operators', 'currencies','labels','oActivityImages','activity_image_name','nIdActivity','DomainList','oVideos'));
    }
    

    public function ImageUpload(Request $oRequest) 
    {
        $success = FALSE;
        $data = array();
        $error = array();
        // determine if a file has been uploaded

        if (Input::hasFile('activityImage')) 
        {
            $nActivityId = Input::get('activity_id');
            $image = Input::file('activityImage')[0];
            
            $oRequest->offsetSet('extension',$image->getClientOriginalExtension());
            $oValidator = Validator::make($oRequest->all(), [
                                        'activityImage' => 'required',
                                        'extension' => 'required|in:jpg,jpeg,png'
                                        ]);
            if($oValidator->fails()) 
            {
                return response()->json(['success' => $oValidator->errors()],422);
            }
            
            $image_validator = image_validator($image);
            // check if image is valid
            if ($image_validator['fails']) {
                $error = ['message' => $image_validator['message']];
            } else {
                if (!is_numeric($nActivityId)) {
                    $nActivityId = substr($nActivityId, 5);
                    $values = array('random_activity_id' => $nActivityId);
                } else { // if rand activity id 
                    $values = array('activity_id' => $nActivityId);
                }
                $image = Input::file('activityImage')[0];
                $imageName = time().'.'.$image->getClientOriginalExtension(); 
                $filePath = 'activities/' . $imageName;
                $t = \Storage::disk('s3')->put($filePath, file_get_contents($image), 'public');
                $imageUrl = \Storage::disk('s3')->url($filePath);
                
                $values = array_merge($values, ['original' => $imageName]);

                $sort_order = ActivityImage::where('activity_id', '=', $nActivityId)->max('sort_order');
                $values = array_merge($values, ['is_active' => 1, 'sort_order' => $sort_order + 1, 'is_primary' => 0]);

                // STORE TO DATABASE
                $id = DB::table('zactivityimages')->insertGetId($values);

                $values = array_merge($values, ['image_id' => $id,'is_active' => 1, 'sort_order' => $sort_order + 1, 'is_primary' => 0]);
                //$values = array_merge($values, ['is_active' => 1, 'sort_order' => $sort_order + 1, 'is_primary' => 0]);

                if ($id) {
                    $success = TRUE;
                    $data = $values; //->toArray();
                    $data['img_url'] = $imageUrl;
                    $data['image_name'] = $imageName;
                    $data = ['success' => true, $data];
                } else {
                    $error = ['success' => false];
                }
            }
        } else {
            $error = ['success' => false];
        }
        return response_format($success, $data, $error);
    }

    public function sortImages()
    {
        //echo "dsfd";exit;
        $data = Input::only('image_id');
        if($data){
            foreach ($data['image_id'] as $key => $value) {
                if($key == 0){
                        $primary = 1;
                }else{
                        $primary = 0;
                }
                $sort_order = $key + 1;
                ActivityImage::where(['id' => $value])->update(['is_primary' => $primary,'sort_order'=>$sort_order]);
            }
        }
        return "success";
    }

    public function callActivitySeasonCreate(Request $oRequest,$nId='',$nFromFlag='') 
    {
       
       
       
        $data = Input::except('price');
        if ($oRequest->isMethod('post'))
    	{
           
            $aRule = array();
            if($nFromFlag == '' && $nId != '')
                $data = Input::except(['price','name','activity_id','date_from','date_to']); // this fields are not updatable
            else {
                $aRule = [  'name' => 'required',
                            'activity_id' => 'required_without:from_flag',
                            'date_from'   => 'required|date',
                            'date_to'     => 'required|date|after:date_from'];
            }
            array_merge($aRule, ['allotment'   => 'required',
                                    'release'     => 'required',
                                    'minimum_pax' => 'required',
                                    'operates'    => 'required',
                                    'currency_id'    => 'required']);
            
            $oValidator = Validator::make($oRequest->all(), $aRule);
            if($oValidator->fails()) 
            {
                return Redirect::back()->withErrors($oValidator)->withInput();
            }
            $nSeasonId='';
            if($oRequest->from_flag !='')
                $nActivityId = $oRequest->id;
            else{
                $nSeasonId = $oRequest->id;
                $nActivityId= $oRequest->activity_id;
            } 
            $oActivity = Activity::find($nActivityId);
            //print_r($oRequest->days);
            $cancellation_array = [];
            $i = 0;
            foreach ($oRequest->days as $key => $days ) {
                $percent = $data['percent'][$key];
                if($days && $percent)
                {
                    $cancellation_array[$i]['days'] = $days;
                    $cancellation_array[$i]['percent'] = $percent;
                    $i++;
                }
            }
            //print_r($cancellation_array);exit;
            $data['cancellation_formula'] = ($cancellation_array) ? json_encode($cancellation_array) : NULL;
            unset($data['days']);
            unset($data['percent']);
            unset($data['activity_id']);

            $domains = '';
			if($oRequest->domains){
				$domains = implode(',',$oRequest->domains);
            }
            //dd($domains);
            // $data['domain_ids'] = $domains;
			// $data['licensee_id'] = Auth::user()->licensee_id;
            // $data['user_id'] = Auth::user()->id;
            
            $base_price = ActivityBasePrice::updateOrCreate(['activity_price_id' => $nSeasonId],['base_price' => $oRequest->price]);
            $data['operates'] = implode(",", $data['operates']);
            $markup = ActivityMarkup::where(['is_default' => TRUE])->first();
            $data = array_merge($data, [
                                        'activity_base_price_id' => $base_price->id,
                                        'activity_markup_id' => $markup->id,
                                        'activity_markup_percentage_id' => $markup->activity_markup_percentage_id,
                                        'activity_supplier_id' => $oActivity->default_activity_supplier_id,
                                        'activity_id' => $nActivityId,
                                        'domain_ids' => $domains,
                                        'licensee_id' => Auth::user()->licensee_id,
                                        'user_id' => Auth::user()->id,
                                    ]);

            $season = ActivityPrice::updateOrCreate(['id' => $nSeasonId], $data);
            ActivityBasePrice::where('id','=',$base_price->id)->update(['activity_price_id' => $season->id]);
            if($nSeasonId != '')
                Session::flash('message', trans('messages.success_updated'));
            else
                Session::flash('message', trans('messages.success_inserted'));
            
            if($oRequest->from_flag !=''){
                return Redirect::to('activity/activity-list');
            }else{
                return Redirect::to('activity/activity-season-list');
            }
        }
        if($nFromFlag == '' && $nId != ''){
            $oSeason = ActivityPrice::with('currency')->where('id',$nId)->with('base_price')->first();
            $cancellation_formula = $oSeason->cancellation_formula;
            $decoded_formula = json_decode($cancellation_formula);
            $oSeason['operates'] = explode(',',$oSeason['operates']);
        }
        $markups = ActivityMarkup::orderBy('is_default', 'ASC')->where('is_active',1)->with('markup_percentage')->get();
        $activities = ['' => 'Select Activity']+ Activity::orderBy('name','asc')->pluck('name','id')->toArray();
        $operates   = [ '1' => 'Monday',
                        '2' => 'Tuesday',
                        '3' => 'Wednesday',
                        '4' => 'Thursday',
                        '5' => 'Friday',
                        '6' => 'Saturday',
                        '7' => 'Sunday'
                        ];
        $currencies = ['' => 'Select Currency']+Currency::orderBy('code','asc')->pluck('code','id')->toArray();
        //$DomainList= Domain::where('status',1)->get();
        $DomainList=Domain::join('user_domains', 'user_domains.domain_id', '=', 'domains.id')
        ->where('user_domains.user_id', Auth::user()->id)  
        ->select(
           'domains.name as name',
           'user_domains.domain_id as id'
       )->orderBy('user_domains.domain_id')->get();

        return \View::make('WebView::activity.activity_season_create',compact('activities','operates','currencies','nId','nFromFlag','oSeason','decoded_formula','DomainList'));
    }
    
     public function callActivitySeasonList(Request $oRequest) 
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'activity_season' || $oRequest->query('isreset') == 1)
            $oRequest->session()->forget('activity_season');

        session(['page_name' => 'activity_season']);
        $aData = session('activity_season') ? session('activity_season') : array();

        $oRequest->session()->forget('activity_season');
        
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sSearchBy = ($oRequest->has('search_by')) ? $oRequest->search_by : ((count($aData)) ? $aData['search_by'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'id');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'desc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        if(count($aData) &&  $sSearchStr != $aData['search_str'])
            $nPage = 1;
        
        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });
        //DB::enableQueryLog();
        switch($sSearchBy)
        {
            case "price":
                    if (is_numeric($sSearchStr)) {
                            $oMarkup         = ActivityMarkup::with(['markup_percentage'])->where(['is_default' => 1])->first();
                            $nBasePrice     = round(( $sSearchStr / 100 ) * (100 - $oMarkup->markup_percentage->percentage ));
                            $aBasePriceIds = ActivityBasePrice::where(['base_price' => $nBasePrice])->pluck('id');
                            $aActivityIds   = ActivityPrice::whereIn('activity_base_price_id', $aBasePriceIds)->pluck('activity_id');
                            $aPriceIds      = ActivityPrice::whereIn('activity_base_price_id', $aBasePriceIds)->pluck('id');
                            $oActivitySeasonList     = Activity::with(['city', 'operator',
                                                                'price' => function($query) use ($aPriceIds)
                                                                        {
                                                                        $query->with(['currency','supplier'])
                                                                            ->whereIn('zactivityprices.id', $aPriceIds);
                                                                        }
                                                            ])
                                                    ->whereIn('id', $aActivityIds)
                                                    ->orderBy($sOrderField, $sOrderBy)
                                                    ->paginate($nShowRecord);
                    }
                    break;
            case "allotment":
                    if (is_numeric($sSearchStr)) {
                            $aActivityIds = ActivityPrice::where('allotment', $sSearchStr)->pluck('activity_id');
                            $aPriceIds    = ActivityPrice::where('allotment', $sSearchStr)->pluck('id');
                            $oActivitySeasonList   = Activity::with(['city', 'operator',
                                                                    'price' => function($query) use ($aPriceIds)
                                                                            {
                                                                            $query->with(['currency','supplier'])
                                                                                ->whereIn('zactivityprices.id', $aPriceIds);
                                                                            }
                                                            ])
                                                    ->whereIn('id', $aActivityIds)
                                                    ->orderBy($sOrderField, $sOrderBy)
                                                    ->paginate($nShowRecord);
                            //$activities->appends(Request::only('search','search-by'))->links();	
                    }
                    break;				
            case "pax":
                    if (is_numeric($sSearchStr)) {
                            $aActivityIds = ActivityPrice::where('minimum_pax', $sSearchStr)->pluck('activity_id');
                            $aPriceIds    = ActivityPrice::where('minimum_pax', $sSearchStr)->pluck('id');
                            $oActivitySeasonList   = Activity::with(['city', 'operator',
                                                                    'price' => function($query) use ($aPriceIds)
                                                                            {
                                                                            $query->with(['currency','supplier'])
                                                                                ->whereIn('zactivityprices.id', $aPriceIds);
                                                                            }
                                                            ])
                                                    ->whereIn('id', $aActivityIds)
                                                    ->orderBy($sOrderField, $sOrderBy)
                                                    ->paginate($nShowRecord);
                            //$activities->appends(Request::only('search','search-by'))->links();
                    }
                    break;
            case "name":
                    $aSeasonIds   = ActivityPrice::where('name', 'LIKE', '%' . $sSearchStr . '%')->pluck('id');
                    $aActivityIds = ActivityPrice::where('name', 'LIKE', '%' . $sSearchStr . '%')->pluck('activity_id');
                    $oActivitySeasonList   = Activity::with(['city', 'operator',
                                                                'price' => function($query) use ($aSeasonIds)
                                                                        {
                                                                        $query->with(['currency','supplier']);
                                                                        $query->whereIn('zactivityprices.id', $aSeasonIds);
                                                                        }
                                                        ])
                                                ->whereIn('id', $aActivityIds)
                                                ->orderBy($sOrderField, $sOrderBy)
                                                ->paginate($nShowRecord);					
                    //$activities->appends(Request::only('search','search-by'))->links();
                    break;
            case "supplier":
                    $aSupplierIds = ActivitySupplier::where('name', 'LIKE', '%' . $sSearchStr . '%')->pluck('id');
                    $aActivityIds = ActivityPrice::whereIn('activity_supplier_id', $aSupplierIds)->pluck('activity_id');
                    $aPriceIds    = ActivityPrice::whereIn('activity_supplier_id', $aSupplierIds)->pluck('id');
                    $oActivitySeasonList   = Activity::whereIn('id', $aActivityIds)
                                            ->with(['City', 'Operator',
                                                            'price' => function($query) use ($aPriceIds, $aSupplierIds)
                                                            {
                                                                    $query->whereIn('zActivityPrices.id', $aPriceIds)
                                                                            ->with(['currency',
                                                                                    'supplier' => function($query) use ($aSupplierIds)
                                                                                    {
                                                                                    $query->whereIn('zactivitysuppliers.id', $aSupplierIds);
                                                                                    }
                                                                            ]);
                                                            }
                                                    ])
                                            ->orderBy($sOrderField, $sOrderBy)
                                            ->paginate($nShowRecord);
                    //$activities->appends(Request::only('search','search-by'))->links();
                    break;
            default:
                $oActivitySeasonList = Activity::getActivitySeasonList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);
        }
        
        //dd(DB::getQueryLog());
        setSession($sSearchStr,'',$sOrderField,$sOrderBy,$nShowRecord,$oActivitySeasonList->currentPage(),'activity_season');
        
        
        if($oRequest->page > 1)
            $oViewName =  'WebView::activity._activity_season_list_ajax';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::activity.activity_season_list' : 'WebView::activity._activity_season_list_ajax';
        
        return \View::make($oViewName, compact('oActivitySeasonList','sSearchStr','sOrderField','sOrderBy','nShowRecord')); 
    }
    
    public function callActivitySeasonDelete($nIdActivity) 
    {
        $oSeason = ActivityPrice::with('base_price')->find($nIdActivity);
        $oSeason->delete();
        return 1;
    }
    
    public function callActivitySupplierList(Request $oRequest) 
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'activity_supplier' || $oRequest->query('isreset') == 1)
            $oRequest->session()->forget('activity_supplier');

        session(['page_name' => 'activity_supplier']);
        $aData = session('activity_supplier') ? session('activity_supplier') : array();

        $oRequest->session()->forget('activity_supplier');
        
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'id');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'desc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        if(count($aData) &&  $sSearchStr != $aData['search_str'])
            $nPage = 1;
        
        Paginator::currentPageResolver(function() use($nPage) {
            return $nPage;
        });
        
        $oActivitySupplierList = ActivitySupplier::getActivitySupplierList($sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);

        setSession($sSearchStr,'',$sOrderField,$sOrderBy,$nShowRecord,$oActivitySupplierList->currentPage(),'activity_supplier');
        
        if($oRequest->page > 1)
            $oViewName =  'WebView::activity._activity_supplier_list_ajax';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::activity.activity_supplier_list' : 'WebView::activity._activity_supplier_list_ajax';
        
        return \View::make($oViewName, compact('oActivitySupplierList','sSearchStr','sOrderField','sOrderBy','nShowRecord')); 
    }
    
    public function callActivitySupplierCreate(Request $oRequest,$nIdActivitySupplier='')
    {
        if ($oRequest->isMethod('post'))
    	{
            $oValidator = Validator::make($oRequest->all(), [
                                    'name' => 'required',
                                    'marketing_contact_title' => 'required',
                                    'marketing_contact_name' => 'required',
                                    'marketing_contact_email' => 'required',
                                    'marketing_contact_phone' => 'required',
                                    'reservation_contact_name' => 'required',
                                    'reservation_contact_email' => 'required',
                                    'reservation_contact_landline' => 'required',
                                    'reservation_contact_free_phone' => 'required',
                                    'accounts_contact_name' => 'required',
                                    'accounts_contact_email' => 'required',
                                    'accounts_contact_title' => 'required',
                                    'accounts_contact_phone' => 'required',
                                    'logo' => 'required_without:id_activity|image',
                                    'description' => 'required',
                                    'special_notes' => 'required',
                                    'remarks' => 'required',
                                    ]);
            if($oValidator->fails()) 
            {
                return Redirect::back()->withErrors($oValidator)->withInput();
            }
            
            $aData = $oRequest->except([ 'id_activity','_token' ]);
            if(Input::hasFile('logo')){
                    $image   = Input::file('logo');
                    $image_validator = image_validator($image);
                // check if image is valid
                if( $image_validator['fails'] ){
                    //$error = ['message' => $image_validator['message']];
                }else{
                    $filename  = time(). '_' . $image->getClientOriginalName();
                    $destination = public_path('uploads/activities/supplier');
                    File::makeDirectory( public_path($destination), 0775, FALSE, TRUE);
                    $image->move($destination, $filename);
                    $data['logo'] = $filename;
                }
            }    
            
            //insert data
            $oSupplier = ActivitySupplier::updateOrCreate(['id' => $oRequest->id_activity],$aData);
            if($oRequest->id_activity != '')
                Session::flash('message', trans('messages.success_updated'));
            else
                Session::flash('message', trans('messages.success_inserted'));
            return Redirect::route('activity.activity-supplier-list');
        }
        $oActivitySupplier = ActivitySupplier::where('id',$nIdActivitySupplier)->first();
        return \View::make('WebView::activity.activity_supplier_create',compact('nIdActivitySupplier','oActivitySupplier'));
    }
    
    public function callActivitySupplierDelete($nIdActivitySupplier)
    {
        $oSupplier = ActivitySupplier::find($nIdActivitySupplier);
        $oSupplier->delete();
        return 1;
    }



    public function callActivityMarkupList(Request $oRequest) 
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'activity_mark' || $oRequest->query('isreset') == 1)
            $oRequest->session()->forget('activity_mark');

        session(['page_name' => 'activity_mark']);
        $aData = session('activity_mark') ? session('activity_mark') : array();

        $oRequest->session()->forget('activity_mark');
        
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sSearchBy = ($oRequest->has('search_by')) ? $oRequest->search_by : ((count($aData)) ? $aData['search_by'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'am.id');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'asc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        
        $oActivityMarkupList = ActivityMarkup::getActivityMarkupList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);
        setSession($sSearchStr,'',$sOrderField,$sOrderBy,$nShowRecord,'','activity_mark');
        
        if ($oActivityMarkupList) {
            foreach ($oActivityMarkupList as $key => $value) { //print_r($value);exit;
                switch ($value->allocation_type) {
                    case 'activity':
                        //echo 'activity'.$value->allocation_type,$value['allocation_id'];exit;
                        $sActivity = Activity::where('id',$value['allocation_id'])->value('name');
                        $oActivityMarkupList[$key]['allocation_name'] = 'Activity - ' .$sActivity;
                        break;
                    case 'city':
                        $sCity = City::where('id',$value['allocation_id'])->value('name');
                        $oActivityMarkupList[$key]['allocation_name'] = 'City - ' .$sCity;
                        break;
                    case 'country':
                        $sCountry = Country::where('id',$value['allocation_id'])->value('name');
                        $oActivityMarkupList[$key]['allocation_name'] = 'Country - ' . $sCountry;
                        break;
                    case 'all':
                        $oActivityMarkupList[$key]['allocation_name'] = 'All';
                        break;
                }
            }
        }

        $oViewName =  ($oRequest->isMethod('Get')) ? 'WebView::activity.activity_markup_list' : 'WebView::activity._more_activity_markup_list';
        
        
        return \View::make($oViewName, compact('oActivityMarkupList','sSearchStr','sOrderField','sOrderBy','nShowRecord','sSearchBy')); 
    }
    
    public function callActivityMarkupCreate(Request $oRequest, $nIdMarkup ='') 
    {
        if ($oRequest->isMethod('post'))
    	{
            $allocation_id_validator = '';
            if($oRequest->allocation_type != 'all')
                $allocation_id_validator = 'required';
            
            $data = Input::only('name','description','allocation_id');
            $data['allocation_type'] =  $oRequest->allocation_type;
            $data['is_active']  = ( !$oRequest->is_active ) ? FALSE: TRUE;
            $data['is_default'] = ( !$oRequest->is_default ) ? FALSE: TRUE;
            //print_r($data);exit; 
            $oValidator = Validator::make($oRequest->all(), [
                                        'name'                => 'required',
                                        'description'         => 'required',
                                        'allocation_type'     => 'required',
                                        'allocation_id'       => $allocation_id_validator,
                                        'markup_percentage'   => 'required',
                                        'agent_percentage'    => 'required',
                                    ]);
            if($oValidator->fails()) 
            {
                return Redirect::back()->withErrors($oValidator)->withInput();
            }
            if( $oRequest->is_default )
                ActivityMarkup::where('is_default', TRUE)->update(['is_default' => FALSE]);
            
            $oActivityMarkup = ActivityMarkup::updateOrCreate(['id' => $oRequest->id_activity ], $data );
            
            $oAMP = ActivityMarkupPercentage::firstOrNew(['activity_markup_id' 	=> $oActivityMarkup->id]); 
            $oAMP->percentage =  $oRequest->markup_percentage;
            $oAMP->save();
            
            $oAMAC = ActivityMarkupAgentCommission::firstOrNew(['activity_markup_id' 	=> $oActivityMarkup->id]);
            $oAMAC->percentage =  $oRequest->agent_percentage;
            $oAMAC->save();
            
            $oAMS = ActivityMarkupSupplierCommission::firstOrNew(['activity_markup_id' 	=> $oActivityMarkup->id]);
            $oAMS->percentage =  $oRequest->supplier_percentage;
            $oAMS->save();
            ActivityMarkup::where('id', $oActivityMarkup->id)->update([
                            'activity_markup_percentage_id' => $oAMP->id,
                            'activity_markup_agent_commission_id' => $oAMAC->id,
                            'activity_markup_supplier_commission_id' => $oAMS->id
                    ]);
            if($oRequest->id_activity != '')
                Session::flash('message', trans('messages.success_updated'));
            else
                Session::flash('message', trans('messages.success_inserted'));
            
            return Redirect::to('activity/activity-markup-list');
	   
        }
        if($nIdMarkup !=''){
            $oActivityMarkup = ActivityMarkup::where('id',$nIdMarkup)->with(['markup_percentage','agent_commission','supplier_commission'])->first()->toArray();
            $oActivityMarkup['markup_percentage'] = $oActivityMarkup['markup_percentage']['percentage'];
            $oActivityMarkup['agent_percentage'] = $oActivityMarkup['agent_commission']['percentage'];
            $oActivityMarkup['supplier_percentage'] = $oActivityMarkup['supplier_commission']['percentage'];
        }
                
        return \View::make('WebView::activity.activity_markup_create',compact('nIdMarkup','oActivityMarkup'));
    }
    //markup create
    public function getAllActivity(){
        return Activity::select('id', 'name')->get();
    }
    
    public function callActivityOperatorList(Request $oRequest) 
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'activity_operator' || $oRequest->query('isreset') == 1)
            $oRequest->session()->forget('activity_operator');

        session(['page_name' => 'activity_operator']);
        $aData = session('activity_operator') ? session('activity_operator') : array();

        $oRequest->session()->forget('activity_operator');
        
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'id');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'desc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        if(count($aData) &&  $sSearchStr != $aData['search_str'])
            $nPage = 1;
        
        Paginator::currentPageResolver(function() use($nPage) {
            return $nPage;
        });
        
        $oActivityList = ActivityOperator::getActivityOperatorList($sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);

        setSession($sSearchStr,'',$sOrderField,$sOrderBy,$nShowRecord,$oActivityList->currentPage(),'activity_operator');
        
        if($oRequest->page > 1)
            $oViewName =  'WebView::activity._activity_operator_list_ajax';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::activity.activity_operator_list' : 'WebView::activity._activity_operator_list_ajax';
        
        return \View::make($oViewName, compact('oActivityList','sSearchStr','sOrderField','sOrderBy','nShowRecord')); 
    }
    
    public function getActivityOperatorDelete($nIdOperator) 
    {
        ActivityOperator::find($nIdOperator)->delete();
        return redirect()->route('activity.activity-operator-list');
    }
    
    public function callActivityOperatorCreate(Request $oRequest,$nIdActivitySupplier='')
    {
        if ($oRequest->isMethod('post'))
    	{
            $oValidator = Validator::make($oRequest->all(), [
                                    'name' => 'required',
                                    'marketing_contact_title' => 'required',
                                    'marketing_contact_name' => 'required',
                                    'marketing_contact_email' => 'required',
                                    'marketing_contact_phone' => 'required',
                                    'reservation_contact_name' => 'required',
                                    'reservation_contact_email' => 'required',
                                    'reservation_contact_landline' => 'required',
                                    'reservation_contact_free_phone' => 'required',
                                    'accounts_contact_name' => 'required',
                                    'accounts_contact_email' => 'required',
                                    'accounts_contact_title' => 'required',
                                    'accounts_contact_phone' => 'required',
                                    'logo' => 'required_without:id_activity|image',
                                    'description' => 'required',
                                    'special_notes' => 'required',
                                    'remarks' => 'required',
                                    ]);
            if($oValidator->fails()) 
            {
                return Redirect::back()->withErrors($oValidator)->withInput();
            }
            
            $aData = $oRequest->except([ 'id_activity','_token' ]);
            if(Input::hasFile('logo')){
                    $image   = Input::file('logo');
                    $image_validator = image_validator($image);
                // check if image is valid
                if( $image_validator['fails'] ){
                    //$error = ['message' => $image_validator['message']];
                }else{
                    $filename  = time(). '_' . $image->getClientOriginalName();
                    $destination = public_path('uploads/activities/operator');
                    File::makeDirectory( public_path($destination), 0775, FALSE, TRUE);
                    $image->move($destination, $filename);
                    $data['logo'] = $filename;
                }
            }    
            
            //insert data
            $oSupplier = ActivityOperator::updateOrCreate(['id' => $oRequest->id_activity],$aData);
            
            if($oRequest->id_activity != '')
                Session::flash('message', trans('messages.success_updated'));
            else
                Session::flash('message', trans('messages.success_inserted'));
            
            return Redirect::route('activity.activity-operator-list');
        }
        $oActivitySupplier = ActivityOperator::where('id',$nIdActivitySupplier)->first();
        //echo "<pre>";print_r($oActivitySupplier);exit;
        return \View::make('WebView::activity.activity_operator_create',compact('nIdActivitySupplier','oActivitySupplier'));
    }

    public function callActivityImageDelete($nIdActivityImage)
    {
        $data    = array();
        $success = TRUE;
        $oImage = ActivityImage::find($nIdActivityImage);

        $error  = "";
        //print_r($oImage);exit;
        if( $oImage )
        {            
            $filePath = 'activities/' . $oImage->original;
            $delete = \Storage::disk('s3')->delete($filePath);
            
            if(!$delete){
                    $success = FALSE;
                    $error = ['message' => 'An error has occured while trying to delete the image.'];
            }else
            {
                $delete = $oImage->delete();
                $data = ['message' => 'Successfully deleted image.'];
                if($oImage->is_primary == TRUE){
                    $count = ActivityImage::where(['activity_id' => $oImage->activity_id])->count();
                    if( $count > 1){
                        $new_primary_image = ActivityImage::where(['activity_id' => $oImage->activity_id])->first();
                        ActivityImage::where(['id' => $new_primary_image->id])->update(['is_primary' => 1]);
                    }   
                }
            }
            
        }
        return response_format($success, $data, $error);
    }

   
}
