<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;
class UserLogController extends Controller
{
	public function __construct(){
	}


	/**
	 * function to store logs from authentication page.
	 *
	 * @param  char name, char ip address  
	 * @return No Response need to send
	 */
	public function callUserLog()
	{

		$aUserLog['name'] =  Input::get('name');
		$aUserLog['ip_address'] = Input::get('ip_address');
		$aUserLog['created_at'] = date('Y-m-d H:i:s');

		DB::table('tbluserlog')->insert($aUserLog);
		
	}
}
