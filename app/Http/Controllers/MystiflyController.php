<?php

namespace App\Http\Controllers;

use Chrisbjr\ApiGuard\Controllers\ApiGuardController;
use Illuminate\Support\Facades\Input;
use App\Jobs\MystiflyOrderTicketCall;
use DB;
use SoapClient;
use App\MystiflySession;
use App\CityIata;
use App\StaggingMystifly;
use App\Airline;
use Config;
use stdClass;
use Log;
use Exception;
use Cache;

class MystiflyController extends Controller
{

	private $client;
	const ACCOUNT_NUMBER = 'MCN000201';
	const USERNAME       = 'ATGXML';
	const PASSWORD       = 'ATG2017_XML';
	const TARGET         = 'Test';    
    const ENDPOINT_URL   = 'http://onepointdemo.myfarebox.com/V2/OnePoint.svc?singleWsdl';

	public function __construct(){
		try{
			$this->client = new SoapClient(	self::ENDPOINT_URL, 
										array(	'trace'      => 1, 
												'cache_wsdl' => WSDL_CACHE_NONE
												)
										);	
		} catch (Exception $e) {
			$this->client = FALSE;
		}
	}	
	
	public function get_session_id(){
		if (!Cache::has('MystiflySessionID') && Cache::get('MystiflySessionID') =='') {
			$session_id = $this->createSession();
    		Cache::put('MystiflySessionID',$session_id,20);
		}
		return Cache::get('MystiflySessionID');
	}

	

	
	private function createSession(){
		$params = new stdClass();
		$params->rq = new stdClass();
		$params->rq->AccountNumber = self::ACCOUNT_NUMBER;
		$params->rq->UserName      = self::USERNAME;
		$params->rq->Password      = self::PASSWORD;
		$params->rq->Target        = self::TARGET;
		$this->logRequest('CreateSession', $params);
		
		try {
			$response              = $this->client->CreateSession( $params );
			$res                   = get_object_vars($response);
			$create_session_result = get_object_vars($res['CreateSessionResult']);
			$session_id            = $create_session_result['SessionId'];
			return $session_id;
		} catch (Exception $e) {
			
			Log::error('Caught exception @createSession->createSession() : '. $e->getMessage() );
		}
	}
		

	public function airLowFareSearchController(){
		$success = TRUE;
		$data    = array();
		$error   = array();

		try {

			if($this->client !== FALSE)
			{    
				
				$CabinPreference = Input::get('CabinPreference');			
								
				$CabinPreference = Input::get('CabinPreference');

				if($CabinPreference == 'Any')
				{
					Input::merge(['CabinPreference' => 'Y']);
					Input::merge(['CabinType' => 'Y']);
					Input::merge(['PreferenceLevel' => 'Preferred']);
				}
				if($CabinPreference == 'Y')
				{
					Input::merge(['CabinPreference' => 'Y']);
					Input::merge(['CabinType' => 'Y']);
					Input::merge(['PreferenceLevel' => 'Restricted']);
				}
				if($CabinPreference == 'C')
				{
					Input::merge(['CabinPreference' => 'C']);
					Input::merge(['CabinType' => 'C']);
					Input::merge(['PreferenceLevel' => 'Restricted']);
				}
				if($CabinPreference == 'F')
				{
					Input::merge(['CabinPreference' => 'F']);
					Input::merge(['CabinType' => 'F']);
					Input::merge(['PreferenceLevel' => 'Restricted']);
				}
				if($CabinPreference == 'S')
				{
					Input::merge(['CabinPreference' => 'S']);
					Input::merge(['CabinType' => 'S']);
					Input::merge(['PreferenceLevel' => 'Restricted']);
				}


		

				$inputData = Input::all();
				Log::channel('mystifly_search')->info('Request : '.json_encode($inputData));
				$success = TRUE;
				$data    = array();
				$error   = array();
				$result = $this->airLowFareSearch(
						$this->get_session_id(),								
						Input::get('PricingSourceType', 'All'),    				
						$this->convertToBoolean(							
							Input::get('IsRefundable', 0)					
							),						
						$this->passengerTypeQuantities(  					
							$this->passengerTypeQuantity(					
								Input::get('Code'),          				
								Input::get('Quantity') 					
								)
							),	
						Input::get('RequestOptions', 'Fifty'),     			
						$this->convertToBoolean(
							Input::get('IsResidentFare', 0)					
							),				
						self::TARGET,								
						$this->convertToBoolean(
							Input::get('NearByAirports', 0)					
							),				
						$this->originDestinationInformations( 				
							$this->originDestinationInformation( 			
								$this->departureDateTime(					
									Input::get('DepartureDate'),
									Input::get('DepartureTime', '00:00:00')
									),
								Input::get('DestinationLocationCode'),		
								Input::get('OriginLocationCode')			
								)
							), 
						$this->travelPreferences(							
							Input::get('AirTripType', 'OneWay'),  		
							Input::get('CabinPreference'),					
							Input::get('MaxStopsQuantity', 'All'),
							$this->cabinClassPreference(				
								Input::get('CabinType'),
								Input::get('PreferenceLevel')
								),
							Input::get('VendorPreferenceCodes', NULL) 			
							)
						);


				$origin_airport_temp      = CityIata::where(['iata_code' => Input::get('OriginLocationCode')])->first();
				$destination_airport_tmp  = CityIata::where(['iata_code' => Input::get('DestinationLocationCode')])->first();
				$origin_airport_info      = !empty($origin_airport_temp) ? $origin_airport_temp->toArray() : [];
				$destination_airport_info = !empty($destination_airport_tmp) ? $destination_airport_tmp->toArray() : [];
		 		$result                   = json_decode( json_encode($result), TRUE);
				$result                   = $result['AirLowFareSearchResult'];
				$result                   = array_merge($result, [
					'origin_airport_info' => $origin_airport_info,
					'destination_airport_info' => $destination_airport_info
					]);

				$domain = Input::get('domain');
		        if ($domain) {
		            $commissions = getDomainData($domain);
		        }

				if( $result['Success'] === TRUE )
				{
					$airports = CityIata::selectRaw("CONCAT (COALESCE(`city_name`,''),'!@',COALESCE(`country_name`,''),'!@',COALESCE(`airport_name`,''),'!@',COALESCE(`country_code`,'')) as columns, iata_code")->pluck('columns', 'iata_code');

					$airline_info = Airline::pluck("airline_name","airline_code");

					if( is_array($result['PricedItineraries']) && count( $result['PricedItineraries']) >= 1 )
					{
						$priced_itineraries = $result['PricedItineraries']['PricedItinerary'];
						if (isset($commissions) && $commissions['pricing'][0]['status'] == 0) {
							$priced_itineraries = updateTransportPricing($result['PricedItineraries']['PricedItinerary'],'transport','mystifly',$commissions['eroam'],$commissions['pricing']);		
						}
						foreach( $priced_itineraries as $pr_it_key => $priced_itinerary )
						{
							if( isset( $priced_itinerary['OriginDestinationOptions'] ) )
							{
								$flight_segments = $priced_itinerary['OriginDestinationOptions']['OriginDestinationOption']['FlightSegments'];	
								if(! isset($flight_segments['FlightSegment']['ArrivalAirportLocationCode'])  )
								{
									foreach($flight_segments['FlightSegment'] as $fl_se_key => $flight_segment)
									{
										// echo "first";
										// exit;
										$marketing_airline_code = $flight_segment['MarketingAirlineCode'];
										$operating_airline_code = $flight_segment['OperatingAirline']['Code'];

										$arrival_data = isset($airports[$flight_segment['ArrivalAirportLocationCode']]) ? $airports[$flight_segment['ArrivalAirportLocationCode']] : array() ;
										if(!empty($arrival_data))
										{
											$arrival_data = explode('!@', $arrival_data);
										}

										$departure_data = isset($airports[$flight_segment['DepartureAirportLocationCode']]) ? $airports[$flight_segment['DepartureAirportLocationCode']] : array();
										if(!empty($departure_data))
										{
											$departure_data = explode('!@', $departure_data);
										}

										$city_info['departureCityName'] = isset($departure_data[0]) ? $departure_data[0] : '';
										$city_info['arrivalCityName'] = isset($arrival_data[0]) ? $arrival_data[0] : '';
										
										$flight_segment = array_merge( $flight_segment, [
											// 'MarketingAirlineName' => ($marketing_airline) ? $marketing_airline->airline_name: NULL,
											'MarketingAirlineName' => isset($airline_info[$marketing_airline_code]) ? $airline_info[$marketing_airline_code]: NULL,
											'ArrivalData'          => isset($arrival_data[0]) ? $arrival_data[2].', '.$flight_segment['ArrivalAirportLocationCode'].' '.$arrival_data[0]: NULL,
											'DepartureData'        => isset($departure_data[0]) ? $departure_data[2].', '.$flight_segment['DepartureAirportLocationCode'].' '.$departure_data[0]: NULL,
											]
										);

										$flight_segments['FlightSegment'][$fl_se_key] = array_merge( $flight_segments['FlightSegment'][$fl_se_key], $flight_segment );		

										if( $airline_info )
										{
											$flight_segment['OperatingAirline'] = array_merge( $flight_segment['OperatingAirline'],
												['Name' => isset($airline_info[$operating_airline_code]) ? $airline_info[$operating_airline_code] : ''
												] );
										}	
										$flight_segments['FlightSegment'][$fl_se_key]['cityInfo'] = $city_info;						
										$flight_segments['FlightSegment'][$fl_se_key]['OperatingAirline'] = array_merge( $flight_segments['FlightSegment'][$fl_se_key]['OperatingAirline'], $flight_segment['OperatingAirline'] );
									}

								}
								else
								{

									foreach($flight_segments as $fl_se_key => $flight_segment)
									{
										$marketing_airline_code = $flight_segment['MarketingAirlineCode'];
										$operating_airline_code = $flight_segment['OperatingAirline']['Code'];
										
										$arrival_data = isset($airports[$flight_segment['ArrivalAirportLocationCode']]) ? $airports[$flight_segment['ArrivalAirportLocationCode']] : array() ;
										$departure_data = isset($airports[$flight_segment['DepartureAirportLocationCode']]) ? $airports[$flight_segment['DepartureAirportLocationCode']] : array();
										if(!empty($arrival_data))
										{
											$arrival_data = explode('!@', $arrival_data);
										}
										if(!empty($departure_data))
										{
											$departure_data = explode('!@', $departure_data);
										}

										$city_info['departureCityName'] = isset($departure_data[0]) ? $departure_data[0] : '';
										$city_info['arrivalCityName'] = isset($arrival_data[0]) ? $arrival_data[0] : '';

										$flight_segment = array_merge( $flight_segment, [
											
											'MarketingAirlineName' => isset($airline_info[$marketing_airline_code]) ? $airline_info[$marketing_airline_code]: NULL,
											'ArrivalData'          => isset($arrival_data[0]) ? $arrival_data[2].', '.$flight_segment['ArrivalAirportLocationCode'].' '.$arrival_data[0]: NULL,
											'DepartureData'        => isset($departure_data[0]) ? $departure_data[2].', '.$flight_segment['DepartureAirportLocationCode'].' '.$departure_data[0]: NULL,
											]
										);

										$flight_segments[$fl_se_key] = array_merge( $flight_segments[$fl_se_key], $flight_segment );
										if( !empty($airline_info) ){
											$flight_segment['OperatingAirline'] = array_merge( $flight_segment['OperatingAirline'],
												['Name' => isset($airline_info[$operating_airline_code]) ? $airline_info[$operating_airline_code] : '' ]
												);
										}		
										$flight_segments[$fl_se_key]['cityInfo'] = $city_info;	
										$flight_segments[$fl_se_key]['OperatingAirline'] = array_merge( $flight_segments[$fl_se_key]['OperatingAirline'], $flight_segment['OperatingAirline'] );
									}
								}
								$priced_itinerary['OriginDestinationOptions']['OriginDestinationOption']['FlightSegments'] = $flight_segments;
								$priced_itineraries[ $pr_it_key ] = $priced_itinerary;
								$validating_airline_code          = $priced_itinerary['ValidatingAirlineCode'];
								$airline                          = Airline::where(['airline_code' => $validating_airline_code])->first();
								if( !empty($airline) ){
									$validating_airline_name        = array('ValidatingAirlineName' => $airline->airline_name);
									$priced_itineraries[$pr_it_key] = array_merge($priced_itineraries[$pr_it_key], $validating_airline_name );
								}	

							}
							else
							{
								$success = FALSE;
								foreach( $result['Errors'] as $k => $v){
									array_push($error, $v);
								}	
							}	
						}
						$result['PricedItineraries']['PricedItinerary'] = $priced_itineraries;
						$data = $result;

					}else{
						$success = FALSE;
						foreach( $result['Errors'] as $k => $v){
							array_push($error, $v);
						}	
					}
				}else{
					$success = FALSE;
					foreach( (array)$result['Errors'] as $k => $v){
						array_push($error, $v);
					}
				}

				$domain = request()->header('domain');
		        if ($domain) {
		            $commissions = getDomainData($domain);
		            if ($commissions['pricing'][0]['status'] == 0) {
		            	$result = updateTransportPricing($data,'transport','mystifly',$commissions['eroam'],$commissions['pricing']);
			        }else{
			        	$result['domain'] = FALSE;
			        }
		        }else{
		        	$result['domain'] = FALSE;
		        }
			

		        $data = $result;

				
		        $returnData = response_format($success, $data, $error);
		        Log::channel('mystifly_search')->info('Response : '.$returnData);
        		Log::channel('mystifly_search')->info('--------------------------------------------');
				return $returnData;
			}else{
				$success = FALSE;
				$data    = array();
				$error   = array();
				$returnData = response_format($success, $data, $error);
		        Log::channel('mystifly_search')->info('Response : '.$returnData);
        		Log::channel('mystifly_search')->info('--------------------------------------------');           
				return $returnData;
			}
		} catch (Exception $e) {
			
			$returnData = response_format($success, $data, $error);
			return $returnData;
			Log::error( '----------------in------------------------------' );
		                    Log::error( [
		                       'headers' => 'AirLowFareSearch',
		                       'error' => $error
		                   ] );
		}				
	}
	



	
	private function airLowFareSearch(
		$SessionId, 
		$PricingSourceType, 
		$IsRefundable, 
		$PassengerTypeQuantities, 
		$RequestOptions, 
		$IsResidentFare, 
		$Target, 
		$NearByAirports, 
		$OriginDestinationInformations, 
		$TravelPreferences 
	){	
		$returnData = array();
		$ptq_max_count = 2; 
		$ptq_count     = 0;
		$params = new stdClass();
		$params->rq = new stdClass();
		$params->rq->SessionId                     = $SessionId;	
		$params->rq->OriginDestinationInformations = $OriginDestinationInformations;
		$params->rq->TravelPreferences             = $TravelPreferences;
		$params->rq->PricingSourceType             = $PricingSourceType;		
		$params->rq->IsRefundable                  = $IsRefundable;
		$params->rq->PassengerTypeQuantities       = array();
		foreach($PassengerTypeQuantities as $key => $value)
		{
			if($ptq_count < $ptq_max_count)
			{
				$params->rq->PassengerTypeQuantities = array_merge( $params->rq->PassengerTypeQuantities, array($key => $value) );
				$ptq_count++;
			}
		}
		$params->rq->RequestOptions                = $RequestOptions;
		$params->rq->NearByAirports                = $NearByAirports;
		$params->rq->IsResidentFare                = $IsResidentFare;
		$params->rq->Target                        = $Target;
		
		$send_success = TRUE;
		
			try{

				$response = $this->client->AirLowFareSearch( $params );
				$send_success = FALSE;
				return (array)$response;
			}catch(Exception $e){
				return $returnData;
				Log::error('ERROR MESSAGE airLowFareSearch11:'.$e->getMessage() );
			}
			

		//return (array)$response;
	}

	
	private function originDestinationInformations($OriginDestinationInformation){
		$data = array();
		$data['OriginDestinationInformation'] = $OriginDestinationInformation;
		return $data;
	}

	
	
	public function fareRulesCall(){
	
		$success = TRUE;
		$data    = array();
		$error   = array();
		
		$inputData = Input::all();
		Log::channel('mystifly_rules')->info('Request : '.json_encode($inputData));

		$params     = new stdClass();
		$params->rq = new stdClass();
		$params->rq->SessionId      = $this->get_session_id();
		$params->rq->FareSourceCode = Input::get('FareSourceCode');
		$params->rq->target         = self::TARGET;
		
		try{
				
				$StaggingMystifly = StaggingMystifly::where('fare_source_code',Input::get('FareSourceCode'))->first();
				if($StaggingMystifly != NULL){

					$data = $StaggingMystifly->fare_rules;

				}else{

					$response = $this->client->FareRules1_1( $params );
					$result   = get_object_vars($response);
					if( (int)$result['FareRules1_1Result']->Success === 1 ){
						$data = ['BaggageInfos' => (array)$result['FareRules1_1Result']->BaggageInfos,
						'FareRules' => (array)$result['FareRules1_1Result']->FareRules
						];
					}

					StaggingMystifly::create([
			        'fare_source_code' => Input::get('FareSourceCode'),
			        'fare_rules' => $data
			        ]);
				}
				
				$returnData = response_format($success, $data, $error);
			}catch(Exception $e){
				$returnData = response_format($success, $data, $error);
				return $returnData;

				Log::error('ERROR MESSAGE airLowFareSearch11:'.$e->getMessage() );
			}

		

		Log::channel('mystifly_rules')->info('Response : '.$returnData);
		Log::channel('mystifly_rules')->info('--------------------------------------------');

		return $returnData;
	}

	public function airRevalidateCall()
	{
				
		$success = TRUE;
		$data    = array();
		$error   = array();
		
		$inputData = Input::all();
		
		
		Log::channel('mystifly_revalidate')->info('Request : '.json_encode($inputData));
	

		$params = new stdClass();
		$params->rq = new stdClass();
		$params->rq->SessionId = $this->get_session_id();
		$params->rq->FareSourceCode = Input::get('FareSourceCode');
		$params->rq->target = self::TARGET;
		

		try {

			$response = $this->client->AirRevalidate( $params );
			$result = get_object_vars($response);
			$result = json_decode( json_encode($result), TRUE);
			$result = $result['AirRevalidateResult'];

			if($result['Success'] === TRUE ){
				$data = $result;
			} else {
				$success = TRUE;
				foreach( $result->Errors as $k => $v ) {
					array_push( $error, $v );
				}	
			}
			$returnData = response_format( $success, $data, $error );	
		}catch( Exception $e ) {
			$returnData = response_format($success, $data, $error);
			return $returnData;
			$error = ['An error has occured during mystifly airRevalidateCall'];
		}

		

		Log::channel('mystifly_revalidate')->info('Response : '.$returnData);
		Log::channel('mystifly_revalidate')->info('--------------------------------------------');

        return $returnData;
	}

	public function bookingCall(){
		$success = TRUE;
		$data    = [];
		$error   = [];
		

		//"Message":"Invalid Child's Age. Should be less than or equal to 12 years"
		//"Message":"Invalid Infant's Age. Should be less than or equal to 2 years."
		//Max 9 Pax
		//No of infant should be less or equal to adult
		//"Message":"Passengers (PassengerFirstName) name cannot be same"

		$inputData = Input::all();

		Log::channel('mystifly_book')->info('Request : '.json_encode($inputData));

		$ExtraServiceId = Input::get('Services');
		$ExtraServices1_1 = array();
		if(!empty($ExtraServiceId))
		{
			for($i = 0;$i<count($ExtraServiceId);$i++)
			{
				$ExtraServices1_1['Services']['ExtraServiceId'][$i] = $ExtraServiceId[$i];
			}
		}
	
		$cnt = count(Input::get('passenger_dob'));
		$TravelerInfo = array();
		$PassengerName = array();
		for($i=0;$i<$cnt;$i++) {
			if(!empty($ExtraServices1_1))
			{
				$AirTraveler['AirTraveler'][$i]['ExtraServices1_1'] = $ExtraServices1_1;
			}

			$AirTraveler['AirTraveler'][$i]['DateOfBirth'] = Input::get('passenger_dob')[$i];
			$AirTraveler['AirTraveler'][$i]['Gender'] = Input::get('passenger_gender')[$i];

			$PassengerName['PassengerFirstName'] = Input::get('passenger_first_name')[$i];
			$PassengerName['PassengerLastName'] = Input::get('passenger_last_name')[$i];
			$PassengerName['PassengerTitle'] = Input::get('passenger_title')[$i];
			
			$AirTraveler['AirTraveler'][$i]['PassengerName'] = $PassengerName;

			$AirTraveler['AirTraveler'][$i]['PassengerNationality'] = Input::get('passenger_country')[$i];
			$AirTraveler['AirTraveler'][$i]['PassengerType'] = Input::get('passenger_type')[$i];

			if(Input::get('passenger_passport_country',false) !== false){
				$Passport['Country'] = Input::get('passenger_passport_country')[$i];
				$Passport['ExpiryDate'] = Input::get('passenger_passport_expiry_date')[$i];
				$Passport['PassportNumber'] = Input::get('passenger_passport_number')[$i];
				$AirTraveler['AirTraveler'][$i]['Passport'] = $Passport;
			}
			

			$SpecialServiceRequest['MealPreference'] = Input::get('passenger_meal_preference')[$i];
			$SpecialServiceRequest['SeatPreference'] = 'Any';

			$AirTraveler['AirTraveler'][$i]['SpecialServiceRequest'] = $SpecialServiceRequest;

				
		}

		$TravelerInfo['AirTravelers'] = $AirTraveler;
		$TravelerInfo['AreaCode'] = Input::get('passenger_area_code')[0];
		$TravelerInfo['CountryCode'] = Input::get('passenger_country_code')[0];
		$TravelerInfo['Email'] =  Input::get('passenger_email')[0];
		$TravelerInfo['PhoneNumber'] = Input::get('passenger_contact_no')[0];
		
		if(Input::get('passenger_post_code',false) !== false){
				$TravelerInfo['PostCode'] = Input::get('passenger_post_code')[0];
		}

		$notes = Input::get('Notes');
		$FareType = Input::get('FareType');
		$params                      = new stdClass();
		$params->rq                  = new stdClass();
		$params->rq->FareSourceCode  = Input::get('FareSourceCode');
		$params->rq->SessionId       = $this->get_session_id();
		$params->rq->target          = self::TARGET;
		$params->rq->TravelerInfo    = $TravelerInfo;
		
		//echo "<pre>";print_r($params);exit;
		try {
			$response              = $this->client->BookFlight( $params );
			$res                   = get_object_vars($response);
			$res = json_decode(json_encode($res),true);
			$data = $res;
			if($res['BookFlightResult']['Success'] == 1){
				$returntripDetails = $this->tripDetailsCall($res['BookFlightResult']['UniqueID']);
				$data['tripDetails'] = $returntripDetails;
				
			}
			if($res['BookFlightResult']['Success'] == 1 && $notes != ''){
				$returnNotes = $this->addBookingNotesCall($res['BookFlightResult']['UniqueID'],$notes);
				$data['BookingNotes'] = $returnNotes;
			}
			if($FareType != 'WebFare'){

					//Job call if GDS carriers
					//Order ticket API is a simple request for ordering the blocked itinerary for the GDS full service carriers
					MystiflyOrderTicketCall::dispatch($res['BookFlightResult']['UniqueID'],$this->get_session_id());
				}
			
		}
		catch( Exception $e ) {
			$error = ['An error has occured during mystifly bookingCall'];
		}
		
		$returnData = response_format( $success, $data, $error );

		Log::channel('mystifly_book')->info('Response : '.$returnData);
		Log::channel('mystifly_book')->info('--------------------------------------------');	
        return $returnData;
	}

	public function ticketOrderCall($FareSourceCode,$UniqueID){
		
		
		$jsonData = array();
		$jsonData['FareSourceCode'] = $FareSourceCode;
		$jsonData['UniqueID'] = $UniqueID;

		Log::channel('mystifly_ticket')->info('Request : '.json_encode($jsonData));


		$params                      = new stdClass();
		$params->rq                  = new stdClass();
		$params->rq->SessionId       = $this->get_session_id();
		$params->rq->FareSourceCode  = $FareSourceCode;
		$params->rq->UniqueID        = $UniqueID;
		$params->rq->target          = $this->target;
		$response              = $this->client->TicketOrder( $params );
		$res                   = get_object_vars($response);

        
        Log::channel('mystifly_ticket')->info('Response : '.\Response::json( $res ));
		Log::channel('mystifly_ticket')->info('--------------------------------------------');

        return $res;
			
	}

	private function tripDetailsCall($UniqueID)
	{
		  $jsonData = array();
		  $jsonData['UniqueID'] = $UniqueID;

		
		  Log::channel('mystifly_details')->info('Request : '.json_encode($jsonData));

		  $params                      = new stdClass();
		  $params->rq                  = new stdClass();
		  $params->rq->SessionId       = $this->get_session_id();
		  $params->rq->UniqueID        = $UniqueID;
		  $params->rq->target          = self::TARGET;
		  $response              = $this->client->TripDetails( $params );
		  $res                   = get_object_vars($response);
		  $result = array(
					'success' => 1,
					'data' => $res
					); 
		  return $result;
         
          Log::channel('mystifly_details')->info('Response : '.\Response::json( $result ));
		  Log::channel('mystifly_details')->info('--------------------------------------------');

          return $result;
	}

	private function addBookingNotesCall($UniqueID,$Notes)
	{

		if(!empty($UniqueID) && !empty($Notes)){

			$jsonData = array();
			$jsonData['UniqueID'] = $UniqueID;
			$jsonData['Notes'] = $Notes;

			Log::channel('mystifly_notes')->info('Request : '.json_encode($jsonData));


			$params                = new stdClass();
			$params->rq            = new stdClass();
			$params->rq->SessionId = $this->get_session_id();
			$params->rq->UniqueID  = $UniqueID;
			$params->rq->Notes     = $Notes;
			$params->rq->Target    = self::TARGET;
			$response              = $this->client->AddBookingNotes( $params );
			$res                   = get_object_vars($response);

			
			
			Log::channel('mystifly_notes')->info('Response : '.\Response::json( $res ));
			Log::channel('mystifly_notes')->info('--------------------------------------------');
			return $res;
		}
		 
	}

	private function originDestinationInformation(
		$DepartureDateTime, 
		$DestinationLocationCode, 
		$OriginLocationCode)
	{
		$data   = array();
		$values = array();
		if(!is_array($DepartureDateTime)){
			$data['DepartureDateTime']       = $DepartureDateTime;
			$data['DestinationLocationCode'] = $DestinationLocationCode;
			$data['OriginLocationCode']      = $OriginLocationCode;
			}
		if(is_array($DepartureDateTime)){
			foreach($DepartureDateTime as $key => $value){
				$values['DepartureDateTime']       = $value;
				$values['DestinationLocationCode'] = $DestinationLocationCode[$key];
				$values['OriginLocationCode']      = $OriginLocationCode[$key];
				array_push($data, $values);
				}
			}
		return $data;
	}


	private function passengerTypeQuantities($PassengerTypeQuantity){
		
		$data = array();
		$data['PassengerTypeQuantity'] = array();
		if($PassengerTypeQuantity){
			foreach($PassengerTypeQuantity as $key => $value){
				$data['PassengerTypeQuantity'] = array_merge($data['PassengerTypeQuantity'], array($key => $value));
			}
		}
		return $data;
	}	

	
	private function passengerTypeQuantity($Code, $Quantity){
		
		$data = array();
		foreach($Code as $key => $code){
			$data[$key]['Code'] = $code;
			$data[$key]['Quantity'] = $Quantity[$key];
		}
		return $data;
	}	

	
	private function travelPreferences(
		$AirTripType, 
		$CabinPreference, 
		$MaxStopsQuantity,
		$Preferences, 
		$VendorPreferenceCodes)
	{
		$data = array();
		$data['AirTripType']      = $AirTripType;
		$data['CabinPreference']  = $CabinPreference;
		$data['MaxStopsQuantity'] = $MaxStopsQuantity;		
		$data['Preferences']      = $Preferences;
		return $data;
	}

	
	private function cabinClassPreference($CabinType, $PreferenceLevel){
		$data = array();
		$data['CabinType']        = $CabinType;
		$data['PreferenceLevel']  = $PreferenceLevel;
		return $data;
	}	

	public function departureDateTime($date, $time){
		$t = new \DateTime('2000-01-01');
		return $date.'T'.$t->format('H:i:s');
	}
	
	

	private function get_iata_code_by_city_id( $city_id ){
		$result = CityIata::where(['city_id' => $city_id ])->get();
		return $result->toArray();
	}

	private function get_city_name_and_country_name_by_city_id( $city_id ){
		$city    = City::where(['id' => $city_id])->pluck('name');
		$country = City::find( $city_id )->country;	
		return $city.', '.$country->name;
	}


	
	public function convertToBoolean($val){
		if($val == 0)
			return FALSE;
		return TRUE;
	}	

	private function logRequest($function_name, $request){
	}

	
	private function logResponse($function_name, $response){
		$res    = get_object_vars($response);
		$rs     = get_object_vars($res[$function_name.'Result']);
		$errors = get_object_vars($rs['Errors']);
		
	}

	public function getCityAirportCode(){
		$success = TRUE;
		$data    = [];
		$error   = [];
		$data = CityIata::pluck('city_name', 'iata_code');

		return response_format( $success, $data, $error );
	}

	
	public function response_format($success, $data, $error){
		if(is_array($data)){
			if(count($data) < 1)
				$data = NULL;
		}
		if(is_array($error)){
			if(count($error) < 1)
				$error = NULL;
		}		
		$result = array(
					'success' => $success,
					'data' => $data,
					'error' => $error
					);
		return \Response::json( $result );
	}
	
}
