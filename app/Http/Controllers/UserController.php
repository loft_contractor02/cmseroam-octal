<?php
namespace App\Http\Controllers;
//priya
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use App\Mail\SendMailable;
use App\Http\Requests\UserRequest;
use Carbon\Carbon;
use App\User;
use App\Licensee;
use App\UserDomain;
use App\AgentCommissionPercentage;
use App\Agent;
use App\Product;
use App\Brand;
use App\Currency;
use App\Customer;
use App\Itinerary;
use App\PasswordReset;
use App\ItenaryOrder;
use App\PassengerInformation;
use App\AgentNotification;
use App\BookingCommission;
use App\Domain;
use App\SavedTrips;
use Auth;
use Hash;
use DB;
use Crypt;
use Session;
use App\Cmsuserlog;



class UserController extends Controller
{
    //const EROAM_EMAIL = 'info@eroam.com';
    const EROAM_EMAIL = 'support@eroam.com';
    protected $cms_log;
    public function __construct()
    {
        $this->cms_log = new Cmsuserlog;
        $this->middleware('auth', ['only' => ['callUserList', 'callCreateUserLicensee', 'checkUserNameExists', 'getRandomPassword',
                                                'callCreateUserAgent','callCreateUserCustomer','callCreateUserProduct']]);
    }
    
    public function callUserlogin(Request $oRequest)
    {
    	if ($oRequest->isMethod('post'))
    	{
             $oValidator = Validator::make($oRequest->all(), [
                                    'username' => 'required',
                                    'password' => 'required'
                                    ]);
            if($oValidator->fails()) 
            {
                //print_r($oValidator->messages());exit;
                return Redirect::back()->withErrors($oValidator)->withInput( $oRequest->except( 'password' ) );
            }
            $aData =  $oRequest->all();
            $sUser = $this->loginUser($aData);

            if ($sUser['auth'] == 'valid') {
                
                $this->cms_log->saveLog($sUser);

                Auth::login($sUser);
                session()->put('role',$sUser->type);
                session()->put('user_id',$sUser->id);

                if ($sUser->type == 'agent') {
                    $domain = UserDomain::where('user_id',$sUser->id)->with('domains')->first();
                    if (!empty($domain->toArray())) {
                        $arr = $domain->toArray();
                        session()->put('domain_id',$arr['domains']['id']);
                    }
                }

                return Redirect::to('/');
            } elseif ($sUser['auth'] == 'inactive') {
                // if account is not active
                $oValidator->getMessageBag()->add('password', 'Account not active.');
                return Redirect::back()->withErrors($oValidator)->withInput( $oRequest->except( 'password' ) );
            } else {
                //if invalid credentials
                $oValidator->getMessageBag()->add('password', 'Invalid username or password.');
                return Redirect::back()->withErrors($oValidator)->withInput( $oRequest->except( 'password' ) );
            }
        }
        return view('WebView::home.login');
    }

    
    public function dashboard(Request $oRequest) {

		$oOrderBy = $oRequest->order_by; 
		$oOrderField = $oRequest->order_field; 
		
		if(session('page_name') != 'dashboard') {
            session()->forget('dashboard');
        }
		session(['page_name' => 'dashboard']);
        if(session()->has('role') && session()->get('role') === 'agent') {  
        $agent_id = Auth::user()->id;
           
            $domain_id = $data = session()->get('domain_id');      
            $userIds = User::where('user_id',$agent_id)->where('domain_id',$domain_id)->where('type','customer')->get(); 

            if(!empty($userIds)){
                $userIds = $userIds->toArray();
                $userIds = array_column($userIds, 'id');
            }            

            $firstDayOfMonth = Carbon::now()->startOfMonth()->format('Y-m-d').' 00:00:00.000000';
            $endDayOfMonth = Carbon::now()->endOfMonth()->format('Y-m-d').' 23:59:59.000000';

            $bookingCount = ItenaryOrder::whereIn('user_id',$userIds)
            ->whereBetween('created_at', [$firstDayOfMonth, $endDayOfMonth])->where('agent_id',$agent_id)->count();
            
            //$totalSalesSum = DB::table('itenaryorders')->whereIn('user_id',$userIds)->sum('total_amount');
            $totalSalesSum = ItenaryOrder::whereIn('user_id',$userIds)
            ->whereBetween('created_at', [$firstDayOfMonth, $endDayOfMonth])
            ->where('agent_id',Auth::user()->id)
            ->sum('total_amount');

            $getBookingIds = ItenaryOrder::where('agent_id',$agent_id)
                                        ->select('order_id')
                                        ->get();
            $booking_id = array();
            if(!empty($getBookingIds)){
                $getBookingIds = $getBookingIds->toArray();
                foreach($getBookingIds as $values){  
                    $booking_id[] = $values['order_id'];
                }
                $commsionSum = BookingCommission::whereIn('booking_id',$booking_id)
                                            ->whereBetween('created_at', [$firstDayOfMonth, $endDayOfMonth])
                                            ->sum('licensee_benefit');
                $commsionSum  = round($commsionSum,2);
            }
            
            //Searching logic start here
            $aData = session('pending_booking') ? session('pending_booking') : array();
            $currentData = session('current_booking') ? session('current_booking') : array();
            $oRequest->session()->forget('pending_booking');

            $oRequest->session()->forget('current_booking');
            $status = ($oRequest->has('status')) ? $oRequest->status :'';
            $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'id');
            $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'DESC');


            $currentOrderField = !empty($oRequest->has('current_order_field')) ? $oRequest->current_order_field : ((count($currentData)) ? $currentData['current_order_field'] : 'id');

            $currentOrderBy = ($oRequest->has('current_order_by')) ? $oRequest->current_order_by : ((count($currentData)) ? $currentData['current_order_by'] : 'DESC');

            if($status == "Pending"){
                $status = ($oRequest->has('status')) ? $oRequest->status :'Pending';
                $pendingBookings = ItenaryOrder::getPendingBooking($userIds,$sOrderField,$sOrderBy,$status);
                setPendingSession($sOrderField,$sOrderBy,'pending_booking');
            }if($status == "Active"){
                $status = ($oRequest->has('status')) ? $oRequest->status :'Active';
                $currentBookings = ItenaryOrder::getPendingBooking($userIds,$currentOrderField,$currentOrderBy,$status);
                setPendingSession($currentOrderField,$currentOrderBy,'current_booking');
            } 
            if(empty($status)){ 
                //For Pending Booking
				$now = Carbon::now()->format('Y-m-d').' 00:00:00';
				$pendingBookings = SavedTrips::from('saved_trips as i')
                                    ->select('i.*',
                                                    'u.name as username')
                                   ->leftjoin('users as u','u.id','=','i.user_id')
									->where('i.created_at', '>=', $now)
									->where('u.domain_id',$domain_id)
									->where('i.user_id',$agent_id)
									->orderBy('i.id','DESC')
                                    ->groupBy('i.id')
									->limit(5)
                                    ->get();
		
				// $pendingBookings = SavedTrips::leftjoin('users','users.id','=','saved_trips.user_id')
                                                // ->select('saved_trips.*',
                                                        // 'users.name as username')
                                               // ->whereIn('saved_trips.user_id',$userIds)
												// ->where('users.domain_id',$domain_id)
												// ->where('saved_trips.user_id',$agent_id)
												// ->where('saved_trips.created_at', '>=', $now)
                                                // ->orderBy('saved_trips.id','DESC')
                                                // ->limit(5)
                                                // ->get();
												
				$currentBookings = ItenaryOrder::leftjoin('users','users.id','=','itenaryorders.user_id')
                                                ->select('itenaryorders.*',
                                                        'users.name as username',
                                                        DB::raw('(SELECT COUNT(passenger_information_id) FROM passengerinformations WHERE itenary_order_id =itenaryorders.order_id) as pax_count'),
                                                        DB::raw('(SELECT sum(licensee_benefit) FROM booking_commissions WHERE booking_id =itenaryorders.order_id) as commission')
                                                        )
                                                ->whereIn('itenaryorders.user_id',$userIds)
												->where('itenaryorders.created_at', '>=', $now)
                                                ->orderBy('itenaryorders.order_id','DESC')
                                                ->limit(5)
                                                ->get();
				$notifications = $this->getNotificationItenary($agent_id);
			}
            if($oRequest->status == "Pending"){
                $oViewName =  'WebView::agent._pending_list_ajax';
            }if($oRequest->status == "Active"){
                $oViewName =  'WebView::agent._current_list_ajax';
            }if($oRequest->status == "notification"){
				
				$notifications = $this->getNotificationItenary($agent_id,$oOrderBy,$oOrderField);
                $oViewName =  'WebView::agent._notification_list_ajax';
            }else{
                $oViewName = $oRequest->isMethod('GET') ? 'WebView::home.agent-dashboard' : 'WebView::agent._pending_list_ajax';
            }
			
			return view($oViewName,compact('bookingCount','userIds','totalSalesSum','pendingBookings','currentBookings','sOrderField','sOrderBy','currentOrderField','currentOrderBy','notifications','commsionSum'));
        }
        return view('WebView::home.dashboard');
    }
	
    public function getTotalCommission(){
        //Get First and Last Day of the current week
        $startDayOfWeek = Carbon::now()->startOfWeek()->format('Y-m-d').' 00:00:00.000000';
        $endDayOfWeek = Carbon::now()->endOfWeek()->format('Y-m-d').' 23:59:59.000000';
        $todayDate  = date("Y-m-d");

        //Get First And Last Day of the month
        $firstDayOfMonth = Carbon::now()->startOfMonth()->format('Y-m-d').' 00:00:00.000000';
        $endDayOfMonth = Carbon::now()->endOfMonth()->format('Y-m-d').' 23:59:59.000000';

        //get first and last month's date
        $firstDayOfYear = Carbon::now()->startOfYear()->format('Y-m-d').' 00:00:00.000000';
        $endDayOfYear = Carbon::now()->endOfYear()->format('Y-m-d').' 23:59:59.000000';

        $totalBookingCount = Input::get('totalBookingCount');
        $duration = Input::get('duration');
        $user_id = Input::get('user_id');
        $domain_id = session()->get('domain_id');
        $userIds = User::where('type','customer')->where('user_id',$user_id)->where('domain_id',$domain_id)->select('id')->get();
        $getBookingIds = ItenaryOrder::where('agent_id',$user_id)
        ->select('order_id')
        ->get();

        $booking_id = array();
        if(empty($getBookingIds)){
           return 0;
        }
        
        $getBookingIds = $getBookingIds->toArray();
        
        foreach($getBookingIds as $values){  
            $booking_id[] = $values['order_id'];
        }

        if($duration =="Day"){ 
            $getBookinCount = BookingCommission::whereIn('booking_id',$booking_id)
            ->whereBetween('created_at', [$todayDate." 00:00:00.000000", $todayDate." 23:59:59.000000"])
            ->sum('licensee_benefit');

        }elseif($duration == "Week"){ 
            $getBookinCount = BookingCommission::whereIn('booking_id',$booking_id)
            ->whereBetween('created_at', [$startDayOfWeek, $endDayOfWeek])
            ->sum('licensee_benefit');
            $getBookinCount  = round($getBookinCount,2);

        }elseif($duration == "Month"){ 
            $getBookinCount = BookingCommission::whereIn('booking_id',$booking_id)
            ->whereBetween('created_at', [$firstDayOfMonth, $endDayOfMonth])
            ->sum('licensee_benefit');
            $getBookinCount  = round($getBookinCount,2);

        }elseif($duration == "Year"){ 
            $getBookinCount = BookingCommission::whereIn('booking_id',$booking_id)
            ->whereBetween('created_at', [$firstDayOfYear, $endDayOfYear])
            ->sum('licensee_benefit');
            $getBookinCount  = round($getBookinCount,2);
        }

        return $getBookinCount;
    }
	
	public function getNotificationItenary($agent_id,$oOrderBy='desc',$oOrderField='i.id'){
		$dt = Carbon::now()->format('Y-m-d');
		return $notifications = AgentNotification::from('agent_notifications as i')
								->select('io.created_at as created_date',
								'i.id',
								'io.from_date as start_date',
								'io.to_date as end_date',
								'i.type as notification_type',
								'i.notification as notification_title',
								'io.num_of_travellers as total_pax'
								) 
								->leftjoin('itenaryorders as io','i.booking_id','=','io.order_id')
									->where('i.agent_id',$agent_id)
									->where('i.showing_date',$dt)
									//->orderBy($oOrderField,$oOrderBy)
									->get();
									//pr($notifications);die;
	}

    public function callUserLogout() 
    {
        session()->forget('user_auth');
        session()->forget('travel_preferences');
        Auth::logout();
        return redirect('/login');
    }

    public function logoutEroam() 
    {
        $user_id = session()->get('redirect_user_id');
        $user = User::where('id', $user_id)->first();
        $domain = Domain::where('id',$user['domain_id'])->first()['name']; 
        $url = "http://{$domain}/logout";
        
        //print_r($domain);die();

        return response()->json([
            'url' => $url
        ]);
    }

    //get Total Booking Count For Dashboard ajax request
    public function getBookingCount(){
        //Get First and Last Day of the current week
        $startDayOfWeek = Carbon::now()->startOfWeek()->format('Y-m-d').' 00:00:00.000000';
        $endDayOfWeek = Carbon::now()->endOfWeek()->format('Y-m-d').' 23:59:59.000000';
        $todayDate  = date("Y-m-d");

        //Get First And Last Day of the month
        $firstDayOfMonth = Carbon::now()->startOfMonth()->format('Y-m-d').' 00:00:00.000000';
        $endDayOfMonth = Carbon::now()->endOfMonth()->format('Y-m-d').' 23:59:59.000000';

        //get first and last month's date
        $firstDayOfYear = Carbon::now()->startOfYear()->format('Y-m-d').' 00:00:00.000000';
        $endDayOfYear = Carbon::now()->endOfYear()->format('Y-m-d').' 23:59:59.000000';

        $totalBookingCount = Input::get('totalBookingCount');
        $duration = Input::get('duration');
        $user_id = Input::get('user_id');
        $domain_id = session()->get('domain_id');
        $userIds = User::where('type','customer')->where('user_id',$user_id)->where('domain_id',$domain_id)->select('id')->get();
        
        if(!empty($userIds)){
            $userIds = $userIds->toArray();
            $userIds = array_column($userIds, 'id');
        }
        
        if($duration =="Day"){ 
            $getBookinCount = ItenaryOrder::whereIn('user_id',$userIds)
            ->whereBetween('created_at', [$todayDate." 00:00:00.000000", $todayDate." 23:59:59.000000"])
            ->where('agent_id',$user_id)
            ->count();

        }elseif($duration == "Week"){ 
            $getBookinCount = ItenaryOrder::whereIn('user_id',$userIds)
            ->whereBetween('created_at', [$startDayOfWeek, $endDayOfWeek])
            ->where('agent_id',$user_id)
            ->count();

        }elseif($duration == "Month"){ 
            $getBookinCount = ItenaryOrder::whereIn('user_id',$userIds)
            ->whereBetween('created_at', [$firstDayOfMonth, $endDayOfMonth])
            ->where('agent_id',$user_id)
            ->count();

        }elseif($duration == "Year"){ 
            $getBookinCount = ItenaryOrder::whereIn('user_id',$userIds)
            ->whereBetween('created_at', [$firstDayOfYear, $endDayOfYear])
            ->where('agent_id',$user_id)
            ->count();
        }

        return $getBookinCount;
    }

    public function getTotalSales(){
        //Get First and Last Day of the current week
        $startDayOfWeek = Carbon::now()->startOfWeek()->format('Y-m-d').' 00:00:00.000000';
        $endDayOfWeek = Carbon::now()->endOfWeek()->format('Y-m-d').' 23:59:59.000000';
        $todayDate  = date("Y-m-d");

        //Get First And Last Day of the month
        $firstDayOfMonth = Carbon::now()->startOfMonth()->format('Y-m-d').' 00:00:00.000000';
        $endDayOfMonth = Carbon::now()->endOfMonth()->format('Y-m-d').' 23:59:59.000000';

        //get first and last month's date
        $firstDayOfYear = Carbon::now()->startOfYear()->format('Y-m-d').' 00:00:00.000000';
        $endDayOfYear = Carbon::now()->endOfYear()->format('Y-m-d').' 23:59:59.000000';

        $totalBookingCount = Input::get('totalBookingCount');
        $duration = Input::get('duration');
        $user_id = Input::get('user_id');
        $domain_id = session()->get('domain_id');
        $userIds = User::where('type','customer')->where('user_id',$user_id)->where('domain_id',$domain_id)->select('id')->get();
        
        if(!empty($userIds)){
            $userIds = $userIds->toArray();
            $userIds = array_column($userIds, 'id');
        }

        if($duration =="Day"){ 
            $getTotalSales = ItenaryOrder::whereIn('user_id',$userIds)
            ->whereBetween('created_at', [$todayDate." 00:00:00.000000", $todayDate." 23:59:59.000000"])
            ->where('agent_id',$user_id)
            ->sum('total_amount');
        
        }elseif($duration == "Week"){ 
            $getTotalSales = ItenaryOrder::whereIn('user_id',$userIds)
            ->whereBetween('created_at', [$startDayOfWeek, $endDayOfWeek])
            ->where('agent_id',$user_id)
            ->sum('total_amount');

        }elseif($duration == "Month"){ 
            $getTotalSales = ItenaryOrder::whereIn('user_id',$userIds)
            ->whereBetween('created_at', [$firstDayOfMonth, $endDayOfMonth])
            ->where('agent_id',$user_id)
            ->sum('total_amount');

        }elseif($duration == "Year"){ 
            $getTotalSales = ItenaryOrder::whereIn('user_id',$userIds)
            ->whereBetween('created_at', [$firstDayOfYear, $endDayOfYear])
            ->where('agent_id',$user_id)
            ->sum('total_amount');
        }

        return $getTotalSales;
    }

    public function callUserList(Request $oRequest, $sUserType='')
    {
        //remove session when it comes from sidebar
        if(session('page_name') != $sUserType || $oRequest->query('isreset') == 1)
            $oRequest->session()->forget('user');
        
       // echo $oRequest->has('search_str');
        session(['page_name' => $sUserType]);
        $aData = session('user') ? session('user') : array();
        $oRequest->session()->forget('user');
        
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'name');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'asc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        
        if(count($aData) && $sSearchStr != $aData['search_str'])
            $nPage = 1;
        
        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });
        
        $oUserList = User::getUserList($sUserType,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);
        //dd(DB::getQueryLog());

        setSession($sSearchStr,'',$sOrderField,$sOrderBy,$nShowRecord,$oUserList->currentPage(),'user');
        
        if($oRequest->page > 1)
            $oViewName =  'WebView::user._user_list_ajax';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::user.user_list' : 'WebView::user._user_list_ajax';
        
        return \View::make($oViewName, compact('oUserList','sUserType','sSearchStr','sOrderField','sOrderBy','nShowRecord'));
    }
    
    public function callCreateUserLicensee(Request $oRequest)
    {
        session(['page_name' => 'licensee']);
        if ($oRequest->isMethod('post'))
    	{
            $oValidator = Validator::make($oRequest->all(), [
                                    'name' => 'required',
                                    'domain' => 'required|unique:licensees,domain',
                                    'username' => 'required|email|unique:users,username',
                                    'password' => 'required|min:6',
                                    'duration' => 'required'
                                    ]);
            if($oValidator->fails()) 
            {
                //print_r($oValidator->messages());exit;
                return Redirect::back()->withErrors($oValidator)->withInput( $oRequest->except( 'password' ) );
            }
            $oUser = User::firstOrCreate([
				'name' => $oRequest->name,
				'username' => $oRequest->username,
				'password' => Hash::make( $oRequest->password ),
				'type' => config('constants.USERTYPELICENSEE'),
				'active' => 0,
			]);
            $oLicensee = Licensee::firstOrCreate([
				'user_id' => $oUser->id,
				'domain' => $oRequest->domain,
				'expiry_date' => date( 'Y-m-d', strtotime( '+' . $oRequest->duration .' days' ) )
			]);

            \Session::flash('message', trans('messages.success_inserted'));
            
                return Redirect::route('user.list',[ 'sUserType' => 'licensee' ])->with( [
                'message' => 'success', 
                'user_id' => $oUser->id, 
                'licensee_name' => $oUser->name 
                ]);
        }
        return \View::make('WebView::user.user_create_licensee');
    }
    
    public function checkUserNameExists(Request $oRequest) 
    {
        $result = User::where(['username' => $oRequest->username ])->get();
        return (count($result) > 0) ? 0: 1;
    }
    
    private function getRandomPassword() 
    {
        $length = 8; // pw length
    	$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    	$password = substr( str_shuffle( $chars ), 0, $length );
    	return $password;
    }
    
    public function callCreateUserAgent(Request $oRequest)
    {
        session(['page_name' => 'agent']);
        if ($oRequest->isMethod('post'))
    	{
            $oValidator = Validator::make($oRequest->all(), [
                                    'first_name'     => 'required',
                                    'last_name'      => 'required',
                                    'username'       => 'required|unique:users,username',
                                    'contact_number' => 'required',
                                    'address'        => 'required',
                                    'percentage'     => 'required|numeric'
                                    ]);
            if($oValidator->fails()) 
            {
                //print_r($oValidator->messages());exit;
                return Redirect::back()->withErrors($oValidator)->withInput();
            }
            $sPassword = '123456'; //$this->getRandomPassword();
            $oUser = User::firstOrCreate([
				'name' => $oRequest->first_name.' '.$oRequest->last_name,
				'username' => $oRequest->username,
				'password' => Hash::make( $sPassword ),
				'type' => config('constants.USERTYPEAGENT'),
				'active' => 1,
			]);
            $oAgent = Agent::firstOrCreate([
				'user_id' => $oUser->id,
				'contact_number' => $oRequest->contact_number,
                                'address' => $oRequest->address
			]);
            $oAgentCommissionPercentage = AgentCommissionPercentage::firstOrCreate([
                                                    'percentage' => $oRequest->percentage,
                                                    'agent_id' => $oAgent->id
                                            ]);
            Agent::where(['id' => $oAgent->id])->update(['agent_commission_percentage_id' => $oAgentCommissionPercentage->id]);
            
            Session::flash('message', trans('messages.success_inserted'));
            /*return Redirect::back()->with( [
                'message' => 'success', 
                'user_id' => $oUser->id, 
                'agent_name' => $oUser->name 
                ]);*/
                return Redirect::route('user.list',[ 'sUserType' => 'agent'  ])->with( [
                'message' => 'success', 
                'user_id' => $oUser->id, 
                'agent_name' => $oUser->name 
                ]);
        }
        return \View::make('WebView::user.user_create_agent');
    }
    
    public function callCreateUserCustomer(Request $oRequest) 
    {
        session(['page_name' => 'customer']);
        $oCurrency = Currency::all();
        if ($oRequest->isMethod('post'))
    	{
            $oValidator = Validator::make($oRequest->all(), [
                                    'first_name'  => 'required',
                                    'last_name'   => 'required',
                                    'username'    => 'required|unique:users,username',
                                    'currency_id' => 'required',
                                    'address'     => 'required'
                                    ]);
            if($oValidator->fails()) 
            {
                return Redirect::back()->withErrors($oValidator)->withInput();
            }
            $sPassword = $this->getRandomPassword();
            $oUser = User::firstOrCreate([
				'name' => $oRequest->first_name.' '.$oRequest->last_name,
				'username' => $oRequest->username,
				'password' => Hash::make( $sPassword ),
				'type' => config('constants.USERTYPECUSTOMER'),
				'active' => 1,
			]);
            $oCurrencyCode = Currency::select('code')->where('id', $oRequest->currency_id)->first();
            
            $customer = Customer::firstOrCreate([
                'first_name' =>$oRequest->first_name,
                'last_name' =>$oRequest->last_name,
                'user_id' => $oUser->id,
                'email' => $oRequest->username,
                'currency' =>$oCurrencyCode->code
            ]);
            Mail::to($customer->email)->send(new SendMailable($oRequest->first_name.' '.$oRequest->last_name,$sPassword));

            Session::flash('message', trans('messages.success_inserted'));
            
            return Redirect::route('user.list',[ 'sUserType' => 'customer'  ])->with( [
                'message' => 'success', 
                'user_id' => $oUser->id, 
                'agent_name' => $oUser->name 
                ]);
        }
        return \View::make('WebView::user.user_create_customer', compact('oCurrency'));
    }
    
    public function callCreateUserProduct(UserRequest $oRequest,$user_id='')
    {
        session(['page_name' => 'product']);
        if($user_id != '')
        $users = User::find($user_id);
        if ($oRequest->isMethod('post'))
    	{              
            $aDatauser = $oRequest->except('_token');
            $user_id = $oRequest->user_id ?:'';
            $aDatauser['name']=$oRequest->name; 
            $aDatauser['username']=$oRequest->username; 
            $aDatauser['password']=Hash::make($oRequest->password);
            $aDatauser['type']=config('constants.USERTYPEPRODUCT'); 
            $aDatauser['active']=1; 
            $user= User::updateOrCreate(['id' => $user_id], $aDatauser);  
            Mail::to($oRequest->username)->send(new SendMailable($oRequest->username,$oRequest->name,$oRequest->password));            
            $productData['user_id']=$user->id;
            $productData['name']=$oRequest->name; 
            $productData['status']=1;
            Product::updateOrCreate(['user_id' => $user_id], $productData);
             
            if($user_id != '')
            \Session::flash('message', trans('messages.success_updated'));
            else
            \Session::flash('message', trans('messages.success_inserted'));
            return Redirect::route('user.list',[ 'sUserType' => 'product' ])->with( [
                'message' => 'success'               
                ]);
        }
       // $users = User::find($user_id);      
        return \View::make('WebView::user.user_create_product',compact('users','user_id'));
    }

    public function callCreateUserBrand(UserRequest $oRequest,$user_id='')
    {
        session(['page_name' => 'brand']);
        if($user_id != '')
        $users = User::find($user_id);
        if ($oRequest->isMethod('post'))
    	{
            $aDatauser = $oRequest->except('_token');
            $user_id = $oRequest->user_id ?:'';
            $aDatauser['name']=$oRequest->name; 
            $aDatauser['username']=$oRequest->username; 
            $aDatauser['password']=Hash::make($oRequest->password);
            $aDatauser['type']=config('constants.USERTYPEBRAND'); 
            $aDatauser['active']=1; 
            $user= User::updateOrCreate(['id' => $user_id], $aDatauser);  
            Mail::to($oRequest->username)->send(new SendMailable($oRequest->username,$oRequest->name,$oRequest->password));            
            $brandData['user_id']=$user->id;
            $brandData['name']=$oRequest->name; 
            $brandData['status']=1;
            Brand::updateOrCreate(['user_id' => $user_id], $brandData);
             
            if($user_id != '')
            \Session::flash('message', trans('messages.success_updated'));
            else
            \Session::flash('message', trans('messages.success_inserted'));
            return Redirect::route('user.list',[ 'sUserType' => 'brand' ])->with( [
                'message' => 'success'               
                ]);     

        }
        return \View::make('WebView::user.user_create_brand',compact('users','user_id'));
    }

    public function check_customer()
    {
        
        $input = Input::only('email', 'password','logo');
        $data = User::where('username', $input['email'])->first();  
        
        if($data)
        {
            if($data->social_type != '')
            {
                $code = [
                'id' => Crypt::encrypt($data->id)          
                ];

                return \Response::json([
                    'successful' => 1,
                    'code'       => $code,
                ]); 
            }
            $error = array(
                'email' => 'Email Already Exists'
                );
            return \Response::json([
                'successful' => 0, 'message' => $error, 'data' => $data
            ]); 
        }
        else
        {
            return \Response::json([
                'successful' => true,
                'data' => $data
            ]); 
        }
    }

    public function customer_registeration() 
    { 
		$input = Input::only('first_name','last_name','social','email','social_id','image_path', 'password','url','logo');
        $check_user = User::where('username', $input['email'])->first();
        if($check_user)
        {
            $code = [
                'id' => Crypt::encrypt($check_user->id)    
            ];

            return \Response::json([
                    'successful' => true,
                    'code'       => $code,
                ]); 
        }
        // INSERT TO USERS
        $return_user = [];
        if(isset($input['social']) && $input['social'] != '')
        {           
            $user = User::Create([
                'username'  => $input['email'],             
                'name'      => $input['first_name']." ".$input['last_name'],
                'password'  => '',              
                'type'      => 'customer',
                'active'    => 1,
                'social_type'=> $input['social'],
                'social_id' => $input['social_id'],
                'created_at'=> date("Y-m-d H:i:s")
            ]);

            $customer = Customer::Create([
                'first_name'    => $input['first_name'],
                'last_name'     => $input['last_name'],
                'user_id'       => $user->id,
                'is_confirmed'  => 1,
                'email'         => $input['email'],
                'currency'      => '',
            ]);
            $return_user = User::with('customer')->find($user->id); 
        }
        else
        {
            $user = User::Create([
                'username' => $input['email'],
                'password' => Hash::make($input['password']),
                'name' => '',
                'type' => 'customer',
                'active' => 0,
                'created_at' => date("Y-m-d H:i:s")
            ]);

            $customer = Customer::Create([
                'first_name' => '',
                'last_name' => '',
                'user_id' => $user->id,
                'is_confirmed' => 0,
                'email' => $user->username,
                'currency' => '',
            ]);
            $return_user = User::with('customer')->find($user->id); 
        }
        // INSERT TO CUSTOMERS
        

        if ($customer) {
            if(!isset($input['social']))
            {
                $id = Crypt::encrypt($customer->user_id);

               Mail::send('WebView::customer.registration-email', [
               'first_name' => 'User',
               'link'       => $input['url'].'/confirmation/'.$id,
			   'logo' =>$input['logo'],
			   ], function($message) use($input) {
                   $message->to($input['email'])->subject('Welcome to Eroam!');
               });
            }
            $user     = User::where(['username' => $input['email']])->first();
            
            $id = Crypt::encrypt($user->id);

            $code = [
                'id' => $id          
            ];

            
            return \Response::json([
                    'successful' => true,
                    'user'       => $return_user,
                    'code'       => $code
                ]); 
        }
        else
        {
            return \Response::json([
                    'successful' => false,
                    'message' => 'Something went wrong.'
                ]);
        }
    }

    public function update_user_profile_step1()
    {   

        // Input::merge(['first_name' => 'chintan']);
        // Input::merge(['last_name' => 'chintan']);
        // Input::merge(['currency' => 'AUD']);
        // Input::merge(['contact_no' => '9998004908']);
        // Input::merge(['title' => 'mrs']);
        // Input::merge(['pref_gender' => 'other']);
        // Input::merge(['pref_age_group_id' => 4]);
        // Input::merge(['pref_nationality_id' => 86]);
        // Input::merge(['old_password' => '']);
        // Input::merge(['user_id' => 1141]);
        // Input::merge(['new_password' => '']);

        // $input = Input::only('first_name', 'last_name', 'currency', 'contact_no', 'title', 'pref_gender', 'pref_age_group_id', 'pref_nationality_id','old_password','user_id','new_password');
        
        $input = Input::all();

        $get_customer = User::where('id', $input['user_id'])->first();
        
        if(isset($input['old_password']) && $input['old_password'] != '')
        {           
            if(!(Hash::check(Input::get('old_password'), $get_customer->password)))
            {
                $error = array(
                    'old_password' => 'Existing password not matching',
                    );
                
                return \Response::json([
                    'successful' => false, 'message' => $error,'adasd'=>'asdad'
                ]); 
            }
        }

        if(isset($input['new_password']) && $input['new_password'] != '')
        {
            
            $new_password = Hash::make($input['new_password']);

            $data_user = array(
                            'password' => $new_password,
                            );
            $customer = User::where('id', $input['user_id'])->update($data_user);
        }

       
        $data = array(
                'first_name'     => (isset($input['first_name']) ? $input['first_name'] : ''),
                'last_name'     => (isset($input['last_name']) ? $input['last_name'] : ''),
                'currency'     => (isset($input['currency']) ? $input['currency'] : ''),
                'contact_no'     => (isset($input['contact_no']) ? $input['contact_no'] : ''),
                'title'     => (isset($input['title']) ? $input['title'] : ''),
                'pref_gender'     => (isset($input['pref_gender']) ? $input['pref_gender'] : ''),
                'pref_age_group_id'     => (isset($input['pref_age_group_id']) ? $input['pref_age_group_id'] : ''),
                'pref_nationality_id'     => (isset($input['pref_nationality_id']) ? $input['pref_nationality_id'] : ''),
                'step_1'                => 1
            );
        $customer = Customer::where('user_id', $input['user_id'])->update($data);
         

        if($customer)
        {

            return \Response::json(['successful' => true, 'message' => "Your profile updated successfully!"]);
        }
    }

    public function update_user_profile_step2()
    {
        // $input = Input::only('phy_address_1', 'phy_address_2', 'phy_state', 'phy_city', 'phy_zip', 'phy_country', 'bill_address_1', 'bill_address_2', 'bill_state','bill_city','bill_zip','bill_country','user_id');

       

        $input = Input::all();

        $data = array(
                'phy_address_1'     => (isset($input['phy_address_1']) ? $input['phy_address_1'] : ''),
                'phy_address_2'     => (isset($input['phy_address_2']) ? $input['phy_address_2'] : ''),
                'phy_state'     => (isset($input['phy_state']) ? $input['phy_state'] : ''),
                'phy_city'     => (isset($input['phy_city']) ? $input['phy_city'] : ''),
                'phy_zip'     => (isset($input['phy_zip']) ? $input['phy_zip'] : ''),
                'phy_country'     => (isset($input['phy_country']) ? $input['phy_country'] : ''),
                'bill_address_1'     => (isset($input['bill_address_1']) ? $input['bill_address_1'] : ''),
                'bill_address_2'     => (isset($input['bill_address_2']) ? $input['bill_address_2'] : ''),
                'bill_state'     => (isset($input['bill_state']) ? $input['bill_state'] : ''),
                'bill_city'     => (isset($input['bill_city']) ? $input['bill_city'] : ''),
                'bill_zip'     => (isset($input['bill_zip']) ? $input['bill_zip'] : ''),
                'bill_country'     => (isset($input['bill_country']) ? $input['bill_country'] : ''),
                'step_2' 			=> 1
            );
        $customer = Customer::where('user_id', $input['user_id'])->update($data);
        if($customer)
        {
              return \Response::json(['successful' => true, 'message' => "Your profile updated successfully!"]);
        }
    }

    public function update_user_profile_step3()
    {
        
        $input      = Input::only('user_id');

        $id = Crypt::decrypt($input['user_id']);

        $user_id    = $id;
        $data       = array(
            'step_3' => 1
        );

        $customer = Customer::where('user_id', $user_id)->update($data);

        if($customer)
        {
            return \Response::json(['successful' => true, 'message' => "Your profile updated successfully!"]);
        }
    }

    public function update_user_profile_step4()
    {
        // $input = Input::only('phy_address_1', 'phy_address_2', 'phy_state', 'phy_city', 'phy_zip', 'phy_country', 'contact_no', 'mobile_no','pref_contact_method','user_id');
        $input = Input::all();
        $data = array(
                'phy_address_1'     => (isset($input['phy_address_1']) ? $input['phy_address_1'] : ''),
                'phy_address_2'     => (isset($input['phy_address_2']) ? $input['phy_address_2'] : ''),
                'phy_state'     => (isset($input['phy_state']) ? $input['phy_state'] : ''),
                'phy_city'     => (isset($input['phy_city']) ? $input['phy_city'] : ''),
                'phy_zip'     => (isset($input['phy_zip']) ? $input['phy_zip'] : ''),
                'phy_country'     => (isset($input['phy_country']) ? $input['phy_country'] : ''),
                'contact_no'     => (isset($input['contact_no']) ? $input['contact_no'] : ''),
                'mobile_no'     => (isset($input['mobile_no']) ? $input['mobile_no'] : ''),
                'pref_contact_method'     => (isset($input['pref_contact_method']) ? $input['pref_contact_method'] : ''),
               'step_4'            => 1
            );
        $customer = Customer::where('user_id', $input['user_id'])->update($data);

        if($customer)
        {
            return \Response::json([
                'successful' => true, 'message' => "Your profile updated successfully!"
                ]);
        }
    }
    
    public function user_get_by_id($id) 
    {
        $id = Crypt::decrypt($id);
        $user = User::with('customer')->find($id);
        return $user;
    }

    public function update_user_preferences() 
    {
        $customer = Customer::where(['user_id' => Input::get('user_id')])->first();
        if ($customer) {
            $customer->pref_hotel_categories = Input::has('hotel_categories') ? implode(',', Input::get('hotel_categories')) : null;
            $customer->pref_hotel_room_types = Input::has('hotel_room_types') ? implode(',', Input::get('hotel_room_types')) : null;
            $customer->pref_transport_types = Input::has('transport_types') ? implode(',', Input::get('transport_types')) : null;
            $customer->pref_nationality_id = Input::get('nationality');
            $customer->pref_age_group_id = Input::get('age_group');
            $customer->pref_gender = Input::get('gender');
            $customer->pref_cabin_class = Input::get('cabin_class');
            $customer->interests = Input::has('interests') ? implode(',', Input::get('interests')) : null;
            $customer->save();
        }
    }

    public function get_user_itinerary() 
    {
        $reference_no = Input::has('reference_no') ? Input::get('reference_no') : 0;
        return Itinerary::where(['reference_no' => $reference_no])->first();
    }
    
    public function request_change_password()
    {

        $email = Input::get('email');
        $url = Input::get('url');
        $logo = Input::get('logo');
        $result = User::where(['username' => $email ])->first();
		if($result){

            $first_name = 'eRoam';
            $last_name  = 'User';
            if($result->name != '')
            {
                $name = explode(' ', $result->name);
                $first_name = ucfirst($name[0]);
                $last_name = ucfirst($name[1]);
            }
        
            $today = date('Y-m-d h:i:s A');
            $status = PasswordReset::where(['email' => $email, 'expired' => 0 ])->where('expired_date', '>', $today)->first();

            if($status){
                $token = $status->token;
            }else{
                $token = PasswordReset::store_token();
                $date = date('Y-m-d h:i:s A', strtotime('+24 hours') );

                PasswordReset::create([
                        'email' => $email,
                        'token' => $token,
                        'expired_date' => $date,
                        'expired' => 0
                ]); 
            }

            $data = [
                'link' => $url.$token,
                'logo' => $logo,
                'name' => ucwords($first_name.' '. $last_name)
            ];
			Mail::send('WebView::emails.forgot-password', $data, function($message) use($email, $first_name, $last_name) {
               $message->to($email, $first_name.' '.$last_name)->subject('Eroam - Password Reset Instructions.');
               $message->from(self::EROAM_EMAIL, 'Eroam');
			});

            return \Response::json([
                'response_status' => true,
                'message' => 'The password reset link has been sent to your email.'
            ]);
        }else{
            return \Response::json([
                'response_status' => false,
                'message' => 'Email Address doesn\'t exist'
            ]);
        }
    }
    
    public function check_token()
    {
        $token = Input::get('token');
        $today = date('Y-m-d h:i:s A');
        $result = PasswordReset::where(['token' => $token, 'expired' => 0 ])->where('expired_date', '>', $today)->first();

        if($result){
            $user = User::where(['username' => $result->email, 'type' => 'customer' ])->first();
            if($user){
                return \Response::json([
                    'response_status' => true,
                    'code' => Crypt::encrypt($user->id)
                ]);
            }else{
                return \Response::json([
                    'response_status' => false,
                    'expire' => false,
                ]);
            }
        }else{
            return \Response::json([
                'response_status' => false,
                'expire' => true,
            ]);
        }
    }
    
    public function reset_user_password_from_app()
    {
        $token = Input::get('token');

        $id = Crypt::decrypt(Input::get('id'));

        $user = User::find($id);
        $password = Input::get('password');

        $user->password = Hash::make($password);
        $user->save();

        PasswordReset::where(['token' => $token])->update( ['expired' => 1] );

        return \Response::json([
            'response_status' => true
        ]);
    }

    public function confirm_registration() 
    {
        // Updated by RGR

        $user_id = Crypt::decrypt(Input::get('id'));
         
        $customer = Customer::where(['user_id' => $user_id])->first();
        
        if ($customer->is_confirmed == 1)
        {
            $error = array('message'=>'Account is already confirmed. Login instead.');          
            return \Response::json([
                'result'  => true,
                'message' => 'Account is already confirmed. Login instead.'
                ]);
        }
        elseif ($customer->is_confirmed == 0)
        {

            $customer->update(array('is_confirmed' => 1));
            // Update user active field to 1
            User::where(['id' => $user_id])->update(['active' => 1]);

            return \Response::json([
                'result'    => true,
                'type'      => "customer",
                'message'   => "Your account confirmed successful."
                ]);
            
        }
        else
        {
            $error = array('message' => 'Invalid parameters.');
            return \Response::json([
                'result' => false
                ]);
        }
    }
    
    public function get_user_itineraries() 
    {
        return Itinerary::where(['customer_id' => Input::get('customer_id')])->orderBy('id', 'desc')->get();
    }

    public function get_user_trips($user_id)
    {
        $user_id = Crypt::decrypt($user_id);

        $itenary_orders = ItenaryOrder::where('user_id',$user_id)
                        ->orderBy('order_id','desc')
                        ->get();
        
        $iOrder = [];
        $i = 0;
        foreach ($itenary_orders as $order) {
            $passenger_information = PassengerInformation::where('itenary_order_id',$order->order_id)->get();
            $iOrder[$i] = $order;
            $iOrder[$i]['passenger_information'] = $passenger_information;
            $i++;
        }
        return \Response::json([
                'successful' => true, 'response' => $iOrder
                ]);
    }

    public function user_check_login()
    {
       
        $response = 'invalid';
        $code = [];
        $user_id  = 0;
        $type     = null;
        $customer = null;
        $user     = User::where(['username' => Input::get('username')])->with('customer')->first();

        //if(isset($user) && $user->active == 0) comment as said by kane ticket no ROAM-2747
        if(isset($user) && empty($user))
        {
            $data = ['successful' => false ,'status' => 'confirm_first'];
            return \Response::json(['d' => $data]);
        }
        else
        {
            if (($user && Hash::check(Input::get('password'), $user->password) && $user->type != 'admin' && $user->type != 'licensee') || ($user && !input::has('password') && $user->customer->fb_login == 1)) {
                // exists
                $response = 'valid';
                $code = [
                    'id' => Crypt::encrypt($user->id),
                    'user_id' => $user->id,
                    'name' => $user->name,
                    'image_url' => isset( $user->customer->image_url ) ? $user->customer->image_url : ''
                ];
            }

            $data = ['status' => $response, 'code' => $code];

            return \Response::json(['d' => $data]);
        }
    }

    public function userSetting(Request $oRequest)
    {
        session(['page_name' => 'setting']);
        $get_user = User::where('id', Auth::user()->id)->first();
        if ($oRequest->isMethod('post'))
    	{
            $oValidator = Validator::make($oRequest->all(), [
                                    'old_password' => 'required|min:6',
                                    'password'  =>'required',                                 
                                    ]);
            if($oValidator->fails()) 
            {             
                return Redirect::back()->withErrors($oValidator);
            }
                   
            if(isset($input['old_password']) && $input['old_password'] != '')
            {           
                if(!(Hash::check(Input::get('old_password'), $get_user->password)))
                {  
                        Session::flash('message', trans('messages.old_password_error'));
                        return Redirect::back();                       
                }
            }
            if(isset($input['password']) && $input['password'] != '')
            {

                $new_password = Hash::make($input['password']);

                $data_user = array(
                    'password' => $password,
                    );
                User::where('id', $get_user->id)->update($data_user);
                Session::flash('message', trans('messages.password_changes_msg'));
                return Redirect::back();    
            }
           
          
        }

        return \View::make('WebView::user.user_setting');
    }
 
  public function test()
  {
    $get_customer = User::where('id', $input['user_id'])->first();
        
    if(isset($input['old_password']) && $input['old_password'] != '')
    {           
        if(!(Hash::check(Input::get('old_password'), $get_customer->password)))
        {
            $error = array(
                'old_password' => 'Existing password not matching',
                );
            
            return \Response::json([
                'successful' => false, 'message' => $error,'adasd'=>'asdad'
            ]); 
        }
    }

    if(isset($input['new_password']) && $input['new_password'] != '')
    {
        
        $new_password = Hash::make($input['new_password']);

        $data_user = array(
                        'password' => $new_password,
                        );
        $customer = User::where('id', $input['user_id'])->update($data_user);
    }
  }

  public function callUserActive($userId) 
    {        
        $user = User::find($userId);
        $is_active=$user->active;
        if(($is_active)=='1')
        {  
            \DB::table('users')
            ->where('id', $userId)
            ->update(['active' => "0"]);     
        }
        else
        {     
            \DB::table('users')
            ->where('id', $userId)
            ->update(['active' => "1"]);         
        }
        return 1;   
        
    }
}
