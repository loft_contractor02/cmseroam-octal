<?php
//priya
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

use App\Country;
use App\PassengerInformation;
use App\User;
use App\Customer;
use App\HotelCategory;
use App\ItenaryOrder;
use App\BookingRequest;

use Session;
use File;
use Image;
use DB,Auth,View;
use Mail;

class AgentController  extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $oRequest)  
    {     
       //remove session when it comes from sidebar sOrderBy
        if(session('page_name') != 'agent_users' || $oRequest->query('isreset') == 1)
        $oRequest->session()->forget('agent_users');
        session(['page_name' => 'agent_users']);
        $aData = session('agent_users') ? session('agent_users') : array();
        $oRequest->session()->forget('agent_users');
        
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : NULL);

        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'name');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'asc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        
        if(count($aData) && $sSearchStr != $aData['search_str'])
            $nPage = 1;
        
        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });

        $user_id = Auth::user()->id;
        if(session()->has('new_agent')) {
            $user_id = session()->get('new_agent')['id'];
        }
        $licensee_id = Auth::user()->licensee_id;
        $domain_id = session()->get('domain_id');
      
        $oUserList = User::getAgentUserList($sSearchStr,$sOrderField,$sOrderBy,$nShowRecord,$licensee_id,$domain_id);
        setSession($sSearchStr,'',$sOrderField,$sOrderBy,$nShowRecord,$oUserList->currentPage(),'agent_users');
        if($oRequest->page > 1)
            $oViewName =  'WebView::agent._user_list_ajax';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::agent.user_list' : 'WebView::agent._user_list_ajax';
        
        return \View::make($oViewName, compact('oUserList','sSearchStr','sOrderField','sOrderBy','nShowRecord'));  
    }
    public function paxDetails($user_id=null){
        $passengerList = PassengerInformation::where('itenary_order_id',DB::raw("(SELECT order_id FROM itenaryorders WHERE itenaryorders.user_id =$user_id LIMIT 1)")) 
                         ->select('passengerinformations.*',DB::raw('(SELECT name FROM zcountries WHERE zcountries.id =passengerinformations.country) as nationality'))
                         ->first();

        $userdetails = User::where('id',$user_id)->get();
        $personalPreferences = Customer::where('user_id',$user_id)->first();
        $acoomodationsType = HotelCategory::where('id',$personalPreferences->pref_hotel_categories)->first();
        $roomType = \App\HotelRoomType::where('id',$personalPreferences->pref_hotel_room_types)->first();
        $transportType = \App\TransportType::where('id',$personalPreferences->pref_transport_types)->first();
        $pendingItenaryHistory =\App\ItenaryOrder::where('user_id',$user_id)->where('status','Pending')->first();
        $itenaryHistory =\App\ItenaryOrder::where('user_id',$user_id)->where('status','Archived')->first();
        $oViewName =  'WebView::agent.view';
        return \View::make($oViewName, compact('userdetails','passengerList','personalPreferences','acoomodationsType','roomType','transportType','itenaryHistory','pendingItenaryHistory','user_id'));
    }
    
    public function itenaryDetails($nBookId)
    {    
        $page_name = 'itenary_details';
        $passanger = \App\PassengerInformation::where('itenary_order_id', $nBookId)->get();
        $total_travller = count($passanger);
        $types = array('hotel', 'activities', 'transport');
        $city = \App\ItenaryLeg::from('itenarylegs')
                ->select(
                        'itenarylegs.from_city_name','itenarylegs.country_code'
                )
                ->where('itenarylegs.itenary_order_id', $nBookId)
                ->groupBy('itenarylegs.itenary_leg_id')
                ->get();

        $allcities = $city->toArray();
        $cities_arr = array_column($allcities, 'from_city_name');
        $cities_codes = array_column($allcities, 'country_code');
        $oItinerary = \App\LegDetail::from('legdetails')
                ->leftjoin('itenarylegs', 'itenarylegs.itenary_leg_id', '=', 'legdetails.itenary_leg_id')
                ->leftjoin('zhotel', 'zhotel.id', '=', 'legdetails.hotel_id')
                ->select(
                        'legdetails.*', 'itenarylegs.from_city_name as from_city', 'itenarylegs.to_city_name as to_city', 'itenarylegs.from_date as city_from_date', 'itenarylegs.to_date as city_to_date', 'itenarylegs.itenary_leg_id as leg_detail_ids', 'itenarylegs.itenary_order_id as leg_detail_order_id','itenarylegs.country_code as country_code','zhotel.checkin_time as checkin_time','zhotel.checkout_time as checkout_time','zhotel.cancellation_policy as cancellation_policy'
                )
                ->where('legdetails.itenary_order_id', $nBookId)
                ->get();

        $personalDetails = \App\ItenaryOrder::where('itenary_order_id',$nBookId)
                            ->leftjoin('passengerinformations', 'passengerinformations.itenary_order_id', '=', 'itenaryorders.order_id')
                            ->select('itenaryorders.*','passengerinformations.*')
                            ->first();
        foreach ($cities_arr as $key => $val) {
            foreach ($oItinerary as $details) {
                foreach ($types as $type) {
                    if ($details['from_city'] == $val && $details['leg_type'] == $type) {
                        $product_details['leg_detail_id'] = $details['leg_detail_id'];
                        $product_details['leg_detail_ids'] = $details['leg_detail_ids'];
                        $product_details['itenary_leg_id'] = $details['itenary_leg_id'];
                        $product_details['leg_detail_order_id'] = $details['leg_detail_order_id'];
                        $product_details['city_to_date'] = $details['city_to_date'];
                        $product_details['city_from_date'] = $details['city_from_date'];
                        $product_details['from_city'] = $details['from_city'];
                        $product_details['to_city'] = $details['to_city'];
                        $product_details['from_city'] = $details['from_city'];
                        $product_details['reseller_cost'] = $details['reseller_cost'];
                        $product_details['licensee_benefit'] = $details['licensee_benefit'];
                        $product_details['sellprice'] = $details['sellprice'];
                        $product_details['booking_id'] = $details['booking_id'];
                        $product_details['leg_name'] = $details['leg_name'];
                        $product_details['country_code'] = $details['country_code'];
                        $product_details['leg_type'] = $details['leg_type'];
                        $product_details['nights'] = $details['nights'];
                        $product_details['price'] = $details['price'];
                        $product_details['hotel_room_type_name'] = $details['hotel_room_type_name'];
                        $product_details['provider_booking_status'] = $details['provider_booking_status'];
                        $product_details['notes'] = $details['notes'];
                        $product_details['supplier_id'] = $details['supplier_id'];
                        $product_details['address'] = $details['address'];
                        $product_details['provider'] = $details['provider']; 
                        $product_details['currency'] = $details['currency']; 
                        $product_details['voucher_url'] = $details['voucher_url'];  
                        $product_details['departure_text'] = $details['departure_text'];   
                        $product_details['arrival_text'] = $details['arrival_text'];   
                        $product_details['booking_summary_text'] = $details['booking_summary_text'];   
                        $product_details_all[$val][$type] = $product_details;  
                    }
                }
            }
        }
        return View::make('WebView::booking.itenary_details', compact('page_name', 'nBookId', 'oItinerary', 'total_travller', 'product_details_all', 'cities_arr', 'types','cities_codes','personalDetails'));
    }
    public function getItenaryUpdateView(){
        $itenary_order_id = Input::get('lagDetailsId');
        $buttonType = Input::get('buttonType');
        return View::make('WebView::booking.update_revised_itenary', compact('itenary_order_id','buttonType'));
    }
    public function updateRevisedItenary(){
        $itenary_order_id = Input::get('lagDetailsId');
        $obj =   \App\LegDetail::find($itenary_order_id); 
        $obj->notes  =  Input::get('notes');
        $obj->save();

        echo $obj->notes;die;
        /*$response   =   array(
                    'success'   =>  $recordId,
                    'errors'    =>  trans($itemType." added successfully")
                );
        return  Response::json($response); 
        die;*/

    }
    public function revisedItenaryEmail($nBookId){
        $passanger = \App\PassengerInformation::where('itenary_order_id', $nBookId)->get();
        $total_travller = count($passanger);
        $types = array('hotel', 'activities', 'transport');
        $city = \App\ItenaryLeg::from('itenarylegs')
                ->select(
                        'itenarylegs.from_city_name','itenarylegs.country_code'
                )
                ->where('itenarylegs.itenary_order_id', $nBookId)
                ->groupBy('itenarylegs.itenary_leg_id')
                ->get();

        $allcities = $city->toArray();
        $cities_arr = array_column($allcities, 'from_city_name');
        $cities_codes = array_column($allcities, 'country_code');
        $oItinerary = \App\LegDetail::from('legdetails')
                ->leftjoin('itenarylegs', 'itenarylegs.itenary_leg_id', '=', 'legdetails.itenary_leg_id')
                ->leftjoin('zhotel', 'zhotel.id', '=', 'legdetails.hotel_id')
                ->select(
                        'legdetails.*', 'itenarylegs.from_city_name as from_city', 'itenarylegs.to_city_name as to_city', 'itenarylegs.from_date as city_from_date', 'itenarylegs.to_date as city_to_date', 'itenarylegs.itenary_leg_id as leg_detail_ids', 'itenarylegs.itenary_order_id as leg_detail_order_id','itenarylegs.country_code as country_code','zhotel.checkin_time as checkin_time','zhotel.checkout_time as checkout_time','zhotel.cancellation_policy as cancellation_policy'
                )
                ->where('legdetails.itenary_order_id', $nBookId)
                ->get();

        $personalDetails = \App\ItenaryOrder::where('itenary_order_id',$nBookId)
                            ->leftjoin('passengerinformations', 'passengerinformations.itenary_order_id', '=', 'itenaryorders.order_id')
                            ->select('itenaryorders.*','passengerinformations.*')
                            ->first();
        //$itineraryId[$data['leg'][$i]['from_city_name']] = $return_data['itineraryId'];
        $itineraryId = array();
        foreach ($cities_arr as $key => $val) {
            foreach ($oItinerary as $details) {
                foreach ($types as $key1 =>$type) {
                    if ($details['from_city'] == $val && $details['leg_type'] == $type) {
                        $product_details['leg_detail_id'] = $details['leg_detail_id'];
                        $product_details['leg_detail_ids'] = $details['leg_detail_ids'];
                        $product_details['itenary_leg_id'] = $details['itenary_leg_id'];
                        $product_details['leg_detail_order_id'] = $details['leg_detail_order_id'];
                        $product_details['city_to_date'] = $details['city_to_date'];
                        $product_details['city_from_date'] = $details['city_from_date'];
                        $product_details['from_city'] = $details['from_city'];
                        $product_details['to_city'] = $details['to_city'];
                        $product_details['from_city'] = $details['from_city'];
                        $product_details['reseller_cost'] = $details['reseller_cost'];
                        $product_details['licensee_benefit'] = $details['licensee_benefit'];
                        $product_details['sellprice'] = $details['sellprice'];
                        $product_details['booking_id'] = $details['booking_id'];
                        $product_details['leg_name'] = $details['leg_name'];
                        $product_details['country_code'] = $details['country_code'];
                        $product_details['leg_type'] = $details['leg_type'];
                        $product_details['nights'] = $details['nights'];
                        $product_details['price'] = $details['price'];
                        $product_details['hotel_room_type_name'] = $details['hotel_room_type_name'];
                        $product_details['provider_booking_status'] = $details['provider_booking_status'];
                        $product_details['notes'] = $details['notes'];
                        $product_details['supplier_id'] = $details['supplier_id']; 
                        $product_details['address'] = $details['address']; 
                        $product_details['provider'] = $details['provider']; 
                        $product_details['currency'] = $details['currency']; 
                        $product_details['checkin_time'] = $details['checkin_time']; 
                        $product_details['checkout_time'] = $details['checkout_time']; 
                        $product_details['cancellation_policy'] = $details['cancellation_policy']; 
                        $product_details['voucher_url'] = $details['voucher_url'];  
                        $product_details['departure_text'] = $details['departure_text'];   
                        $product_details['arrival_text'] = $details['arrival_text'];    
                        $product_details['booking_id'] = $details['booking_id'];    
                        $product_details['booking_summary_text'] = $details['booking_summary_text'];   
                        $product_details_all[$val][$type] = $product_details; 
                        if($details['booking_id']){
                            $itineraryId[$details['from_city']] = !empty($details['booking_id']) ? $details['booking_id'] :'';
                        } 
                        
                    } 
                }
            }
        } 
        $data = ['leg' => $product_details_all,'personalDetails' => $personalDetails,'cities_codes'=>$cities_codes,'cities_arr'=>$cities_arr];
        session()->put('orderDataItenary', $data);
        //return View::make('mail.book', compact('data'));
        $invoiceNumber = $personalDetails->invoice_no;
        $customerEmail = "ankit.pandey@octalinfosolution.com";//$personalDetails->email;        
        Mail::send(['html' => 'mail.book'], $data, function ($message) use ($customerEmail,$itineraryId,$invoiceNumber,$data) {   
            $message->to($customerEmail)->subject('Booking summary for Invoice number - ' . $invoiceNumber);
            $message->from('res@eroam.com','eRoam');
            if(isset($itineraryId) && count($itineraryId)){
                foreach ($itineraryId as $key => $value) {
                    if(isset($value) && $value != ''){ 
                       $transportFilePath = 'vouchers/transport/'.$key.'_TransportVoucher_'.$value.'.pdf';
                       $activityFilePath = 'vouchers/activity/'.$key.'_ActivityVoucher_'.$value.'.pdf';
                       $hotelFilePath = 'vouchers/hotels/'.$key.'_HotelVoucher_'.$value.'.pdf';

                       // $$transportPdfExists = \Storage::disk('s3')->exists( $transportFilePath );
                        if(\Storage::disk('s3')->exists( $transportFilePath )){
                                $transportFile = \Storage::disk('s3')->get($transportFilePath); 
                        }if(\Storage::disk('s3')->exists( $activityFilePath )){
                                $activityFile = \Storage::disk('s3')->get($activityFilePath); 
                        }if(\Storage::disk('s3')->exists( $hotelFilePath )){
                                $hotelFile = \Storage::disk('s3')->get($hotelFilePath); 
                        }            

                        if(!empty($transportFile))
                        {
                            $message->attachData($transportFile, $key.'_TransportVoucher_'.$value.'.pdf');
                        }if(!empty($activityFile))
                        {
                            $message->attachData($activityFile, $key.'_ActivityVoucher_'.$value.'.pdf');
                        }if(!empty($hotelFile))
                        {
                            $message->attachData($hotelFile, $key.'_HotelVoucher_'.$value.'.pdf');
                        }
                    }
                }
            }
        });
        return back()->with('success', 'Itenary send successfully.');
    }
}