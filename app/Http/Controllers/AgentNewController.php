<?php
//priya
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use Validator;
use App\UserDomain;
use App\User;
use App\PassengerInformation;
use App\Customer;

class AgentNewController  extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
    
    public function settings() {
        session()->put('page_name','settings');
    	$user = Auth::user();
    	$user_domains = UserDomain::where('user_id',$user->id)->with('domains')->get();
    	return view('WebView::agent.settings',compact('user','user_domains'));
    }

    public function addSettings(Request $request) {
        $rules = [
            'title' => 'required',
            'name' => 'required',
            'fname' => 'required',
            'gender' => 'required',
            'contact_no' => 'required',
            'email' => 'required|email',
            'consultant_id' => 'required',
            'password' => 'confirmed'
        ];  

        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()) {
            return ['result' => 0,'errors' =>$validator->messages()];
        }

    	$user_id = Auth::user()->id;
        
            
        $fields = [
            'title' => $request->title,
            'name' => $request->name.' '.$request->fname,
            'gender' => $request->gender,
            'contact_no' => $request->contact_no,
            'username' => $request->email,
            'consultant_id' => $request->consultant_id
        ];

        if($request->password) {
            $fields['password'] = bcrypt($request->password);
        }

        User::where('id',$user_id)->update($fields);
        return ['result'=> 1,'msg'=> 'Settings updated Successfully'];
    }

    public function locationDetails($order_id){   
        $page_name = 'location_details';

        $nBookId = $order_id;                 

        $trip_details = $this->getItineraryDetail($order_id);        
        
        foreach ($trip_details['itinerary_leg'] as $key => $leg) {
            $datetime1 = new \DateTime($leg['from_date']);
            $datetime2 = new \DateTime($leg['to_date']);
            $difference = $datetime1->diff($datetime2);
            $nights = $difference->d;
            $trip_details['itinerary_leg'][$key]['nights'] = $nights;
            $trip_details['city_detail'][$key]['default_nights'] = $nights;
        }

        $searchitinerary = [];
        foreach ($trip_details['city_detail'] as $city) {
            $searchitinerary[]['city'] = $city;
        }

        $search_session = ['itinerary'=>$searchitinerary];

        $route = ['type'=>'auto','routes'=>[0=>[]],'auto_sort'=>'on'];
        $route['routes'][0]['default'] = 'yes'; 
        $route['routes'][0]['cities'] = $trip_details['city_detail']; 
        $cityArray = array();

        foreach($trip_details['city_detail'] as $city){
            $cityArray[] = $city['cityId'];
        }

        return \View('WebView::booking.location_detail')->with(compact('search_session','page_name','nBookId','oItinerary','trip_details','route','cityArray'));
    }

    public function getItineraryDetail($order_id) {
        $itinerary_order = \App\ItenaryOrder::where('order_id',$order_id)->first();
        $itinerary_leg = \App\ItenaryLeg::where('itenary_order_id',$order_id)->get();

        $response['itinerary_order'] = $itinerary_order;

        foreach ($itinerary_leg as $leg) {
            $city = \App\City::getCityDetail($leg['from_city_id']);
            $cityLatLong = \App\CitiesLatLong::where('city_id',$leg['from_city_id'])->first();

            if(!empty($city['description'])) {
                $leg['city_description'] = $city['description'];
            } else {
                $leg['city_description'] = '';
            }
            if(!empty($city['small'])) {
                $leg['city_image'] = 'https://cms.eroam.com/'.$city['small'];
            } else {
                $leg['city_image'] = 'http://dev.eroam.com/assets/images/no-image-2.jpg';
            }

            $response['itinerary_leg'][] = $leg;
                  
            $leg_detail = \App\LegDetail::where('itenary_order_id',$order_id)->where('itenary_leg_id',$leg['itenary_leg_id'])->get();
            $leg['leg_detail'] = $leg_detail;
            $city_detail = [];
            
            $city_detail = ['cityId'=>$cityLatLong['city_id'],'lat'=>$cityLatLong['lat'],'lng'=>$cityLatLong['lng'],'name'=> $city['name']];   
            $response['city_detail'][] = $city_detail;
        }

        return $response;
    }

    public function switchDomains(){
        session()->put('page_name','switchDomains');
        $user = Auth::user();
        $current_domain = session()->get('domain_id');
        $domains = UserDomain::where('user_id',$user->id)->with('domains')->get();
        return \View('WebView::agent.switch-domains')->with(compact('domains','current_domain'));
    }

    public function switchAgentDomain() {
        $domain_id = request()->domain_id;
        session()->put('domain_id',$domain_id);
        return ['success' => true];
    }
}