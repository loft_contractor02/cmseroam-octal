
@foreach ($oActivitySeasonList as $key => $aActivity)	
    
    <tr class="clickable <?php echo (($key == 0) ? 'open':'')?>" data-toggle="collapse" id="row-{{$aActivity->id}}" data-target=".row-{{$aActivity->id}}">
        <td><i class="icon-unfold-less"></i></td>
        <td>
            <a href="#">
                {{ $aActivity->name }}
            </a>    
        </td>
        <td>{{ $aActivity->city_name }}</td>
        <td>{{ $aActivity->operator_name }}</td>
        <td>-</td>  
        <td>-</td>
        <td>-</td>
        <td>-</td>  
        <td>-</td>
        <td>-</td>
        <td>-</td>
        <td class="text-center">-</td>
    </tr>
    @foreach($aActivity->price as $season) <?php #print_r($season->domain_ids);exit;?>
    <tr class="collapse row-{{$aActivity->id}}">
        <td>
            <label class="radio-checkbox label_check" for="checkbox-{{$season->id}}">
                <input type="checkbox" id="checkbox-{{$season->id}}" value="{{$season->id}}">&nbsp;
            </label>
        </td>
        <td>-</td>  
        <td>-</td>
        <td>-</td>
        <td><a href="{{ route('activity.activity-season-create',['nSeasonId'=>$season->id])}}">{{$season->name}}</a></td>
        <td>{{ ($season->date_from != '') ? date('d/m/y',strtotime($season->date_from)) : '-'}}</td>
        <td>{{ ($season->date_to != '') ? date('d/m/y',strtotime($season->date_to)) : '-'}}</td>        
        <td>{{$season->minimum_pax}}</td>
        <td>{{$season->allotment}}</td>
        <td>
            {{isset($season->currency) ? $season->currency->code : ''}} {{number_format(round($season->price, 2), 2, '.', ',')}}
        </td>
        {{-- <td>   --}}
            @php
                // $domain_array = [];
                // if($aActivity->domains){
                //     $domain_array = explode(',',$aActivity->domains);
                //     foreach($domain_array as $key1=>$value1){
                //         echo domianName($value1)->name.'<br/>';                         
                //     }
                // }
            @endphp
            {{-- </td> --}}
        <td>
            <?php if ($season->supplier): ?>
                {{ $season->supplier->name }}	
            <?php endif ?>
        </td>
        <td>
            <a href="{{ route('activity.activity-season-create',['nSeasonId'=>$season->id])}}" class="button success tiny btn-primary btn-sm pull-left m-r-10" style="margin-bottom:2px;">Update</a>
            <input type="button" class="button btn-delete tiny btn-primary btn-sm m-r-10" value="{{ trans('messages.delete_btn') }}" onclick="callDeleteRecord(this,'{{ route('activity.activity-season-delete',['nIdActivity'=> $season->id]) }}','{{ trans('messages.delete_label')}}')">
        </td>
    </tr>
    @endforeach
        
    </tr> 
@endforeach