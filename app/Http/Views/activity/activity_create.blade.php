@extends( 'layout/mainlayout' )

@section('content')
<?php 
    $noAddonOption  = array(1=>1,2,3,4,5,6);
    $random_activity_id =  (isset($nIdActivity) && $nIdActivity !='') ? $nIdActivity : 'rand_'.rand(10000, 99999);
?>
<div class="content-container">
        
    @if(isset($oActivity))
        <h1 class="page-title">{{ trans('messages.update',['name' => 'Activity']) }}</h1>
    @else
        <h1 class="page-title">{{ trans('messages.add',['name' => 'Activity']) }}</h1>
    @endif

    <div class="row">
        @if (Session::has('message'))
        <div class="small-12 small-centered columns success-box">{{ Session::get('message') }}</div>
        @endif
    </div>
    <br>
    <div class="box-wrapper">

        <p>Activity Details</p>
        <div class="m-t-20">
            <form enctype="multipart/form-data" id="image-form">
                <div class="file-upload1">
                    <input type="hidden" id="activityId" name="activityId" value="<?php echo $random_activity_id; ?>"/>
                    <input type="file" class="file-input" id="activityImage" name="activityImage"/>
                </div>
                <span class="input-filename"></span>
                <span id="image-submit-load" style="display:none;">
                    <i class="fa fa-circle-o-notch fa-spin fa-lg fa-fw image-file-loader"></i>
                </span>
                {{ csrf_field() }}
            </form>
        </div>
        <div class="m-t-20">
            <div class="table-responsive" id="panel-drag">
                <table class="table table-center-all">
                    <thead>
                        <tr>
                            <th>{{ trans('messages.re_order')}}</th>
                            <th>{{ trans('messages.preview')}}</th>
                            <!-- <th>{{ trans('messages.download')}}</th> -->
                            <th>{{ trans('messages.remove')}}</th>
                        </tr>
                    </thead>
                    <tbody class="sortable-list">
                        @if(isset($oActivityImages) && count($oActivityImages) > 0) 
                            <!-- loop through each image for this hotel -->                           
                            @foreach ($oActivityImages as $key => $image)
                            <?php
                                $image_id =  explode("_",$image->id);
                            ?>
                                <tr data-id="{{ $image->id }}" id="image_{{ $image->id }}">
                                    <td>
                                        <a href="#" class="sortable-icon"><i class="icon-reorder-black"></i></a></td>
                                    <td class="table-no-padding">
                                        <div class="table-image">
                                            <img src="{{ trans('messages.image_url',['image_title' => 'activities/'.$image->original]) }}" class="img-responsive">
                                        </div>
                                    </td>
                                    <!-- <td>
                                        <a href="{{ trans('messages.image_url',['image_title' => 'activities/'.$image->original]) }}" class="action-icon" download="{{ $image->original }} "><i class="icon-download"></i></a>
                                    </td> -->
                                    <td><a href="#" class="action-icon image-click-event image-delete" data-action="delete" data-id="{{ $image->id }}" id="{{ $image->id }}" data-type="activity"><i class="icon-delete-forever"></i></a></td>
                                </tr>
                                
                            @endforeach 
                        <!-- show the default blank image whenever there is no image uploaded for image for the hotel -->
                        @else
                            <tr>
                                <td colspan="7">{{ trans('messages.no_activity_image')}}<td>
                            <tr>
                        @endif     
                    </tbody>
                </table>
            </div>
        </div>

        @if(isset($oActivity))
        {{ Form::model($oActivity, array('url' => route('activity.activity-create') ,'method'=>'POST','enctype'=>'multipart/form-data')) }}
        @else
        {{Form::open(array('url' => 'activity/activity-create','method'=>'Post','enctype'=>'multipart/form-data')) }}
        @endif

        <input type="hidden" name="id_activity" value="{{ $nIdActivity }}"/>

        <input type="hidden" name="random_activity_id" value="<?php echo $random_activity_id; ?>"/>
       
    </div>
    @if((auth::user()->type) !="admin")
            <div class="form-group m-t-30">
                <label class="label-control">Domains <span class="required">*</span></label>
                    <select  class="form-control" multiple="true" name="domains[]">
                        @if(isset($DomainList) && $DomainList->count() > 0)
                            @foreach($DomainList as $key=>$domain)
                                <option @if(isset($oActivity))@if ( in_array( $domain->id, explode(",",$oActivity->domain_ids) ) ) selected   @endif  @endif	value="{{$domain->id}}"  id="domain_{{$domain->id}}" >{{$domain->name}}</option>
                            @endforeach
                        @endif	
                    </select>
            </div>  
        @endif        
        <div class="form-group m-t-30">
            <label class="label-control">Name <span class="required">*</span></label>
            <?php
            $attributes = 'form-control';
            ?>
            {{Form::text('name',Input::old('name'),['id'=>'name','class'=>$attributes,'placeholder'=>'Enter Name'])}}

        </div>
        @if ( $errors->first( 'name' ) )
        <small class="error">{{ $errors->first('name') }}</small>
        @endif
        <div class="form-group m-t-30">
            <label class="label-control">City Name <span class="required">*</span></label>
            <?php
            $attributes = 'form-control';
            ?>
            {{Form::select('city_id',$cities,Input::old('city_id'),['id'=>'city_id','class'=>$attributes])}}

        </div>
        @if ( $errors->first( 'city_id' ) )
        <small class="error">{{ $errors->first('city_id') }}</small>
        @endif
        <div class="form-group m-t-30">

            <label class="label-control">{{ trans('messages.end_up_diff_city') }}</label>
            <div>
                <?php $has_destination_city = (isset($oActivity['destination_city_id'])) ? $oActivity['destination_city_id'] : Input::old( 'has_destination_city' ); ?>
                <label class="radio-checkbox label_radio" for="radio-01">
                    <input type="radio" id="radio-01" value="1" class="has-destination-city" name="has_destination_city" {{ ($has_destination_city) !="" ? 'checked': '' }}> {{ trans('messages.yes') }}
                </label> 
                <label class="radio-checkbox label_radio" for="radio-02">
                    <input type="radio" id="radio-02" value="0" class="has-destination-city" name="has_destination_city" {{ ($has_destination_city) =="" ? 'checked': '' }}> {{ trans('messages.no') }}
                </label>
            </div>
        </div>
        <div class="form-group m-t-30 destination-city-id-container {{ ($has_destination_city !='') ? '' : 'hidden-field' }}">

            <label class="label-control">Destination City <span class="required">*</span></label>
            <?php
            $attributes = 'form-control';
            ?>
            <select name="destination_city_id" id="destination-city-id" class="{{ $attributes }}" 
                    {{ ($has_destination_city !="") ? ' required ': ' disabled ' }}>
                   <!--  <option disabled {{ ($has_destination_city != "") ? 'selected="selected"': '' }}>
                    Select Destination City
            </option> -->
            <?php foreach ($cities as $city_id => $city_name): ?>

                <option value="{{ $city_id }}" 

                        <?php if ($has_destination_city): ?>

                            {{ ($has_destination_city == $city_id) ? 'selected="selected"': '' }} 	
                        <?php endif ?>
                        >{{ $city_name }}</option>
                    <?php endforeach ?>
        </select>
    </div>
    <!--  @if ( $errors->first( 'destination_city_id' ) )
                                           <small class="error">{{ $errors->first('destination_city_id') }}</small>
                           @endif -->
    <div class="form-group m-t-30">

        <label class="label-control">Are accommodations included?</label>
        <div>
                <?php $has_accomodation = (isset($oActivity['has_accomodation'])) ? $oActivity['has_accomodation'] : Input::old('has_accomodation'); 
                $accommodation_nights = (isset($oActivity['accommodation_nights'])) ? $oActivity['accommodation_nights'] : Input::old('accommodation_nights');
                $accommodation_details = (isset($oActivity['accommodation_details'])) ? $oActivity['accommodation_details'] : Input::old('accommodation_details');?>
            <label class="radio-checkbox label_radio" for="radio-03">
                <input type="radio" id="radio-03" value="1" class="has-accomodation" name="has_accomodation" {{ ($has_accomodation) == '1' ? 'checked': '' }}> {{ trans('messages.yes') }}
            </label> 
            <label class="radio-checkbox label_radio" for="radio-04">
                <input type="radio" id="radio-04" value="0" class="has-accomodation" name="has_accomodation" {{ ($has_accomodation) == '0' || $has_accomodation==null ? 'checked': '' }}> {{ trans('messages.no') }}
            </label>
        </div>    
    </div>
    @if ( $errors->first( 'has_accomodation' ) )
        <small class="error">{{ $errors->first('has_accomodation') }}</small>
    @endif
    <div class="accommodation-details-container {{ ($has_accomodation== 1) ? '':'hidden-field' }}">
        <div class="form-group m-t-30">
            <label class="label-control">Accommodation Nights <span class="required">*</span></label>
            <?php
            $attributes = 'form-control';
            ?>
            <input type="number" name="accommodation_nights" id="accommodation-nights" {{ ($has_accomodation==1) ? 'required':'disabled' }} min="0" value="{{ $accommodation_nights }}" class="{{$attributes}}" placeholder="Enter Accommodation Nights"/>
        </div>

        <div class="form-group m-t-30">
            <label class="label-control">Accommodation Details <span class="required">*</span></label>
            <?php
            $attributes = 'form-control';
            ?>
            <textarea name="accommodation_details" id="accommodation-details" cols="30" rows="6" 
            {{ ($has_accomodation==1) ? 'required':'disabled' }} class="{{$attributes}}">{{ $accommodation_details }}</textarea>
        </div>

    </div>	
    <div class="form-group m-t-30">

        <label class="label-control">Are transport included?</label>
        <div>
            <?php $has_transport = (isset($oActivity['has_transport'])) ? $oActivity['has_transport'] : Input::old( 'has_transport' ); 

                $transport_details = (isset($oActivity['transport_details'])) ? $oActivity['transport_details'] : Input::old( 'transport_details' ); 
            ?>
            <label class="radio-checkbox label_radio" for="radio-05">
                <input type="radio" id="radio-05" value="1" class="has-transport" name="has_transport" {{($has_transport == '1') ? 'checked': ''}}> {{ trans('messages.yes') }}
            </label> 
            <label class="radio-checkbox label_radio" for="radio-06">
                <input type="radio" id="radio-06" value="0" class="has-transport" name="has_transport" {{ ($has_transport == '0' || $has_transport==null) ? 'checked': '' }}> {{ trans('messages.no') }}
            </label>
        </div>    
    </div>
    @if ( $errors->first( 'has_transport' ) )
        <small class="error">{{ $errors->first('has_transport') }}</small>
    @endif
    <div class="form-group m-t-30 transport-details-container {{ ($has_transport==1) ? '':'hidden-field' }}">
        <label class="label-control">Transport Details <span class="required">*</span></label>
        <?php
        $attributes = 'form-control';
        ?>
        <textarea placeholder="Enter Transport Details" name="transport_details" id="transport-details" cols="30" rows="6" 
        {{ ($has_transport==1) ? 'required':'disabled' }} class="{{$attributes}}">{{ $transport_details }}</textarea>
    </div>

</div>	
<div class="box-wrapper">
    <p>Activity-Settings</p>
    <div class="form-group m-t-30">

        <label class="label-control">Default</label>
        <div>
            <?php $default = (isset($oActivity['default'])) ? $oActivity['default'] : Input::old( 'default' ); ?>
            <label class="radio-checkbox label_radio" for="radio-07">
            <input type="radio" id="radio-07" value="1" name="default" {{($default=='1') ? 'checked' : ''}}> Yes
            </label> 
            <label class="radio-checkbox label_radio" for="radio-08">
                <input type="radio" id="radio-08" value="0" name="default" {{($default=='0' || $default==null) ? 'checked' : ''}}> No
            </label>
        </div>
    </div>
    <div class="form-group m-t-30">
        <?php
        $attributes = 'form-control';
        ?>
        <label class="label-control">Pick Up <span class="required">*</span></label>

        {{Form::text('pickup',Input::old('pickup'),['id'=>'pickup','class'=>$attributes,'placeholder'=>'Enter Pick Up'])}}
    </div>
    @if ( $errors->first( 'pickup' ) )
    <small class="error">{{ $errors->first('pickup') }}</small>
    @endif
    <div class="form-group m-t-30">

        <?php
        $attributes = 'form-control';
        ?>
        <label class="label-control">Drop Off <span class="required">*</span></label>

        {{Form::text('dropoff',Input::old('dropoff'),['id'=>'dropoff','class'=>$attributes,'placeholder'=>'Enter Drop Off'])}}	
    </div>
    @if ( $errors->first( 'dropoff' ) )
    <small class="error">{{ $errors->first('dropoff') }}</small>
    @endif
    <div class="form-group m-t-30">
        <?php
        $attributes = 'form-control';
        ?>
        <label class="label-control">Duration</label>
        {{Form::number('duration',Input::old('duration'),['id'=>'duration','min'=> '1','class'=>$attributes,'placeholder'=>'Enter Duration'])}}
    </div>
    <div class="form-group m-t-30">
        <?php
        $attributes = 'form-control';
        ?>
        <label class="label-control">Supplier <span class="required">*</span></label>
        {{Form::select('default_activity_supplier_id',$suppliers,Input::old('default_activity_supplier_id'),['id'=>'default_activity_supplier_id','class'=>$attributes])}}
    </div>
    @if ( $errors->first( 'default_activity_supplier_id' ) )
    <small class="error">{{ $errors->first('default_activity_supplier_id') }}</small>
    @endif
    <div class="form-group m-t-30">
        <?php
        $attributes = 'form-control';
        ?>
        <label class="label-control">Operator <span class="required">*</span></label>
        {{Form::select('activity_operator_id',$operators,Input::old('activity_operator_id'),['id'=>'activity_operator_id','class'=>$attributes])}}
    </div>
    @if ( $errors->first( 'activity_operator_id' ) )
    <small class="error">{{ $errors->first('activity_operator_id') }}</small>
    @endif
    <div class="form-group m-t-30">
        <?php
        $attributes = 'form-control';
        ?>
        <label class="label-control">Currency <span class="required">*</span></label>
        {{Form::select('currency_id',$currencies,Input::old('currency_id'),['id'=>'currency_id','class'=>$attributes])}}
    </div>
    @if ( $errors->first( 'currency_id' ) )
    <small class="error">{{ $errors->first('currency_id') }}</small>
    @endif
    <div class="form-group m-t-30">

        <label class="label-control">Eroam Stamp Status</label>
        <div>
            <label class="radio-checkbox label_radio" for="radio-09">
                <?php $eroam_stamp = (isset($oActivity['eroam_stamp'])) ? $oActivity['eroam_stamp'] : Input::old( 'eroam_stamp' ); ?>
                 
                <input type="radio" id="radio-09" value="1" name="eroam_stamp" {{ ($eroam_stamp == '1') ? 'checked' : '' }}> Yes
            </label> 
            <label class="radio-checkbox label_radio" for="radio-10">
                <input type="radio" id="radio-10" value="0" name="eroam_stamp" {{ ($eroam_stamp == '0' || $eroam_stamp==null) ? 'checked' : '' }}> No
            </label>
        </div>
    </div>

</div> 
<div class="box-wrapper">
    <p>Activity Description</p>
    <div class="form-group m-t-30">

        <label class="label-control">Approve By Eroam</label>
        <div>
            <?php $approved_by_eroam  = (isset($oActivity['approved_by_eroam'])) ? $oActivity['approved_by_eroam'] : Input::old( 'approved_by_eroam' ); ?>
            <label class="radio-checkbox label_radio" for="radio-11">
            <input type="radio" id="radio-11" value="1"  name="approved_by_eroam" {{($approved_by_eroam == '1') ? 'checked' : ''}}> Yes
            </label> 
            <label class="radio-checkbox label_radio" for="radio-12">
                <input type="radio" id="radio-12" value="0"  name="approved_by_eroam" {{($approved_by_eroam == '0' || $approved_by_eroam==null) ? 'checked' : ''}}> No
            </label>
        </div>
    </div> 
    <div class="form-group m-t-30">
        <?php
        $attributes = 'form-control';
        ?>
        <label class="label-control">Description <span class="required">*</span></label>
        {{Form::textarea('description',Input::old('description'),['id'=>'description','rows'=>"5",'class'=>$attributes])}}			
    </div>
    @if ( $errors->first( 'description' ) )
    <small class="error">{{ $errors->first('description') }}</small>
    @endif
    <div class="form-group m-t-30">
        <?php
        $attributes = 'form-control';
        ?>
        <label class="label-control">Ranking <span class="required">*</span></label>
        {{Form::select('ranking',['' => 'Select Ranking','1' => '1','2' => '2','3' => '3','4'=>'4','5'=>'5'],Input::old('ranking'),['id'=>'ranking','class'=>$attributes])}}		
    </div>
    @if ( $errors->first( 'ranking' ) )
    <small class="error">{{ $errors->first('ranking') }}</small>
    @endif
    <div class="form-group m-t-30">
        <?php
        $attributes = 'form-control';
        ?>
        <label class="label-control">Voucher Comments</label>
        {{Form::text('voucher_comments',Input::old('voucher_comments'),['id'=>'voucher_comments','class'=>$attributes])}}
    </div>   
</div> 
<div class="box-wrapper">
    <p>Activity Location</p>
    <div class="form-group m-t-30">
        <?php
        $attributes = 'form-control';
        ?>
        <label class="label-control">Address 1</label>
        {{Form::text('address_1',Input::old('address_1'),['id'=>'address_1','class'=>$attributes,'placeholder'=>'Enter Address 1'])}}
    </div> 
    <div class="form-group m-t-30">
        <?php
        $attributes = 'form-control';
        ?>
        <label class="label-control">Address 2</label>
        {{Form::text('address_2',Input::old('address_2'),['id'=>'address_2','class'=>$attributes,'placeholder'=>'Enter Address 2'])}}	
    </div>
    <div class="form-group m-t-30">
        <?php
        $attributes = 'form-control';
        ?>
        <label class="label-control">Reception Phone</label>
        {{Form::text('reception_phone',Input::old('reception_phone'),['id'=>'reception_phone','class'=>$attributes,'placeholder'=>'Enter Reception Phone'])}}	
    </div> 
    @if ( $errors->first( 'reception_phone' ) )
        <small class="error">{{ $errors->first('reception_phone') }}</small>
    @endif
    <div class="form-group m-t-30">
        <?php
        $attributes = 'form-control';
        ?>
        <label class="label-control">Reception Email</label>
        {{Form::text('reception_email',Input::old('reception_email'),['id'=>'reception_email','class'=>$attributes,'placeholder'=>'Enter Reception Email'])}}			
    </div> 
    @if ( $errors->first( 'reception_email' ) )
        <small class="error">{{ $errors->first('reception_email') }}</small>
    @endif
    <div class="form-group m-t-30">
        <?php
        $attributes = 'form-control';
        ?>
        <label class="label-control">Website</label>
        {{Form::text('website',Input::old('website'),['id'=>'website','class'=>$attributes,'placeholder'=>'Enter Website'])}}
    </div> 
    @if ( $errors->first( 'website' ) )
        <small class="error">{{ $errors->first('website') }}</small>
     @endif
    <div class="form-group m-t-30">
        <?php
        $attributes = 'form-control';
        ?>
        <label class="label-control">Pax Type</label>
        {{Form::text('pax_type',Input::old('pax_type'),['id'=>'pax_type','class'=>$attributes,'placeholder'=>'Enter Pax Type'])}}
    </div>  
</div> 
<div class="box-wrapper">
    <p>Activity Label</p> 
    <div class="form-group m-t-30">
        <?php
        $attributes = 'form-control';
        ?>
        <label class="label-control">Labels <span class="required">*</span></label>
        {{Form::select('labels[]',$labels,$aLable,['id'=>'labels','class'=>$attributes,'multiple' => 'true' ])}}
    </div>
    @if ( $errors->first( 'labels' ) )
    <small class="error">{{ $errors->first('labels') }}</small>
    @endif
    @if(Input::old('video_url'))
        @foreach(Input::old('video_url') as $key => $day)
        <div class="row tb_added">
            <div class="col-sm-6">
                <div class="form-group m-t-30">
                    <label class="label-control">{{ trans('messages.video_url') }}</label>
                    <input placeholder="Enter Video URL" type="url" class="form-control" name="video_url[]"  value="{{$day}}">
                </div> 
            </div>
        </div>
        @endforeach
    @elseif(isset($oVideos))
        @foreach($oVideos as $key => $day)
        <div class="row tb_added">
            <div class="col-sm-6">
                <div class="form-group m-t-30">
                    <label class="label-control">{{ trans('messages.video_url') }}</label>
                    <input placeholder="Enter Video URL" type="url" class="form-control" name="video_url[]" value="{{ $day->original }}">
                </div> 
            </div>
        </div>
        @endforeach
    @else
        <div class="row tb_added">
            <div class="col-sm-6">
                <div class="form-group m-t-30">
                    <label class="label-control">{{ trans('messages.video_url') }}</label>
                    {{Form::url('video_url[]',Input::old('days'),['class'=>'form-control','placeholder'=>'Enter Video URL'])}}
                </div> 
            </div>
        </div>
    @endif
    <div class="clones"></div>
    <div class="row">
        <div class="col-sm-12">
            <div class="text-center">
                <i class="fa fa-minus-square right fa-2x" id="fa-minus" aria-hidden="true"></i>
                <i class="fa fa-plus-square right fa-2x" id="fa-plus" aria-hidden="true"></i>
            </div> 
        </div>      
    </div>
</div> 

<div class="m-t-20 row col-md-8 col-md-offset-2">
    <div class="row">
        <div class="col-sm-6">
            {{Form::submit('Save',['class'=>'button success btn btn-primary btn-block']) }}
        </div>
        <div class="col-sm-6">
            <a href="{{ route('activity.activity-list') }}" class="btn btn-primary btn-block">{{ trans('messages.cancel_btn') }}</a>
        </div>
    </div>
</div>	
{{Form::close()}}
</div>
@stop

@section('custom-js')
<script type="text/javascript">
    $(document).ready(function() {

        $('.sortable-list')
            .sortable({
               revert       : true,
               connectWith  : ".sortable-list",
               stop         : function(event,ui){ 
                 
                 var image_id = new Array();
                 $('.sortable-list tr').each(function(){
                        id = $(this).attr('data-id');
                        image_id.push(id);
            });
                 
                 if(image_id) {
                    $.ajax({
                        url: "{{ route('activity.sort-activity-images') }}",
                        method: 'post',
                        data: {
                            image_id: image_id,
                            _token: '{{ csrf_token() }}'
                        },
                        success: function( response ) {
                        }
                    });
                 }
               }
        });
        // enable fileuploader plugin
        $('input[name="activityImage"]').fileuploader({

            changeInput:'<div class="fileuploader-input">' +
                            '<div class="fileuploader-input-inner">' +
                                '<h3 class="fileuploader-input-caption"><span><i class="icon-add-photos"></i> Drag Activity Images here to upload.</span></h3>' +
                            '</div>' +
                        '</div>' + 
                        '<div class="fileupload-link"><span>Click here to upload images from your computer</span> (Dimension 1000 X 259, max file size, 2mb. Supported file types, .jpeg, .jpg, .png).</div>',
            theme: 'dragdrop',
            upload: {
                url: siteUrl('activity/activity-images'),
                data: {activity_id:$('#activityId').val()},
                type: 'POST',
                enctype: 'multipart/form-data',
                start: true,
                synchron: true,
                beforeSend: null,
                onSuccess: function(result, item) {
                    var data = result;

                    if(data.success === true){

                        $('#emptyActivity').hide();
                        $('.fileuploader-items-list').find('li').remove();
                        item.name = data.data[0].image_name;
                        item.image_id = data.data[0].image_id;

                        //Append to html
                        var img_html = '<tr data-id="'+data.data[0].image_id+'" id="image_'+data.data[0].image_id+'">';
                                img_html +='<td><a href="#" class="sortable-icon"><i class="icon-reorder-black"></i></a></td>';
                                img_html +='<td class="table-no-padding">';
                                img_html +='<div class="table-image">';
                                img_url = '{{ trans('messages.image_url',['image_title' => 'activities/']) }}';
                                img_html +='<img src="'+img_url+data.data[0].image_name+'" class="img-responsive">';
                                img_html +='</div>';
                                img_html +='</td>';
                                //img_html +='<td class="text-left blue-text">'+data.data[0].image_name+'</td>';          
                                img_url = siteUrl(data.data[0].image_large);       
                                //img_html +='<td><a href="'+img_url+'" class="action-icon" download><i class="icon-download"></i></a></td>';          
                                img_html +='<td><a href="javascript://" class="delete_activity_img action-icon image-click-event image-delete" data-action="delete" data-id="'+data.data[0].image_id+'" id="'+data.data[0].image_id+'" data-type="activity"><i class="icon-delete-forever"></i></a></td>';          
                            img_html +='</tr>';        
                        $('.sortable-list').append(img_html);

//                            $('.image-click-event').click(function(){
//                                    removeImage($(this).attr('id'));
//                            });
                    }
                        else{
                            $('.fileuploader-items-list').find('li').remove();
                        }
                    // if warnings
                    if (data.hasWarnings) {
                        for (var warning in data.warnings) {
                            alert(data.warnings);
                        }
                        
                        item.html.removeClass('upload-successful').addClass('upload-failed');
                        // go out from success function by calling onError function
                        // in this case we have a animation there
                        // you can also response in PHP with 404
                        return this.onError ? this.onError(item) : null;
                    }
                    
                    item.html.find('.column-actions').append('<a class="fileuploader-action fileuploader-action-remove fileuploader-action-success" title="Remove"><i></i></a>');
                    setTimeout(function() {
                        item.html.find('.progress-bar2').fadeOut(400);
                    }, 400);
                },
                onError: function(result, item,response) {
                        $('.error_message_image').html('<span>The activity image must be a file of type: jpeg, jpg, png.</span>')
                },
                onProgress: function(data, item) {
                    var progressBar = item.html.find('.progress-bar2');
                    
                    if(progressBar.length > 0) {
                        progressBar.show();
                        progressBar.find('span').html(data.percentage + "%");
                        progressBar.find('.fileuploader-progressbar .bar').width(data.percentage + "%");
                    }
                },
                onComplete: null,
            },

            captions: {
                feedback: '<span><i class="icon-add-photos"></i> Drag Activity Images here to upload.</span>',
                feedback2: '<span><i class="icon-add-photos"></i> Drag Activity Images here to upload.</span>',
                drop: '<span><i class="icon-add-photos"></i> Drag Activity Images here to upload.</span>'
            },
        });
    });

    $('#fa-plus').click(function () {
        var count = $('.clones').children().length+1;
        $('.tb_added:last').clone().appendTo(".clones");
        var numItems = $('.tb_added').length;
        $('.tb_added:last input').val('');

    });
    $('#fa-minus').click(function () {
        var numItems = $('.tb_added').length;
        if (numItems > 1) {
            $('.tb_added:last').remove();
        }
    });

    // show destination city field when #has-destination-city is "yes"
    $('.has-destination-city').change(
        function ()
        {
            console.log('innn ioio');
            if ($(this).val() == 1)
            {
                $('.destination-city-id-container').show();
                $('#destination-city-id').prop('required', true);
                $('#destination-city-id').prop('disabled', false);
            } else
            {
                $('.destination-city-id-container').hide();
                $('#destination-city-id').prop('required', false);
                $('#destination-city-id').prop('disabled', true);
                $("#destination-city-id").val('');
            }
        }
    );
    // show accomodation nights and details field when #has-accomodation is "yes"
    $('.has-accomodation').change(
        function ()
        {
            if ($(this).val() == 1)
            {
                $('.accommodation-details-container').show();
                $('#accommodation-details').prop('required', true);
                $('#accommodation-details').prop('disabled', false);
                $('#accommodation-nights').prop('required', true);
                $('#accommodation-nights').prop('disabled', false);
            } else
            {
                $('.accommodation-details-container').hide();
                $('#accommodation-details').prop('required', false);
                $('#accommodation-details').prop('disabled', true);
                $('#accommodation-nights').prop('required', false);
                $('#accommodation-nights').prop('disabled', true);
                $("#accommodation-nights").val('');
                $("#accommodation-details").val('');
            }
        }
    );
    // show transport nights and details field when #has-transport is "yes"
    $('.has-transport').change(
        function ()
        {
            if ($(this).val() == 1)
            {
                $('.transport-details-container').show();
                $('#transport-details').prop('required', true);
                $('#transport-details').prop('disabled', false);
            } else
            {
                $("#transport-details").val('');
                $('.transport-details-container').hide();
                $('#transport-details').prop('required', false);
                $('#transport-details').prop('disabled', true);
            }
        }
    );

    tinymce.init({
        selector: '#cancellation_policy',
        height: 200,
        menubar: false
    });
    tinymce.init({
        selector: '#voucher_comments',
        height: 200,
        menubar: false
    });
    tinymce.init({
        selector: '#description',
        height: 200,
        menubar: false
    });
</script>
@stop
@section('custom-css')
<style>

    .error{
        color:red !important;
    }
    .hidden-field{
        display:none;
    }
    .error_message{
        color:red !important;
    }
    .with_error{
        border-color: red !important;
    }
    .success_message{
        color:green !important;
        text-align: center;
    }
    div .with_error{
        border:1px solid black;
    }
    label {
        font-weight: bold;
    }
    .fa-plus-square{
        color:green;
        cursor:pointer;
    }
    .fa-minus-square{
        color:red;
        cursor:pointer;
    }
</style>
@stop

