@foreach ($currentBookings as $value)		
    <tr>
        <td>{{$value->username}}</td>
        <td>{{!empty($value->pax_count) ? $value->pax_count :'' }}</td>
        <td>{{ !empty($value->created_at)? date('d/m/Y',strtotime($value->created_at)):''}}</td>
        <td>{{ !empty($value->travel_date)? date('d/m/Y',strtotime($value->travel_date)):''}}</td>
        <td>Single Product</td>
        <td><i class="fa fa-usd"></i> {{!empty($value->total_amount) ? $value->total_amount :'' }}</td>
        <td><i class="fa fa-usd"></i> {{!empty($value->cost_per_day) ? $value->cost_per_day :'' }}</td>
        <td>2.5</td>
      </tr>
    </tr> 
@endforeach