@extends( 'layout/mainlayout' )

@section('content')
	<div class="content-container">
		<div class="row">
			<div class="col-md-12 text-right">
	            <a href="{{ route('agent.edit-customer',$user_id) }}" class="button success tiny btn-primary btn-sm m-b-3" title="Edit">Edit</a>	
				<a href="{{ route('agent.redirect-page',$user_id) }}" class="button success tiny btn-primary btn-sm m-b-3" title="Create New Booking">Create New Booking</a>
	            <a href="#" class="button success tiny btn-primary btn-sm m-b-3" title="Load Previous Booking">Load Previous Booking</a>
			</div>			
		</div>
		
		<div class="box-wrapper">
		<div class="panel panel-default">
            <div class="panel-heading"><b>PAX Details</b></div>
			
            <div class="panel-body">
				@if(!empty($passengerList)) 
					@foreach($passengerList as $list)
	                	<table class="table table-responsive">
					    	<tbody>
							    <tr>
		                            <td><b>Account ID:</b> {{ !empty($list->passenger_information_id) ? $list->passenger_information_id:'' }} </td>
		                            <td><b>Title:</b> {{ !empty($list->title) ? $list->title:'N/A'  }}</td>
		                        </tr>
								<tr>
		                            <td><b>Given Name:</b> {{ !empty($list->first_name) ? $list->first_name:'N/A'  }} </td>
		                            <td><b>Family Name:</b> {{ !empty($list->last_name) ? $list->last_name:'N/A'  }}</td>
		                        </tr>
								<tr>
		                            <td><b>Gender: </b>{{ !empty($list->gender) ? $list->gender :'N/A'  }} </td>
		                            <td><b>Year of Birth:</b> {{ !empty($list->dob) ? date('d/m/Y',strtotime($list->dob)) :'N/A'  }}</td>
		                        </tr>
								<tr>
		                            <td><b>Nationality:</b> {{ !empty($list->nationality) ? $list->nationality:'N/A'  }}</td>
		                            <td><b>Passport Number (If Applicable):</b> {{ !empty($list->passport_num) ? $list->passport_num:'N/A'  }}</td>
		                        </tr>
								<tr>
		                            <td><b>Passport Expiry Date:</b> {{ !empty($list->passport_expiry_date) ? $list->passport_expiry_date:'N/A'  }}</td>
		                            <td></td>
		                        </tr>
							</tbody>
						</table>
					@endforeach
				@else
					<table class="table table-responsive">
					<tbody>
					<tr>
                        <td><b>No records founds</b></td>
                        <td></td>
                    </tr>
					</tbody>
					</table>
				@endif
            </div>
		</div>
		
		
		<div class="panel panel-default">
            <div class="panel-heading"><b>Contact Details</b></div>
			
            <div class="panel-body">
				@if(!empty($passengerList)) 
				<?php foreach($passengerList as $list){ ?>
                <table class="table table-responsive">
				    <tbody>
					    <tr>
                            <td><b>Phone: </b>{{ !empty($list->contact) ? $list->contact:'N/A'  }} </td>
                            <td><b>Email Address:</b> {{ !empty($list->email) ? $list->email:'N/A'  }}</td>
                        </tr>
						<tr>
                            <td><b>Address (Line One): </b>{{ !empty($list->address_one) ? $list->address_one:'N/A'  }} </td>
                            <td><b>Address (Line Two):</b> {{ !empty($list->address_two) ? $list->address_two:'N/A'  }}</td>
                        </tr>
						<tr>
                            <td><b>City:</b> {{ !empty($list->suburb) ? $list->suburb:'N/A'  }} </td>
                            <td><b>State / Province / Region:</b> {{ !empty($list->state) ? $list->state:'N/A'  }}</td>
                        </tr>
						<tr>
                            <td><b>ZIP / Postal Code:</b> {{ !empty($list->zip) ? $list->zip:'N/A'  }}</td>
                            <td><b>Country:</b> {{ !empty($list->nationality) ? $list->nationality:'N/A'  }}</td>
                        </tr>
						</tbody>
					</table>
				<?php } ?>
					@else
						<table class="table table-responsive">
						<tbody>
						<tr>
                            <td><b>No records founds</b></td>
                            <td></td>
                        </tr>
						</tbody>
						</table>
					@endif
            </div>
		</div>
		
		
		<div class="panel panel-default">
            <div class="panel-heading"><b>Personal Preferences</b></div>
			
            <div class="panel-body">
				@if(!empty($personalPreferences))
				<table class="table table-responsive">
				    <tbody>
					    <tr>
                            <td><b>Frequently Used Products:</b> {{ !empty($personalPreferences->freq_used_products) ? $personalPreferences->freq_used_products :'N/A' }} </td>
                            <td></td>
                        </tr>
						</tbody>
					</table>
				   @else
						<table class="table table-responsive">
						<tbody>
						<tr>
                            <td><b>No records founds</b></td>
                            <td></td>
                        </tr>
						</tbody>
						</table>
					@endif
            </div>
		</div>
		
		<div class="panel panel-default">
            <div class="panel-heading"><b>Accommodation</b></div>
			<div class="panel-body">
				<table class="table table-responsive">
				    <tbody>
					    <tr>
                            <td><b>Accommodation Type:</b> {{ !empty($acoomodationsType->name) ? $acoomodationsType->name :'N/A' }}</td>
                            <td><b>Room Type:</b> {{ !empty($roomType->name) ? $roomType->name :'N/A' }}</td>
                        </tr>
						</tbody>
					</table>
			</div>
		</div>
		<div class="panel panel-default">
            <div class="panel-heading"><b>Transportation</b></div>
			<?php $cabinType = ['Y'=>'Economy','C'=>'Business','F'=>'First','S'=>'Premium Economy']; 
			 $interests = array(
										15=>'4WD',6=>'Astronomy',18=>'Backpacking',8=>'Urban-Adventures',
										5=>'Ballooning', 12=>'Camping',9=>'Overland', 1=>'Expeditions',
										13=>'Festivals', 24=>'Cycling',3=>'Family',23=>'Fishing',
										29=>'HorseRiding',28=>'Kayaking',19=>'Wildlife',4=>'Short-Breaks',
										11=>'Sailing', 16=>'ScenicFlight',17=>'Sky-Diving',2=>'Snorkelling',
										20=>'Surfing', 14=>'Walking-Trekking', 7=>'Polar', 10=>'Food'
										);
							$databaseIntrest = !empty($personalPreferences->interests) ? explode(',', $personalPreferences->interests):array();
							//pr($interests);
					 ?>
			<div class="panel-body">
				<table class="table table-responsive">
				    <tbody>
					    <tr>
                            <td><b>Transport Type:</b> {{ !empty($transportType->name) ? $transportType->name :'N/A' }}</td>
							<td><b>Cabin Type:</b> {{ !empty($personalPreferences->pref_cabin_class) ? $cabinType[$personalPreferences->pref_cabin_class] :'N/A' }}</td>
                        </tr>  
						<tr>
                            <td><b>Seat Type:</b> {{ !empty($personalPreferences->pref_seat_type) ? $personalPreferences->pref_seat_type :'N/A' }}</td>
							<td><b>Meal Type: </b>{{ !empty($personalPreferences->pref_meal_type) ? $personalPreferences->pref_meal_type :'N/A' }}</td>
                        </tr>
						<tr>
                            <td><b>Activity / Tour Type:</b>@if(!empty($databaseIntrest))
									@foreach($databaseIntrest as $value)
										@if(array_key_exists($value, $interests))
										@if(end($databaseIntrest) !=$value)
										 {{ $interests[$value]."," }}
										@else
										 {{ $interests[$value] }}
										@endif
										@endif
									@endforeach
									@else
									"N/A"
									@endif
							</td>
							<td><b>Additional Information:</b> {{ !empty($personalPreferences->additional_info) ? $personalPreferences->additional_info :'N/A' }}</td>
                        </tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading"><b>Itinerary History</b></div>
				<center><p class="h4">Saved Itineraries</p></center>
				<div class="panel-body">
				@if(!empty($itenaryHistory))
					<table class="table table-responsive">
						<tbody>
							<tr>
								<td><b>Invoice No.:</b> {{ !empty($itenaryHistory->invoice_no) ? $itenaryHistory->additional_info :'N/A' }}</td>
								<td><b>From City.:</b> {{ !empty($itenaryHistory->from_city_name) ? $itenaryHistory->additional_info :'N/A' }}</td>
							</tr>
							<tr>
								<td><b>To City.:</b> {{ !empty($itenaryHistory->to_city_name) ? $itenaryHistory->to_city_name :'N/A' }}</td>
								<td><b>No. of Travellers:</b> {{ !empty($itenaryHistory->no_of_travellers) ? $itenaryHistory->no_of_travellers :'N/A' }}</td>
							</tr>
							<tr>
								<td><b>Travel Date:</b> {{ !empty($itenaryHistory->travel_date) ? $itenaryHistory->travel_date :'N/A' }}</td>
								<td><b>Total Amount:</b> {{ !empty($itenaryHistory->total_amount) ? $itenaryHistory->total_amount :'N/A' }}</td>
							</tr>
							<tr>
								<td><b>Total Days:</b> {{ !empty($itenaryHistory->total_days) ? $itenaryHistory->total_days :'N/A' }}</td>
								<td><b>Cost Per Day:</b> {{ !empty($itenaryHistory->cost_per_day) ? $itenaryHistory->cost_per_day :'N/A' }}</td>
							</tr>
							<tr>
								<td><b>Total Per Person:</b> {{ !empty($itenaryHistory->total_per_person) ? $itenaryHistory->total_per_person :'N/A' }}</td>
								<td><b>Cost Per Person:</b> {{ !empty($itenaryHistory->cost_per_person) ? $itenaryHistory->cost_per_person :'N/A' }}</td>
							</tr>
						</tbody>
					</table>
				@else
					<table class="table table-responsive">
						<tbody>
							<tr>
								<td><b>No records founds</b></td>
								<td></td>
							</tr>
						</tbody>
					</table>
			@endif
			<div class="panel-body">
			<center><p class="h4">Pending Itineraries</p></center>
			</div>
			@if(!empty($pendingItenaryHistory))
				<table class="table table-responsive">
						<tbody>
							<tr>
								<td><b>Invoice No.:</b> {{ !empty($pendingItenaryHistory->invoice_no) ? $pendingItenaryHistory->additional_info :'N/A' }}</td>
								<td><b>From City.:</b> {{ !empty($pendingItenaryHistory->from_city_name) ? $pendingItenaryHistory->additional_info :'N/A' }}</td>
							</tr>
							<tr>
								<td><b>To City.:</b> {{ !empty($pendingItenaryHistory->to_city_name) ? $pendingItenaryHistory->to_city_name :'N/A' }}</td>
								<td><b>No. of Travellers:</b> {{ !empty($pendingItenaryHistory->no_of_travellers) ? $pendingItenaryHistory->no_of_travellers :'N/A' }}</td>
							</tr>
							<tr>
								<td><b>Travel Date:</b> {{ !empty($pendingItenaryHistory->travel_date) ? $pendingItenaryHistory->travel_date :'N/A' }}</td>
								<td><b>Total Amount:</b> {{ !empty($pendingItenaryHistory->total_amount) ? $pendingItenaryHistory->total_amount :'N/A' }}</td>
							</tr>
							<tr>
								<td><b>Invoice NO.:</b> {{ !empty($pendingItenaryHistory->invoice_no) ? $pendingItenaryHistory->additional_info :'N/A' }}</td>
								<td><b>From City.:</b> {{ !empty($pendingItenaryHistory->from_city_name) ? $pendingItenaryHistory->additional_info :'N/A' }}</td>
							</tr>
							<tr>
								<td><b>Total Days:</b> {{ !empty($pendingItenaryHistory->total_days) ? $pendingItenaryHistory->total_days :'N/A' }}</td>
								<td><b>Cost Per Day:</b> {{ !empty($pendingItenaryHistory->cost_per_day) ? $pendingItenaryHistory->cost_per_day :'N/A' }}</td>
							</tr>
							<tr>
								<td><b>From Date:</b> {{ !empty($itenaryHistory->from_date) ? $itenaryHistory->from_date :'N/A' }}</td>
								<td><b>To Date:</b> {{ !empty($itenaryHistory->to_date) ? $itenaryHistory->to_date :'N/A' }}</td>
							</tr>
							
						</tbody>
					</table>
			
			@else
				<table class="table table-responsive">
					<tbody>
						<tr>
							<td><b>No records founds</b></td>
							<td></td>
						</tr>
					</tbody>
				</table>
			@endif
				</div>
			</div>
		</div>
	</div>
@endsection