<table class="table">
    <thead>
        <tr>
            <th onclick="getNotificationSort(this,'notification_title');">Notification
                    <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'notification_title')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th>Number of PAX</th>
            <th onclick="getNotificationSort(this,'created_date');">Created Date
                 <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'created_date')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getNotificationSort(this,'start_date');">Departure Date
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'start_date')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th>Return Date</th>
            <th onclick="getNotificationSort(this,'notification_type');">Itinerary Type
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'notification_type')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
           
        </tr>
    </thead>
    <tbody class="user_list_ajax">
    @if(count($notifications) > 0)
        @include('WebView::agent._more_notification_list')
    @else
        <tr><td colspan="10" class="text-center">{{ trans('messages.no_record_found') }}</td></tr>
    @endif
    </tbody>
</table>