<table class="table">
    <thead>
      <tr>
        <th>Account ID </th>
        <th>Given Name </th>
        <th>Family Name</th>
        <th>Gender</th>
        <th>Phone</th>
        <th>Email Address</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
        
    @foreach($customers as $customer)
        @php $name = explode(' ',$customer->name);
        $fname = $name[0];
        if(isset($name[1])) {
            $lname = $name[1];
        } else {
            $lname = '';
        }
        @endphp
      <tr>
        <td>{{ $customer->id }}</td>
        <td>{{ $fname }}</td>
        <td>{{ $lname }}</td>
        <td>{{ $customer->gender }}</td>
        <td>{{ $customer->contact_no }}</td>
        <td>{{ $customer->username }}</td>
        <td>
            <a target="_blank" href="{{ route('agent.redirect-page',$customer->id) }}" class="button success tiny btn-primary btn-sm m-b-3" title="Create New Booking">Create</a>
            <a href="{{ route('user.saved-trips',$customer->id) }}" class="button success tiny btn-primary btn-sm m-b-3" title="Load Previous Booking">Previous</a>
            <a href="{{ route('user.paxDetails',$customer->id) }}" class="button success tiny btn-primary btn-sm m-b-3" title="Update Preferences">Preferences</a>
        </td>
      </tr>
    @endforeach
    </tbody>
</table>