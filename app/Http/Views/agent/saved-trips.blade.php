@extends('layout/mainlayout')
@section('content')
<div class="content-container">
    <h1 class="page-title">Previously Saved Booking</h1>
    <div class="box-wrapper">
    	<table class="table">
    	    <thead>
    	      <tr>
    	        <th>S.No. </th>
                <th>Booking Type</th>
    	        <th>From City</th>
    	        <th>To City</th>
                <th>Travel Date</th>
    	        <th>Saved Date</th>
    	        <th>Action</th>
    	      </tr>
    	    </thead>
    	    <tbody>
    	        @foreach($trips as $trip)
    	    		@php
    	    			$tripdata = json_decode($trip['trip_data'],true); 
    	    			$mapdata = json_decode($trip['map_data'],true);
                        if($mapdata['type'] === 'auto') {
                            $all_cities = $mapdata['routes'][0]['cities'];
                            $cities = array_column($all_cities,'name');
                            $city_count = count($cities);
                        }
    	    			
    	    		@endphp
    	    	<tr>
                    <td>{{ $loop->iteration }}</td>
    	    		<td>{{ $mapdata['type'] }}</td>
    	    		<td>@if(isset($cities)){{ $cities[0] }}@endif</td>
    	    		<td>@if(isset($cities)){{ $cities[$city_count - 1] }}@endif</td>
                    <td>{{ date('d-m-Y',strtotime($tripdata['travel_date'])) }}</td>
    	    		<td>{{ date('d-m-Y',strtotime($trip['created_at'])) }}</td>
    	    		<td>
    	    			<a href="{{ route('agent.redirect-page',['user_id'=>$trip->user_id,'trip_id'=>$trip->id]) }}" class="button success tiny btn-primary btn-sm m-b-3" target="_blank" title="Open Trip">Open</a>
    	    		</td>
    	    	</tr>
    	    	@endforeach
    	    </tbody>
    	</table>
    </div>
</div>
@endsection