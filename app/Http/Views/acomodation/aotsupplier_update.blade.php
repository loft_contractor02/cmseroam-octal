@extends( 'layout/mainlayout' )

@section('content')
<div class="content-container">
    <h1 class="page-title">AOT Supplier Update</h1>
    <div class="row">
        @if (Session::has('message'))
        <div class="small-12 small-centered columns success_message">{{ Session::get('message') }}</div>
        <br>
        @endif

        @if ($errors->any())
        <div class="small-6 small-centered columns error_message">{{$errors->first()}}</div>
        @endif
    </div>
    <div class="box-wrapper">
    {{ Form::model($supplier, array('url' =>  route('acomodation.aotsupplier-update') ,'method'=>'POST','enctype'=>'multipart/form-data')) }}
    <div class="row">
        <div class="">
            <h4>{{$supplier->Name}}</h4>

        </div>
    </div>
    
    <div class="m-t-20">

        @if ($supplier->Images)
            <img src="{{ URL::asset('uploads/hotels/aot_default.jpg') }}">
        @else
            <img src="{{ URL::asset('uploads/hotels/aot_default.jpg') }}">
        @endif
    </div>
    
    <div class="m-t-20">
        <h4>LocationName: </h4>
        <p class="m-t-10">{{$supplier->LocationName}},{{$supplier->Address1}} </p>
    </div>
    <div class="m-t-20">
        <h4>Description:</h4> <p class="text-justify m-t-10">{{$supplier->Description}}</p>
    </div>

    <div>
        <div class="form-group m-t-30">
            <label class="label-control">Give Eroam Stamp?<span class="required">*</span></label>
        @if(!$supplier->updated_supplier)
            <div>
                <label class="radio-checkbox label_radio" for="gradio-01">
                    <input type="radio" id="gradio-01" value="X" class="has-destination-city" name="approved_by_eroam" {{ (Input::old('approved_by_eroam') == 'X') ? 'checked': '' }}> {{ trans('messages.yes') }}
                </label> 
                <label class="radio-checkbox label_radio" for="gradio-02">
                    <input type="radio" id="gradio-02" value="" class="has-destination-city" name="approved_by_eroam" {{ (!Input::old('approved_by_eroam')) ? 'checked': '' }}> {{ trans('messages.no') }}
                </label>
            </div>
        @elseif($supplier->updated_supplier->approved_by_eroam)
                <div>
                    <label class="radio-checkbox label_radio" for="gradio-01">
                        <input type="radio" id="gradio-01" value="X" class="has-destination-city" name="approved_by_eroam" {{ ($supplier->updated_supplier->approved_by_eroam == 'X') ? 'checked': '' }}> {{ trans('messages.yes') }}
                    </label> 
                    <label class="radio-checkbox label_radio" for="gradio-02">
                        <input type="radio" id="gradio-02" value="" class="has-destination-city" name="approved_by_eroam" {{ ($supplier->updated_supplier->approved_by_eroam == '') ? 'checked': '' }}> {{ trans('messages.no') }}
                    </label>
                </div>
        @endif
        </div>

    </div>

    <div>
        <div class="form-group m-t-30" >
        <label class="label-control">Set as Default?</label>
        
        @if(!$supplier->updated_supplier)
        <div>
            <label class="radio-checkbox label_radio" for="sradio-01">
                <input type="radio" id="sradio-01" value="X" class="has-destination-city" name="default_hotel" {{ (Input::old('approved_by_eroam') == 'X') ? 'checked': '' }}> {{ trans('messages.yes') }}
            </label> 
            <label class="radio-checkbox label_radio" for="sradio-02">
                <input type="radio" id="sradio-02" value="" class="has-destination-city" name="default_hotel" {{ (!Input::old('approved_by_eroam')) ? 'checked': '' }}> {{ trans('messages.no') }}
            </label>
        </div>
        @elseif($supplier->updated_supplier->default_hotel)
            <div>
                <label class="radio-checkbox label_radio" for="sradio-01">
                    <input type="radio" id="sradio-01" value="X" class="has-destination-city" name="default_hotel" {{ ($supplier->updated_supplier->default_hotel == 'X') ? 'checked': '' }}> {{ trans('messages.yes') }}
                </label> 
                <label class="radio-checkbox label_radio" for="sradio-02">
                    <input type="radio" id="sradio-02" value="" class="has-destination-city" name="default_hotel"
                           {{ ($supplier->updated_supplier->default_hotel == '') ? 'checked': '' }}> {{ trans('messages.no') }}
                </label>
            </div>
        @endif
        </div>
    </div>

    {{Form::hidden('hotel_supplier_code',$supplier->SupplierCode)}}

    <div class="row">
        <div class="m-t-20 row col-md-8 col-md-offset-2">
            <div class="row">
                <div class="col-sm-6">
                    <input class="button success btn btn-primary btn-block" type="submit" value="{{ trans('messages.save_btn') }}">
                </div>
                <div class="col-sm-6">
                    <a href="{{ route('acomodation.hotel-aotsupplier-list') }}" class="btn btn-primary btn-block">{{ trans('messages.cancel_btn') }}</a>
                </div>
            </div>    
        </div>
    </div>
<!--        <div>
            <ul class="button-group">
                <li>{{Form::submit('Update',['class'=>'button success']) }}</li>
                <li><a href="{{URL::to('aot/supplier')}}" class="button">Cancel</a></li>
            </ul>
        </div>-->
    {{Form::close()}}
    </div>
</div>

@stop

@section('custom-css')
<style>
    .error_message{
        color:red !important;
    }
    .with_error{
        border-color: red !important;
    }
    .success_message{
        color:green !important;
        text-align: center;
    }
</style>
@stop

@section('custom-js')
<script>
    $('.alert').click(function () {
        var c = confirm("Are you sure you want to delete this record?");
        return c;
    });
</script>
@stop
