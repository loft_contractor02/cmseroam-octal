@extends( 'layout/mainlayout' )

@section('content')
<div class="content-container">

    @if(isset($oLabel))
        <h1 class="page-title">{{ trans('messages.update',['name' => 'Label']) }}</h1>
    @else
        <h1 class="page-title">{{ trans('messages.add',['name' => 'Label']) }}</h1>
    @endif
    
    <div class="row">
        @if (Session::has('message'))
        <div class="small-12 small-centered columns success-box">{{ Session::get('message') }}</div>
        @endif

    </div>
    <br>
    @if(isset($oLabel))
    {{ Form::model($oLabel, array('url' => route('acomodation.hotel-label-create') ,'method'=>'POST','enctype'=>'multipart/form-data')) }}
    @else
    {{Form::open(array('url' => 'acomodation/hotel-label-create','method'=>'Post','enctype'=>'multipart/form-data')) }}
    @endif
        <div class="box-wrapper">

            <p>Accommodation Labels Add</p>
            <div class="form-group m-t-30">
                <label class="label-control">Label name <span class="required">*</span></label>
                <?php
                $attributes = 'form-control';
                ?>
                {{Form::text('name',Input::old('name'),['id'=>'name','class'=>$attributes,'placeholder'=>'Enter Name'])}}
            </div>

            @if ( $errors->first( 'name' ) )
            <small class="error">{{ $errors->first('name') }}</small>
            @endif
            
            <div class="form-group m-t-30">
                <label class="label-control">Description </label>
                <?php 
                    $attributes ='form-control';
                ?>
                {{ Form::textarea('description',Input::old('description'), ['rows' => '5','id'=>'description','class'=>$attributes]) }}
                <!-- {{ Form::hidden('label_id',Input::old('id'), ['id'=>'id']) }} -->
            </div>
                    
        </div>
           
          
        <div class="m-t-20 row col-md-8 col-md-offset-2">
            <div class="row">
                <div class="col-sm-6">
                     <input type="hidden" value="{{ $nIdLabel }}" name="label_id" >
                {{Form::submit('Save',['class'=>'button success btn btn-primary btn-block']) }}
                </div>
                <div class="col-sm-6">
                  <a href="{{ route('acomodation.hotel-label-list')}}" class="btn btn-primary btn-block">Cancel</a>
                </div>
            </div>
        </div>        
    {{Form::close()}}
</div>
@stop

@section('custom-css')
<style>    
    .error{
        color:red !important;
    }
    .with_error{
        border-color: red !important;
    }
    
    div .with_error{
        border:1px solid black;
    }
    .error_message{
        color:red !important;
    }
    
    .success_message{
        color:green !important;
        text-align: center;
    }
</style>
@stop

@section('custom-js')
<script>
    tinymce.init({
        selector:'#description',
        height: 200,
        menubar: false
    });    
</script>
@stop
