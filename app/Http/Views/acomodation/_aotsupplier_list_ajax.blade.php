<table class="table">
    <thead>
        <tr>
            <th>
                <label class="radio-checkbox label_check" for="checkbox-00">
                    <input type="checkbox" id="checkbox-00" value="1" onchange="selectAllRow(this);">&nbsp;
                </label>
            </th>
            <th onclick="getSortData(this,'SupplierCode');">Supplier Code
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'SupplierCode')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getSortData(this,'Name');">Name  
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'Name')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getSortData(this,'LocationID');">Location Id  
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'LocationID')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getSortData(this,'LocationName');">Location Name  
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'LocationName')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getSortData(this,'Address1');">Address
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'Address1')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody class="hotel_list_ajax">
    @if(count($oHotelList) > 0)
        @include('WebView::acomodation._more_aotsupplier_list')
    @else
        <tr><td colspan="10" class="text-center">{{ trans('messages.no_record_found') }}</td></tr>
    @endif
    </tbody>
</table>
<div class="clearfix">
    <div class="col-sm-5"><p class="showing-result">{{ trans('messages.show_out_of_record',['current' => $oHotelList->count() , 'total'=>$oHotelList->total() ]) }}</p></div>
    <div class="col-sm-7 text-right">
      <ul class="pagination">
        
      </ul>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.pagination').pagination({
            pages: {{ $oHotelList->lastPage() }},
            currentPage: {{ $oHotelList->currentPage() }},
            displayedPages:2,
            edges:1,
            onPageClick(pageNumber, event){
                getPaginationListing(siteUrl('acomodation/hotel-aotsupplier-list?page='+pageNumber),event,'table_record');
//                if(pageNumber > 1)
//                    getMoreListing(siteUrl('acomodation/hotel-aotsupplier-list')+'?page='+pageNumber,event,'hotel_list_ajax');
//                else
//                    getMoreListing(siteUrl('acomodation/hotel-aotsupplier-list')+'?page='+pageNumber,event,'table_record');
                $('#checkbox-00').prop('checked',false);
                setupLabel();
            }
        });
    });
</script>