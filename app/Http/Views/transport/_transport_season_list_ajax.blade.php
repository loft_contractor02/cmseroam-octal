<table class="table">
    <thead>
        <tr>
            <th>
                <label class="radio-checkbox label_check" for="checkbox-00">
                    <input type="checkbox" id="checkbox-00" value="1" onchange="selectAllRow(this);">&nbsp;
                </label>
            </th>
            <!--onclick="getTransportSeasonSort(this,'fc.name');-->
            <th onclick="getTransportSeasonSort(event,this,'from_city_name');">{{ trans('messages.origin') }} <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'from_city_name')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></th>
            <th onclick="getTransportSeasonSort(event,this,'tc.name');" >{{ trans('messages.destination') }} <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'tc.name')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></th>
            <th onclick="getTransportSeasonSort(event,this,'tt.name');">{{ trans('messages.type') }} <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'tt.name')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></th>
            <th onclick="getTransportSeasonSort(event,this,'to.name');">{{ trans('messages.operator') }} <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'to.name')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></th>
            <th>{{ trans('messages.season_only_name') }}</th>
            <th>{{ trans('messages.from') }}</th>
            <th>{{ trans('messages.to') }}</th>
            <th>{{ trans('messages.pax') }}</th>
            <th>{{ trans('messages.allotment') }}</th>
            <th>{{ trans('messages.price') }}</th>
            <th>{{ trans('messages.supplier') }}</th>
			{{-- <th>{{ trans('messages.domains') }}</th> --}}
            <th width="17%" class="text-center">{{ trans('messages.action') }}</th>
        </tr>
    </thead>
    <tbody class="city_list_ajax">
        @if(count($oTransportSeasonList) > 0)
        @include('WebView::transport._more_transport_season_list')
        @else
        <tr><td colspan="10" class="text-center">{{ trans('messages.no_record_found') }}</td></tr>
        @endif
    </tbody>
</table>
<div class="clearfix">
    <div class="col-sm-5"><p class="showing-result">{{ trans('messages.show_out_of_record',['current' => $oTransportSeasonList->count() , 'total'=>$oTransportSeasonList->total() ]) }}</p></div>
    <div class="col-sm-7 text-right">
        <ul class="pagination">
            
        </ul>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $('.pagination').pagination({
            pages: {{ $oTransportSeasonList->lastPage() }},
            itemsOnPage: 10,
            currentPage: {{ $oTransportSeasonList->currentPage() }},
            displayedPages:2,
            edges:1,
            onPageClick(pageNumber, event){
                getPaginationListing(siteUrl('transport/transport-season-list?page='+pageNumber),event,'table_record');
//                if(pageNumber > 1)
//                    getMoreListing('{{ route('transport.transport-season-list')}}?page='+pageNumber,event,'city_list_ajax');
//                else
//                    getMoreListing('{{ route('transport.transport-season-list')}}?page='+pageNumber,event,'table_record');
                $('#checkbox-00').prop('checked',false);
            }
        });
    });
</script>