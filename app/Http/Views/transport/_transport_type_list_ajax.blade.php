<table class="table">
    <thead>
        <tr>
            <th>
                <label class="radio-checkbox label_check" for="checkbox-00">
                    <input type="checkbox" id="checkbox-00" value="1" onchange="selectAllRow(this);">&nbsp;
                </label>
            </th>
            <th>{{ trans('messages.sequence') }}</th>
            <th onclick="getTransportTypeSort(this,'name');">{{ trans('messages.name') }} <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'name')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></th>
            <th>{{ trans('messages.show_on_eroam') }}</th>
        </tr>
    </thead>
    <tbody class="city_list_ajax">
        @if(count($oTransportTypeList) > 0)
        @include('WebView::transport._more_transport_type_list')
        @else
        <tr><td colspan="10" class="text-center">{{ trans('messages.no_record_found') }}</td></tr>
        @endif
    </tbody>
</table>
<div class="clearfix">
    <div class="col-sm-5"><p class="showing-result">{{ trans('messages.show_out_of_record',['current' => $oTransportTypeList->count() , 'total'=>$oTransportTypeList->total() ]) }}</p></div>
    <div class="col-sm-7 text-right">
        <ul class="pagination">
            
        </ul>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $('.pagination').pagination({
            pages: {{ $oTransportTypeList->lastPage() }},
            itemsOnPage: 10,
            currentPage: {{ $oTransportTypeList->currentPage() }},
            displayedPages:2,
            edges:1,
            onPageClick(pageNumber, event){
                getPaginationListing(siteUrl('transport/transport-type-list?page='+pageNumber),event,'table_record');
//                if(pageNumber > 1)
//                    getMoreListing(siteUrl('transport/transport-type-list?page='+pageNumber),event,'city_list_ajax');
//                else
//                    getMoreListing(siteUrl('transport/transport-type-list?page='+pageNumber),event,'table_record');
                $('#checkbox-00').prop('checked',false);
            }
        });
    });
</script>