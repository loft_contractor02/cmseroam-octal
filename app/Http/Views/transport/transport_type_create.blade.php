@extends( 'layout/mainlayout' )

@section('content')
<div class="content-container">
    @if(isset($oTransportType))
        <h1 class="page-title">{{ trans('messages.update',['name' => 'Transport Type']) }}</h1>
    @else
        <h1 class="page-title">{{ trans('messages.add',['name' => 'Transport Type']) }}</h1>
    @endif
    <div class="row">
        @if (Session::has('message'))
        <div class="small-12 small-centered columns success-box">{{ Session::get('message') }}</div>
        @endif
        @if ($errors->any())
        <div class="small-6 small-centered columns error-box">{{$errors->first()}}</div>
        @endif
    </div>
    <br>
    <div class="box-wrapper">

        <p>{{ trans('messages.add_new_supplier') }}</p>
        @if(isset($oTransportType))
        {{ Form::model($oTransportType, array('url' => route('transport.transport-type-create') ,'method'=>'POST','enctype'=>'multipart/form-data','id'=>'addForm')) }}
        @else
        {{Form::open(array('url' => 'transport/transport-type-create','method'=>'Post','enctype'=>'multipart/form-data','id' => 'addForm')) }}
        @endif
        <div class="form-group m-t-30">

            <label class="label-control">{{ trans('messages.name') }}<span class="required">*</span></label>

            {{Form::text('name',Input::old('name'),['id'=>'name','class'=>'form-control','placeholder'=>'Enter Name'])}}
        </div>
        @if ( $errors->first( 'name' ) )
            <small class="error">{{ $errors->first('name') }}</small>
        @endif

        <div class="form-group m-t-30">
            <label class="label-control">Transport Mode</label>
            {{Form::select('transport_mode', array('single' => 'Single', 'combo' => 'Combo'),Input::old('transport_mode'),['id'=>'transport_mode','class'=>'form-control'])}}
        </div>
        @if ( $errors->first( 'transport_mode' ) )
            <small class="error">{{ $errors->first('transport_mode') }}</small>
        @endif


        <div class="form-group m-t-30">
            <label class="label-control">Description</label>
           {{Form::textarea('description',Input::old('description'),['id'=>'description','rows'=>5,'class'=>'form-control'])}}
        </div>
        @if ( $errors->first( 'description' ) )
        <small class="error">{{ $errors->first('description') }}</small>
        @endif


         <div class="row">  
            <div class="form-group col-md-10">
                <label for="allocation-type" class="label-control">Is Enabled</label>
                {{ Form::checkbox('is_enabled', 1, Input::old('is_enabled'), ['class' => 'show-on-eroam-btn switch1-state1']) }}
                <!--<input id="is-active" type="checkbox" name="is_active" class="show-on-eroam-btn switch1-state1"/>-->
                <label for="is-active"></label>
            </div>
        </div>

        <input type="hidden" value="{{ $nId }}" name="id" >  
        <div class="m-t-20 row col-md-8 col-md-offset-2">
        <div class="row">
            <div class="col-sm-6">
                {{Form::submit('Save',['class'=>'button success btn btn-primary btn-block']) }}
            </div>
            <div class="col-sm-6">
                <a href="{{ route('transport.transport-supplier-list')}}" class="btn btn-primary btn-block">{{ trans('messages.cancel_btn') }}</a>
            </div>
        </div>	
    {{Form::close()}}
</div>
@stop

@section('custom-css')
<style>
    .error{
        color:red !important;
    }
    .error_message{
        color:red !important;
    }
    .with_error{
        border-color: red !important;
    }
    .success_message{
        color:green !important;
        text-align: center;
    }
    div .with_error{
        border:1px solid black;
    }
</style>
@stop

@section('custom-js')
<script>
$('.switch1-state1').bootstrapSwitch();
    $(function () {

        tinymce.init({
            selector: '#description',
            height: 200,
            menubar: false
        });
        tinymce.init({
            selector: '#remarks',
            height: 200,
            menubar: false
        });
        tinymce.init({
            selector: '#special_notes',
            height: 200,
            menubar: false
        });
    });
$(function() {
    $( "#addForm" ).validate({
        rules: {
            name : 'required',
            transport_mode : 'required'
        },
        errorPlacement: function(error, element) {
            var placement = $(element).parent();
            if (placement) {
              $(error).insertAfter(placement)
            } else {
              error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
                form.submit();
            }
    });
});
</script>
@stop
