@extends( 'layout/mainlayout')
@section('custom-css')
<style>

    .error{
        color:red !important;
    }

    div .with_error{
        border:1px solid black;
    }
    .test{
        text-decoration: none;
    }
    .error_message{
        background: #f2dede;
        border: solid 1px #ebccd1;
        color: #a94442;
        padding: 11px;
        text-align: center;
        cursor: pointer;
    }
    .with_error{
        border-color: red !important;
    }
    .success_message{
        color:green !important;
        text-align: center;
    }
    .fa-plus-square{
        color:green;
        cursor:pointer;
    }
    .fa-minus-square{
        color:red;
        cursor:pointer;
    }

    .nav>li>a:focus, .nav>li>a:hover {
        background-color: #eee;
    }

    .address{
        margin-left: 45px;
        font-family: sans-serif;
        font-size: 14px;
    }
</style>
@stop
@section('content')

<div class="content-container" >
    <h1 class="page-title">Itenary Details</h1> 

    @include('WebView::booking.review_booking_menu')
    <div class="box-wrapper">
    <p class="h4">Personal Details</p><hr>
    <div class="row">
        <div class="col-sm-6">
            <p>Booking Id: {{!empty($personalDetails['invoice_no']) ? $personalDetails['invoice_no']:'N/A'}}</p>
        </div>
        <div class="col-sm-6">
            <p>Customer Name: {{!empty($personalDetails['first_name']) ? $personalDetails['first_name']." ".$personalDetails['last_name']:'N/A'}}</p>
        </div>
     </div>
     <div class="row">
        <div class="col-sm-6">
            <p>Customer Email: {{!empty($personalDetails['email']) ? $personalDetails['email']:'N/A'}}</p>
        </div>
        <div class="col-sm-6">
            <p>Customer Contact: {{!empty($personalDetails['contact']) ? $personalDetails['contact']:'N/A'}}</p>
        </div>
     </div>
     <div class="row">
        <div class="col-sm-6">
            <p>Customer Dob: {{!empty($personalDetails['dob']) ? date('d-m-Y',strtotime($personalDetails['dob'])):'N/A'}}</p>
        </div>
        <div class="col-sm-6">
            <p>Days: {{!empty($personalDetails['total_days']) ? $personalDetails['total_days']:'N/A'}}</p>
        </div>
     </div>
     <div class="row">
        <div class="col-sm-6">
            <p>Total Amount: {{!empty($personalDetails['total_amount']) ? $personalDetails['currency']."".$personalDetails['total_amount']:'N/A'}}</p>
        </div>
        <div class="col-sm-6">
            <p>Dates: <?php echo date("j M Y", strtotime($personalDetails['from_date']));?> - <?php echo date("j M Y", strtotime($personalDetails['to_date']));?></p>
        </div>
     </div>
     <div class="row">
        <div class="col-sm-6">
            <p>Total Paid Amount: {{!empty($personalDetails['total_amount']) ? $personalDetails['currency']."".$personalDetails['total_amount']:'N/A'}}</p>
        </div>
        <div class="col-sm-6">
            <p>Travellers: {{!empty($personalDetails['num_of_travellers']) ? $personalDetails['num_of_travellers']:'N/A'}}</p>
        </div>
     </div>
</div>
    @foreach($cities_arr as $key=>$val)
 <div class="box-wrapper">
        
        <p class="h4">{{ucfirst($val)}} , {{$cities_codes[$key]}}</p>
        @foreach($product_details_all[$val] as $productkey=>$productval)
        <?php 
        if ($productval['supplier_id'] != '') {
            $supplier = $productval['supplier_id'];
            $product = "Dynamic";
        } elseif ($productval['provider'] != '') {
            $supplier = $productval['provider'];
            $product = "Dynamic";
        } else {
            $supplier = 'Eroam';
            $product = "Static";
        }
        ?>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid">
                    <div class="box-body">
                        <div class="box-group" id="accordion">
                            <div class="box box-success">
                                <div class="box-header with-border">
                                    <p class="h4">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree{{$productval['leg_detail_id']}}">
                                            {{ucfirst($productkey)}}
                                        </a>
                                    </p>
                                </div>
                                <hr>
                                <div id="collapseThree{{$productval['leg_detail_id']}}" class="panel-collapse ">
                                    <div class="box-body">
                                        @if(($productkey == 'hotel') && ($productval['leg_name'] != "Own Arrangement"))
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <p>Accommodation Name:  {{$productval['leg_name']}}</p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p>Bed Preferences: {{!empty($productval['hotel_room_type_name'])?$productval['hotel_room_type_name']:'N/A'}}</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <p>Accommodation Address Details: {{!empty($productval['address'])?$productval['address']:'N/A'}}</p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p>Duration  : {{!empty($productval['nights']) ? $productval['nights']:'N/A'}}</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <p>Check In Time : {{!empty($productval['checkin_time']) ? $productval['checkin_time']:'N/A'}}</p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p>Check Out Time : {{!empty($productval['checkout_time']) ? $productval['checkout_time']:'N/A'}}</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <p>Check In Date : {{!empty($productval['city_from_date']) ? $productval['city_from_date']:'N/A'}}</p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p>Check Out Date : {{!empty($productval['city_to_date']) ? $productval['city_to_date']:'N/A'}}</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <p>Hotel Fee: {{!empty($productval['price']) ? $productval['currency']." ". $productval['price']:'N/A'}}</p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p>Cancellation Policy :{{!empty($productval['cancellation_policy']) ? $productval['cancellation_policy']:'N/A'}}</p>
                                            </div>
                                        </div>
                                        @endif
                                        <?php if($productkey== 'hotel' && $productval['leg_name'] == "Own Arrangement"){ ?>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    Own Arrangement  
                                                </div>
                                                <div class="col-sm-6">
                                                    <a href="javascript:void(0)" class="btn btn-info activityButton" data-id="{{$productval['leg_detail_id']}}" data-type="hotel">Add Notes</a>
                                                </div>
                                                
                                            </div>
                                            <?php if(!empty($productval['notes'])){ ?>
                                                <div class="row">
                                                    <div class="col-sm-6 addHotelDiv_{{$productval['leg_detail_id']}}">
                                                       <b>Additional Notes: </b> {{$productval['notes']}}
                                                    </div>
                                                </div>
                                                <?php }else{ ?>
                                            <div class="row">
                                                <div class="col-md-6 addHotelDiv_{{$productval['leg_detail_id']}}"></div>
                                            </div>
                                        <?php } } ?>
                                        @if(($productkey == 'activities')&& ($productval['leg_name'] != "Own Arrangement"))
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <p>Activity Name:  {{$productval['leg_name']}}</p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p>Date: {{!empty($productval['from_date'])?date( 'j M Y', strtotime($value1['from_date'])):'N/A'}}</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <p>Number Purchased: {{$total_travller}}</p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p>Date of Activity  : {{!empty($productval['city_from_date']) ? $productval['city_from_date']:'N/A'}}</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <p>Price: {{!empty($productval['price']) ? $productval['currency']." ".$productval['price']:'N/A'}}</p>
                                            </div>
                                            <?php if($productval['provider_booking_status'] == 'CF'){ ?>
                                            <div class="col-sm-6">
                                                <p>Duration of Activity : {{!empty($productval['duration']) ? $productval['duration']:'N/A'}}</p>
                                            </div>
                                            <?php }else{ ?>
                                            <div class="col-sm-6"><strong>Booking Status: </strong>Not Confirmed *</div>
                                            <?php } ?>
                                        </div>
                                        @if($productval['provider'] == 'viator')
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <p>Provider Name: Viator</p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p>Voucher Link: <a href="{{$productval['voucher_url']}}" target="_blank">Click Here</a></p>

                                            </div>
                                        </div>
                                        @endif
                                        @endif
                                        <?php if($productkey== 'activities' && $productval['leg_name'] == "Own Arrangement"){ ?>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        Own Arrangement  
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <a href="javascript:void(0)" class="btn btn-info activityButton" data-id="{{$productval['leg_detail_id']}}" data-type="activity">Add Notes</a>
                                                    </div>
                                                    
                                                </div>
                                                <?php if(!empty($productval['notes'])){ ?>
                                                    <div class="row">
                                                        <div class="col-sm-6 addActivityDiv_{{$productval['leg_detail_id']}}">
                                                           <b>Additional Notes: </b> {{$productval['notes']}}
                                                        </div>
                                                    </div>
                                                    <?php }else{ ?>
                                                <div class="row">
                                                    <div class="col-md-6 addActivityDiv_{{$productval['leg_detail_id']}}"></div>
                                                </div>
                                        <?php } }?>
                                        @if($productkey == 'transport' && $productval['leg_name'] != "Own Arrangement")
                                         <?php
                                            $depart = $productval['departure_text'];
                                            $arrive = $productval['arrival_text'];
                                            if($productval['leg_name'] == 'Flight'){
                                                if(!empty($productval['booking_summary_text'])){
                                                    $leg_depart = explode("Depart:",$productval['booking_summary_text'],2);
                                                    $leg_arrive = explode("Arrive:",$productval['booking_summary_text'],2);

                                                    if(isset($leg_depart[1])){
                                                        $depart = explode("</small>",$leg_depart[1],2);
                                                        $depart = $depart[0];
                                                    }else{
                                                        $depart = '';
                                                    }
                                                    if(isset($leg_arrive[1])){
                                                        $arrive = explode("</small>",$leg_arrive[1],2);
                                                        $arrive = $arrive[0];
                                                    }else{
                                                        $arrive = '';
                                                    }
                                                }else{
                                                    $depart = '';
                                                    $arrive = '';
                                                }
                                            }
                                            if($productval['provider'] == 'busbud'){
                                                $depart = $productval['departure_text'];
                                                $arrive = $productval['arrival_text'];
                                            }
                                            $leg_name = explode('Depart:', $productval['booking_summary_text']);
                                                if(isset($productval['booking_summary_text']) && !empty($productval['booking_summary_text'])){
                                                    $leg_name = trim(strip_tags($leg_name[0]));
                                                }else{
                                                    $leg_name = strip_tags($productval['leg_name']);
                                                }
                                            ?>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <p>Service: {{$leg_name}}</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <p>Depart: {{strip_tags($depart)}}</p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p> Arrive: {{strip_tags($arrive)}}</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <p>Price: {{!empty($productval['price']) ? $productval['currency'].$productval['price']:'N/A'}}</p>
                                            </div>
                                            <?php if($productval['provider_booking_status'] == 'CF'){ ?>
                                            <div class="col-sm-6">
                                                <p>Booking Id: {{!empty($productval['booking_id']) ? $productval['booking_id']:'N/A'}} </p>
                                            </div>
                                         <?php } else{ ?>
                                            <div class="col-sm-6">
                                                <p>Booking Id: Not Confirmed * </p>
                                            </div>
                                        <?php } ?>
                                        </div>
                                        @endif
                                        <?php if($productval['leg_type'] == 'transport' && $productval['leg_name'] == "Own Arrangement"){ ?>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                Own Arrangement  
                                            </div>
                                            <div class="col-sm-6">
                                                <a href="javascript:void(0)" class="btn btn-info activityButton" data-id="{{$productval['leg_detail_id']}}" data-type="transport">Add Notes</a>
                                            </div>
                                            
                                        </div>
                                        <?php if(!empty($productval['notes'])){ ?>
                                            <div class="row">
                                                <div class="col-sm-6 addTransportDiv_{{$productval['leg_detail_id']}}">
                                                   <b>Additional Notes: </b> {{$productval['notes']}}
                                                </div>
                                            </div>
                                            <?php }else{ ?>
                                        <div class="row">
                                            <div class="col-md-6 addTransportDiv_{{$productval['leg_detail_id']}}"></div>
                                        </div>
                                        <?php } } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>  
    @endforeach
	<div class="m-t-20 row">
		<div class="col-sm-offset-2 col-sm-8">
			<div class="row">
				<div class="col-sm-4">
					 
				</div>
				<div class="col-sm-3">
				  <div class="col-md-12"><a href="{{ route('booking.booking-itenary-detail-email',['nItenaryId'=>$nBookId])}}" class="btn btn-primary btn-block">Send Itinerary</a></div>
				</div>
			</div>
		</div>
    </div>
    <div class="m-t-20 row">
		<div class="col-sm-offset-2 col-sm-8">
			<div class="row">
				<div class="col-sm-6">
					<a href="{{ route('booking.booking-supplier-detail',['nItenaryId'=>$nBookId]) }}" class="btn btn-primary btn-block">Previous</a>
				</div>
				<div class="col-sm-6">
				   <a href="{{ route('booking.booking-voucher-detail',['nItenaryId'=>$nBookId]) }}" class="btn btn-primary btn-block">Next</a>
				</div>
			</div>
		</div>
    </div>


<script type="text/javascript">
$(".activityButton").click(function(){
    showLoader();
    var lagDetailsId = $(this).attr("data-id");
    var buttonType = $(this).attr("data-type");
    $.ajax({
        method: 'post',
        url: '{{ url('update-own-arrangement') }}',
        data: {
        _token: '{{ csrf_token() }}',
                lagDetailsId: lagDetailsId,
                buttonType:buttonType
        },
        success: function(response) {
            if( buttonType == "activity"){
                $(".addActivityDiv_"+lagDetailsId).html(response);
            }if( buttonType == "transport"){
                $(".addTransportDiv_"+lagDetailsId).html(response);
            }if( buttonType == "hotel"){
                $(".addHotelDiv_"+lagDetailsId).html(response);
            }
            hideLoader();
        }
    });
});  
</script>
@stop


