@foreach ($oBookingRequest as $aTour)				
    <tr>
		<td> {{ $aTour->tour_code }}</td>
        <td> {{ $aTour->FName }} {{ $aTour->SurName }}</td>
		<td> {{ $aTour->request_date }}</td>
        <td> {{ $aTour->departure_date }}</td>
        <td> {{ $aTour->return_date }}</td>
        <td style="text-align: center;"> {{ getAgentName($aTour->Agent_id)}}</td>
        <td class="text-center">
        <a href="{{ route('booking.tour-pax-detail',['nItenaryId'=>$aTour->request_id])}}" class="button success tiny btn-primary btn-sm">View</a>
		</td>
    </tr> 
@endforeach