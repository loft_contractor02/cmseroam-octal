@extends( 'layout/mainlayout')
@section('custom-css')
<style>

    .error{
        color:red !important;
    }

    div .with_error{
        border:1px solid black;
    }
    .test{
        text-decoration: none;
    }
    .error_message{
        background: #f2dede;
        border: solid 1px #ebccd1;
        color: #a94442;
        padding: 11px;
        text-align: center;
        cursor: pointer;
    }
    .with_error{
        border-color: red !important;
    }
    .success_message{
        color:green !important;
        text-align: center;
    }
    .fa-plus-square{
        color:green;
        cursor:pointer;
    }
    .fa-minus-square{
        color:red;
        cursor:pointer;
    }

    .nav>li>a:focus, .nav>li>a:hover {
        background-color: #eee;
    }
    @import url("https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css");
    .panel-default 
    {
        border-color: transparent;
    }
    .panel-default > .panel-heading
    {
        background-color: transparent;
        border-color: transparent;
        padding: 0px 0;
    }
    .panel-group .title
    {
        cursor: pointer;
    }
    .panel-group .title span
    {
        font-size: 16px;
        font-family: 'Open Sans', sans-serif;
        color: #464646;
        font-weight: bold;
        text-transform: uppercase;
    }
    .panel-heading .title:before {

        font-family: FontAwesome;
        content:"\f106";
        font-size: 25px;
        padding-right: 10px;
        line-height: 25px;
        float: right;
    }
    .panel-heading .title.collapsed:before {
        font-size: 25px;
        padding-right: 10px;
        line-height: 25px;
        float: right;
        content:"\f107";
    }
    .panel-body{
        padding: 5px !important;
    }
</style>
@stop
@section('content')

<div class="content-container" style="overflow:hidden;">
    <h1 class="page-title">{{ trans('messages.product_details') }}</h1> 

    <div class="container">
        <ul class="nav nav-pills nav-tabs">
            <li ><a href="{{ route('booking.tour-pax-detail',['nItenaryId'=>$request_id])}}">PAX Details</a></li>
            <li class="active"><a href="{{ route('booking.tour-product-detail',['nItenaryId'=>$request_id])}}">Product Details</a></li>
            <li><a href="{{ route('booking.tour-supplier-detail',['nItenaryId'=>$request_id])}}">Supplier Details</a></li>
        </ul>
    </div>
    </br>
    </br>
	<div class="box-wrapper">
        <p class="h4">Tour Products Details</p>
        <hr>
			<div class="panel-body">
				<table class="table table-responsive">
				<?php  
					if ($productDetials['provider'] != '') {
						$supplier = getProviderName($productDetials['provider']);
						$product = "Dynamic";
					} else {
						$supplier = 'Eroam';
						$product = "Static";
					}
				?>
					<tbody>
						<tr>
							<td>Product Name : {{!empty($productDetials['tour_title']) ? $productDetials['tour_title']:'N/A'}}
							<td>Product Type / Description : {{!empty($productDetials['short_description']) ? $productDetials['short_description']:'N/A'}}</td>
						</tr>
						<tr>
							<td>Number of Purchased : N/A</td>
							<td>Date of Tour: {{!empty($productDetials['departure_date']) ? $productDetials['departure_date']:'N/A'}}</td>
						</tr>
						<tr>
							<td>Time of Tour :  N/A</td>
							<td>Duration of Tour : {{!empty($productDetials['no_of_days']) ? $productDetials['no_of_days']:'N/A'}}</td>
						</tr>
						<tr>
							<td>Supplier  : {{$supplier}}</td>
							<td>Dynamic / Static Product : {{$product}}</td>
						</tr>
						<tr>
							<td>Retail Price : {{!empty($productDetials['reseller_cost']) ? $productDetials['reseller_cost']:'N/A'}}</td>
							<td>Sell Price : {{!empty($productDetials['sellprice']) ? $productDetials['sellprice']:'N/A'}}</td>
						</tr>
						<tr>
							<td>Commission  : {{!empty($productDetials['licensee_benefit']) ? $productDetials['licensee_benefit']:'N/A'}}</td>
							<td>Supplier Booking ID : {{!empty($productDetials['provider']) ? $productDetials['provider']:'N/A'}}</td>
						</tr>
						<tr>
                            <td>Booking Status : N/A </td>
							<td>Additional Notes :{{!empty($productDetials['tour_notes']) ? $productDetials['tour_notes']:'N/A'}}</td>
						</tr>
					</tbody></table>
			</div>
        </div>
		<div class="m-t-20 row">
            <div class="col-sm-offset-2 col-sm-8">
                <div class="row">
                    <div class="col-sm-6">
                        <a href="{{ route('booking.tour-pax-detail',['nItenaryId'=>$request_id]) }}" class="btn btn-primary btn-block">Previous</a>
                    </div>
                    <div class="col-sm-6">
                       <a href="{{ route('booking.tour-supplier-detail',['nItenaryId'=>$request_id]) }}" class="btn btn-primary btn-block">Next</a>
                    </div>
                </div>
            </div>
        </div>
	</div> 
	@stop