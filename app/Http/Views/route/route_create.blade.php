@extends( 'layout/mainlayout' )

@section('custom-css')
<link rel="stylesheet" type="text/css" href="{{ url('assets/css/map.css') }}">
<style type="text/css">
    .ui-widget {
        font-size: 13px;
    }
    .ui-widget-content {
        font-size: 13px;
        z-index: 9999;
        max-height: 500px;
        overflow-y: auto; 
        overflow-x: hidden;
    }
    #map {
        padding: 0;
        margin: 0;
        height: 450px;
        background: #EAEAEA;
    }
    #route-plan {
        max-height: 400px;
        overflow-y: auto;
        overflow-x: hidden;
    }
    #route-plan p {
        display: block;
        width: 100%;
        padding: 11px 15px;
        margin-bottom: 10px;
        font-size: 13px;
        background: #008cba;
        color: #ffffff;
    }
    #route-plan p a.remove-city {
        float: right;
    }
    #route-plan i {
        color: #ffffff;
    }
    #save-route-plan-btn {
        display: none;
        margin-bottom: 1.5rem;
    }
    #add-city-btn {
        display: none;
    }
</style>
@stop

@section('content')
<div class="content-container">
    <h1 class="page-title">{{ trans('messages.update',['name' => 'Route']) }}</h1>
    <div class="row">
        @if (Session::has('message'))
            <div class="small-12 small-centered columns success-box">{{ Session::get('message') }}</div>
        @endif

    </div>	
    <br>
    <div id="route-plan-container" class="full-width-row side-padding">
        <div id="map-loader">
            <h5><i class="fa fa-circle-o-notch fa-spin"></i> LOADING MAP</h5>
        </div>
        <div id="map"></div>
        <p style="font-size: 14px" class="m-t-10"><i class="fa fa-info-circle"></i> 
            <b>Note: If you create a route plan that already exists based on it's starting point &amp; destination, it will create an alternative route to it instead.
                <br/> &nbsp;&nbsp;&nbsp;At least three cities required to create a Route.</b>
           </p>
        <div class="m-t-20 col-md-8">
            
            <div class="form-group m-t-30">
                <label class="label-control">Name<span class="required">*</span></label>
                <input id="add-city" type="text" class="autocomplete-city form-control" placeholder="Enter a City" autofocus>
                <a href="#" id="add-city-btn" class="button tiny"><i class="fa fa-plus"></i> Add</a>
<!--                <input type="text" id="name" class="form-control" placeholder="Enter Name" name="name" value="">-->
            </div>
           
            <a href="#" id="save-route-plan-btn" class="button tiny success btn btn-primary">Save Route Plan</a>
            <div id="route-plan" class="m-t-10">
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="city-list" value='{{ json_encode($cities) }}'>
<input type="hidden" id="new-location">
@stop

@section('custom-js')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD14SXDwQRXOfUrldYNP-2KYVOm3JjZN9U&libraries=places,geometry,drawing&v=3&client=gme-adventuretravelcomau"></script>

<script type="text/javascript" src="{{ url('assets/js/markerlabel.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/js/infobox.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/js/compass.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/js/eroam-map.js') }}"></script>
<script type="text/javascript">
	var RoutePlan = (function(Map) {
		var cities = JSON.parse($('#city-list').val());

			// METHODS
			function populateAutoComplete() {
				var citySource = [];
				$.each(cities, function(k, v) {
					found = Map.get().markers.some(function (value, key) {
                                                    return value.cityId == v.id;
                                                });
					// Check if not yet added
					if (!found) {
                                            if(v.country != null)
                                                citySource.push({value: v.id, label: v.name + ', ' + v.country.name});
                                            else
                                                citySource.push({value: v.id, label: v.name});
					}
				});

				$('.autocomplete-city').autocomplete({
					source: citySource,
					select: function(event, ui) {
						event.preventDefault();
						var label = ui.item.label,
							value = ui.item.value;
						$(this).val(label);
						$('#new-location').val(value);
						$('#add-city-btn').click();
					},
					focus: function(event, ui) {
						event.preventDefault();
					}
				});
			}

			function addCity(event) {
                            debugger;
				event.preventDefault();
				if ($('#new-location').val() != '') {
					$.grep (cities, function(v, k) {
						if (v.id == $('#new-location').val()) {
							Map.addLocation(v);
							populateAutoComplete();
							updateRoutePlan();
						}
					});
					$('#add-city').val('').focus();
					$('#new-location').val('');
					$('#route-plan').scrollTop($('#route-plan')[0].scrollHeight);
				}
			}

			function removeCity(event) {
				event.preventDefault();
				var index = $(this).data('marker-index');
				var parent = $(this).parent('p');
				Map.removeLocation(index);
				parent.fadeOut(500, function() {
					populateAutoComplete();
					updateRoutePlan();
					parent.remove();
				});
			}

			function updateRoutePlan() {
				var markers = Map.get().markers;
				if (markers.length >= 3) {
					$('#save-route-plan-btn').css('display', 'block');
				} else {
					$('#save-route-plan-btn').css('display', 'none');
				}
				$('#route-plan').html('');
				$.each(markers, function(key, value) {
					$('#route-plan').append('<p>' + value.city.name + ', ' + value.city.country.name + ' <a href="#" class="remove-city" data-marker-index="'+ key +'"><i class="fa fa-times"></i></a></p>');
				});
			}

			function save(event) {
				event.preventDefault();
				var cityIds = [];
				$(this).html('<i class="fa fa-spin fa-circle-o-notch"></i> Saving route ');

				$.each(Map.get().markers, function(key, value) {
					cityIds.push(value.cityId);
				});
				$.ajax({
					method: 'post',
					url: siteUrl('/route/route-create'),
					data: {
						cityIds: cityIds,
						_token: '{{ csrf_token() }}'
					},
					success: function(response) {
						window.location.href = siteUrl('route/route-view/' + response.route_plan_id);
					}
				});
			}

			function init() {
				Map.init();
				populateAutoComplete();
			}

		return {
			init: init,
			addCity: addCity,
			removeCity: removeCity,
			save: save
		};

	})(Map);

	$(document).ready(function() {
		RoutePlan.init();
		$('#add-city-btn').click(RoutePlan.addCity);
		$('#add-city').keypress(function(e) {
			if (e.which == 13) {
				$('#add-city-btn').click();
			}
		});

		$('body').on('click', '.remove-city', RoutePlan.removeCity);
		$('#save-route-plan-btn').click(RoutePlan.save);
	}); // end document ready
</script>
@stop