@if(isset($cregion) && $cregion != null)
<div class="col-sm-12 cregion{{$cregion->id}} m-t-10">
	<p><b>{{$cregion->name}}</b></p>
	@if(count($countries) < 1)
	<p>No countries available in this region.</p>
	<hr>
	@endif
</div>
@endif

@foreach($countries as $rKey => $region)
	@if(!isset($cregion))
	<div class="col-sm-12 cregion{{$rKey}} m-t-10">
		<p><b>{{$regions[$rKey]}}</b></p>
	</div>
	@endif
	@foreach($region as $country)
		<div class="col-sm-6">
		    <div class="form-group">
			
		        <label class="radio-checkbox label_check m-t-10 @if(isset($sel_countries) && in_array($country['id'],$sel_countries)) c_on @endif" for="country{{$country['id']}}"><input class="geocountries" type="checkbox" @if(isset($sel_countries) && in_array($country['id'],$sel_countries)) checked @endif id="country{{$country['id']}}" value="{{$country['id']}}" data-region="{{$country['region_id']}}">{{$country['name']}}
				</label>
				
		    </div>
		</div>
	@endforeach
@endforeach