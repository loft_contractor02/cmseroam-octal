@extends( 'layout/mainlayout' )
@section('content')
<div class="content-container">
    <div class="m-t-10">
        @include('WebView::onboarding.includes.onboarding-steps')
    </div>
    <div class="alert" role="alert" id="error_msg"></div>
    <h1 class="page-title">Step 4: GEO Data</h1>
    <form method="post" action="@isset($sel_countries){{route('geodata.update',[$licensee_id,$domain_id])}}@else{{route('geodata.add')}}@endif" class="save-geodata">
        {{csrf_field()}}
        @isset($sel_countries)
            @method('PUT')
        @endisset
        <input type="hidden" value="{{route('geodata.getGeodata')}}" id="geodata">
        <input type="hidden" value="{{$licensee_id}}" name="licensee_id" id="licensee_id">
        <input type="hidden" value="{{$domain_id}}" name="domain_id" id="domain_id">
        <div class="box-wrapper">
            <h5>Regions</h5>
            <div class="m-t-10 row">
                @foreach($regions as $rKey => $region)
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="radio-checkbox label_check m-t-10 @if(isset($sel_regions) && in_array($rKey,$sel_regions)) c_on @endif" for="region{{$rKey}}"><input class="georegions" type="checkbox" @if(isset($sel_regions) && in_array($rKey,$sel_regions)) checked @endif id="region{{$rKey}}" value="{{$rKey}}">{{$region}}</label>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="box-wrapper slim-scroll maxheight">
            <h5>Countries</h5>
            <div class="m-t-10 row countries">
                <div class="overlay"></div>
                @include('WebView::onboarding.includes.geocountries')
                @if(!isset($sel_countries))
                <div class="col-sm-12 noresults_country">
                    <p>Please select regions to fetch countries.</p>
                </div>
                @endif
            </div>
        </div>
        <div class="box-wrapper slim-scroll maxheight">
            <h5>Cities</h5> <button class="btn btn-primary btn-sm pull-right" id="clearall">Clear All</button>
            <div class="m-t-10 row cities" id="citiesList">
                @include('WebView::onboarding.includes.geocities')
                @if(!isset($sel_cities))
                <div class="col-sm-12 noresults_city">
                    <p>Please select countries to fetch cities.</p>
                </div>
                @endif
            </div>
        </div>
        <div class="m-t-20 row">
            <div class="col-sm-8">
                <div class="error_here"></div>
            </div>            
        </div>
        <div class="m-t-20 row">
            <div class="col-sm-offset-4 col-sm-8">
                <div class="row">
                    @if(isset($sel_countries))
                    <div class="col-sm-6">
                        <button type="sumbit" name="" class="btn btn-primary btn-block">Update</button>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="m-t-20 row">
            <div class="col-sm-offset-2 col-sm-8">
                <div class="row">
                    <div class="col-sm-6">
                        <a href="{{ route('onboarding.backend',[$licensee_id,$domain_id]) }}" class="btn btn-primary btn-block">Previous</a>
                    </div>
                    <div class="col-sm-6 ">
                        @if(isset($sel_countries))
                            <a href="{{ route('onboarding.inventory',[$licensee_id,$domain_id]) }}" class="btn btn-primary btn-block">Next</a>
                        @else
                            <button type="sumbit" name="" class="btn btn-primary btn-block">Next</button>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
@push('scripts')
<script type="text/javascript" src="{{ asset('assets/js/blockui.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/onboarding.js') }}"></script>
@endpush