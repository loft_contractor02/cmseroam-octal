@extends( 'layout/mainlayout' )
@section('content')

    <div class="content-container">
        <div class="m-t-10">
            <?php
            if(isset($licensee_id) && $licensee_id!=''){ $licensee_id = $licensee_id; } else { $licensee_id = ''; }
            ?>
            @include('WebView::onboarding.includes.onboarding-steps', array('id' => $licensee_id))
        </div>
        <div class="alert" role="alert" id="error_msg"></div>
        <h1 class="page-title">Step 4: GEO Data</h1>
        <div class="formbox">
            @if($total < 1)
                <form class="add-form" method="post" id="save-geodata" action="{{route('geodata.add')}}">
                    <input type="hidden" name="licensee_id" value="{{ $licensee_id }}">
                    <div class="box-wrapper">
                        <a data-toggle="collapse" data-parent="#accordion" href="#firstdomain"><p>Consumer GEO Data</p></a>
                        <div class="collapse" id="firstdomain">
                            <div class="m-t-10 row">
                                <input type="checkbox" value="region" name="data[]" />&nbsp;&nbsp;Region
                            </div>

                            <div class="m-t-10 row">
                                <input type="checkbox" value="country" name="data[]" />&nbsp;&nbsp;Country
                            </div>

                            <div class="m-t-10 row">
                                <input type="checkbox" value="city" name="data[]" />&nbsp;&nbsp;City
                            </div>

                            <div class="m-t-20 row">
                                <div class="col-sm-8">
                                    <div class="error_here"></div>
                                </div>
                            </div>
                            <div class="m-t-20 row">
                                <div class="col-sm-offset-4 col-sm-8">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <?php if(isset($extrasDmains) && $extrasDmains->count() > 0) { ?>
                                            <button type="sumbit" data-id="{{$licensee_id}}" name="" class="btn btn-primary btn-block save_geodata">Update</button>
                                            <?php } else { ?>
                                            <button type="sumbit" data-id="{{$licensee_id}}" name="" class="btn btn-primary btn-block save_geodata">Save</button>
                                            <?php } ?>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            @endif
            <?php $x=1; ?>
            @foreach($domain_ids as $domain_id)
                <form method="post" action="@if(isset($extrasDmains) && $extrasDmains->count() > 0){{route('geodata.update',[$licensee_id,$domain_id])}}@else{{route('geodata.add')}}@endif" class="save-geodata">
                    {{csrf_field()}}
                    <input type="hidden" value="{{route('geodata.getGeodata')}}" id="geodata">
                    <input type="hidden" value="{{$licensee_id}}" name="licensee_id" id="licensee_id">
                    <input type="hidden" value="{{$domain_id}}" name="domain_id" id="domain_id">
                    <div class="box-wrapper">
                        <a data-toggle="collapse" data-parent="#accordion" href="#firstdomain{{$x}}"><p>Consumer GEO Data</p></a>
                        <div class="collapse" id="firstdomain{{$x}}">
                            <div class="m-t-10 row">
                                <input type="checkbox" value="region"
                                       @php
                                           if(isset($extrasDmains) && $extrasDmains->count() > 0 ){
                                               foreach($extrasDmains as $key=>$edomain):
                                                   if(($edomain->type =='region' ) && ($edomain->domain_id == $domain_id )){ echo 'checked'; }
                                               endforeach;
                                           }
                                       @endphp
                                       name="data_{{$domain_id}}[]" />&nbsp;&nbsp;Region
                            </div>

                            <div class="m-t-10 row">
                                <input type="checkbox" value="country"
                                       @php
                                           if(isset($extrasDmains) && $extrasDmains->count() > 0 ){
                                               foreach($extrasDmains as $key=>$edomain):
                                                   if(($edomain->type =='country') && ($edomain->domain_id == $domain_id )){ echo 'checked'; }
                                               endforeach;
                                           }
                                       @endphp
                                       name="data_{{$domain_id}}[]" />&nbsp;&nbsp;Country
                            </div>

                            <div class="m-t-10 row">
                                <input type="checkbox" value="city"
                                       @php

                                           if(isset($extrasDmains) && $extrasDmains->count() > 0 ){
                                               foreach($extrasDmains as $key=>$edomain):
                                                   if(($edomain->type =='city') && ($edomain->domain_id == $domain_id )){ echo 'checked'; }
                                               endforeach;
                                           }

                                       @endphp
                                       name="data_{{$domain_id}}[]" />&nbsp;&nbsp;City
                            </div>

                            <div class="m-t-20 row">
                                <div class="col-sm-8">
                                    <div class="error_here"></div>
                                </div>
                            </div>
                            <div class="m-t-20 row">
                                <div class="col-sm-offset-4 col-sm-8">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <?php if(isset($extrasDmains) && $extrasDmains->count() > 0) { ?>
                                            <button type="sumbit" data-id="{{$licensee_id}}" data-val="{{$domain_id}}" name="" class="btn btn-primary btn-block save_geodata">Update</button>
                                            <?php } else { ?>
                                            <button type="sumbit" data-id="{{$licensee_id}}" data-val="{{$domain_id}}" name="" class="btn btn-primary btn-block save_geodata">Save</button>
                                            <?php } ?>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <?php $x++; ?>
            @endforeach
            <div class="m-t-20 row">
                <div class="col-sm-offset-2 col-sm-8">
                    <div class="row">
                        <div class="col-sm-6">
                            <a href="{{ route('onboarding.backend',[$licensee_id]) }}" class="btn btn-primary btn-block">Previous</a>
                        </div>
                        <div class="col-sm-6 ">
                            <a href="{{ route('onboarding.inventory',[$licensee_id]) }}" class="btn btn-primary btn-block">Next</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script type="text/javascript" src="{{ asset('assets/js/blockui.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/onboarding.js') }}"></script>
@endpush