@foreach ($oUserList as $aUser)				
    <tr>
        <td>
            <label class="radio-checkbox label_check" for="checkbox-<?php echo $aUser->id;?>">
                <input type="checkbox" class="cmp_check" id="checkbox-<?php echo $aUser->id;?>" value="<?php echo $aUser->id;?>">&nbsp;
            </label>
        </td>
        <td>
            <a href="#">
                {{ $aUser->name }}
            </a>
        </td>
        <td>{{ $aUser->username }}</td> 
        <td>{{ $aUser->type }}</td>            
        <td class="text-center"><i class="fa fa-{{ $aUser->active == 1 ? 'check' : 'times' }}"></i></td>
        <td class="text-center">
            @if($aUser->type!='admin')
            <div class="switch tiny switch_cls">
             <?php $routedaynamic='user.create-'.$aUser->type;?>   
            <a href="{{ route($routedaynamic,[ 'user_id' => $aUser->id ])}}" class="button success tiny btn-primary btn-sm">{{ trans('messages.update_btn') }}</a>
            @if(($aUser->active)=='1')
            <input type="button" class="button btn-delete tiny btn-primary btn-sm" value="{{ trans('messages.inactive_btn') }}" onclick="callPublishRecord(this,'{{ route('user.active',['userId'=> $aUser->id]) }}')">
            @endif
            @if(($aUser->active)!='1')
            <input type="button" class="button btn-delete tiny btn-primary btn-sm" value="{{ trans('messages.active_btn') }}" onclick="callPublishRecord(this,'{{ route('user.active',['userId'=> $aUser->id]) }}')">
            @endif   
        </div>  
        @endif  
        </td>	     
    </tr> 
@endforeach