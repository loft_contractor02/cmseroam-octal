@extends( 'layout/mainlayout' )
@section( 'content')
<style type="text/css"> 
.card-body {
    height: 100px;
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 30px;
}
.custom-box{
    height: 100px !important;
}
</style>
<div class="content-container">
    <h1 class="hide"></h1>
    <section>
        <h2 class="page-title">Agent Dashboard</h2>
        <div class="row">
            <div class="col-sm-12">
                <ul class="nav navbar-nav">
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle btn btn-primary" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Create New Booking</a>
                    <ul class="dropdown-menu">
                      <li><a href="{{ route('agent.create-customer') }}">Create New Consumer</a></li>
                      <li><a href="{{ route('agent.list-customer') }}">Search Existing Consumer</a></li>
                    </ul>
                  </li>
                </ul>
                
            </div>
        </div>
        <div class="row m-t-20">
            <div class="col-sm-4">
                <div class="dashboard-box-wrapper">
                    <div class="dashboard-box-inner">
                        <div class="dashboard-box custom-box">
                            <div class="row">
                                <div class="col-sm-10">
                                    <p>Number of Bookings</p>
                                </div>
                            </div>
                            <div class="card">
                              <div class="card-body"><span class="bookingSpan">{{$bookingCount}}</span></div>
                            </div>
                           
                        </div>
                    </div>
                    <div class="box-border">
                        {{ Form::select('is_active',array('Day'=>'Day','Week'=>'Week','Month'=>'Month','Year'=>'Year'),'Month', ['class' => 'form-control choosen_selct','id'=>'duration']) }}
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="dashboard-box-wrapper">
                    <div class="dashboard-box-inner">
                        <div class="dashboard-box custom-box">
                            <div class="row">
                                <div class="col-sm-10">
                                    <p>Total Sales</p>
                                </div>
                                <div class="col-sm-2 text-right"></div>
                            </div>
                             <div class="card">
                              <div class="card-body">AUD&nbsp;<span class="totalSales"> {{$totalSalesSum}}</span></div>
                            </div>
                            <div id="CustomerChart12" class="m-t-20"></div>
                        </div>
                    </div>
                    <div class="box-border">
                        {{ Form::select('is_active',array('Day'=>'Day','Week'=>'Week','Month'=>'Month','Year'=>'Year'),'Month', ['class' => 'form-control choosen_selct','id'=>'totalSales']) }}
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="dashboard-box-wrapper">
                    <div class="dashboard-box-inner">
                        <div class="dashboard-box custom-box">
                            <div class="row">
                                <div class="col-sm-10">
                                    <p> Commission</p>
                                </div>
                            </div>
                            <div class="card">
                              <div class="card-body">AUD&nbsp;<span class="totalCommission"> {{$commsionSum}}</span></div>
                            </div>
                            <div id="AgentChart12" class="m-t-20"></div>
                        </div>
                    </div>
                    <div class="box-border">
                        {{ Form::select('is_active1',array('Day'=>'Day','Week'=>'Week','Month'=>'Month','Year'=>'Year'),'Month', ['class' => 'form-control choosen_selct','id'=>'totalCommission']) }}
                    </div>
                </div>
            </div>
    </section>

    <section class="m-t-20">
        <h2 class="page-title">Pending Booking</h2>
        <div class="row m-t-30">
            <div class="col-md-12">
                <div class="dashboard-box-wrapper">
                    <div class="dashboard-box-inner">
                        <div class="dashboard-box">
                            <!-- <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option>Date Created</option>
                                            <option>Departure Date </option>
                                            <option>Name </option>
                                            <option>Price </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <input class="form-control" name="search_text" id="myInput" type="text" placeholder="Search..">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <input type="button" name="" class="btn btn-primary btn-block" value="Search">
                                </div>
                            </div> -->
                            <input type="hidden" name="order_field" value="{{ $sOrderField }}" />
                            <input type="hidden" name="order_by" value="{{ $sOrderBy }}" />
                            <div class="table-responsive m-t-20 table_record">
                                @include('WebView::agent._pending_list_ajax')
                            </div>
                          </div>
                      </div>
                  </div>
            </div>
        </div>
    </section>
    <section class="m-t-20">
        <h2 class="page-title">Current Booking</h2>
        <div class="row m-t-30">
            <div class="col-md-12">
                <div class="dashboard-box-wrapper">
                    <div class="dashboard-box-inner">
                        <div class="dashboard-box">
                           <!--  <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option>Date Created</option>
                                            <option>Departure Date </option>
                                            <option>Return Date </option>
                                            <option>Name </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <input class="form-control" name="search_text" id="myInput" type="text" placeholder="Search..">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <input type="button" name="" class="btn btn-primary btn-block" value="Search">
                                </div>
                            </div> -->

                            <input type="hidden" name="current_order_field" value="{{ $currentOrderField }}" />
                            <input type="hidden" name="current_order_by" value="{{ $currentOrderBy }}" />
                            <div class="table-responsive m-t-20 current_table_record">
                                @include('WebView::agent._current_list_ajax')
                            </div>
                          </div>
                      </div>
                  </div>
            </div>
        </div>
    </section>
	<section class="m-t-20">
        <h2 class="page-title">Notifications / Task List</h2>
        <div class="row m-t-30">
            <div class="col-md-12">
                <div class="dashboard-box-wrapper">
                    <div class="dashboard-box-inner">
                        <div class="dashboard-box">
                            <!-- <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option>Date Created</option>
                                            <option>Departure Date </option>
                                            <option>Name </option>
                                            <option>Price </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <input class="form-control" name="search_text" id="myInput" type="text" placeholder="Search..">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <input type="button" name="" class="btn btn-primary btn-block" value="Search">
                                </div>
                            </div> -->
                            <input type="hidden" name="order_field" value="{{ $sOrderField }}" />
                            <input type="hidden" name="order_by" value="{{ $sOrderBy }}" />
                            <div class="table-responsive m-t-20 table_record">
                                @include('WebView::agent._notification_list_ajax')
                            </div>
                          </div>
                      </div>
                  </div>
            </div>
        </div>
    </section>
    
    <section class="m-t-20">
        <h2 class="page-title">ERRATA Notice Board</h2>
        <div class="row m-t-30">
            <div class="col-md-12">
                <div class="dashboard-box-wrapper">
                    <div class="dashboard-box-inner">
                        <div class="dashboard-box">
                            <table class="table">
                                <thead>
                                  <tr>
                                    <th>Client Name</th>
                                    <th>Number of PAX</th>
                                    <th>Created Date</th>
                                    <th>Departure Date</th>
                                    <th>Return Date</th>
                                    <th>Itinerary Type</th>
                                    <th>Cost Price</th>
                                    <th>Commission</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>John</td>
                                    <td>5</td>
                                    <td>10-10-2018</td>
                                    <td>10-10-2018</td>
                                    <td>Single Product</td>
                                    <td><i class="fa fa-rupee"></i> 200</td>
                                    <td><i class="fa fa-rupee"></i> 200</td>
                                    <td>2.5</td>
                                  </tr>
                                  <tr>
                                    <td>John</td>
                                    <td>5</td>
                                    <td>10-10-2018</td>
                                    <td>10-10-2018</td>
                                    <td>Single Product</td>
                                    <td><i class="fa fa-rupee"></i> 200</td>
                                    <td><i class="fa fa-rupee"></i> 200</td>
                                    <td>2.5</td>
                                  </tr>
                                  <tr>
                                    <td>John</td>
                                    <td>5</td>
                                    <td>10-10-2018</td>
                                    <td>10-10-2018</td>
                                    <td>Single Product</td>
                                    <td><i class="fa fa-rupee"></i> 200</td>
                                    <td><i class="fa fa-rupee"></i> 200</td>
                                    <td>2.5</td>
                                  </tr>
                                  <tr>
                                    <td>John</td>
                                    <td>5</td>
                                    <td>10-10-2018</td>
                                    <td>10-10-2018</td>
                                    <td>Single Product</td>
                                    <td><i class="fa fa-rupee"></i> 200</td>
                                    <td><i class="fa fa-rupee"></i> 200</td>
                                    <td>2.5</td>
                                  </tr>
                                  <tr>
                                    <td>John</td>
                                    <td>5</td>
                                    <td>10-10-2018</td>
                                    <td>10-10-2018</td>
                                    <td>Single Product</td>
                                    <td><i class="fa fa-rupee"></i> 200</td>
                                    <td><i class="fa fa-rupee"></i> 200</td>
                                    <td>2.5</td>
                                  </tr>
                                </tbody>
                              </table>
                          </div>
                      </div>
                  </div>
            </div>
        </div>
    </section>
</div>
</div>
@stop

@section('custom-js')

<!-- <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> -->
<script type="text/javascript"> 

// google.charts.load("current", {
//  	packages: ["corechart"]
// });
// google.charts.setOnLoadCallback(drawChart);
// google.charts.setOnLoadCallback(drawChart1);
// google.charts.setOnLoadCallback(bookingChart);

// function drawChart() {
//  	var data = google.visualization.arrayToDataTable([
// 		['Day', ''],
//         ['Day', 15],
//         ['Week', 20],
//         ['Month', 60],
//         ['Year', 20]
//  	]);

//  	var options = {
// 		title: '',
// 		pieHole: 0.5,
// 		 colors: ['#006080','#76DDFB', '#2C82BE', '#DBECF8'],
//         legend: {
//         position: 'top',
//         },
//         pieSliceText: 'value',
//         'tooltip' : {
//           trigger: 'none'
//         }
//  	};

//  	var chart = new google.visualization.PieChart(document.getElementById('CustomerChart'));
//  		chart.draw(data, options);
// }

// function bookingChart() {
//     var data = google.visualization.arrayToDataTable([
//         ['Day', ''],
//         ['Day', 15],
//         ['Week', 20],
//         ['Month', 60],
//         ['Year', 20]
//     ]);

//     var options = {
//         title: '',
//         pieHole: 0.5,
//         colors: ['#006080','#76DDFB', '#2C82BE', '#DBECF8'],
//         legend: {
//         position: 'top',
//         },
//         pieSliceText: 'value',
//         'tooltip' : {
//           trigger: 'none'
//         }
//     };

//     var chart = new google.visualization.PieChart(document.getElementById('bookingChart'));
//         chart.draw(data, options);
// }



// function drawChart1() {
// 	var data = google.visualization.arrayToDataTable([
// 		['Day', ''],
//         ['Day', 15],
//         ['Week', 20],
//         ['Month', 60],
//         ['Year', 20]
// 	]);

//  	var options = {
// 		title: '',
// 		pieHole: 0.5,
// 		colors: ['#006080', '#2C82BE', '#DBECF8', '#2C82BE'],
//         legend: {
//         position: 'top',
//         },
//         pieSliceText: 'value',
//         'tooltip' : {
//           trigger: 'none'
//         }
//  	};

//  	var chart = new google.visualization.PieChart(document.getElementById('AgentChart'));
//  		chart.draw(data, options);
// 	} 
//Get Total Number Booking Count 
$("#duration").change(function(){
    showLoader();
    var totalBookingCount = "{{$bookingCount}}";
    var user_id = "{{Auth::user()->id}}";
    var duration = $(this).val();
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url:"{{  url('booking-count') }}",
        data: {'totalBookingCount': totalBookingCount, 'duration': duration, 'user_id' :user_id},
        success: function (data) {
            $(".bookingSpan").html(data);
            hideLoader();
        },
        error: function (data) {
        }
    });
});

//Get Total sales sum totalCommission
$("#totalSales").change(function(){
    showLoader();
    var totalBookingCount = "{{$totalSalesSum}}";
    var user_id = "{{Auth::user()->id}}";
    var duration = $(this).val();
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url:"{{  url('total-sales') }}",
         data: {'totalBookingCount': totalBookingCount, 'duration': duration, 'user_id' :user_id},
        success: function (data) {
            $(".totalSales").html(data);
            hideLoader();
        },
        error: function (data) {
        }
    });
});

//Get Total sales totalCommission
$("#totalCommission").change(function(){
    showLoader();
    var totalBookingCount = "{{$commsionSum}}";
    var user_id = "{{Auth::user()->id}}";
    var duration = $(this).val();
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url:"{{  url('total-commission') }}",
        data: {'duration': duration, 'user_id' :user_id},
        success: function (data) {
            $(".totalCommission").html(data);
            hideLoader();
        },
        error: function (data) {
        }
    });
});

function getUserSort(element,sOrderField)
{  
    if($(element).find( "i" ).hasClass('fa-caret-down'))
    {
        $(element).find( "i" ).removeClass('fa-caret-down');
        $(element).find( "i" ).addClass('fa-caret-up');
        $("input[name='order_field']").val(sOrderField);
        $("input[name='order_by']").val('desc');
    }
    else
    { 
        $(element).find( "i" ).removeClass('fa-caret-up');
        $(element).find( "i" ).addClass('fa-caret-down');
        $("input[name='order_field']").val(sOrderField);
        $("input[name='order_by']").val('asc');
    }
    getMoreListingPending(siteUrl('?status=Pending'),event,'table_record');
    //callAgentUserListing(element,'table_record');
}


function getNotificationSort(element,sOrderField)
{  
    if($(element).find( "i" ).hasClass('fa-caret-down'))
    {
        $(element).find( "i" ).removeClass('fa-caret-down');
        $(element).find( "i" ).addClass('fa-caret-up');
        $("input[name='order_field']").val(sOrderField);
        $("input[name='order_by']").val('desc');
    }
    else
    { 
        $(element).find( "i" ).removeClass('fa-caret-up');
        $(element).find( "i" ).addClass('fa-caret-down');
        $("input[name='order_field']").val(sOrderField);
        $("input[name='order_by']").val('asc');
    }
    getMoreNotification(siteUrl('?status=notification'),event,'notification_record');
    //callAgentUserListing(element,'table_record');
}


function getCurrentBookingSort(element,sOrderField)
{  
    if($(element).find( "i" ).hasClass('fa-caret-down'))
    {
        $(element).find( "i" ).removeClass('fa-caret-down');
        $(element).find( "i" ).addClass('fa-caret-up');
        $("input[name='current_order_field']").val(sOrderField);
        $("input[name='current_order_by']").val('desc');
    }
    else
    { 
        $(element).find( "i" ).removeClass('fa-caret-up');
        $(element).find( "i" ).addClass('fa-caret-down');
        $("input[name='current_order_field']").val(sOrderField);
        $("input[name='current_order_by']").val('asc');
    }
    getMoreListingCurrent(siteUrl('?status=Active'),event,'current_table_record');
    //callAgentUserListing(element,'table_record');
}
//Pending Booking sorting
</script>

@stop