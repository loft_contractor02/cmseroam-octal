<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class AECity extends Model
{
    protected $fillable = [
        'RegionId','CountryId','Code','name'
    ];
    protected $table = 'zaecities';
    protected $primaryKey = 'id';
    
    public static function getAECity($sCountryCode) {
        return AECity::from('zaecities as ct')
                    ->join('zaecountries as co','ct.CountryId','=','co.CountryId')
                    ->where('co.code',$sCountryCode)
                    ->select('ct.RegionId', 'ct.Name', 'ct.CountryId','ct.id')
                    ->orderBy('ct.Name')
                    ->get();
        
    }
    
    public function ae_country(){
        return $this->hasOne('App\AECountry', 'CountryId', 'CountryId');
    }
    
    public function ae_region()
    {
        return $this->hasMany('App\AERegion', 'RegionId', 'RegionId');
    }
}
