<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ViatorLocationsMapped extends Model
{
    protected $fillable = [
        'city_id','viator_destination_id'
    ];
    protected $table = 'zviatorlocationsmapped';
    protected $primaryKey = 'id';
    use SoftDeletes;
    public $timestamps = false;
    
    protected $dates = ['deleted_at'];
    
    public function viator_location()
    {
        return $this->hasOne('App\ViatorLocation', 'id', 'viator_destination_id');
    }
}
