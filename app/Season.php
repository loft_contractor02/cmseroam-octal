<?php

namespace App;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Season extends Model
{
    protected $fillable = [
        'tour_id', 'StartDate','EndDate','AdultPrice','ChildPrice','Availability','adv_purchase','is_mon','is_tue','is_wed','is_thu','is_fri','is_sat'
        ,'is_sun','SeasonName','AdultPriceSingle','ChildPriceSingle','AdultSupplement','ChildSupplement','flight_currency_id','flightPrice','flightDescription'
        , 'flightReturn','flightDepart','sumPrice'
    ];
    protected $table = 'tblseason';
    protected $primaryKey = 'season_id';
    public $timestamps = false;
    
    public static function getSeasonList($nTourId,$sSearchStr)
    {
        return Season::where('tour_id', '=', $nTourId)
                       ->when($sSearchStr, function($query) use($sSearchStr) {
                           $query->where('SeasonName','LIKE', '%'.$sSearchStr.'%');
                       })
                       ->get();
    }
    //api function
    public static function getSeasonFlightList($aTourIds)
    {
        //echo Carbon::now();exit;
        return Season::from('tblseason as s')
                    ->where('s.EndDate','>=',Carbon::now())
                    ->whereIn('s.tour_id',$aTourIds)
                    ->join('tblflightpayment as fp', 'fp.season_id', '=', 's.season_id','INNER')
                    ->join('zcities as c', 'fp.flight_depart_city', '=', 'c.id','INNER')
                    ->select('*')
                    ->get();
    }
    
    public static function getTourSeason($aTourId)
    {
        return Season::from('tblseason as s')
                     ->where('tour_id', '=', $aTourId)
                     ->orderBy('season_id', 'asc')
                     ->select('season_id', 'adv_purchase', 'flightPrice', 'flightReturn')
                     ->get();
    }
}
