<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class City extends Model
{
    protected $fillable = [
        'name','region_id','goehash','optional_city','description','small_image','row_id','country_id','default_nights',
        'is_disabled','airport_codes','timezone_id'
    ];
    protected $table = 'zcities';
    protected $primaryKey = 'id';
    use SoftDeletes;
    
    protected $dates = ['deleted_at'];
    public $timestamps = false;
    public function timezone()
    {
        return $this->hasOne('App\Timezone','id','timezone_id');
    }
    
    public function country(){
    	return $this->hasOne('App\Country','id','country_id');
    }

    public function countrywithregion(){
        return $this->hasOne('App\Country','id','country_id')->with('region');
    }
    
    public function image(){
        return $this->hasMany('App\CityImage');
    }
    
    public function latlong(){
        return $this->hasOne('App\CitiesLatLong');
    }
    
    public function hb_destinations_mapped()
    {
        return $this->hasMany('App\HBDestinationsMapped', 'city_id', 'id')->select('city_id', 'hb_destination_id')->with([
            'hbDestination' => function($query)
            {
                $query->addSelect('id','destination_name', 'destination_code')->orderBy('destination_name')->with([
                    'hbZones' => function($q)
                    {
                        $q->addSelect('id', 'hb_destination_id', 'zone_name', 'zone_number')->orderBy('zone_name');
                    }
                ]);
            }
        ]);
    }
    
    public function aot_supplier_locations_mapped(){
        return $this->hasMany('App\AOTMapSupplierCity', 'city_id', 'id')->with([
            'locations' => function($query)
            {
                $query->orderBy('LocationType')->orderBy('LocationName');
            }
        ]);
    }
    
    public function viator_locations_mapped(){
        return $this->hasMany('App\ViatorLocationsMapped', 'city_id', 'id')->with([
            'viator_location' => function($query)
            {
                $query->orderBy('destinationType')->orderBy('destinationName');
            }
        ]);
    }  
    
    public function ae_city_mapped()
    {
        return $this->hasMany('App\AECitiesMapped', 'eroam_city_id', 'id')->with(['ae_city' => function( $query )
                {
                    $query->orderBy('Name')->with('ae_region');
                }
            ]);
    }


    public function yalago_city_mapped()
    {
        return $this->hasMany('App\YalagoCitiesMapped', 'eroam_city_id', 'id')->with(['yalago_city' => function( $query )
                {
                    $query->orderBy('locationName')->with('yalagoProvince', 'yalagoCountry');
                }
            ]);
    }

    
    public static function geCityList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord = 10)
    {
        return City::from('zcities as c')
                    ->leftJoin('zcountries as co','co.id','=','c.country_id')
                    ->when($sSearchStr, function($query) use($sSearchStr,$sSearchBy) {
                            $query->where($sSearchBy,'like','%'.$sSearchStr.'%');
                        })
                    ->select(
                        'c.id as id',
                        'c.name as name',
                        'c.optional_city as optional_city',
                        'c.created_at as created_at',
                        'c.updated_at as updated_at',
                        'co.name as country_name'
                        )
                    ->whereNull('c.deleted_at')
                    ->withTrashed()
                    ->orderBy($sOrderField, $sOrderBy)
                    ->paginate($nShowRecord);
    }

    public static function getCityDetail($city_id) {
        return City::select('zcities.description','zcities.name','ci.small')->where('zcities.id',$city_id)->leftJoin('zcityimages as ci','ci.city_id','=', 'zcities.id')->first();
    }

     public function iata(){
        return $this->hasOne('App\CityIata','city_id','id');
    }

    public function iatas(){
        return $this->hasMany('App\CityIata', 'city_id', 'id');
    }
    public function suburbs(){
        return $this->hasMany('App\Suburb','city_id','id');
    }

    public static function getDestinations()
    {
        //$results = $db_ext->select("select zc.id as city_id,zc.country_id as country_id, vl.destinationId,zc.name,zc.region_id as region_id,tc.name as country_name from zCities zc left join zViatorLocations vl on zc.name = vl.destinationName left join zCountries tc on zc.country_id = tc.id where vl.destinationType = 'CITY'");

        return City::from('zcities')
                    ->leftJoin('zviatorlocations as vl','zcities.name','=','vl.destinationName')
                    ->leftJoin('zcountries as tc','zcities.country_id','=','tc.id')
                    ->select(
                        'zcities.id as city_id',
                        'zcities.country_id as country_id',
                        'vl.destinationId',
                        'zcities.name',
                        'zcities.region_id',
                        'tc.name as country_name'
                        )
                    ->where('vl.destinationType','=','CITY')
                    ->get();
    }

    public function location_city() {
        return $this->belongsTo('App\City');
    }
}   
