<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TourStandardRemark extends Model
{
    protected $fillable = [
        'standard_remarks_id','tour_id'
    ];
    protected $table = 'tbltourstandardremarks';
    protected $primaryKey = 'tour_standard_id';
    public $timestamps = false;
}
