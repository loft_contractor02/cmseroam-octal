<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserIpDetails extends Model
{
    protected $table = 'user_ip_details';
}
