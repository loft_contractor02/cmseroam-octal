<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HBDestinationsMapped extends Model
{
    protected $fillable = [
        'city_id','hb_destination_id'
    ];
    protected $table = 'zhbdestinationsmapped';
    protected $primaryKey = 'id';
    use SoftDeletes;
    public $timestamps = false;

    protected $dates = ['deleted_at'];
    
    public function hbDestination()
    {
        return $this->hasOne('App\HBDestination', 'id', 'hb_destination_id');
    }
}
