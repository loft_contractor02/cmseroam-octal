<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActivityMarkupSupplierCommission extends Model
{
    protected $table = 'zactivitymarkupsuppliercommissions';
    protected $primaryKey = 'id';
    protected $fillable = ['activity_markup_id','percentage'];
    use SoftDeletes;

    protected $dates = ['deleted_at'];
}
