<?php

namespace App\Services\RoomerFlex;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Command\Guzzle\Description;
use GuzzleHttp\Command\Guzzle\GuzzleClient;
use GuzzleHttp\Command\Guzzle\Serializer;
use App\Services\RoomerFlex\Resources\ServiceDescriptionV3;

class RoomerFlexClient extends GuzzleClient {

    public static function createRequestToken($config = []) {
        // Load the service description file.

        $service_description = new Description(ServiceDescriptionV3::getRequestToken());

        // Creates the client and sets the default request headers.
        $client = new HttpClient([
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Authorization' => 'Token token='.config('roomerflex_variables.AuthToken'),
                'Partner' => config('roomerflex_variables.Partner'),
                'API-Version' => config('roomerflex_variables.Version')
            ],
        ]);
        $config['validate'] = false;

        return new static($client, $service_description, NULL, NULL, NULL, $config);
    }

    public static function createProtectionToken($config = []) {
        // Load the service description file.

        $service_description = new Description(ServiceDescriptionV3::getProtectionToken());

        // Creates the client and sets the default request headers.
        $client = new HttpClient([
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Authorization' => 'Token token='.config('roomerflex_variables.AuthToken'),
                'Partner' => config('roomerflex_variables.Partner'),
                'API-Version' => config('roomerflex_variables.Version')
            ],
        ]);
        $config['validate'] = false;

        return new static($client, $service_description, NULL, NULL, NULL, $config);
    }
}