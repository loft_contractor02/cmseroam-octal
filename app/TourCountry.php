<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TourCountry extends Model
{
    protected $fillable = [
        'country_id','tour_id'
    ];
    protected $table = 'tbltourcountry';
    public $timestamps = false;
}
