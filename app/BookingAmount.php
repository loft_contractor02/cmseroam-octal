<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class BookingAmount extends Model {

    protected $table = 'booking_amount';
    protected $fillable = ['order_id', 'total', 'deposit', 'pending'];

}
