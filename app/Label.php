<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Label extends Model
{
    protected $fillable = [
        'name','description'
    ];
    protected $table = 'zlabels';
    protected $primaryKey = 'id';
    
    public function Label(){
        return $this->hasOne('App\Label','id','label_id');
    }
    
    public static function geLabelList($sSearchStr,$sOrderField,$sOrderBy,$nShowRecord = 10){
        return User::from('zlabels as l')
                    ->when($sSearchStr, function($query) use($sSearchStr) {
                            $query->where('l.name','like','%'.$sSearchStr.'%');
                        })
                    ->select(
                        'l.id as id',
                        'l.name as name',
                        'l.description as description',
                        'l.created_at as created_at',
                        'l.updated_at as updated_at'
                        )
                    ->orderBy($sOrderField, $sOrderBy)
                    ->paginate($nShowRecord);
    }
}
