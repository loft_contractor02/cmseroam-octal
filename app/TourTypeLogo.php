<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TourTypeLogo extends Model
{
    protected $fillable = [
        'title','logo_path'
    ];
    protected $table = 'tbltourtypelogo';
    protected $primaryKey = 'id';
    
    public static function getTourTypeLogoList($sSearchStr,$sOrderField,$sOrderBy) 
    {
        $TourTypeLogo = TourTypeLogo::When($sSearchStr, function($query) use($sSearchStr){
                                        $query->where('title','LIKE', '%'.$sSearchStr.'%');
                                    })
                                    ->orderBy($sOrderField, $sOrderBy)
                                    ->get();
        return $TourTypeLogo;
    }
}
