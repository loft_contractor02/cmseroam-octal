<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Hotel extends Model
{
    protected $table = 'zhotel';
    protected $guarded = [];
    protected $primaryKey = 'id';
    public $timestamps = false;
    
    public function city(){
    	return $this->hasOne('App\City','id','city_id')->with('country');
    }

    public function category(){
    	return $this->hasOne('App\HotelCategory','id','hotel_category_id');
    }

    public function currency()
    {
        return $this->hasOne('App\Currency','id','currency_id');
    }
    public function price(){
        return $this->hasMany('App\HotelPrice', 'hotel_id', 'id')->price_summary();
    }
    public function prices(){
        return $this->hasMany('App\HotelPrice', 'hotel_id', 'id')->price_summary();
    } 
    
    public function image(){
        return $this->hasMany('App\HotelImage', 'hotel_id', 'id')->orderByPrimary();
    }
    public static function geHotelList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord = 10)
    {
        $user_type=Auth::user()->type; 
        return Hotel::from('zhotel as h')
                    ->leftJoin('zhotelsuppliers as s', 's.id', '=', 'h.supplier_id')
                    ->when($sSearchStr, function($query) use($sSearchStr,$sSearchBy) {
                            $query->where($sSearchBy,'like','%'.$sSearchStr.'%');
                        })
                    ->where(function($query) use ($user_type)  {
                        if(isset($user_type) && $user_type!='admin' && $user_type !='eroamProduct') {
                            $query->where('h.user_id', Auth::user()->id);
                        }
                     })
                    ->select(
                        'h.name as name',
                        'h.is_publish as is_publish',
                        'h.eroam_code as eroam_code',
                        'h.eroam_code as eroam_code',
                        'h.address_1 as address_1',
                        's.name as supplier_name',
                        'h.id as id',
                        'h.domain_ids as domains'
                    )
                    ->orderBy($sOrderField, $sOrderBy)
                    ->paginate($nShowRecord);
    }
    
    public static function getHotelPriceList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord)
    {
        return Hotel::from('zhotel')
            ->select('zhotel.id',
                'zhotel.name', 
                'city_id', 
                'hotel_category_id'
                )
            ->when($sSearchStr, function($query) use($sSearchStr,$sSearchBy) {
                            $query->where($sSearchBy,'like','%'.$sSearchStr.'%');
                        })
            ->orderBy('zhotel.name', 'ASC')
            ->with([ 
                'prices' => function($query)
                {
                    $query->with([
                            'room_type' => function($query)
                            {   
                                $query->addSelect('id', 'name');
                            },
                            'season' => function($query)
                            {
                                $query->join('zhotelsuppliers as hs', 'hs.id', '=', 'zhotelseasons.hotel_supplier_id')
                                    ->addSelect('zhotelseasons.id',
                                        'zhotelseasons.name',
                                        'zhotelseasons.from',
                                        'zhotelseasons.to',
                                        'hotel_supplier_id',
                                        'currency_id',
                                        'hs.name as hotel_supplier_name')
                                    ->orderBy('hotel_supplier_name', 'ASC')
                                    ->orderBy('zhotelseasons.name', 'ASC')
                                    ->with([
                                        'supplier' => function($query)
                                        {
                                            $query->addSelect('id', 'name');
                                        },
                                        'currency' => function($query)
                                        {
                                            $query->addSelect('id', 'name', 'code');
                                        }
                                    ]);
                            }
                        ]);
                },
                'city' => function($query)
                {
                    $query->addSelect('id', 'name');
                },
                'category' => function($query)
                {
                    $query->addSelect('id', 'name');
                }
            ])->paginate($nShowRecord);
           
    }
}
