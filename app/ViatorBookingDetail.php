<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ViatorBookingDetail extends Model
{
    protected $table = 'tblviatorbookingdetail';
    protected $guarded = [];
}
