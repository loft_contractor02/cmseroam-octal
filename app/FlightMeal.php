<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FlightMeal extends Model
{
	protected $guarded =[];
    protected $table = 'tblflightmealpreference';
    protected $primaryKey = 'id';
}
