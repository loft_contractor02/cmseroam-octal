<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = [
        'image_name','image_thumb','tour_id','image_url','title','user_id','is_deleted','is_active','is_canceled','random_tour_id','image_small',
        'image_medium','image_large','sort_order','is_primary'
    ];
    protected $table = 'tblimages';
    protected $primaryKey = 'image_id';
    public $timestamps = false; 
}
