<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Timezone extends Model
{
    protected $fillable = [
        'name','country_code'
    ];
    protected $table = 'ztimezones';
    protected $primaryKey = 'id';
    public $timestamps = false;  
}
