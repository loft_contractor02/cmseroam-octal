<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CityImage extends Model
{
    protected $fillable = [
        'city_id','is_primary','description','original','thumbnail','small','medium','large'
    ];
    protected $table = 'zcityimages';
    protected $primaryKey = 'id';
}
