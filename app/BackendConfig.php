<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BackendConfig extends Model
{
    protected $table = 'backend_config';
}
