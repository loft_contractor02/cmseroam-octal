<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TourPickup extends Model
{
    protected $fillable = [
        'pickup_id','tour_id'
    ];
    protected $table = 'tbltourpickups';
    protected $primaryKey = 'tour_pickup_id';
    public $timestamps = false;
}
