<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class YalagoProvinces extends Model
{
    protected $fillable = [
        'provinceId','yalagoProvinceId','yalagoCountryId','countryId','provinceName'
    ];
    protected $table = 'yalago_provinces';
    protected $primaryKey = 'provinceId';
}
