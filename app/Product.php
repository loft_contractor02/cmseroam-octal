<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
   
    protected $fillable = ['name', 'status', 'user_id'];
    protected $table = 'products';

}
