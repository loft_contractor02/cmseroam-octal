<?php

namespace App;
///priya
use Illuminate\Database\Eloquent\Model;
use Auth;
class Activity extends Model
{
    protected $fillable = [
        'name','city_id','default','pickup','dropoff','has_accomodation','has_transport','approved_by_eroam',
        'activity_category_id','default_activity_supplier_id','currency_id','ranking','description','voucher_comments','address_1','address_2','notes',
        'reception_phone','reception_email','website','pax_type','activity_operator_id','duration','country_id','eroam_stamp','destination_city_id','accommodation_nights',
        'accommodation_details','transport_details','is_publish','code','domain_ids','licensee_id','user_id'
    ];
    protected $table = 'zactivity';
    protected $primaryKey = 'id';
    
    public static function getActivityList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord)
    {   $user_type=Auth::user()->type;
        return Activity::from('zactivity as a')
                    ->leftJoin('zcities as c','c.id','=','a.city_id')
                    ->leftJoin('zcountries as co','co.id','=','c.country_id')
                    ->leftJoin('zcurrencies as cu','cu.id','=','a.currency_id')
                    ->leftJoin('zactivitysuppliers as acs','acs.id','=','a.default_activity_supplier_id')
                    ->leftJoin('zactivityoperators as ao','ao.id','=','a.activity_operator_id')
                    ->when($sSearchStr, function($query) use($sSearchStr,$sSearchBy) {
                            $query->where($sSearchBy,'like','%'.$sSearchStr.'%');
                        })
                    ->where(function($query) use ($user_type)  {
                        if(isset($user_type) && $user_type!='admin') {
                            $query->where('a.user_id', Auth::user()->id);
                        }
                        })    
                    ->select(
                        'a.id as id',
                        'a.name as name',
                        'a.is_publish as is_publish',
                        'a.eroam_stamp as eroam_stamp',
                        'a.duration as duration',
                        'c.name as city_name',
                        'a.is_publish as is_publish',
                        'cu.code as currency_code',
                        'acs.name as supplier_name',
                        'ao.name as operator_name',
                        'a.domain_ids as domains'
                        )
                    ->orderBy($sOrderField, $sOrderBy)
                    ->groupBy('a.id')
                    ->paginate($nShowRecord);
    }
    
    public static function getActivitySeasonList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord)
    {
        $user_type=Auth::user()->type;
        $query = Activity::from('zactivity as a')
                        ->leftJoin('zcities as c','c.id','=','a.city_id')
                        ->leftJoin('zactivityoperators as ao','ao.id','=','a.activity_operator_id')
                        ->leftJoin('zactivityprices as cp','cp.activity_id','=','a.id')
                        ->where(function($query) use ($user_type)  {
                            if(isset($user_type) && $user_type!='admin' && $user_type!="eroamProduct") {
                                $query->where('cp.user_id', Auth::user()->id);
                            }
                            })    
                        ->select(
                            'a.id as id',
                            'a.name as name',
                            'a.eroam_stamp as eroam_stamp',
                            'a.duration as duration',
                            'c.name as city_name',
                            'ao.name as operator_name',
                            'cp.domain_ids as domains'                           
                            )
                        ->orderBy($sOrderField, $sOrderBy)
                        ->with('price');
                        
        if($sSearchStr != '' && ($sSearchBy == 'c.name' ||  $sSearchBy == 'ao.name' || $sSearchBy == 'a.name' ))
        {
            $query->when($sSearchStr, function($query) use($sSearchStr,$sSearchBy) {
                            $query->where($sSearchBy,'like','%'.$sSearchStr.'%');
                        });
        }
        return $query->paginate($nShowRecord);
    }
    
    public function price(){
        return $this->hasMany('App\ActivityPrice','activity_id','id')->with('supplier', 'currency','base_price')->price_summary();   
    }
    public function City()
    {
        return $this->hasOne('App\City','id','city_id')->with('country');
    }
    public function Operator(){
        return $this->hasOne('App\ActivityOperator','id','activity_operator_id');
    }
}
