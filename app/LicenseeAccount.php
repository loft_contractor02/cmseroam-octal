<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LicenseeAccount extends Model{
    protected $table = 'licensee_account';
    protected $fillable = ['licensee_id','representative_id','notes'];

    
}
