<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelImage extends Model
{
    protected $table = 'zhotelimages';
    protected $guarded = [];
    protected $primaryKey = 'id';
    public $timestamps = false;
    
    public function scopeOrderByPrimary($query){
        return $query->orderBy('is_primary','DESC');
    }
}
