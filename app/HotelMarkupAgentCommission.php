<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelMarkupAgentCommission extends Model
{
    protected $table = 'zhotelmarkupagentcommissions';
    protected $guarded = [];
    protected $primaryKey = 'id';
}
