<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class ApiSettings extends Model {

	protected $table = 'zapisettings';

	protected $fillable = ['api_id', 'settings'];

	public function api() {
		return $this->belongsTo('App\Api');
	}
}