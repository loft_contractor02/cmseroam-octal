<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelSupplierCommission extends Model
{
    protected $table = 'zhotelmarkupsuppliercommissions';
    protected $guarded = [];
    protected $primaryKey = 'id';
}
