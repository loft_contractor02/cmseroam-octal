<?php
namespace Libraries;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use App\YalagoLocations;

class Yalago
{
	public function __construct(){

        $this->url      = 'http://pp.api.yalago.com';
        $this->apiKey   = "3c0c1b08-e95e-4d6f-96e4-c8d20bfbb68b";
        $this->culture  = 'en-GB';
        $this->SourceMarket = 'AE';
    }

    public  function getAllHotels($data){
        $hotels = array();
        $location = YalagoLocations::getYalagoCity($data['searchCityName'])->toArray();
        if(isset($location[0]['yalagoProvinceId'])){
            $path = '/hotels/availability/Get';
            $logname = 'yalago_avaibility';
            $roomArray = array();
            $rooms = $data['rooms'];
            for ($i=0; $i < $rooms; $i++) { 
                $roomArray[$i]['Adults'] = $data['search_input']['num_of_adults'][$i];

                if(isset($data['child'])){
                    $roomArray[$i]['ChildAges'] = array_key_exists($i, $data['child']) ? $data['child'][$i] : array();  
                } else {
                    $roomArray[$i]['ChildAges'] = array();
                } 
            }

            $yalagodata = array(
                "CheckInDate"=>date('Y-m-d', strtotime($data['date_from'])),
                "CheckOutDate"=>date('Y-m-d', strtotime($data['date_from'].'+'.$data['city_default_night'].'Days')),
                "ProvinceId"=>$location[0]['yalagoProvinceId'],
                "LocationId"=>$location[0]['yalagoLocationId'],
                "Culture"=>$this->culture,
                "Rooms"=>$roomArray,
                "GetpackagePrice" => true,
                "SourceMarket"=>$this->SourceMarket,
                //"IsFlightPlus" => false,
            );
            
            $hotels = $this->process_request($yalagodata,'post',$path,null,$logname);
        }
        return $hotels;
    }

    public function getApiData($data,$path,$logname){
    	return $this->process_request($data,'post',$path,null,$logname);
    }

    public function process_request($body,$method,$path,$string,$logname){
    	$client = new Client();
        $headers =  [
                        'x-api-key' => $this->apiKey,
                        'Content-Type' => 'application/json'
                    ];
        try{    
            Log::channel($logname)->info('Request : '.$this->url.$path.'?x-api-key='.$this->apiKey. json_encode($body));

            $response = ( $client->$method( $this->url . $path, [
                'body'      => json_encode($body),
                'headers'   => $headers
            ] ) ); 
            
            $status = $response->getStatusCode();
            $response  = json_decode($response->getBody(), true);

            /*$ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $this->url . $path);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
            curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($body));
            curl_setopt($ch, CURLOPT_HTTPHEADER,array('x-api-key:'.$this->apiKey,'Content-Type:application/json'));
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $response = curl_exec($ch);
            $headers = curl_getinfo($ch);
            curl_close($ch);
            $status = $headers['http_code'];
            echo '<pre>'; print_r($headers); print_r(json_decode($response));  die;*/

            Log::channel($logname)->info('Status : '.$status);
            Log::channel($logname)->info('Response : '.json_encode($response));
            Log::channel($logname)->info('--------------------------------------------');

            return  \Response::json([
                'status' =>$status,
                'data' => $response
            ]);
        }
        catch (\Exception $e) {

            $req = $e->getMessage();
            Log::channel($logname)->info('Response : '.$req);
            Log::channel($logname)->info('--------------------------------------------');
            
            return \Response::json([
                'status' => 200,
                'data' => $req,
            ]);
        }
    }
}