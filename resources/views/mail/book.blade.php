<?php 
	$data = session()->get('orderDataItenary');
	$personalDetails = $data['personalDetails'];
	$cities_arr = $data['cities_arr'];
	$product_details_all = $data['leg'];
	$cities_codes  = $data['cities_codes'];
	 ?>
<!doctype html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>eRoam</title>
	</head>
	<body style="margin: 0px; background: #e3e5e7;">
		<?php 
			/*$logo = getDomainLogo();
			if(!$logo) {*/
				$logo = "http://dev.eroam.com/images/email-logo-footer.png";
			//}
			?>
		<table width="100%" style="background: #e3e5e7">
			<tr>
				<td>
					<table style="margin:0 auto; background: #ffffff;" width="760px" border="0" cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<th style="background-color: #212121; padding-top:20px; padding-bottom: 20px; text-align: center;">
									<a href="#"><img src="<?php echo $logo; ?>" width="180px" alt="" /> </a>
								</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style="background: #fafafa; border-bottom: 1px solid #f0f0f0; padding-top: 5px; padding-bottom: 5px;">
									<table width="100%">
										<?php /*<tr>
											<td style="text-align: center">
											    <img src="http://dev.eroam.com/images/user_icon.png" alt="" />
											</td>
											</tr>*/ ?>
										<tr>
											<td style="text-align: center">
												<p style="font-family: Arial; font-size: 18px; color: #212121; margin-top: 0px; margin-bottom: 0px;  "><strong> Hi, <?php echo ucwords($personalDetails['invoice_no']);?>!</strong></p>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<h1 style="font-size: 20px; color: #212121; letter-spacing: 0.71px; line-height: 18px; margin-top: 20px; margin-bottom: 20px;margin-left:3px;">Booking Information</h1>
								</td>
							</tr>
							<tr>
								<td>
									<table style="width: 100%;">
										<tr>
											<td>
												<table border="0" width="100%" cellspacing="2" cellpadding="2" style="padding: 15px; position:relative; margin-bottom:20px; background:#FFFFFF; border:2px solid #efefef;font-size:14px;">
													<tr>
														<td colspan="2">
															<h4 style="font-size: 20px; line-height: 22px; font-weight: 500; margin-top: 0px; margin-bottom: 20px;"><strong>Personal Details</strong></h4>
														</td>
													</tr>
													<tr>
														<td colspan="2"><strong>Booking Id:</strong> <?php echo $personalDetails['invoice_no'];?></td>
													</tr>
													<tr>
														<td width="50%"><strong>Customer Name:</strong> <?php echo ucwords($personalDetails['first_name'].' '. $personalDetails['last_name']);?></td>
														<td width="50%"><strong>Customer Email:</strong> <?php echo $personalDetails['email'];?></td>
													</tr>
													<tr>
														<td width="50%"><strong>Customer Contact:</strong> <?php echo $personalDetails['contact'];?></td>
													</tr>
													<tr>
														<td width="50%"><strong>Customer Dob:</strong> <?php echo $personalDetails['dob'];?></td>
														<?php /*<td width="50%"><strong>Customer Country:</strong> <?php //echo $personalDetails['passenger_country_name'];?></td>*/ ?>
													</tr>
													<tr>
														<td width="50%"><strong>Days:</strong> <?php echo $personalDetails['total_days'];?></td>
														<td width="50%"><strong>Total Amount:</strong> AUD <?php echo $personalDetails['total_amount'];/*echo $personalDetails['cost_per_day'];?> per day (TOTAL AUD <?php echo $personalDetails['total_per_person'];  (per person))*/?></td>
													</tr>
													<tr>
														<td width="50%"><strong>Date:</strong> <?php echo date("j M Y", strtotime($personalDetails['from_date']));?> - <?php echo date("j M Y", strtotime($personalDetails['to_date']));?></td>
														<td width="50%"><strong>Total Paid Amount:</strong> AUD $<?php echo $personalDetails['total_amount'];?></td>
													</tr>
													<tr>
														<td width="50%"><strong>Travellers:</strong> <?php echo $personalDetails['num_of_travellers'];?></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td>
												@foreach($cities_arr as $key=>$val)
												<table border="0" width="100%" cellspacing="2" cellpadding="2" style="padding: 15px; position:relative; margin-bottom:20px; background:#FFFFFF; border:2px solid #efefef;font-size:14px;">
													<tr>
														<td colspan="2">
															<h4 style="font-size: 20px; line-height: 22px; margin-top: 0px; margin-bottom: 20px;">{{ucfirst($val)}}, {{$cities_codes[$key]}}</h4>
														</td>
													</tr>
													@foreach($product_details_all[$val] as $productkey=>$productval)
													
													<?php
														if($productkey == 'hotel' && !empty($productkey)) {
														?>
													<tr>
														<td width="50%"><strong>Accommodation Name:</strong> {{$productval['leg_name']}}</td>
														<td width="50%"><strong>Duration: </strong>{{!empty($productval['nights']) ? $productval['nights']:'N/A'}} Nights</td>
													</tr>
													<tr>
														<td width="50%"><strong>Address:</strong> {{!empty($productval['address']) ? $productval['address']:'N/A'}}</td>
														<?php if(!empty($value['provider_booking_status'])){ ?>
														<td width="50%"><strong>Booking Id: </strong>{{$productval['booking_id']}}</td>
														<?php }else{ ?>
														<td width="50%"><strong>Booking Status:</strong> Not Confirmed *</td>
														<?php } ?>
													</tr>
													<tr>
														<td width="50%"><strong>Check-in: </strong>  {{ !empty($productval['checkin_time']) ? date( 'j M Y', strtotime($productval['checkin_time'])): '' }}</td>
														<td width="50%"><strong>Check-out: </strong>{{ !empty($productval['checkout_time']) ? date( 'j M Y', strtotime($productval['checkout_time'])): '' }}</td>
													</tr>
													<tr>
														<td colspan="2"><strong>Bed Preferences:</strong> 
															<strong>{{!empty($productval['hotel_room_type_name'])?$productval['hotel_room_type_name']:'N/A'}}</strong>
														</td>
													</tr>
													<tr>
														<td colspan="2"><strong>Cancellation Policy: </strong>{{$productval['cancellation_policy']}}</td>
													</tr>
													<?php 
													} ?>
<tr>
	<td colspan="2"></td>
</tr>
											<?php
												if($productkey == 'activities' && !empty($productkey))
												{ ?>
													<tr>
	<td colspan="2">
		<h5 style="font-size: 16px; margin: 0px;"><img src="{{ url( 'images/activity-icon.png' ) }}" alt="" style="color: #000; margin-right: 5px; position: relative; top: 2px;font-size:14px;"/> &nbsp; Activity Summary</h5>
	</td>
</tr>
		
													<tr>
														<td width="50%"><strong>Activity Name:</strong> {{$productval['leg_name']}}</td>
														<td width="50%"><strong>Date:</strong> {{!empty($productval['from_date'])?date( 'j M Y', strtotime($value1['from_date'])):'N/A'}}</td>
													</tr>
													<tr>
														<td width="50%"><strong>Price:</strong> {{!empty($productval['price']) ? $productval['currency']." ".$productval['price']:'N/A'}}</td>
														<?php if($productval['provider_booking_status'] == 'CF'){ ?>
														<td width="50%"><strong>Booking Id: </strong>{{$productval['booking_id']}}</td>
														<?php }else{ ?>
														<td width="50%"><strong>Booking Status: </strong>Not Confirmed *</td>
														<?php } ?>
													</tr>
													@if(!empty($productval['voucher_url']))
													<tr>
														<td width="50%"><strong>Provider Name:</strong> Viator</td>
														<td width="50%"><strong>Voucher Link:</strong> <a href="{{$productval['voucher_url']}}" target="_blank">Click Here</a></td>
													</tr>
													@endif
													<?php  
												}?>
													<tr>
														<td colspan="2"></td>
													</tr>
													<?php 
														if($productkey == 'transport' && !empty($productkey)) 
														{ ?>
															<tr>
																<td colspan="2">
																	<h5 style="font-size: 16px; margin: 0px;"><img src="{{ url( 'images/transport-icon.png' ) }}" alt="" style="color: #000; margin-right: 5px; position: relative; top: 2px;font-size:14px;"/> &nbsp;Transport</h5>
																</td>
															</tr>
															<?php
																$depart = $productval['departure_text'];
																$arrive = $productval['arrival_text'];
																if($productval['leg_name'] == 'Flight'){
																	if(!empty($productval['booking_summary_text'])){
																		$leg_depart = explode("Depart:",$productval['booking_summary_text'],2);
																		$leg_arrive = explode("Arrive:",$productval['booking_summary_text'],2);
																
																		if(isset($leg_depart[1])){
																			$depart = explode("</small>",$leg_depart[1],2);
																			$depart = $depart[0];
																		}else{
																			$depart = '';
																		}
																		if(isset($leg_arrive[1])){
																			$arrive = explode("</small>",$leg_arrive[1],2);
																			$arrive = $arrive[0];
																		}else{
																			$arrive = '';
																		}
																	}else{
																		$depart = '';
																		$arrive = '';
																	}
																}
																if($productval['provider'] == 'busbud'){
																	$depart = $productval['departure_text'];
																	$arrive = $productval['arrival_text'];
																}
																?>
															<?php
																$leg_name = explode('Depart:', $productval['booking_summary_text']);
																if(isset($productval['booking_summary_text']) && !empty($productval['booking_summary_text'])){
																	$leg_name = trim(strip_tags($leg_name[0]));
																}else{
																	$leg_name = strip_tags($productval['leg_name']);
																}
																?>
															<tr>
																<td colspan="2"><strong>Service:</strong>{{$leg_name}}</td>
															</tr>
															<tr>
																<td width="50%"><strong>Depart:</strong> {{strip_tags($depart)}}</td>
																<td width="50%"><strong>Arrive:</strong> {{strip_tags($arrive)}}</td>
															</tr>
															<tr>
																<td width="50%"><strong>Price: </strong>{{$productval['currency']}} {{$productval['price']}}</td>
																<?php if($productval['provider_booking_status'] == 'CF'){ ?>
																<td width="50%"><strong>Booking Id: </strong>{{$productval['booking_id']}}</td>
																<?php }else{ ?>
																<td width="50%"><strong>Booking Status:</strong> Not Confirmed *</td>
																<?php } ?>
															</tr>
															<?php }?>
														@endforeach
														</table>
														
												@endforeach
												
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</tbody>
						<tfoot>
							<tr>
								<td style="background-color: #394951; padding-top:6px; padding-bottom: 6px; padding-right: 6px; padding-left: 6px; text-align: center;">
									<table style="width: 100%;">
										<tr>
											<td style="font-family: Arial; font-size: 14px;"> <a href="#"><img src="<?php echo $logo; ?>" alt="" /> </a></td>
											<td style="color: #fff;font-size: 12px; text-align: right; font-family: Arial; ">Powered by eRoam &copy; Copyright 2018 - 2019. All Rights Reserved. Patent pending AU2016902466</td>
										</tr>
									</table>
								</td>
							</tr>
						</tfoot>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>