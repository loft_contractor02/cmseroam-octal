@if(session()->has('role') && session()->get('role') == 'agent')
<?php //print_r(session()->all());  ?>
    <div class="page-sidebar-wrapper">
        <nav class="pushy pushy-left" style="visibility: visible;">
            <div class="pushy-height">
                <ul>
                    <li class="expand-all"><a href="javascript://"><i class="icon-unfold-more"></i><span>Expand All Menu Items</span></a></li>
                    <li class="collapse-all active"><a href="javascript://"><i class="icon-unfold-less"></i><span>Collapse All Menu Items</span></a></li>

                    <li class="pushy-submenu {{ (session('page_name')=='dashboard') ? 'pushy-submenu-open active open_pushy_cls' : 'pushy-submenu-closed' }}"><a href="{{url('/')}}"><i class="icon-dashboard"></i><span>Dashboard</span></a></li>

                    <!-- <li class="pushy-submenu {{ (session('heading_name')=='itenary_booking') ? 'pushy-submenu-open active open_pushy_cls' : 'pushy-submenu-closed' }}"> -->
                    <li class="pushy-submenu {{ (session('page_name')=='pending_booking' || session('page_name')=='active_booking' || session('page_name')=='archived_booking') ? 'pushy-submenu-open active open_pushy_cls' : 'pushy-submenu-closed' }}">
                        <a href="javascript://"><i class="icon-booking"></i><span>{{ trans('messages.itenary_booking_management') }}</span></a>
                        <ul>
                            <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'pending_booking' ? 'active' : '') }}"><a href="{{ url('booking/pending') }}">Pending Bookings</a></li>

                            <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'active_booking' ? 'active' : '') }}"><a href="{{ url('booking/active') }}">Active Bookings</a></li>

                            <li class="pushy_link_cls  {{ (session('page_name') && session('page_name') == 'archived_booking' ? 'active' : '') }}"><a href="{{ url('booking/archived') }}">Archived Bookings</a></li>
                        </ul>
                    </li>
                    <!-- <li class="pushy-submenu {{ (session('heading_name')=='tour-booking') ? 'pushy-submenu-open active open_pushy_cls' : 'pushy-submenu-closed' }}"> -->
                    <li class="pushy-submenu {{ (session('page_name')=='all_tour_booking') ? 'pushy-submenu-open active open_pushy_cls' : 'pushy-submenu-closed' }}">
                        <a href="javascript://"><i class="icon-booking"></i><span>{{ trans('messages.tour_booking_management') }}</span></a>
                        <ul>
                            <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'all_tour_booking' ? 'active' : '') }}"><a href="{{ url('booking/tour-booking-listing') }}">Tours Bookings</a></li>
                        </ul>
                    </li>
                    <li class="{{ (session('page_name') && session('page_name') == 'agent_users' ? 'active' : '') }}">
                        <a href="{{ url('users/user-management') }}"><i class="icon-user"></i><span>Customer Management</span></a>
                    </li>

                    <!-- <li class="pushy-submenu {{ (session('page_name') == 'licensee' || session('page_name') == 'agent' || session('page_name') == 'admin' || session('page_name') == 'customer') ? 'pushy-submenu-open active open_pushy_cls' : 'pushy-submenu-closed' }}"> -->
                    <li class="pushy-submenu {{ (session('page_name') == 'settings' || session('page_name') == 'switchDomains') ? 'pushy-submenu-open active open_pushy_cls' : 'pushy-submenu-closed' }}">
                        <a href="javascript://">
                            <i class="icon-user"></i>
                            <span>General Settings</span>
                        </a>
                        <ul>
                            <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'settings' ? 'active' : '') }}">
                                <a href="{{ route('agent.settings') }}">
                                    Account Details 
                                </a>
                            </li>
                            <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'switchDomains' ? 'active' : '') }}">
                                <a href="{{ route('agent.switch-domains') }}">
                                    Switch Domains
                                </a>
                            </li>

                        </ul>
                    </li>

                    <li class="{{ (session('page_name') && session('page_name') == 'label' ? 'active' : '') }}">
                        <a href="https://eroamsupport.atlassian.net/servicedesk/customer/portal/1" target="_blank">
                            <i class="icon-label"></i>
                            <span>Support & FAQ</span>
                        </a>
                    </li>

                </ul>
            </div>
        </nav>
    </div>
@else
    <div class="page-sidebar-wrapper">
        <nav class="pushy pushy-left" style="visibility: visible;">
            <div class="pushy-height">
                <ul>
                    <li class="expand-all"><a href="javascript://"><i class="icon-unfold-more"></i><span>Expand All Menu Items</span></a></li>
                    <li class="collapse-all active"><a href="javascript://"><i class="icon-unfold-less"></i><span>Collapse All Menu Items</span></a></li>
                    <div class="divider-text">Accounts &amp; Reporting</div>
                    <li class="pushy-submenu {{ (session('page_name')=='dashboard') ? 'pushy-submenu-open active open_pushy_cls' : 'pushy-submenu-closed' }}"><a href="{{url('/')}}"><i class="icon-dashboard"></i><span>Dashboard</span></a></li>

                    @if(Auth::user()->can('accounts_reporting'))
                        <div class="divider-text">Accounts &amp; Reporting</div>
                    @endif
                    <li class="pushy-submenu {{ (session('page_name')=='dashboard') ? 'pushy-submenu-open active open_pushy_cls' : 'pushy-submenu-closed' }}"><a href="{{url('/')}}"><i class="icon-dashboard"></i><span>Dashboard</span></a></li>

                    @if(Auth::user()->can('booking_management'))

                        <li class="pushy-submenu {{ (session('page_name')=='itenary' || session('page_name')=='book_tour') ? 'pushy-submenu-open active open_pushy_cls' : 'pushy-submenu-closed' }}">
                            <a href="javascript://"><i class="icon-booking"></i><span>{{ trans('messages.booking_management') }}</span></a>
                            <ul>
                                <li class="pushy_link_cls {{ (session('page_name') == 'itenary' ? 'active' : '') }}"><a href="{{ route('booking.itenary-list') }}">{{ trans('messages.itinerary_bookings') }}</a></li>
                                <li class="pushy_link_cls {{ (session('page_name') == 'book_tour' ? 'active' : '') }}"><a href="{{ route('booking.booked-tour-list') }}">{{ trans('messages.tour_bookings') }}</a></li>
                                {{-- <li class="pushy_link_cls {{ (session('page_name') == 'viator_activity_booking' ? 'active' : '') }}"><a href="{{ route('booking.viator-booking-list') }}">Viator Bookings</a></li> --}}
                            </ul>
                        </li>
                    @endif
                    @if(Auth::user()->can('currency'))
                        <li class="{{ (session('page_name') && session('page_name') == 'currency' ? 'active' : '') }}">
                            <a href="{{ route('common.currency-list') }}"><i class="icon-currency"></i><span>{{ trans('messages.currency_management') }}</span></a>
                        </li>
                    @endif
                    {{-- administration related path start --}}
                    @if(Auth::user()->can('administration'))
                        <div class="divider-text">{{ trans('messages.admin') }}</div>
                        <li class="pushy-submenu {{ (session('page_name') == 'licensee' || session('page_name') == 'product' || session('page_name') == 'brand' || session('page_name') == 'agent' || session('page_name') == 'admin' || session('page_name') == 'customer') ? 'pushy-submenu-open active open_pushy_cls' : 'pushy-submenu-closed' }}">
                            <a href="javascript://">
                                <i class="icon-user"></i>
                                <span>{{ trans('messages.admin') }}</span>
                            </a>
                            <ul>
                                @if(Auth::user()->can('licensee'))
                                    <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'licensee' ? 'active' : '') }}">
                                        <a href="{{ route('user.list',['sType' => config('constants.USERTYPELICENSEE') ]).'?isreset=1' }}">
                                            {{ trans('messages.licensee') }}
                                        </a>
                                    </li>
                                @endif
                                @if(Auth::user()->can('agent'))
                                    <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'agent' ? 'active' : '') }}">
                                        <a href="{{ route('user.list',['sType' => config('constants.USERTYPEAGENT') ]).'?isreset=1' }}">
                                            {{ trans('messages.agent') }}
                                        </a>
                                    </li>
                                @endif
                                @if(Auth::user()->can('admin'))
                                    <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'admin' ? 'active' : '') }}">
                                        <a href="{{ route('user.list',['sType' => config('constants.USERTYPEADMIN') ]).'?isreset=1' }}">
                                            {{ trans('messages.admin') }}
                                        </a>
                                    </li>
                                @endif
                                @if(Auth::user()->can('customer'))
                                    <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'customer' ? 'active' : '') }}">
                                        <a href="{{ route('user.list',['sType' => config('constants.USERTYPECUSTOMER') ]).'?isreset=1' }}">
                                            {{ trans('messages.customer') }}
                                        </a>
                                    </li>
                                @endif
                                @if(Auth::user()->can('product'))
                                    <li class="pushy_link_cls {{ (session('page_name') && session('page_product') == 'product' ? 'active' : '') }}">
                                        <a href="{{ route('user.list',['sType' => config('constants.USERTYPEPRODUCT') ]).'?isreset=1' }}">
                                            {{ trans('messages.product') }}
                                        </a>
                                    </li>
                                @endif
                                @if(Auth::user()->can('brand'))
                                    <li class="pushy_link_cls {{ (session('page_name') && session('page_brand') == 'brand' ? 'active' : '') }}">
                                        <a href="{{ route('user.list',['sType' => config('constants.USERTYPEBRAND') ]).'?isreset=1' }}">
                                            {{ trans('messages.brand') }}
                                        </a>
                                    </li>
                                @endif

                            </ul>
                        </li>
                    @endif

                    {{-- administration related path end --}}

                    {{-- label related path start --}}
                    @if(Auth::user()->can('label'))
                        <li class="{{ (session('page_name') && session('page_name') == 'label' ? 'active' : '') }}">
                            <a href="{{ route('common.label-list').'?isreset=1' }}">
                                <i class="icon-label"></i>
                                <span>{{ trans('messages.label') }}</span>
                            </a>
                        </li>
                    @endif
                    {{-- label related path end --}}

                    {{-- GeoData related path start --}}

                    @if(Auth::user()->can('geo_data'))
                        <div class="divider-text">{{ trans('messages.manage_geo_data') }}</div>
                        @if(Auth::user()->type!='admin')
                            <li class="pushy-submenu {{ (session('page_name') == 'region' || session('page_name') == 'city' || session('page_name') == 'country' || session('page_name') == 'suburb' ) ? 'pushy-submenu-open active open_pushy_cls' : 'pushy-submenu-closed' }}"><a href="javascript://"><i class="icon-geodata"></i><span>{{ trans('messages.geo_data') }}</span></a>
                                <ul>
                                    <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'region' ? 'active' : '') }}"><a href="{{ route('common.manage_user_geo_data').'?isreset=1' }}">{{ trans('messages.manage_giodata') }}</a></li>
                                </ul>
                            </li>
                        @else
                            <li class="pushy-submenu {{ (session('page_name') == 'region' || session('page_name') == 'city' || session('page_name') == 'country' || session('page_name') == 'suburb' ) ? 'pushy-submenu-open active open_pushy_cls' : 'pushy-submenu-closed' }}"><a href="javascript://"><i class="icon-geodata"></i><span>{{ trans('messages.geo_data') }}</span></a>
                                <ul>
                                    <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'region' ? 'active' : '') }}"><a href="{{ route('common.region-list').'?isreset=1' }}">{{ trans('messages.manage_region') }}</a></li>
                                    <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'country' ? 'active' : '') }}"><a href="{{ route('common.country-list').'?isreset=1' }}">{{ trans('messages.manage_country') }}</a></li>
                                    <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'city' ? 'active' : '') }}"><a href="{{ route('common.city-list').'?isreset=1' }}">{{ trans('messages.manage_city') }}</a></li>
                                    <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'suburb' ? 'active' : '') }}"><a href="{{ route('common.suburb-list').'?isreset=1' }}">{{ trans('messages.manage_suburb') }}</a></li>
                                </ul>
                            </li>
                        @endif
                    @endif
                    {{-- GeoData related path end --}}

                    {{-- client on boarding related path start --}}
                    @if(Auth::user()->can('client_onboarding'))
                        <div class="divider-text">{{ trans('messages.manage_client_onboarding') }}</div>

                        <li class="pushy-submenu {{ (session('page_name') == 'onboarding' || session('page_name') == 'onboarding_list') ? 'pushy-submenu-open active open_pushy_cls':'pushy-submenu-closed' }}"><a href="javascript://"><i class="icon-user"></i><span>{{ trans('messages.client_onboarding') }}</span></a>
                            <ul>
                                <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'onboarding' ? 'active' : '') }}"><a href="{{ route('onboarding.general') }}">{{ trans('messages.client_onboarding') }}</a></li>
                                <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'onboarding_list' ? 'active' : '') }}"><a href="{{ route('onboarding') }}">{{ trans('messages.list') }}</a></li>

                            </ul>
                        </li>
                    @endif
                    {{-- client on boarding related path end --}}

                    {{-- inventory_market_place related path start --}}

                    @if(Auth::user()->can('inventory_market_place'))
                        <div class="divider-text">{{ trans('messages.manage_inventory_market_place') }}</div>

                        <li class="pushy-submenu {{ (session('page_name')=='acomodation' || session('page_name')=='ac_supplier'|| session('page_name')=='ac_label'
                    || session('page_name')=='ac_season' || session('page_name')=='ac_price' || session('page_name')=='ac_markup' || session('page_name')=='ac_room' 
                    || session('page_name')=='aot_location'|| session('page_name')=='aot_supplier' || session('page_name')=='expedia_hotel_mapping' ) ? 'pushy-submenu-open active open_pushy_cls' : 'pushy-submenu-closed' }}">
                            <a href="javascript://"><i class="icon-hotel"></i>
                                <span>{{ trans('messages.acomodation_management') }}</span>
                            </a>
                            <ul>
                                @if(Auth::user()->can('accomodation_management'))
                                    <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'acomodation' ? 'active' : '') }}"><a href="{{ route('acomodation.hotel-list').'?isreset=1' }}">{{ trans('messages.acomodation_management') }}</a></li>
                                @endif
                                @if(Auth::user()->can('acc_manage_suppliers'))
                                    <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'ac_supplier' ? 'active' : '') }}"><a href="{{ route('acomodation.hotel-supplier-list').'?isreset=1' }}">{{ trans('messages.manage_suppliers') }}</a></li>
                                @endif
                                @if(Auth::user()->can('acc_manage_labels_mapping'))
                                    <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'ac_label' ? 'active' : '') }}"><a href="{{ route('acomodation.hotel-label-list').'?isreset=1' }}">{{ trans('messages.manage_mapping') }}</a></li>
                                @endif
                                @if(Auth::user()->can('acc_manage_seasons'))
                                    <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'ac_season' ? 'active' : '') }}"><a href="{{ route('acomodation.hotel-season-list').'?isreset=1' }}">{{ trans('messages.manage_seasons') }}</a></li>
                                @endif
                                @if(Auth::user()->can('acc_manage_prices'))
                                    <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'ac_price' ? 'active' : '') }}"><a href="{{ route('acomodation.hotel-price-list').'?isreset=1' }}">{{ trans('messages.manage_prices') }}</a></li>
                                @endif
                                @if(Auth::user()->can('acc_manage_mark_up'))
                                    <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'ac_markup' ? 'active' : '') }}"><a href="{{ route('acomodation.hotel-markup-list').'?isreset=1' }}">{{ trans('messages.manage_markup') }}</a></li>
                                @endif
                                @if(Auth::user()->can('acc_manage_room_types'))
                                    <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'ac_room' ? 'active' : '') }}"><a href="{{ route('acomodation.hotel-room-list').'?isreset=1' }}">{{ trans('messages.manage_roomtype') }}</a></li>
                                @endif
                                @if(Auth::user()->can('acc_aot_location'))
                                    <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'aot_location' ? 'active' : '') }}"><a href="{{ route('acomodation.hotel-aotlocation-list') }}">{{ trans('messages.manage_aotlocation') }}</a></li>
                                @endif
                                @if(Auth::user()->can('acc_aot_supplier'))
                                    <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'aot_supplier' ? 'active' : '') }}"><a href="{{ route('acomodation.hotel-aotsupplier-list').'?isreset=1' }}">{{ trans('messages.manage_aotsupplier') }}</a></li>
                                @endif
                                @if(Auth::user()->can('acc_expedia_hotel_mapping'))
                                    <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'expedia_hotel_mapping' ? 'active' : '') }}"><a href="{{ route('acomodation.expedia-hotel-mapping').'?isreset=1' }}">{{ trans('messages.manage_expedia_hotel') }}</a></li>
                                @endif
                            </ul>
                        </li>
                        @if(Auth::user()->can('transport_management'))
                            <li class="pushy-submenu {{ in_array(session("page_name") ,array('transport','ts_supplier','ts_operator','ts_season','ts_type','ts_markup','airlines')) ? 'pushy-submenu-open active open_pushy_cls' : 'pushy-submenu-closed' }}">
                                <a href="javascript://"><i class="icon-transport"></i><span>{{ trans('Transport Management') }}</span></a>
                                <ul>
                                    @if(Auth::user()->can('t_manage_transport'))
                                        <li class="pushy_link_cls {{ (session('page_name') == 'transport' ? 'active' : '') }}"><a href="{{ route('transport.transport-list').'?isreset=1' }}">{{ trans('Manage Transport') }} </a></li>
                                    @endif
                                    @if(Auth::user()->can('t_manage_suppliers'))
                                        <li class="pushy_link_cls {{(session('page_name') == 'ts_supplier' ? 'active' : '') }}"><a href="{{ route('transport.transport-supplier-list').'?isreset=1' }}">Manage Suppliers</a></li>
                                    @endif
                                    @if(Auth::user()->can('t_manage_operator'))
                                        <li class="pushy_link_cls {{(session('page_name') == 'ts_operator' ? 'active' : '') }}"><a href="{{ route('transport.transport-operator-list').'?isreset=1' }}">{{ trans('Manage Operator') }}</a></li>
                                    @endif
                                    @if(Auth::user()->can('t_manage_season'))
                                        <li class="pushy_link_cls {{(session('page_name') == 'ts_season' ? 'active' : '') }}"><a href="{{ route('transport.transport-season-list').'?isreset=1' }}">{{ trans('Manage Season') }}</a></li>
                                    @endif
                                    @if(Auth::user()->can('t_transport_type'))
                                        <li class="pushy_link_cls {{(session('page_name') == 'ts_type' ? 'active' : '') }}"><a href="{{ route('transport.transport-type-list').'?isreset=1' }}">{{ trans('Manage Transport Type') }}</a></li>
                                    @endif
                                    @if(Auth::user()->can('t_manage_mark_up'))
                                        <li class="pushy_link_cls {{(session('page_name') == 'ts_markup' ? 'active' : '') }}"><a href="{{ route('transport.transport-markup-list').'?isreset=1' }}">{{ trans('Manage Mark-Up') }}</a></li>
                                    @endif
                                    @if(Auth::user()->can('t_manage_airlines'))
                                        <li class="pushy_link_cls {{(session('page_name') == 'airlines' ? 'active' : '') }}"><a href="{{ route('transport.airlines').'?isreset=1' }}">{{ trans('Manage Airlines') }}</a></li>
                                    @endif
                                </ul>
                            </li>
                        @endif
                        @if(Auth::user()->can('a_activity_management'))
                            <li class="pushy-submenu {{ (session('page_name')=='activity' || session('page_name')=='activity_supplier' || session('page_name')=='activity_season'
                                            || session('page_name')=='activity_mark'|| session('page_name')=='activity_operator') ? 'pushy-submenu-open active open_pushy_cls' : 'pushy-submenu-closed' }}">
                                <a href="javascript://"><i class="icon-activity"></i><span>{{ trans('messages.activity_management') }}</span></a>
                                <ul>
                                    @if(Auth::user()->can('a_manage_activities'))
                                        <li class="pushy_link_cls {{ (session('page_name') == 'activity' ? 'active' : '') }}"><a href="{{ route('activity.activity-list') }}">{{ trans('messages.manage_activity') }}</a></li>
                                    @endif
                                    @if(Auth::user()->can('a_manage_suppliers'))
                                        <li class="pushy_link_cls {{ (session('page_name') == 'activity_supplier' ? 'active' : '') }}"><a href="{{ route('activity.activity-supplier-list').'?isreset=1' }}">{{ trans('messages.manage_suppliers') }}</a></li>
                                    @endif
                                    @if(Auth::user()->can('a_manage_seasons'))
                                        <li class="pushy_link_cls {{ (session('page_name') == 'activity_season' ? 'active' : '') }}"><a href="{{ route('activity.activity-season-list').'?isreset=1' }}">{{ trans('messages.manage_seasons') }}</a></li>
                                    @endif
                                    @if(Auth::user()->can('a_manage_mark_up'))
                                        <li class="pushy_link_cls {{ (session('page_name') == 'activity_mark' ? 'active' : '') }}"><a href="{{ route('activity.activity-markup-list').'?isreset=1' }}">{{ trans('messages.manage_markup') }}</a></li>
                                    @endif
                                    @if(Auth::user()->can('a_activity_operator'))
                                        <li class="pushy_link_cls {{ (session('page_name') == 'activity_operator' ? 'active' : '') }}"><a href="{{ route('activity.activity-operator-list').'?isreset=1' }}">{{ trans('messages.manage_operator') }}</a></li>
                                    @endif
                                </ul>
                            </li>
                        @endif
                        @if(Auth::user()->can('route_plans_create_itinerary'))
                            <li class="{{ (session('page_name') && session('page_name') == 'route' ? 'active' : '') }}">
                                <a href="{{ route('route.route-list').'?isreset=1' }}"><i class="icon-route"></i><span>{{ trans('messages.route_plans') }}</span></a>
                            </li>
                        @endif
                    @endif
                    {{-- inventory_market_place related path end --}}

                    {{-- tour related path start --}}
                    @if(Auth::user()->can('tour_management'))
                        <div class="divider-text">{{ trans('messages.manage_tour_management') }}</div>
                        <li class="pushy-submenu {{ (session('page_name')=='tourlist' || session('page_name')=='addtour' || session('page_name')=='tourtypelogo') ? 'pushy-submenu-open active open_pushy_cls' : 'pushy-submenu-closed' }}">
                            <a href="javascript://"><i class="icon-activity"></i><span>{{ trans('messages.tour_management') }}</span></a>
                            <ul>
                                <li class="pushy_link_cls {{ (session('page_name') == 'tourlist' ? 'active' : '') }}"><a href="{{ route('tour.tour-list').'?isreset=1' }}">{{ trans('messages.search_tour') }} </a></li>
                                <li class="pushy_link_cls {{(session('page_name') == 'addtour' ? 'active' : '') }}"><a href="{{ route('tour.tour-create').'?isreset=1' }}">{{ trans('messages.add_tour') }} </a></li>
                                <li class="pushy_link_cls {{(session('page_name') == 'tourtypelogo' ? 'active' : '') }}"><a href="{{ route('tour.tour-type-logo-list').'?isreset=1' }}">{{ trans('messages.tour_type_logo') }}</a></li>
                            </ul>
                        </li>
                    @endif
                    {{-- tour related path end --}}

                    {{-- market_place related path start --}}
                    @if(Auth::user()->can('offer_place'))
                        <div class="divider-text">{{ trans('messages.offer_manage') }}</div>
                        <li class="{{ (session('page_name') && session('page_name') == 'specialoffer' ? 'active' : '') }}">
                            <a href="{{ route('special-offer.list') }}">
                                <i class="icon-markup"></i>
                                <span>{{ trans('messages.special_offer') }}</span>
                            </a>
                        </li>
                    @endif
                    {{-- market_place related path end --}}

                    {{-- support_faq related path start --}}
                    @if(Auth::user()->can('support_faq'))
                        <div class="divider-text">{{ trans('messages.manage_support') }}</div>
                        <li class="{{ (session('page_name') && session('page_name') == 'support_faq' ? 'active' : '') }}">
                            <a href="#">
                                <i class="icon-faq"></i>
                                <span>{{ trans('messages.support_faq') }}</span>
                            </a>
                        </li>
                    @endif
                    {{-- support_faq related path end --}}

                    {{-- UserManagement related path start --}}
                    @if(Auth::user()->can('user_management'))
                        <div class="divider-text">{{ trans('messages.manage_user_management') }}</div>
                        <li class="{{ (session('page_name') && session('page_name') == 'user_management' ? 'active' : '') }}">
                            <a href="#">
                                <i class="icon-user"></i>
                                <span>{{ trans('messages.user_management') }}</span>
                            </a>
                        </li>
                    @endif
                    {{-- UserManagement related path end --}}

                </ul>
            </div>
        </nav>
    </div>
@endif
