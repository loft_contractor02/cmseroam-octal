@include('layout.header')
<!-- header complete -->
<section class="content-wrapper">
        @include('layout.sidebar')
    <div class="page-content-wrapper">
        <div class="page-container">

          @yield( 'content' )

        </div>
    </div>
</section>

 @include('layout.footer')


