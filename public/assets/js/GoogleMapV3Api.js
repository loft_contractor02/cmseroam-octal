console.log("CONNECTED CNO");

// GLOBAL VARIABLES
var dom, country, _AjaxCtrl, _DomCtrl, _GoogleCtrl, _AlterCtrl,
data_auto_route,
data_alter_id,
data_group_id,
data_temp_coords,
data_async_temp,
data_country,
data_temp_edit_coords = false,
data_temp_marker_visibility = false,
data_diameter = 100,
data_restricted_diameter = 100,
data_coord_origin = {
	name: city_name,
	city_id: city_id
},
data_coord_destination = {
	name: $("#ddlTo option:selected").text(),
	city_id: $("#ddlTo option:selected").val()
},
data_coord_places = [],
data_cities = [],
data_all_cities = [],
data_all_via_cities = [], // added by paul dongallo
data_cost = [],
data_alter_cities = [],
data_group_cities = [],
data_itinerary_data = [],
data_itinerary_record = null,
data_itinerary_details = [],
loaderPercentage = 0;
function init() {

	_DomCtrl.setPageTab(function(eroam_map){
				_AjaxCtrl.promises(data_coord_origin, "GetAllCityData", false).done(function (response) {
					_DomCtrl.setPageLoadMap(eroam_map); // THIS WILL INITIALIZE THE PAGE
				});

	});
}

_AjaxCtrl = (function(){
	function initialize(data, fn, async, additional, status, callback){
		var ajax, result,
		url = '/KeywordSearch.asmx/',
		date = '?'+(new Date()).getTime();
		ajax = {
			contentType: "application/json; charset=utf-8",
			url: url+fn+date,
			type: 'POST',
			data: data || {},
			async: async,
			success: function(response) {
				result = process(response.d, fn, additional);
				if(status){
					callback(result);
				}
			}
		};
		$.ajax(ajax);
		return result;
	}

	function deferred(data, fn, index, ref, additional){
		var ajax, result,
		url = '/KeywordSearch.asmx/',
		date = '?'+(new Date()).getTime(),
		dfd = $.Deferred();
		ajax = {
			contentType: "application/json; charset=utf-8",
			url: url+fn+date,
			type: 'POST',
			data: data || {},
			async: true,
			success: function(response) {
				dfd.resolve(index, process(response.d, fn, additional), ref);
			}
		};	
		$.ajax(ajax);
		return dfd.promise();	
	}

	function promises(data, fn, additional){
		var info = [], 
		result = [],
		max = data.length, 
		count = 0, 
		dfd = $.Deferred(),
		interval, param, ref, validate;
		for (var i = 0; i < max; i++) {
			switch(fn){
				case 'GetAllTransportByCityID':
				case 'GetCityByViaCityID': // added by paul
				case 'GetCountryByCityID':
					param = "{city_id: "+data[i].city_id+",pass_type_ids:''}";
					ref = data[i].city_id;
				break;
				case 'GetAllCityData':
					param = "{country:'"+data[i].country+"'}";
					ref = data[i].country;
					break;
				case 'GetAlterCitiesByCityID':
				case 'GetAlterItineraryByCityID': // located on promises 
					param = "{origin:"+data[i].origin.city_id+", destination:"+data[i].destination.city_id+"}";
					ref = false;
					break;
				case 'GetAlterCitiesByCountries':
					param = "{countries:\""+data[i].join(',')+"\"}";
					ref = false;
					break;
				case 'getAllTransportByCityIds':
				case 'getAllActivitiesByCityIds':
				case 'geAllHotelsByCityIds':

				case 'getAllPassByCityIds':
				case 'getAllCitiesDetailsByCityIds':

					param = "{city_ids: '"+data[i]+"'}";
					ref = false;
					break;
			}

			deferred(param, fn, i, ref, additional).then(function(index, response, ref) {
				response = {
					index : index,
					response : response,
					ref : ref
				};
				switch(fn){
					case 'GetAllTransportByCityID':
					case 'GetCityByViaCityID': // added by paul
					case 'GetCountryByCityID':
						response.city_id = ref;
					break;
				}
				return response;
			}).done(function(response){
				info.push(response);
				count = i;
			});
		};

		interval = setInterval(function(){
			validate = _AlterCtrl.validIndexArray(info, max);
			if(validate && count == max){
				clearInterval(interval);
				_AlterCtrl.setSortArray(info);
				result = info;
				dfd.resolve(result);
			}
		}, 1000);
		return dfd.promise();
	}

	function process(response, fn, additional){ // process working coding inside loop

		var result;
		////console.log("variable result", result); // variable to be returned
		switch(fn){
			case 'GetCountryByCityID': // started from cities selected origin or starting point
				result = response.trim(); // remove white spaces of the Thailand
				////console.log("output result from response", result); // result is the trim from GetCountryByCityID which is Thailand
			break;
			case 'GetCityByViaCityID': // added by paul dongallo
			    result = true;
			    ////console.log("GetCityByViaCityID > result true",result);		
			    if ( response ) {
			    	////console.log("response", "akuakakuauauaua", response); // resposne the relationship has been called
			    	for ( var i = 0; i < response.length; i++ ) {
			    	    var data = {
			    	        city_id: response[i].city_id,
			    	        sequence: response[i].sequence,
			    	        via_name: response[i].city_via_name,
			    	        lat: response[i].lat,
			    	        lng: response[i].lang,
			    	        transport: response[i].trans_type			    	     
			    	    }
			    	    console.log("Get Via City Name", data);
			    	    if(!_AlterCtrl.getCityIDArray(data_all_cities, data.city_id) && !_AlterCtrl.validLatLng(data_all_via_cities, data)){		    	    
			    	  		data_all_via_cities.push(data); // ang parehas ebutang diri
			    	  		////console.log("tanang via cites", data_all_via_cities); // filtered only those has same id
			    	    }
			    	}
			    }
			    break;
			case 'GetAllCityData':
				result = true;
				for (var i = 0; i < response.length; i++) {				
					var data = {
						country : response[i].CountryName,
						city_id : response[i].start_location.toString(),
						name : response[i].start_city.trim(),
						lat : response[i].lat,
						lng : response[i].lng,
						priority : (parseInt(response[i].optional)) ? false : true,
						international : response[i].international,
						duration : response[i].zDuration
					};
		
					if(!_AlterCtrl.getCityIDArray(data_all_cities, data.city_id)){
						data_all_cities.push(data);
					}
				};
			break;
			case 'GetAllTransportByCityIDForGoogleMap':
				result = [];
				for (var i = 0; i < response.length; i++) {
					var data = response[i].split(","),
					data = {
						name : data[2].trim(),
						city_id : data[3].toString(),
						lat : data[0],
						lng : data[1]
					};
					result.push(data);
				}
			break;
			case 'GetAllTransportByCityID':
				switch(additional.option){
					case 'places':
						_AlterCtrl.setCleanRawArray(response, 'cost');
						result = _AlterCtrl.setCleanRawArray(response, 'transport');
						break;
					case 'options':
						_AlterCtrl.setCleanRawArray(response, 'cost');
						var charted = additional.charted,
						expect_charted = additional.expect_charted,
						dom_count = additional.dom_count,
						array = _AlterCtrl.setCleanRawArray(response, 'transport'),
						check = (array && expect_charted) ? _AlterCtrl.getCityIDArray(array, expect_charted.city_id) : false;

						if($('#ddlCity_'+dom_count).length == 0)
							_DomCtrl.setListCityAdd(false);
						if(!check)
							array.push(expect_charted);
						_DomCtrl.setListOptionDom(array, expect_charted, dom_count);
						break;
					case 'cost':
						result = _AlterCtrl.setCleanRawArray(response, 'cost');
						break;
				}
			break;
			case 'GetAllSuggestItineraryByID':
				result = _AlterCtrl.setCleanRawArray(response, 'alternative');
				// //console.log("adfasdf", result);
			break;
			case 'GetAlterCitiesByCityID':
				result = _AlterCtrl.setCleanRawArray(response, 'alternative');
			break;
			case 'GetAlterItineraryByCityID': // process located
				'GetAlterItineraryByCityID';
				result = _AlterCtrl.setCleanRawArray(response, 'itineraries');
				// //console.log("Itinerary Process",result);
				break;
			case 'GetAlterCitiesByCountries':
				result = _AlterCtrl.setCleanRawArray(response, 'transport');
				break;
			case 'AddAlterCitiesData':
				break;
			case 'getAllTransportByCityIds':
				result = _AlterCtrl.setCleanRawArray(response, 'price_transport');
				break;
			case 'getAllActivitiesByCityIds':
				result = _AlterCtrl.setCleanRawArray(response, 'price_activity');
				break;
			case 'geAllHotelsByCityIds':
				result = _AlterCtrl.setCleanRawArray(response, 'price_hotel');
				break;
			case 'getAllPassByCityIds':
				result = _AlterCtrl.setCleanRawArray(response, 'price_transport_pass');
				break;
			case 'getAllCitiesDetailsByCityIds':
				result = _AlterCtrl.setCleanRawArray(response, 'price_cities_details');
				break;
		}
		return result;
	}

	function assortment(fn, callback){
		$.when(fn).then(function(response) {
			callback(response);
		});
	}

	function assortment_classed(fn, callback){
		$.when.apply($, fn).done(function () {
			var data = [];			
		    for (var i = 0; i < arguments.length; i++) {					  
		    	data.push(arguments[i]);
		    }
		    callback(data);
		});
	}

	return {
		initialize : initialize,
		deferred : deferred,
		promises : promises,
		process : process,
		assortment : assortment,
		assortment_classed : assortment_classed
	};

})();

_DomCtrl = (function(){
	function setEventKeys(){
		$(document).keyup(function(e){
			if((e.altKey && e.keyCode == 112) || (e.ctrlKey && e.keyCode == 112) || (e.metaKey && e.keyCode == 112)){
				_DomCtrl.setLearnRoute();
			}else if((e.altKey && e.keyCode == 113) || (e.ctrlKey && e.keyCode == 113) || (e.metaKey && e.keyCode == 113)){
				_DomCtrl.setLearnItinerary();
			}
		});
	}

	function setPageTab(callback){
		$(function(){ 
			var type = itineraryCredentials('get', 'type');
			switch(type){
				case 'oneway':
					$('#TailTabOne').click();
					break;
				case 'multistop':
					$('#TailTabMulti').click();
					break;
				case 'singlestop':
					$('#TailTabSingleStop').click();
					break;
			}
		});
		$('body').on('click', '.TabOptionSelected', function (e) {
			e.preventDefault();
			var content = $(this).attr('data-option-tab'),
				dom = $(this).attr('data-map-init'),
				ddbool = $(this).attr('data-dropdown-bool'),
				btn = $(this).attr('id');
			$('.TabOptionSelected').removeClass('active');
			$('#' + btn).addClass('active');
			$('.TabOptionContent').fadeOut(1000);
			$('#' + content).fadeIn(1000);

			_DomCtrl.setLoadingShow('init');
			callback(dom);
		});
	}

	function setPageSearch(){
		var origin = $("#ddlFrom option:selected").text(),
		destination = $("#ddlTo option:selected").text(),
		traveller = $("#ddlTraveller option:selected").text(),
		date = $('#datePicker').val();

		itineraryCredentials('update', 'origin', origin);
		itineraryCredentials('update', 'destination', destination);
		itineraryCredentials('update', 'date', date);
		itineraryCredentials('update', 'traveller', traveller);
		itineraryCredentials('update', 'alter', '');
		dom_form_submit.click();
	}

	function setPageChangeTravelType(type){
		itineraryCredentials('update', 'type', type);
		dom_form_submit.click();
	}

	function setPageManualOverride(data){
		data_temp_edit_coords = true;
		$(function(){ 
			$('#'+data).click();
		});
	}

	function setPageLoadMap(dom){ /* commented by paul Load Map */
		data_alter_id = setPageAlternativeRoute('alter');
		data_group_id = setPageAlternativeRoute('group');
		dom_object = getDomObject(dom); // selection tab
		if(data_coord_destination.country == 'AUSTRALIA'){
			data_diameter = 500;
			data_restricted_diameter = 260;
		}
		if(data_temp_edit_coords == true){
			_DomCtrl.setDomMultiAlter('alter_dom');
		}

		switch(dom){
			case 'OneWayMap':
				data_auto_route = true;
				//SETTING OF COORDINATE BASED ON DATABASE
				var origin_response = _AlterCtrl.getCityArray(data_all_cities, data_coord_origin.city_id, 'data');
				var destination_response = _AlterCtrl.getCityArray(data_all_cities, data_coord_destination.city_id, 'data');
				var data = {
					data_coord_origin : origin_response,
					data_coord_destination : destination_response,
					data_coord_places : null,
					data_diameter : data_diameter,
					data_restricted_diameter : data_restricted_diameter,
					data_cities : data_cities,
					data_all_cities : data_all_cities,
					data_draggable : false,
					data_event : false,
					data_alter_id : data_alter_id,
					data_group_id : data_group_id,
					data_init_map : 'oneway'
				};

				_GoogleCtrl.initialize(dom_object, data, data_auto_route);

				break;
			case 'MultiStopMap':
				//SETTING OF COORDINATE BASED ON DATABASE
				var origin_response = _AlterCtrl.getCityArray(data_all_cities, data_coord_origin.city_id, 'data');
				// //console.log("Origin Response", origin_response);

				var data = {
					data_coord_origin : origin_response,
					data_coord_destination : null,
					data_coord_places : null,
					data_diameter : data_diameter,
					data_restricted_diameter : data_restricted_diameter,
					data_cities : data_cities,
					data_all_cities : data_all_cities,
					data_draggable : true,
					data_event : true,
					data_alter_id : data_alter_id,
					data_group_id : data_group_id,
					data_init_map : 'multistop'
				};
				_GoogleCtrl.initialize(dom_object, data, false);
				break;
			case 'SingleStopMap':
				//SETTING OF COORDINATE BASED ON DATABASE
				var destination_response = _AlterCtrl.getCityArray(data_all_cities, data_coord_destination.city_id, 'data');
				var data = {
					data_coord_origin : null,
					data_coord_destination : destination_response,
					data_coord_places : null,
					data_diameter : null,
					data_restricted_diameter : null,
					data_cities : null,
					data_all_cities : data_all_cities,
					data_draggable : false,
					data_event : false,
					data_alter_id : null,
					data_group_id : null,
					data_init_map : 'singlestop'
				};

				_GoogleCtrl.initialize(dom_object, data, false);

				break;
		}

	}

	function setPageAlternativeItinerary(countries){

		var dfd = $.Deferred(), data = [], result = [], alter_array = [], alter, alter_coord, alter_id, 
		city_ids, coord, url, details, interval, alter_subtract = 0, alter_cities_ids = [];

		for (var i = 0; i < countries.length; i++) {

			data.push(["'",countries[i],"'"].join(''));

		}

		_AjaxCtrl.promises([{origin : data_coord_origin, destination : data_coord_destination}], "GetAlterCitiesByCityID", false).done(function (response) {
		//_AjaxCtrl.promises([data], "GetAlterCitiesByCountries", false).done(function (response) {

			alter_coord = response[0].response;
			alter_coord = (alter_coord.length) ? _AlterCtrl.setStripAlterCoord(alter_coord) : null;

			if(alter_coord){//IF THERE IS AN ALTERNATIVE DATA FROM THIS ORIGIN AND DESTINATION

				for (var i = 0; i < alter_coord.length; i++) {
	
					if(_AlterCtrl.validCoordAlternativesArray(data_temp_coords, alter_coord[i])){

						alter = (function(alter_data){

							city_ids = alter_data.map(function(value, index){
								return value.city_id;
							}).join(',');

							if(!_AlterCtrl.validCoordAlternativesIds(city_ids, alter_cities_ids)){

								_AjaxCtrl.assortment_classed([
									_AjaxCtrl.promises([city_ids], "getAllTransportByCityIds", false),
									_AjaxCtrl.promises([city_ids], "getAllActivitiesByCityIds", false),
									_AjaxCtrl.promises([city_ids], "geAllHotelsByCityIds", false),
									_AjaxCtrl.promises([city_ids], "getAllCitiesDetailsByCityIds", false)
								],function(array){

									details = {
										transport : array[0][0].response,
										activity : array[1][0].response,
										hotel : array[2][0].response,
										cities : array[3][0].response
									};

									id = alter_data[0].other_id;
									data_alter_cities.push({ id : id, data : alter_data });

									result.push(_DomCtrl.setRouteListAlternatives(alter_data, details, id, 'alter'));

								});

								alter_array.push(alter_data);
								alter_cities_ids.push(city_ids);

							}else{

								alter_subtract++;

							}

						})(alter_coord[i]);

					}else{

						alter_subtract++;

					}
					
				}

				interval = setInterval(function(){

					if((alter_coord.length - alter_subtract) == result.length){

						clearInterval(interval);
						if(result.length === 0){

							dfd.resolve(false, false);
							
						}else{

							dfd.resolve(true, { dom : result.join(''), data : alter_array });

						}

					}

				}, 1000);

			}else{//IF THERE IS "NO" ALTERNATIVE DATA FROM THIS ORIGIN AND DESTINATION

				dfd.resolve(false, false);

			}

		});

		return dfd.promise();

	}

	function setPageGroupItinerary(countries){

		var dfd = $.Deferred(), data = [], result = [], alter_array = [], alter, alter_coord, alter_id, 
		city_ids, coord, url, details, interval, alter_subtract = 0;

		for (var i = 0; i < countries.length; i++) {

			data.push(["'",countries[i],"'"].join(''));
			// //console.log(data);
		}

		_AjaxCtrl.promises([{origin : data_coord_origin, destination : data_coord_destination}], "GetAlterItineraryByCityID", false).done(function (response) {

			alter_coord = response[0].response;
			alter_coord = (alter_coord.length) ? _AlterCtrl.setStripAlterCoord(alter_coord) : null;	

			if(alter_coord){//IF THERE IS AN ALTERNATIVE DATA FROM THIS ORIGIN AND DESTINATION

				for (var i = 0; i < alter_coord.length; i++) {
	
					if(_AlterCtrl.validCoordAlternativesArray(data_temp_coords, alter_coord[i])){

						alter = (function(alter_data){

							city_ids = alter_data.map(function(value, index){
								return value.city_id;
							}).join(',');

							_AjaxCtrl.assortment_classed([
								_AjaxCtrl.promises([city_ids], "getAllTransportByCityIds", false),
								_AjaxCtrl.promises([city_ids], "getAllActivitiesByCityIds", false),
								_AjaxCtrl.promises([city_ids], "geAllHotelsByCityIds", false),
								_AjaxCtrl.promises([city_ids], "getAllCitiesDetailsByCityIds", false)
							],function(array){

								details = {
									transport : array[0][0].response,
									activity : array[1][0].response,
									hotel : array[2][0].response,
									cities : array[3][0].response
								};

								id = alter_data[0].other_id;
								data_group_cities.push({ id : id, data : alter_data });

								result.push(_DomCtrl.setRouteListAlternatives(alter_data, details, id, 'group'));
								// //console.log("find hotels", result);
							});

							alter_array.push(alter_data);

						})(alter_coord[i]);

					}else{

						alter_subtract++;

					}
					
				}

				interval = setInterval(function(){

					if((alter_coord.length - alter_subtract) == result.length){

						clearInterval(interval);
						if(result.length === 0){

							dfd.resolve(false, false);
							
						}else{

							dfd.resolve(true, { dom : result.join(''), data : alter_array });

						}

					}

				}, 1000);

			}else{//IF THERE IS "NO" ALTERNATIVE DATA FROM THIS ORIGIN AND DESTINATION

				dfd.resolve(false, false);

			}

		});

		return dfd.promise();

	}

	function setPageAlternativeRedirect(data_dom){

		var alter_id = $(data_dom).attr('data-attr'),
		alter_type = $(data_dom).attr('data-alter-type'),
		alter_clear;

		if(alter_type == 'alter'){
			alter_array = data_alter_cities;
			alter_clear = 'group';
		}else if(alter_type == 'group'){
			alter_array = data_group_cities;
			alter_clear = 'alter';
		}

		coord = _AlterCtrl.getDataIndexArray(alter_array, alter_id, 'data');

		itineraryCredentials('update', 'origin', coord.data[0].name);
		itineraryCredentials('update', 'destination', coord.data[coord.data.length - 1].name);
		itineraryCredentials('update', alter_type, alter_id);
		itineraryCredentials('update', alter_clear, '');
		dom_form_submit.click();

	}

	function setPageAlternativeRoute(type){

		var alter_id;

		if(type == 'alter'){

			alter_id = $('#hdfAlterRoute').val();

		}else if(type == 'group'){

			alter_id = $('#hdfGroupRoute').val();
		
		}

		result = false;

		if(alter_id){

			result = alter_id;

		}

		return result;

	}

	function setNewListOption(dom_id, auto){

		if($('#ddlCity_'+dom_id).length == 0){
			_DomCtrl.setListCityAdd(true);
		}

	}

	function setListOptionDom(array, data, dom_id){

		$('#ddlCity_'+dom_id).empty();
		$('#ddlCity_'+dom_id).append($("<option></option>").val("-1").html("Where to Next ?"));
		$('#ddlCity_'+dom_id).append($("<option></option>").val("-2").html("Own Arrangements"));

		if(array.length){
		
			for (var i = 0; i < array.length; i++) {
				
				var selected = '';

				if(data.city_id == array[i].city_id){

					selected = "selected='selected'";

				}

				$("#ddlCity_"+dom_id).append($("<option "+selected+"></option>").val(array[i].city_id).html(array[i].name));

			};

		}

	}

	function setListOptionEmpty(){

		$('.ddCity_select').empty();
		$('.ddCity_select').append($("<option></option>").val("-1").html("Where to Next ?"));

	}

	function setListOptionChange(dom_id, dom_origin){

		$('#ddlCity_'+dom_id).empty();
		$('#ddlCity_'+dom_id).append($("#"+dom_origin).html());
		$('#headingRoutePlan').text('Traveling from ' + $("#"+dom_origin+" option:selected").text());

	}

	function setListOptionChangeSelect(dom_id, data, temp_data){

		data_async_temp = [];
		data_async_temp = temp_data;
		$('#ddlCity_'+dom_id).val(data);
		$('#ddlCity_'+dom_id).change();

	}

	function setBtnAutoRouteClick(){

		$('#auto-route-btn').click();

	}

	function setBtnAutoEditClick(){

		$('#edit-map-itinerary-btn').click();

	}

	function setListLastSelected(){

		var result;

		$('.ddCity_select').each(function(i, obj) {

			if($(this).find('option').is(':selected') && !($(this).val() == -1 || !$(this).val() == -2)){

				result = {
					name: $("#"+obj.id+" option:selected").text(),
					city_id: $(this).val(),
					dom_id: parseInt(obj.id.split('_')[1])
				};

			}

		});

		return result;

	}

	function setListCityLoad(dom){

		var dom_id = dom.id,
			dom_index = parseInt(dom_id.split('_')[1]),
			city_id = $("#"+dom_id).val(),
			city_name = $("#"+dom_id+" option:selected").text();

		switch(city_name){

			case '-1':

				$("#ddlCity_" + dom_id).empty();
				$("#ddlCity_" + dom_id).append($("<option selected='selected'></option>").val("-1").html("Where to Next ?"));
				$("#ddlCity_" + parseInt(dom_index) + 1).empty();

				break;
			case '-2':

				$('#ddlCity_' + dom_id).empty();
				$("#ddlCity_" + dom_id).append($("<option selected='selected'></option>").val("-1").html("Where to Next ?"));
				$('#ddlCity_' + dom_id).append($('#ddlFrom').html());
				$("#ddlCity_" + dom_id).val('-1');

				break;

		}

		$(dom).next("select").nextAll("select").val("-1");
		_GoogleCtrl.setListRoute("ddlCity_"+parseInt(dom_index), city_id, true, data_async_temp);

	}

	function setListCityAdd(auto){

		var city_id, city_name,
		list_count = parseInt($("#DDLCount").val()) + 1,
		list_html = [
			"<select name='' class='ddCity_select' id='ddlCity_" + list_count + "'  onchange='_DomCtrl.setListCityLoad(this);' >",
				"<option value='-1'>",
					"Where to Next ?",
				"</option>",
			"</select>"
		].join('');

		$("#DDLCount").val(list_count);
		$('#DDLDiv').append(list_html);

		if(!auto){

			city_id = $("#ddlCity_"+(list_count - 1)).val();
			city_name = $("#ddlCity_"+(list_count - 1)+" option:selected").text();

			if (city_id && city_id != "-1" && city_id != "-2") {
				$('#ddlCity_'+list_count).empty();
				_GoogleCtrl.setListRoute("ddlCity_"+parseInt(list_count - 1), city_id, false, data_async_temp);
			}

		}

	}

	function setRouteOnewayDom(){

		var result = "", valid,
			count = $("#DDLCount").val();

		valid = valitListAtleastOne();

		for (var i = 1; i <= count; i++) {

			var data = $("#ddlCity_"+i).val();

			if(data != "-1" && data != "-2" && data){
				result += data+",";
			}

		};

		$("#hdnCitiesIDs").val(result);
		$("#test_locations").val(result);
		//getAllTransportAndPasses();

	}

	function setRouteOnewayhdDom(array){

		var result = "";

		for (var i = 0; i < array.length; i++) {
			result += array[i].city_id+",";
		};

		data_temp_coords = array;
		$("#hdfCityIDs").val(result);
		$("#test_locations").val(result);
		//getAllTransportAndPasses();

	}

	function setRouteListSelected(array){

		var origin = array[0].name;
		var destination = array[array.length - 1].name;
		var places = [];

		for (var i = 1; i < array.length; i++) {
			places.push(array[i].name);
		};

		var html = [
			'<h4 style="font-weight:bold;color:#008ed6">'+origin+' to '+destination+'</h4>',
			'<div style="color:#7d7d7d;white-space: nowrap;overflow:hidden !important;text-overflow: ellipsis;">',
				'via '+places.join(', '),
			'</div>',
			'<div style="color:#000000;font-weight:bold;" id="SelectedItineraryPrice">',
			'</div>',
		].join('');

		$("#SelectedItinerary").html(html);

	}

	function setRouteListAlternatives(array, details, id, type){

		var origin = array[0].name;
		var destination = array[array.length - 1].name;
		var traveller = parseInt(itineraryCredentials('get', 'traveller'));
		var places = [];
		var info = setRouteListSelectPrice(array, details, traveller, type);
		var s = (info.duration > 1) ? "s" : "";
		var total = info.price / traveller;
		//var duration = setTimeSplit(info.duration);

		for (var i = 1; i < array.length; i++) {
			places.push(array[i].name);
		}

		var html = [
			'<li>',
				'<a href="#" style="text-decoration: none;" class="alternative-event" data-attr="'+id+'" data-alter-type="'+type+'">',
					'<div style="width:100%;height:30%;background:#ffffff;margin:10px 0 10px 0;padding:5px;">',
						'<h4 style="font-weight:bold;color:#008ed6">'+origin+' to '+destination+'</h4>',
						'<div style="color:#7d7d7d;white-space: nowrap;overflow:hidden !important;text-overflow: ellipsis;">',
							'via '+places.join(', '),
						'</div>',
						'<div style="color:#000000;font-weight:bold;">',
							info.duration+' Day'+s+' <span class="alternative-currency-ts">AU$</span>',
							'<span class="alternative-cost-value">'+Math.round(total)+' per person</span>',
							'<span class="alternative-cost-value-hidden" style="display:none;">'+total+'</span>',
						'</div>',
					'</div>',
				'</a>',
				'<div class="alter-loading-'+type+'-'+id+' text-right" style="margin:-30px 10px 0 0"><i class="fa fa-refresh fa-spin"></i></div>',
			'</li>'
		].join('');

		return html;

	}

	function setTimeSplit(duration){

		var result = {};
		duration = parseFloat(duration/24).toFixed(1);
		duration = duration.split('.');
		result.days = (duration[0]) ? duration[0]+" Days " : "";
		result.hours = (duration[1]) ? duration[1]+" Hours " : "";

		return result;

	}

	function setRouteListSelectPrice(array, details, traveller, type){

		var filtered_transport, filtered_activity, filtered_hotel, 
		hotel_cheap_price = 0, hotel_room_type = itineraryCredentials('get', 'room_type'), hotel_room_type_other = null, 
		hotel_category = itineraryCredentials('get', 'category'),
		data, origin, destination, duration, other_data, other_arr, other_price, other_currency,
		fixed_data = null, filter_data, filter_data_ext, filter_price,
		result = {
			price: 0,
			duration: 0
		};

		//IF THERE ARE NO ROOMS TYPES
		if(!hotel_room_type){

			switch(traveller){
				
				case 1:
					hotel_room_type  = 'single';
					hotel_room_type_other = 'dorm';
					break;
				case 2:
					hotel_room_type = 'twin';
					break;
				case 3:
					hotel_room_type = 'triple';
					break;
				case 4:
					hotel_room_type = 'quad';
					break;
				default:
					hotel_room_type  = 'dorm';
					break;

			}

		}

		if(details){

			filtered_hotel = _AlterCtrl.setStripCityIDCoord(details.hotel);
			filtered_hotel = _AlterCtrl.setCityIDCoord(filtered_hotel);

			filtered_activity = _AlterCtrl.setStripCityIDCoord(details.activity);
			filtered_activity = _AlterCtrl.setCityIDCoord(filtered_activity);

			filtered_transport = _AlterCtrl.setStripOriginDestinationCoord(details.transport);
			filtered_transport = _AlterCtrl.setOriginDestinationCoord(filtered_transport);

			for (var i = 0; i < array.length; i++) {
				
				//NEW FUNCTION - ADDED BY CALOY - FOR CALCULATING FIXED GROUP AND ALTER ITINERARY
				if(type == 'group'){

					//DURATION
					data = array[i];

					if(data){
						duration = data.itinerary_hotel_nights;
						result.duration += data.itinerary_hotel_nights;
					}

				}else{

					//DURATION
					data = _AlterCtrl.getCityArray(details.cities, array[i].city_id, 'data');

					if(data){
						duration = data.duration;
						result.duration += data.duration;
					}

				}

				/*//OLD FUNCTION - COMMENT OUT BY CALOY
				//DURATION
				data = _AlterCtrl.getCityArray(details.cities, array[i].city_id, 'data');

				if(data){
					duration = data.duration;
					result.duration += data.duration;
				}*/

				//TOTAL PRICES
				//GET PRICE FOR TRANSPORT
				if(array[i + 1]){

					origin = array[i].city_id;
					destination = array[i + 1].city_id;
					data = _AlterCtrl.getCoordArray(filtered_transport, origin, destination, 'data');

					//NEW FUNCTION - ADDED BY CALOY - FOR FILTER THE GROUP AND ALTER TRANSPORT PRICE
					if(type == 'group'){

						filter_data = (data) ? data.data : null;

						if(filter_data){

							filter_data_ext = (array[i]) ? _AlterCtrl.getItineraryArray(filter_data, array[i].itinerary_transport_id, 'array', 'transport') : null;
							filter_data_ext = (filter_data_ext && filter_data_ext.length) ? _AlterCtrl.getItineraryDefaultArray(filter_data_ext) : null;

							if(filter_data_ext){

								filter_data = filter_data_ext;

							}else{

								filter_data = filter_data;

							}

						}

						filter_price = (data && filter_data) ? _AlterCtrl.getDataPriceArray('transport', filter_data) : 0;

					}else{

						filter_data = (data) ? data.data : null;
						filter_price = (data && filter_data) ? _AlterCtrl.getDataPriceArray('transport', filter_data) : 0;

					}

					result.price += (data) ? filter_price : 0;

					//OLD FUNCTION - COMMENT OUT BY CALOY
					//result.price += (data) ? _AlterCtrl.getDataPriceArray('transport', data.data) : 0;

				}

				//GET PRICE FOR ACTIVITY
				data = _AlterCtrl.getCityArray(filtered_activity, array[i].city_id, 'data');

				//NEW FUNCTION - ADDED BY CALOY - FOR FILTER THE GROUP AND ALTER ACTIVITY PRICE
				if(type == 'group'){

					filter_data = (data) ? data.data : null;

					if(filter_data){

						filter_data_ext = (array[i]) ? _AlterCtrl.getItineraryArray(filter_data, array[i].itinerary_activities_id, 'array', 'activity') : null;
						filter_data_ext = (filter_data_ext && filter_data_ext.length) ? _AlterCtrl.getItineraryDefaultArray(filter_data_ext) : null;

						if(filter_data_ext){

							filter_data = filter_data_ext;

						}else{

							filter_data = filter_data;

						}

					}

					filter_price = (data && filter_data) ? _AlterCtrl.getDataPriceArray('activity', filter_data) : 0;

				}else{

					filter_data = (data) ? data.data : null;
					filter_price = (data && filter_data) ? _AlterCtrl.getDataPriceArray('activity', filter_data) : 0;

				}

				result.price += (data) ? filter_price : 0;
				//OLD FUNCTION - COMMENT OUT BY CALOY
				//result.price += (data) ? _AlterCtrl.getDataPriceArray('activity', data.data) : 0;

				//GET PRICE FOR HOTEL
				data = _AlterCtrl.getCityArray(filtered_hotel, array[i].city_id, 'data');

				//NEW FUNCTION - ADDED BY CALOY - FOR FILTER THE GROUP AND ALTERNATIVES HOTELS
				if(type == 'group'){

					hotel_room_type = array[i].itinerary_hotel_roomtype;

					filter_data = (data) ? data.data : null;

					if(filter_data){

						filter_data_ext = (array[i]) ? _AlterCtrl.getItineraryArray(filter_data, array[i].itinerary_hotel_id, 'array', 'hotel') : null;
						filter_data_ext = (filter_data_ext && filter_data_ext.length) ? filter_data_ext : null;

						if(filter_data_ext){

							filter_data = filter_data_ext;

						}else{

							filter_data = filter_data;

						}

					}
					
				}else{

					filter_data = (data) ? data.data : null;

				}

				if(!hotel_category){

					hotel_cheap_price = _AlterCtrl.getDataPriceArray('hotel', filter_data, hotel_room_type, hotel_room_type_other);

				}else{

					other_data = _AlterCtrl.getCategoryArray(filter_data, hotel_category, 'data');

					if(other_data){

						hotel_cheap_price = convert_to_au(other_data.currency, other_data[hotel_room_type]);

						if(hotel_cheap_price === 0){

							other_arr = [other_data['dorm'] === 0 ? false : other_data['dorm'], 
										other_data['single'] === 0 ? false : other_data['single'], 
										other_data['twin'] === 0 ? false : other_data['twin'],
										other_data['triple'] === 0 ? false : other_data['triple'],
										other_data['quad'] === 0 ? false : other_data['quad'],
										other_data['child_rate'] === 0 ? false : other_data['child_rate']]; // added by new code

							_AlterCtrl.setCleanArray(other_arr);

							hotel_cheap_price = other_arr.reduce(function(a, b, i, arr) {
								return Math.min(a,b)
							});

							hotel_cheap_price = convert_to_au(other_data.currency, hotel_cheap_price);

						}

					}else{

						hotel_cheap_price = _AlterCtrl.getDataPriceArray('hotel', filter_data, hotel_room_type, hotel_room_type_other);

					}

				}

				result.price += hotel_cheap_price * duration;

			}

		}

		return result;

	}

	/* commented by paul selected multistop */
	function setRouteMultistopDom(array){
		var result = "";
		for (var i = 0; i < array.length; i++) {
			result += array[i].city_id+",";
		};
		data_temp_coords = array;
		$("#hdfCityIDs").val(result);
		$("#test_locations").val(result);
		getAllTransportAndPasses(); // TO BE USED

	}

	function setLinkPreventDefault(dom_name){
		$('body').on('click', dom_name, function(e){
			e.preventDefault();
		});
	}

	function setDatePicker(dom_name){
		$(dom_name).datepicker({ dateFormat: 'dd-mm-yy' });
	}

	function setSelectToArray(dom_id){
		var dom = $("select[id*=" + dom_id + "] option"),
		results = [];
		dom.each(function () {
			var val = $(this).val();
			if (val !== '') results.push(val);
		});
		return results;
	}

	function setLoadingShow(type, status, data_ref, data_done){
		var html, icon, data, collected = [];
		$('.ajax-loader-background').show();
		$('.ajax-loader').show();
		switch(type){
			/*
			case 'init':
				if(!status){
					icon = '<i class="fa fa-refresh fa-spin"></i>';
				}else{
					icon = '<i class="fa fa-check text-success"></i>';
				}
				html = icon+' <label>Preparing data  testsetsetrset </label>';
				loaderPercentage += 3;
				break;
			*/
			case 'init':
				if(query_string( 'type' ) === 'singlestop' ){
					if(!status){
						loaderPercentage = 100;
						icon = '<i class="fa fa-refresh fa-spin"></i>';
					}
				}

				if(!status){
					icon = '<i class="fa fa-refresh fa-spin"></i>';
				}else{
					loaderPercentage = 100;
					icon = '<i class="fa fa-check text-success"></i>';
				}
				if(query_string( 'type' ) === 'singlestop' ){
					if(status){
						icon+' <label> Preparing data  </label>';	
					} 
				}
				
				if(query_string( 'type' ) === 'singlestop' ){

					html = icon+' <label>Finalizing</label>';
				} else {
					html = icon+' <label> Preparing data  </label>';
				}	

				break;
			case 'data' :
				data = [];
				////console.log('wut wut');
				//FILTER ALL AND COLLECT THAT ARE DONE REQUESTING
				// //console.log('data done? : ', data_done);
				if(data_done.length){
					for (var i = 0; i < data_done.length; i++) {
						if(collected.length){

							//console.log('check if');
							if(data_done[i].request.origin && !_AlterCtrl.getCityIDArray(collected, data_done[i].request.origin.city_id)){
								if(!_AlterCtrl.validLatLng(data_all_via_cities, data_done[i].request.origin)){
									collected.push(data_done[i].request.origin);
									//console.log(" test validate", collected);
								}
							}
							if(data_done[i].request.destination && !_AlterCtrl.getCityIDArray(collected, data_done[i].request.destination.city_id)){
								if(!_AlterCtrl.validLatLng(data_all_via_cities, data_done[i].request.destination)){
									collected.push(data_done[i].request.destination);

								}
							}
						}else{

							// //console.log('check else');
							if(data_done[i].request.origin && !_AlterCtrl.validLatLng(data_all_via_cities, data_done[i].request.origin)){
								collected.push(data_done[i].request.origin);
							}

							if(data_done[i].request.destination && !_AlterCtrl.validLatLng(data_all_via_cities, data_done[i].request.destination)){
								collected.push(data_done[i].request.destination);
							}

						}
					}
				}

				data_ref = data_ref.map(function(value, index){
					if(value.allow_plot	=== true){
						return value;
					}
				});
				_AlterCtrl.setCleanArray(data_ref);
				_AlterCtrl.setCleanArray(collected);

				if(collected){
				var x = 0;	
					jQuery.each( collected, function( i, val ) {
						var city_name = val.name;
						if(city_name !== undefined && city_name !== ""){
							//console.log(city_name);
							x = x + 1;
						}
					});
				}
				//CHECK FOR STATUS 
				for (var i = 0; i < data_ref.length; i++) {

						if(!_AlterCtrl.getCityIDArray(collected, data_ref[i].city_id)){
							icon = '<i class="fa fa-refresh fa-spin"></i>';	
						}else{
							icon = '<i class="fa fa-check text-success"></i>';
						}
						data.push([
							'<div>',
								icon+' <label class="text-primary">'+data_ref[i].title+'</label>',
							'</div>'
						].join(''));

					//added by paul
					var countprop = countProperties(data);
					function countProperties(data) {
					  var prop;
					  var propCount = 0;
					  for (prop in data) {
					    propCount++;
					  }
					  return propCount;
					}

				}
				if(data_ref.length > 12){
					var split_length = Math.round(data_ref.length / 2);
					var data_dom_1 = []
					var data_dom_2 = [];
					for (var i = 0; i < split_length; i++) {				
						data_dom_1.push(data[i]);
					}
					for (var i = split_length; i < data_ref.length; i++) {					
						data_dom_2.push(data[i]);
					}
					html = ['<div class="col-md-6" style="padding:0">',
								data_dom_1.join(''),
							'</div>',
							'<div class="col-md-6" style="padding:0">',
								data_dom_2.join(''),
							'</div>'].join('');
					$('.ajax-loader-content-init').css({'padding' : 0});
					$('.ajax-loader-content-data').css({'padding' : 0});
					$('.ajax-loader-content-finalize').css({'padding' : 0});
				}else{
					html = ['<div class="row text-loaded">',
								data.join(''),
							'</div>'].join(''); 
				}

			//	loaderPercentage =  ( x / countprop ) * 100 ;
				/* ------------------- updated code here -------------------*/
				if ( query_string( 'type' ) === 'oneway' || query_string( 'type' ) === 'multistop' || query_string( 'type' ) === 'singlestop' ) {		
				 	loaderPercentage = ( x / countprop) * 100;			 
				} 
				break;
			case 'finalize':
				// //console.log("CHECK FINALIZE STATUS : ", status);
				if(!status){
					icon = '<i class="fa fa-refresh fa-spin"></i>';
				}else{
					loaderPercentage = 100;
					icon = '<i class="fa fa-check text-success"></i>';
				}
				html = icon+' <label>Finalizing</label>';
				
				break;
		}
		////console.log("count data", countprop);

		$( '#ajax-loader-percentage' ).text( loaderPercentage.toFixed( 0 ) + '%');
		$('.ajax-loader-content > .ajax-loader-content-'+type).html(html);
	}

	function setLoadingHide(){
		setTimeout(function(){
			$('.ajax-loader').hide();
			$('.ajax-loader-background').hide();
		}, 5000);
	}

	function setLoadingHideQuick(){

		setTimeout(function(){

			$('.ajax-loader').hide();
			$('.ajax-loader-background').hide();

		}, 1000);

	}

	function setLoadingHideAlter(type, id){

		setTimeout(function(){

			$('.alter-loading-'+type+'-'+id).hide();

		}, 1000);

	}

	function setDomSlide(dom_id){

		var div = $('#'+dom_id),
		div = div.length && div,
		offset = 0;

		if(div.length){

			offset = div.offset().top;
			$('html,body').animate({
				scrollTop : offset - 20
			},'slow');

		}

	}

	function setDomShow(dom_id){

		$('#'+dom_id).show();

	}

	function setDomHide(dom_id){

		$('#'+dom_id).hide();

	}

	function setLearnRoute(){
		var valid, array, origin, destination;
		valid = confirm("Are you sure you want to learn this route?");
		if (valid === true) {
			array = _AlterCtrl.setCleanArray($("#hdfCityIDs").val().split(','));
			if(array.length){
				array = _AlterCtrl.setConvertUnitArray(array, 'int');
				charted = array.join(',');
				origin = array[0];
				destination = array[array.length - 1];
				_AjaxCtrl.initialize("{origin:"+origin+", destination:"+destination+", charted:'"+charted+"'}", "GetCityByViaCityID", false);
				alert("Successfull Added New Route!");
			}else{
				alert("Failed to Learn Route. Must select cities.");
			}		  
		} 
	}

	function setLearnItinerary(){

		var valid, array, origin, destination;

		valid = confirm("Are you sure you want to save this itinerary?");
		
		if (valid === true) {
			
			array = _AlterCtrl.setCleanArray($("#hdfCityIDs").val().split(','));

			if(array.length && data_itinerary_record){

				array = _AlterCtrl.setConvertUnitArray(array, 'int');
				charted = array.join(',');
				origin = array[0];
				destination = array[array.length - 1];

				_AjaxCtrl.initialize("{origin:"+origin+", destination:"+destination+", charted:'"+charted+"', itinerary:'"+JSON.stringify(data_itinerary_record)+"'}", "AddAItineraryData", false);
				// //console.log("asdfsdaf", data_itinerary_record);
				alert("Successfull Added New Group Tours!");

			}else{

				alert("Failed to Learn Itinerary. Must select cities.");

			}
		   
		} 

	}

	function setPageRedirectType(type){

		var count = 3;

		$('#MultistopRedirect').modal({backdrop: 'static', keyboard: false});
					
		setInterval(function(){

			 count--;

			 if(count < 0){

				_DomCtrl.setPageChangeTravelType(type);
				return false;

			 }else{

				$('#MultistopRedirect').find('label').html(count);

			 }

		}, 1000);

	}

	function setDomEditBtn(type, id){

		var name, design;

		if(type == 'edit'){

			name = "Edit Itinerary Route";
			design = "btn-success";

		}else{

			name = "Hide Edit Itinerary";
			design = "btn-warning";

		}

		$('#map-itinerary-btn').html('');
		$('#map-itinerary-btn').html([
			'<div id="'+id+'" class="map-itinerary-btn" style="display:none;">',
				'<input type="button" value="'+name+'" class="btn '+design+'" style="border-radius:0;width:180px;">',
			'</div>'
		].join(''));

		$('#map-itinerary-btn').after('<div id="spacer-div" style="padding:3px"></div>');

	}

	function setDomViewProposedBtn(id){

		$('#view-itinerary-btn-dom').html('');
		$('#view-itinerary-btn-dom').html([
			'<div id="view-itinerary-btn" style="display:none;">',
				'<input type="button" value="View Proposed Itinerary" class="btn btn-success" style="border-radius:0;width:180px;">',
			'</div>',
		].join(''));

	}

	function setDomMultiAlter(type){

		switch(type){

			case 'oneway':

				$('.map-alternative').css({'display' : 'inline-block'});
				$('.map-edit-route').hide();
				$('.map-container').css({'width' : '74%'});

				break;
			case 'multistop':

				$('.map-alternative').hide();
				$('.map-edit-route').hide();
				$('.map-container').css({'width' : '100%'});

				break;
			case 'singlestop':

				$('.map-alternative').hide();
				$('.map-edit-route').hide();
				$('.map-container').css({'width' : '100%'});

				break;
			case 'edit_instruction':

				$('.map-alternative').hide();
				$('.map-edit-route').css({'display' : 'inline-block'});
				$('.map-container').css({'width' : '74%'});

				break;
			case 'alter_dom':

				var alter_array = [],
				alter_data = {},
				group_data = {},
				alter_status = false,
				group_status = false,
				currency_ts = global_currency.substring(0, 2) + global_currency_symbol,
				loading_html = ['<div style="width:100%;height: 90%; background:#ffffff;margin:10px 0 10px 0;padding:5px;">',
		  						'<div style="color:#008ed6;text-align:center;font-size:120%;font-weight:bold;padding:10px">Processing Alternatives Itineraries</div>',
		  						'<center><object data="images/reload_loading.svg" type="image/svg+xml">Loading....</object></center>',
		  						'</div>'],
		  		nodata_html = ['<div style="width:100%;height: 90%; background:#ffffff;margin:10px 0 10px 0;padding:5px;">',
		  						'<div style="text-align:center;font-size:120%;font-weight:bold;padding:10px">There are no Alternative\'s itineraries with the selected origin and destination.</div>',
		  						'</div>'];

		  		$('.AlternativeItinerary').html(loading_html.join(''));

				_AjaxCtrl.assortment_classed([
					_DomCtrl.setPageAlternativeItinerary([data_coord_origin.country, data_coord_destination.country]),
					_DomCtrl.setPageGroupItinerary([data_coord_origin.country, data_coord_destination.country])
				],function(array){

					alter_status = array[0][0];
					group_status = array[1][0];
					alter_data.dom = array[0][1].dom;
					alter_data.data = array[0][1].data;
					group_data.dom = array[1][1].dom;
					group_data.data = array[1][1].data;

					if(alter_status){

						$('.AlternativeItinerary').css({'list-style-type': 'none'});
						$('.AlternativeItinerary').html('');
						$('.AlternativeItinerary').html(alter_data.dom);
						$("span.alternative-currency-ts").each(function(index,val){
							$(this).html(currency_ts);
						});

						$('.AlternativeItinerary').css('height', '310px');

						for (var i = 0; i < alter_data.data.length; i++) {

							alter_array.push({
								type : 'alter',
								coord : alter_data.data[i]
							});

						}

					}else{

						$('.AlternativeItinerary').css('height', '310px');
						$('.AlternativeItinerary').html(nodata_html.join(''));

					}

					if(group_status){

						$('.GroupItinerary').css({'list-style-type': 'none'});
						$('.GroupItinerary').html('');
						$('.GroupItinerary').html(group_data.dom);
						$("span.alternative-currency-ts").each(function(index,val){
							$(this).html(currency_ts);
						});

						$('.AlternativeItinerary').css('height', '200px');
						$('.GroupItineraryDom').show();

						for (var i = 0; i < group_data.data.length; i++) {

							alter_array.push({
								type : 'group',
								coord : group_data.data[i]
							});

						}

					}else{

						$('.AlternativeItinerary').css('height', '310px');
						$('.GroupItineraryDom').hide();

					}

					if(alter_array.length){

						_GoogleCtrl.routeVisionAlternatives(alter_array);

					}

				});

				$('body').on('click','.alternative-event',function(e){

					e.preventDefault();

					setPageAlternativeRedirect(this);

				});

				break;

		}

	}

	function getDomObject(dom_name){

		return $('#'+dom_name)[0];

	}

	function valitListAtleastOne(){

		var data = $('#ddlCity_2').val();
		if (data == "-1" || data == "-2" || !data) {
			alert("Please Select Minimum One Route");
			return false;
		}

		return true;

	}

	return {
		setEventKeys : setEventKeys, // USED - FOR DETECTING ALT+F1 OR ALT+F2
		setPageTab : setPageTab, // USED - FOR CHANGING PAGE TYPES(ONEWAY, MULTISTOP, SINGLESTOP)
		setPageSearch : setPageSearch, // USED - FOR SEARCHING OR CHANGING NEW ORIGIN/DESTINATION
		setPageChangeTravelType : setPageChangeTravelType,  // USED - FOR CHANGING PAGE TYPES COMING FROM DIFFERENT TYPE(ONEWAY -> MULTISTOP) 
		setPageManualOverride : setPageManualOverride, // USED - TO REDIRECT TO MULTI-STOP TO ALLOW ADDING/EDITING MARKERS
		setPageLoadMap : setPageLoadMap, //USED - TO LOAD DOM MAP
		setPageAlternativeItinerary : setPageAlternativeItinerary, // USED - FOR APPENDING ALTERNATIVE ITINERARIES AND EVENT WHEN CLICKING THEM
		setPageGroupItinerary : setPageGroupItinerary, //USED - FOR APPENDING GROUP ITINERARIES AND EVENT WHEN CLICKING THEM
		setPageAlternativeRedirect : setPageAlternativeRedirect, // USED - FOR REDIRECTING WITH THE CURRENT SELECTED ALTERNATIVES ITINERARY
		setPageAlternativeRoute : setPageAlternativeRoute, // USED - FOR GETTING THE ALTERNATIVE ITINERARY SELECTED
		setNewListOption : setNewListOption, //NOT USE - (LEGACY) FOR ADDING NEW LIST OPTIONS<li>
		setListOptionDom : setListOptionDom, //NOT USE - (LEGACY) FOR APPENDING CITIES IN THE LIST OPTIONS<li>
		setListOptionEmpty : setListOptionEmpty, //NOT USE - (LEGACY) FOR CLEARING OUT LIST OPTIONS
		setListOptionChange : setListOptionChange, //NOT USE - (LEGACY) FOR ASSIGNING CITY TO THE LIST OPTION
		setListOptionChangeSelect : setListOptionChangeSelect, //NOT USE - (LEGACY) FOR SELECTING CITY IN THE LIST OPTION
		setBtnAutoRouteClick : setBtnAutoRouteClick, //NOT USE - (NONEXISTENCE) FOR AUTO ROUTING
		setBtnAutoEditClick : setBtnAutoEditClick, //NOT USE - (NONEXISTENCE) FOR EDITING ROUTE
		setListLastSelected : setListLastSelected, //NOT USE - (LEGACY) FOR GETTING THE LAST CITY SELECTED
		setListCityLoad : setListCityLoad, //NOT USE - (LEGACY) FOR LOADING THE AVAILABLE CITY BY REFERRING OTHER CITY
		setListCityAdd : setListCityAdd, //NOT USE - (LEGACY) FOR LOADING THE AVAILABLE CITY BY LAST CITY 
		setRouteOnewayDom : setRouteOnewayDom, //NOT USE - (NONEXISTENCE) USED FOR COLLECTING ALL THE SELECTED CITY IDS IN THE ONEWAY
		setRouteOnewayhdDom : setRouteOnewayhdDom, //NOT USE - (LEGACY) USED FOR COLLECTING ALL THE SELECTED CITY IDS IN THE ONEWAY
		setRouteListSelected : setRouteListSelected, //USED - FOR PREVIEWING THE SELECTING CITY IDS(ON THE TOP-LEFT-SIDE)
		setRouteListAlternatives : setRouteListAlternatives, //USED - FOR COLLECTING AND PREVIEWING THE ALTERNATIVE ITINERARIES(ON THE TOP-LEFT-SIDE)
		setTimeSplit : setTimeSplit, //NOT USE - FOR SPLITTING TIME
		setRouteListSelectPrice : setRouteListSelectPrice, //USED - FOR SETTING THE PRICES FOR THE ALTERNATIVE ITINERARIES
		setRouteMultistopDom : setRouteMultistopDom, //USED - USED FOR COLLECTING ALL THE SELECTED CITY IDS IN THE MULTISTOP
		setLinkPreventDefault : setLinkPreventDefault, //USED - FOR PREVENTING DEFAULT WHEN CLICKING ON LINKS
		setDatePicker : setDatePicker, //USED - FOR SELECTING DATA(CHECK IF ITS WORKING FINE)
		setSelectToArray : setSelectToArray, //NOT USE - (LEGACY) FOR GETTING ALL THE VALUES INSIDE THE LIST OPTION
		setLoadingShow : setLoadingShow, //USED - FOR LOADING PURPOSES
		setLoadingHide : setLoadingHide, //USED - FOR HIDING THE LOADING PROCESS WITH A DELAY OF 5 SECONDS
		setLoadingHideQuick : setLoadingHideQuick, //USED - FOR HIDING THE LOADING PROCESS WITH A DELAY OF 1 SECOND
		setLoadingHideAlter : setLoadingHideAlter, //USED - FOR LOADING ALTERNATIVES WHEN PLOTTING ITS ROUTE ON THE MAP
		setDomSlide : setDomSlide, //USED - FOR ANIMATED SCROLL DOWN WITH A GIVEN DOM
		setDomShow : setDomShow, //USED - FOR SETTING DOM SHOW
		setDomHide : setDomHide, //USED - FOR SETTING DOM HIDE
		setLearnRoute : setLearnRoute, //USED - FOR SAVING THE ROUTES THAT ARE PLOTTED ON THE MAP
		setLearnItinerary : setLearnItinerary, //USED - FOR SAVING THE WHOLE ITINERARY THAT WILL BE USED FOR FIX ITINERARY
		setPageRedirectType : setPageRedirectType, //USED - FOR REDIRECTING WHEN ROUTE FAILS(E.G. ONEWAY TO MULTISTOP)
		setDomEditBtn : setDomEditBtn, //USED - APPEND BUTTON FOR EDITING ROUTE
		setDomViewProposedBtn : setDomViewProposedBtn, //USED - APPEND BUTTON FOR VIEWING PROPOSED ITINERARY
		setDomMultiAlter : setDomMultiAlter, //USED - FOR CHANGIN THE LAYOUT OF THE MAP PAGE(I.E. IF YOU WANT TO SHOW ALTERNATIVES WITH A TYPE OF ONEWAY, ETC..)
		getDomObject : getDomObject, //USED - FOR GETTING DOM OBJECT
		valitListAtleastOne : valitListAtleastOne //NOT USE - (LEGACY) VALIDATING IF THERE IS SELECTED AT LEAST ONE CITY IN LIST OPTIONS
	};

})();

_AlterCtrl = (function(){

	function setCleanRawArray(array, type){

		var result = [];

		if(array && array.length){

			for (var i = 0; i < array.length; i++) {
						
				switch(type){

					case 'transport':

						var data = {
							name : array[i].destination_name.trim(),
							city_id : array[i].destination_id.toString(),
							lat : array[i].lat,
							lng : array[i].lng,
							priority : (parseInt(array[i].optional)) ? false : true,
							other_id : (array[i].transport_id) ? array[i].transport_id : 0,
							include : true
						};

						break;

					case 'itineraries':

						var data = {
							name : array[i].destination_name.trim(),
							city_id : array[i].destination_id.toString(),
							itinerary_transport_id : array[i].itinerary_transport_id,
				            itinerary_transport_pass_id : array[i].itinerary_transport_pass_id,
				            itinerary_activities_id : array[i].itinerary_activities_id,
				            itinerary_hotel_id : array[i].itinerary_hotel_id,
				            itinerary_hotel_roomtype : array[i].itinerary_hotel_roomtype,
				            itinerary_hotel_nights : array[i].itinerary_hotel_nights,
							lat : array[i].lat,
							lng : array[i].lng,
							priority : (parseInt(array[i].optional)) ? false : true,
							other_id : (array[i].transport_id) ? array[i].transport_id : 0,
							include : true
						};

						break;

					case 'cost':

						var data = {
							origin_id : array[i].origin_id.toString(),
							destination_id : array[i].destination_id.toString(),
							cost: array[i].cost,
							duration: array[i].zDuration
						};

						var cost_check = getPriceArray(data_cost, data.origin_id, data.destination_id);

						if(!cost_check){
							data_cost.push(data);
						}
						
						break;

					case 'alternative':

						var data = {
							name : array[i].destination_name.trim(),
							city_id : array[i].destination_id.toString(),
							lat : array[i].lat,
							lng : array[i].lng,
							priority : (!array[i].optional) ? false : true,
							other_id : (array[i].transport_id) ? array[i].transport_id : 0,
							include : true
						};

						break;

					case 'price_transport':

						var data = {
							transport_id : array[i].transport_id,
							origin_id : array[i].origin_id.toString(),
							destination_id : array[i].destination_id.toString(),
							tranport_type : array[i].tranport_type,
							cost : array[i].cost,
							duration : array[i].zDuration,
							currency : array[i].currency,
							operates : array[i].operates,
							default_index : (array[i].zDefault == 'X') ? true : false,
							supplier : array[i].supplier,
							international : (array[i].international == 'no') ? false : true,
							season_id : array[i].season_id,
							etd : array[i].etd,
							eta : array[i].eta
						};

						break;

					case 'price_activity':

						var data = {
							activity_id : array[i].id,
							city_id : array[i].city_id.toString(),
							activity : array[i].activity,
							description : array[i].description,
							name : array[i].name,
							currency : array[i].currency,
							cost : array[i].price,
							default_index : (array[i].zdefault == 'X') ? true : false,
							duration : array[i].duration,
							origin : array[i].start_city,
							destination : array[i].end_city,
							accommodation : array[i].has_accommodation,
							transport : array[i].has_transport
						};

						break;

					case 'price_hotel':

						var data = {
							hotel_id : array[i].hotel_id,
							city_id : array[i].CityID.toString(),
							name : array[i].name,
							default_nights : array[i].default_nights,
							currency : array[i].currency,
							single : (array[i].single) ? parseInt(array[i].single) : 0,
							twin : (array[i].twin) ? parseInt(array[i].twin) : 0,
							triple : (array[i].triple) ? parseInt(array[i].triple) : 0,
							quad : (array[i].quad) ? parseInt(array[i].quad) : 0,
							dorm : (array[i].dorm) ? parseInt(array[i].dorm) : 0,
							category : array[i].category.toLowerCase(),
							child_rate : (array[i].child_rate) ? parseInt(array[i].child_rate) : 0,
						};
						// //console.log("qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq");
						break;

					case 'price_transport_pass':

						var data = {
							city_id : array[i].zcity_id,
							pass_id : array[i].zPass_id,
		                    pass_name : array[i].zPass_name,
		                    pass_type_id : array[i].zPass_type_id,
		                    pass_type_name : array[i].zPass_type_name,
		                    pass_type_desc : array[i].zPass_type_desc,
		                    currency_adult : array[i].zAdult_currency,
		                    cost_adult : array[i].zAdult_rate,
		                    currency_backpacker : array[i].zBackpackper_currency,
		                    cost_backpacker : array[i].zBackpackper_rate,
		                    currency_child : array[i].zChild_currency,
		                    cost_child : array[i].zChild_rate,
		                    valid_months : array[i].zValidation,
		                    valid_days : array[i].zValid,
		                    sectors : array[i].zSectors
						};

						break;

					case 'price_cities_details':

						var data = {
							city_id : array[i].city_id.toString(),
		                    name : array[i].city_name,
		                    region_id : array[i].region_id,
		                    duration : array[i].default_nights,
		                    optional_city : array[i].optional_city,
		                    default_nights : array[i].default_nights,
		                    metadesc : array[i].MetaDesc,
		                    country_id : array[i].country_id,
		                    country_name : array[i].country_name ? array[i].country_name.trim().toLowerCase() : itineraryCredentials('get', 'country').trim().toLowerCase()
						};

						break;

				}

				result.push(data);

			}

		}

		return result;

	}

	function setCleanArray(data){

		for (var i = 0; i < data.length; i++) {
			if (!data[i]) {         
				data.splice(i, 1);
				i--;
			}
		}

		return data;

	}

	function setUniqueArray(data) {

		var unique = {}, result = [];

		for (var i = 0, l = data.length; i < l; ++i) {

			if (unique.hasOwnProperty(data[i])) {
				continue;
			}

			result.push(data[i]);
			unique[data[i]] = 1;

		}

		return result;

	}

	function setGroupByArray(array, callback){

		var groups = {};

		array.forEach(function(data){

		var group = JSON.stringify(callback(data));
		groups[group] = groups[group] || [];
		groups[group].push(data); 

		});

		return Object.keys(groups).map(function( group ){
		return groups[group]; 
		});

	}

	function setStripAlterCoord(array){

		var result = _AlterCtrl.setGroupByArray(array, function(data){
		
			return [data.other_id];

		});

		return result;

	}

	function setStripCityIDCoord(array){

		var result = _AlterCtrl.setGroupByArray(array, function(data){
		
			return [data.city_id];

		});

		return result;

	}

	function setStripOriginDestinationCoord(array){

		var result = _AlterCtrl.setGroupByArray(array, function(data){
		
			return [data.origin_id, data.destination_id];

		});

		return result;

	}

	function setAlterIDCoord(array){

		var result = [];

		for (var i = 0; i < array.length; i++) {

			result.push({ alter_id : array[i][0].other_id.toString(), data : array[i] });

		}

		return result;

	}

	function setCityIDCoord(array){

		var result = [];

		for (var i = 0; i < array.length; i++) {

			result.push({ city_id : array[i][0].city_id.toString(), data : array[i] });

		}

		return result;

	}

	function setOriginDestinationCoord(array){

		var result = [];

		for (var i = 0; i < array.length; i++) {

			result.push({ origin_id : array[i][0].origin_id.toString(), destination_id : array[i][0].destination_id.toString(),  data : array[i] });

		}

		return result;

	}

	function setConvertUnitArray(array, type){

		var result;

		switch(type){

			case 'int':

				result = array.map(function(data) {
					return parseInt(data, 10);
				});

				break;
			case 'string':

				result = array.map(function(data) {
					return data.toString();
				});
				
				break;

		}

		return result;
	}

	function setSortNearbyDistance(array){
		array.sort(function(a, b) {
			return parseFloat(a.distance) - parseFloat(b.distance);
		});
	}

	function setSortArray(array){
		array.sort(function(a, b) {
			return parseFloat(a.index) - parseFloat(b.index);
		});
	}

	function setSortArrayOrder(array){
		array.sort(function(a, b) {
			return parseFloat(a.order) - parseFloat(b.order);
		});
	}

	function setMoveArray(array, index_from, index_to){
		return array.splice(index_to, 0, array.splice(index_from, 1)[0]);
	}

	function getIndexArray(array, index){

		//console.log("ARR : ", array);
		var result = false,
		found = array.some(function (value, key) {
			return value.index === index
		});

		if(found)
			result = true;

		return result;

	}

	function getCityIDArray(array, city_id){

		var result = false,
		found = array.some(function (value, key) {
			return value.city_id === city_id
		});

		if(found)
			result = true;

		return result;

	}

	function getCityArray(array, city_id, type){

		var result, found;

		if(type == 'key'){
			result = false;
		}else if(type == 'count'){
			result = 0;
		}else if(type == 'data'){
			result = null;
		}

		found = array.some(function (value, key) {

			if(value.city_id == city_id){

				if(type == 'count'){
					result++;
				}else if(type == 'key'){
					result = key;
				}else if(type == 'data'){
					value.index = key;
					result = value;
				}

			}

		});

		return result;

	}

	function getCoordArray(array, origin_id, destination_id, type){

		var result, found;

		if(type == 'key'){
			result = false;
		}else if(type == 'count'){
			result = 0;
		}else if(type == 'data'){
			result = null;
		}

		found = array.some(function (value, key) {

			if(value.origin_id == origin_id && value.destination_id == destination_id){

				if(type == 'count'){
					result++;
				}else if(type == 'key'){
					result = key;
				}else if(type == 'data'){
					value.index = key;
					result = value;
				}

			}

		});

		return result;

	}

	/* updated new code */
	function getCoordRouteArray(array, origin_id, destination_id, type){

		var result, found;

		if(type == 'key'){
			result = false;
		}else if(type == 'count'){
			result = 0;
		}else if(type == 'data'){
			result = null;
		}

		//console.log("ERROR : ", origin_id);
		//console.log("ERROR DES : ", destination_id);

		found = array.some(function (value, key) {

			//console.log("11 : ", value.request.origin);
			//console.log("12 : ", value.request.origin.lat);
			//console.log("13 : ", value.request.origin.lng);

			if((origin_id && _AlterCtrl.getLatLngArray([origin_id], value.request.origin.lat, value.request.origin.lng, 'regex')) && 
			(destination_id && _AlterCtrl.getLatLngArray([destination_id], value.request.destination.lat, value.request.destination.lng, 'regex'))){
				
				if(type == 'count'){
					result++;
				}else if(type == 'key'){
					result = key;
				}else if(type == 'data'){
					value.index = key;
					result = value;
				}
			}
		});
		return result;
	}
	/*-- end code ------- */

	function getCategoryArray(array, category, type){

		var result, found;

		if(type == 'key'){
			result = false;
		}else if(type == 'count'){
			result = 0;
		}else if(type == 'data'){
			result = null;
		}

		found = array.some(function (value, key) {

			if(value.category == category){

				if(type == 'count'){
					result++;
				}else if(type == 'key'){
					result = key;
				}else if(type == 'data'){
					value.index = key;
					result = value;
				}

			}

		});

		return result;

	}

	/* -------- updated new code ----------*/
	function getLatLngArray(array, lat, lng, type){

		// //console.log("GET LAT ARRAY : ", array);

		var result = false,
		found = array.some(function (value, key) {
			if(type == 'regex'){
				//console.log("CHECK REG "+lat.toString().indexOf(value.lat.toString())+" : ", lat, " ---- ", value.lat);
				return (lat.toString().indexOf(value.lat.toString()) !== -1 && lng.toString().indexOf(value.lng.toString()) !== -1) || (value.lat.toString().indexOf(lat.toString()) !== -1 && value.lng.toString().indexOf(lng.toString()) !== -1)
			}else{
				return value.lat() === lat && value.lng() === lng
			}
		});

		if(found)
			result = true;
		return result;
	}
	/* -------- #end ----------*/

	function getPriceArray(array, origin_id, destination_id){

		var result, found;

		result = null;

		found = array.some(function (value, key) {

			if(value.origin_id == origin_id && value.destination_id == destination_id){

				result = value;

			}

		});

		return result;

	}

	function getItineraryArray(array, id, type, state){

		var result, found, array_id, condition;

		if(type == 'key'){
			result = false;
		}else if(type == 'count'){
			result = 0;
		}else if(type == 'data'){
			result = null;
		}else if(type == 'array'){
			result = [];
		}

		found = array.some(function (value, key) {

			switch(state){

				case 'transport':

					array_id = value.transport_id;
					condition = array_id == id;

					break;

				case 'activity':

					array_id = value.activity_id.toString();
					id = id.split(",");
					condition = id.indexOf(array_id) > -1;
					id = id.join(",");

					break;

				case 'hotel':

					array_id = value.hotel_id;
					condition = array_id == id;

					break;

			}

			if(condition){

				if(type == 'count'){
					result++;
				}else if(type == 'key'){
					result = key;
				}else if(type == 'data'){
					value.index = key;
					result = value;
				}else if(type == 'array'){
					value.index = key;
					result.push(value);
				}

			}

		});

		return result;

	}

	function getItineraryDefaultArray(array){

		var result = [], found;

		found = array.some(function (value, key) {

			value.default_index = true;
			result.push(value);

		});

		return result;

	}

	function getDataPriceArray(type, data, other_data, other_info){

		var result = 0, array = [], temp_result = 0, temp_array = [];

		switch(type){

			case 'transport':
			case 'activity':

				data.map(function(value){

					if(value.default_index === true){
						result += convert_to_au(value.currency, value.cost);
					}

				});

				break;
			case 'hotel':

				data.map(function(value){

					if(value[other_data] !== 0){
						array.push(convert_to_au(value.currency, value[other_data]));
					}

				});

				result = (array.length) ? 
							array.reduce(function(a, b, i, arr) {
								return Math.min(a,b)
							})
						: result;

				if(other_info){

					data.map(function(value){

						if(value[other_info] !== 0){
							temp_array.push(convert_to_au(value.currency, value[other_info]));
						}

					});

					temp_result = (temp_array.length) ? 
								temp_array.reduce(function(a, b, i, arr) {
									return Math.min(a,b)
								})
							: temp_result;

					result = (temp_result !== 0 && temp_result < result) ? temp_result : result;

				}

				break;

		}

		return result;

	}

	function getObjArray(array, dom_id){

		var result = null, found;

		found = array.some(function (value, key) {
			
			if(value.dom_id == dom_id){

				value.index = key;
				result = value;

			}

		});

		return result;

	}

	function getDataIndexArray(array, id, type){

		var result, found;

		if(type == 'key'){
			result = false;
		}else if(type == 'count'){
			result = 0;
		}else if(type == 'data'){
			result = null;
		}

		found = array.some(function (value, key) {

			if(value.id == id){

				if(type == 'count'){
					result++;
				}else if(type == 'key'){
					result = key;
				}else if(type == 'data'){
					value.index = key;
					result = value;
				}

			}

		});

		return result;

	}

	function getAlterIDArray(array, alter_id){

		var result = false,
		found = array.some(function (value, key) {
			return value.alter_id === alter_id
		});

		if(found)
			result = true;

		return result;

	}

	function getAlterArray(array, id, type){

		var result, found;

		if(type == 'key'){
			result = false;
		}else if(type == 'count'){
			result = 0;
		}else if(type == 'data'){
			result = null;
		}

		found = array.some(function (value, key) {

			if(value.alter_id == id){

				if(type == 'count'){
					result++;
				}else if(type == 'key'){
					result = key;
				}else if(type == 'data'){
					result = value;
				}

			}

		});

		return result;

	}

	function getParameterByName(name) {

		name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
		var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		results = regex.exec(location.search);

		return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));

	}

	function getParameterUrlReplace(url, key, value){ 

		var pattern = new RegExp('\\b('+key+'=).*?(&|$)');

		if(url.search(pattern)>=0){ 
			return url.replace(pattern,'$1' + value + '$2'); 
		} 

		return url + (url.indexOf('?')>0 ? '&' : '?') + key + '=' + value 

	}

	function validIndexArray(data, max){

		var result = true;

		for (var i = 0; i < max; i++) {

			if(!_AlterCtrl.getIndexArray(data, i)){
				result = false;
				break;
			}

		};

		return result;

	}

	function validCoordAlternativesArray(coord_array, alter_array){

		var result = true, coord_length = coord_array.length, alter_length = 0, data;

		if(coord_array.length){

			if(coord_array.length == alter_array.length){

				for (var i = 0; i < alter_array.length; i++) {
					
					data = _AlterCtrl.getCityArray(coord_array, alter_array[i].city_id, 'data');

					if(data){

						if(data.index == i){

							alter_length++;

						}

					}

				}

			}

			if(coord_length == alter_length){

				result = false;

			}

		}

		return result;

	}

	function validCoordAlternativesIds(coord_ids, alter_ids){

		var result = false;

		for (var i = 0; i < alter_ids.length; i++) {
			
			if(alter_ids[i] == coord_ids){

				result = true;
				break;

			}

		}

		return result;

	}

	function validLatLng(array, data){

		var result = false, lat, lng, via_name;

		// //console.log("naa kay sulod : ", data);

		lat = data.lat.toString();
		lng = data.lng.toString();
		// via_name = data.via_name.toString();

		if(array && _AlterCtrl.getLatLngArray(array, lat, lng, 'regex')){
			result = true;
		}

		return result;

	}

	return {
		setCleanRawArray : setCleanRawArray, //USED - FOR FILTERING/CLEANING THE DATA COMING FROM THE BACKEND SO IT WOULD BE READABLE
		setCleanArray : setCleanArray, //USED - FOR CLEANING THE ARRAY VALUE AND REMOVING THE UNNECESSARY(i.e. null, false, undefined)
		setUniqueArray : setUniqueArray, //NOT USE - (NONEXISTENCE) FOR GROUPING THE ARRAY VALUES THAT ARE DUPLICATE
		setGroupByArray : setGroupByArray, //USED - FOR FILTERING AND GROUPING THE VALUES WITH A SPECIFIED KEY OBJECT
		setStripAlterCoord : setStripAlterCoord, //USED - FOR GROUPING ALTERNATIVES WITH A GIVEN ALTERNATIVE ID
		setStripCityIDCoord : setStripCityIDCoord, //USED - FOR GROUPING CITIES WITH A GIVEN CITY ID
		setStripOriginDestinationCoord : setStripOriginDestinationCoord, //USED - FOR GROUPING CITIES WITH A GIVEN ORIGIN AND DESTINATION ID
		setAlterIDCoord : setAlterIDCoord, //USED - FOR CLEANING THE STRIPPED ALTERNATIVE DATA BY ALTERNATIVE ID AND CONVERTING THEM INTO ARRAY OBJECTS
		setCityIDCoord : setCityIDCoord, //USED - FOR CLEANING THE STRIPPED CITIES DATA BY CITY ID AND CONVERTING THEM INTO ARRAY OBJECTS
		setOriginDestinationCoord : setOriginDestinationCoord, //USED - FOR CLEANING THE STRIPPED CITIES DATA BY ORIGIN AND DESTINATION ID AND CONVERTING THEM INTO ARRAY OBJECTS 
		setConvertUnitArray : setConvertUnitArray, //USED - FOR PARSING VARIABLE TYPES(E.G. string -> int)
		setSortNearbyDistance : setSortNearbyDistance, //USED - FOR REARRANGING BY DISTANCE FROM LOWEST TO HIGHEST
		setSortArray : setSortArray, //USED - FOR REARRANGING BY INDEX FROM LOWEST TO HIGHEST
		setSortArrayOrder : setSortArrayOrder,
		setMoveArray : setMoveArray, //NOT USE - (NONEXISTENCE) FOR MOVING ARRAY VALUES WITH A GIVEN INDEX
		getIndexArray : getIndexArray, //USED - FOR GETTING THE INDEX VALUE ON THE ARRAY
		getCityIDArray : getCityIDArray, //USED - FOR GETTING THE CITY ID VALUE ON THE ARRAY
		getCityArray : getCityArray, //USED - FOR GETTING DATA FROM A GIVEN CITY ARRAY BASING ON ITS CITY ID AND TYPE OF DATA
		getCoordArray : getCoordArray, //USED - FOR GETTING DATA FROM A GIVEN CITY ARRAY BASING ON DESTINATION ID AND TYPE OF DATA
		getCoordRouteArray : getCoordRouteArray, //USED - FOR GETTING DATA THAT IS REQUESTED TO GOOGLE RETURN WITH GIVEN CITY ARRAY BASING ON CITY ID AND TYPE OF DATA
		getCategoryArray : getCategoryArray, //USED - FOR GETTING DATA FROM HOTELS ARRAY BASING ON ITS CITY ID AND TYPE OF DATA
		getLatLngArray : getLatLngArray, //USED - FOR GETTING DATA FROM A GIVEN CITY ARRAY BASING ON ITS LATITUDE AND LONGITUDE AND TYPE OF DATA
		getPriceArray : getPriceArray, //NOT USE - (LEGACY) FOR FILTERING DATA WITH A PRICE BASING ON ITS LATITUDE AND LONGITUDE
		getItineraryArray : getItineraryArray, //USED
		getItineraryDefaultArray : getItineraryDefaultArray,
		getDataPriceArray : getDataPriceArray, //USED - FOR GETTING THE PRICES BASING ON WHAT TYPE OF DATA(E.G. transport, hotels, activities)
		getObjArray : getObjArray, //NOT USE - (LEGACY) FOR GETTING DATA BASING ON ITS DOM ID
		getDataIndexArray : getDataIndexArray, //USED - FOR GETTING DATA BASING ON ITS ID AND TYPE OF DATA 
		getAlterIDArray : getAlterIDArray, //USED - FOR GETTING DATA FROM A GIVEN ALTERNATIVE ARRAY BASING ON ITS ALTER ID
		getAlterArray : getAlterArray, //USED - FOR GETTING DATA FROM A GIVEN ALTERNATIVE ARRAY BASING ON ITS ALTER ID AND TYPE OF DATA
		getParameterByName : getParameterByName, //NOT USE - (NONEXISTENCE) FOR GETTING URL PARAM VALUE
		getParameterUrlReplace : getParameterUrlReplace, //NOT USE - (NONEXISTENCE) FOR REPLACING URL PARAM VALUE
		validIndexArray : validIndexArray, //USED - FOR VALIDATING ARRAY BASING ON ITS INDEX VALUE
		validCoordAlternativesArray : validCoordAlternativesArray, //USED - FOR VALIDATING ALTERNATIVES ITINERARY IF ITS NOT THE SAME AS THE SELECTED ITINERARY
		validCoordAlternativesIds : validCoordAlternativesIds, //USED,
		validLatLng : validLatLng
	};

})();

// NEW GEN CODE

_GoogleCtrl = (function (){ 

	//Global Variables
	var _map, _map_dom, _map_setting, _map_waypts_collection = [],
	_map_style = {
		0 : {
			name : "eRoam", //"Grass & Water",
			styles : [{"featureType":"administrative","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"},{"weight":6}]},{"stylers":[{"saturation":-100}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#0099dd"}]},{"elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#aadd55"}]},{"featureType":"road.highway","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"road.arterial","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"road.local","elementType":"labels.text","stylers":[{"visibility":"on"}]},{}],
			icon_active : 'images/eroam-pin-green.png',
			icon_inactive : 'images/eroam-pin-red.png',
			icon_optional : 'images/eroam-pin-grey.png',
			route_stroke : {
				strokeOpacity: 1,
				strokeColor:'#ff3c2e',
				strokeWeight: 5,
				zIndex: 70,
				geodesic: true,
			},
			route_stroke_group : {
				strokeOpacity: 1,
				strokeColor:'#00db00',
				strokeWeight: 5,
				zIndex: 65,
				geodesic: true,
			},
			route_stroke_alter : {
				strokeOpacity: 1,
				strokeColor:'#ababab',
				strokeWeight: 5,
				zIndex: 60,
				geodesic: true,
			},
			route_stroke_alter_border : {
				strokeOpacity: 1,
				strokeColor:'#6b6b6b',
				strokeWeight: 7,
				zIndex: 55,
				geodesic: true,
			}
		},
		1 : {
			name : "eRoam", //"Retro",
			styles : [{"featureType":"administrative","stylers":[{"visibility":"on"}]},{"featureType":"poi","stylers":[{"visibility":"simplified"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"simplified"}]},{"featureType":"water","stylers":[{"visibility":"simplified"}]},{"featureType":"transit","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape","stylers":[{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"visibility":"off"}]},{"featureType":"road.local","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"water","stylers":[{"color":"#84afa3"},{"lightness":52}]},{"stylers":[{"saturation":-17},{"gamma":0.36}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"color":"#3f518c"}]}],
			icon_active : 'images/eroam-pin-green.png',
			icon_inactive : 'images/eroam-pin-red.png',
			icon_optional : 'images/eroam-pin-grey.png',
			route_stroke : {
				strokeOpacity: 1,
				strokeColor:'#ff3c2e',
				strokeWeight: 5,
				zIndex: 70,
				geodesic: true,
			},
			route_stroke_group : {
				strokeOpacity: 1,
				strokeColor:'#00db00',
				strokeWeight: 5,
				zIndex: 65,
				geodesic: true,
			},
			route_stroke_alter : {
				strokeOpacity: 1,
				strokeColor:'#ababab',
				strokeWeight: 5,
				zIndex: 60,
				geodesic: true,
			},
			route_stroke_alter_border : {
				strokeOpacity: 1,
				strokeColor:'#6b6b6b',
				strokeWeight: 7,
				zIndex: 55,
				geodesic: true,
			}
		},
		2 : {
			name : "eRoam", //"Nature",
			styles : [{"featureType":"landscape","stylers":[{"hue":"#FFA800"},{"saturation":0},{"lightness":0},{"gamma":1}]},{"featureType":"road.highway","stylers":[{"hue":"#53FF00"},{"saturation":-73},{"lightness":40},{"gamma":1}]},{"featureType":"road.arterial","stylers":[{"hue":"#FBFF00"},{"saturation":0},{"lightness":0},{"gamma":1}]},{"featureType":"road.local","stylers":[{"hue":"#00FFFD"},{"saturation":0},{"lightness":30},{"gamma":1}]},{"featureType":"water","stylers":[{"hue":"#00BFFF"},{"saturation":6},{"lightness":8},{"gamma":1}]},{"featureType":"poi","stylers":[{"hue":"#679714"},{"saturation":33.4},{"lightness":-25.4},{"gamma":1}]}],
			icon_active : 'images/eroam-pin-green.png',
			icon_inactive : 'images/eroam-pin-red.png',
			icon_optional : 'images/eroam-pin-grey.png',
			route_stroke : {
				strokeOpacity: 1,
				strokeColor:'#ff3c2e',
				strokeWeight: 5,
				zIndex: 70,
				geodesic: true,
			},
			route_stroke_group : {
				strokeOpacity: 1,
				strokeColor:'#00db00',
				strokeWeight: 5,
				zIndex: 65,
				geodesic: true,
			},
			route_stroke_alter : {
				strokeOpacity: 1,
				strokeColor:'#ababab',
				strokeWeight: 5,
				zIndex: 60,
				geodesic: true,
			},
			route_stroke_alter_border : {
				strokeOpacity: 1,
				strokeColor:'#6b6b6b',
				strokeWeight: 7,
				zIndex: 55,
				geodesic: true,
			}
		},
		3 : {
			name : "eRoam", //"Bentley",
			styles : [{"featureType":"all","elementType":"labels","stylers":[{"visibility":"off"},{"weight":6}]},{"featureType":"landscape","stylers":[{"hue":"#F1FF00"},{"saturation":-27.4},{"lightness":9.4},{"gamma":1}]},{"featureType":"road.highway","stylers":[{"hue":"#0099FF"},{"saturation":-20},{"lightness":36.4},{"gamma":1}]},{"featureType":"road.arterial","stylers":[{"hue":"#00FF4F"},{"saturation":0},{"lightness":0},{"gamma":1}]},{"featureType":"road.local","stylers":[{"hue":"#FFB300"},{"saturation":-38},{"lightness":11.2},{"gamma":1}]},{"featureType":"water","stylers":[{"hue":"#00B6FF"},{"saturation":4.2},{"lightness":-63.4},{"gamma":1}]},{"featureType":"poi","stylers":[{"hue":"#9FFF00"},{"saturation":0},{"lightness":0},{"gamma":1}]}],
			icon_active : '../images/eroam-pin-green.png',
			icon_inactive : '../images/eroam-pin-red.png',
			icon_optional : '../images/eroam-pin-grey.png',
			route_stroke : {
				strokeOpacity: 1,
				strokeColor:'#ff3c2e',
				strokeWeight: 5,
				zIndex: 70,
				geodesic: true,
			},
			route_stroke_group : {
				strokeOpacity: 1,
				strokeColor:'#00db00',
				strokeWeight: 5,
				zIndex: 65,
				geodesic: true,
			},
			route_stroke_alter : {
				strokeOpacity: 1,
				strokeColor:'#ababab',
				strokeWeight: 5,
				zIndex: 60,
				geodesic: true,
			},
			route_stroke_alter_border : {
				strokeOpacity: 1,
				strokeColor:'#6b6b6b',
				strokeWeight: 7,
				zIndex: 55,
				geodesic: true,
			}
		}
	},
	_coord_origin, _coord_destination, _coord_diameter, _coord_restricted_diameter,
	_coord_cities, _coord_all_cities, _coord_list_cities, _coord_alter_id, _coord_group_id,
	_coord_events = {}, 
	_coord_marker_arr = [],
	_coord_temp_cities = [],
	_coord_temp_cities_transport = [],
	_coord_obj = [],
	_coord_places = [],
	_coord_places_optional = [],
	_coord_marked_identifier = [],
	_coord_visioned_route = [],
	_coord_route_history = [],
	_coord_temp_allowed = 0,
	_ggl_line_option = {
	  path: 'M 0,-1 0,1',
	  strokeOpacity: 1,
	  scale: 5
	},
	_ggl_render_option = {
		draggable: true,
		preserveViewport: true,
		suppressMarkers: true,
		polylineOptions: _map_style[3].route_stroke
	},
	_ggl = google.maps,
	_ggl_geocoder = new _ggl.Geocoder(),
	_ggl_infowindow = new _ggl.InfoWindow,
	_ggl_direction = new _ggl.DirectionsService,
	_ggl_renderer = new _ggl.DirectionsRenderer(_ggl_render_option),
	_ggl_renderer_globally = new _ggl.Polyline(_map_style[3].route_stroke);
	_ggl_renderer_globally_border = new _ggl.Polyline(_map_style[3].route_stroke_alter_border);

	function initialize(dom, data_coord, auto_route){

		// SET REQUIRED VARIABLES
		_coord_origin = data_coord.data_coord_origin || {};
		_coord_destination = data_coord.data_coord_destination || {};
		_coord_places = data_coord.data_coord_places || [];
		_coord_diameter = data_coord.data_diameter || 0;
		_coord_restricted_diameter = data_coord.data_restricted_diameter || 0;
		_coord_cities = data_coord.data_cities || [];
		_coord_all_cities = data_coord.data_all_cities || [];
		_coord_events.option = data_coord.data_event || '';
		_coord_alter_id = data_coord.data_alter_id || '';
		_coord_group_id = data_coord.data_group_id || '';
		_coord_places_optional = [];
		_coord_temp_cities_transport = [];
		_coord_temp_cities = [];
		_coord_visioned_route = [];
		_coord_obj = [];

		console.log(_coord_origin);
		return false;

		switch(data_coord.data_init_map){

		   case 'oneway':

		   		// SET MAP
				setMap(dom, _coord_origin);

				var start_point = 1;

				// INITIALIZE THE FIRST MARKER
				// SET COMPASS FOR BOTH ORIGIN AND DESTINATION
				setCoordCompass(_coord_origin, _coord_destination);
				setCoordCompass(_coord_destination, _coord_origin);

				if(!_AlterCtrl.getCityIDArray(_coord_temp_cities, _coord_origin.city_id)){
					_coord_temp_cities.push(_coord_origin);
				}

				if(data_temp_edit_coords == false){ // IF EVER NO EDIT ROUTE'S IS HAPPENING

					//CHECK IF ALTERNATIVE ROUTE IS SELECTED
					if(_coord_alter_id){

						_DomCtrl.setListOptionEmpty(); // EMPTY ALL THE LIST OPTION DATA
						_DomCtrl.setListOptionChange(start_point, 'ddlFrom'); // SET EVENT LIST TO CHANGE TO ACTIVE THE ONCHANGE FUNCTION

						// ROUTE MARKER FASTEST ROUTE WITHIN 200KM VISION AND ALTERNATIVE ROUTES
						routeVisionCoord(_coord_origin, _coord_destination, _coord_all_cities, _coord_diameter, start_point, _coord_restricted_diameter, _coord_alter_id, 'alter');

					}else if(_coord_group_id){

						_DomCtrl.setListOptionEmpty(); // EMPTY ALL THE LIST OPTION DATA
						_DomCtrl.setListOptionChange(start_point, 'ddlFrom'); // SET EVENT LIST TO CHANGE TO ACTIVE THE ONCHANGE FUNCTION

						// ROUTE MARKER FASTEST ROUTE WITHIN 200KM VISION AND ALTERNATIVE ROUTES
						routeVisionCoord(_coord_origin, _coord_destination, _coord_all_cities, _coord_diameter, start_point, _coord_restricted_diameter, _coord_group_id, 'group');

					}else{ // IF NOT, SET THE STARTING ROUTE THEN AUTOROUTE

						_DomCtrl.setListOptionEmpty(); // EMPTY ALL THE LIST OPTION DATA
						_DomCtrl.setListOptionChange(start_point, 'ddlFrom'); // SET EVENT LIST TO CHANGE TO ACTIVE THE ONCHANGE FUNCTION

						// ROUTE MARKER FASTEST ROUTE WITHIN 200KM VISION AND ALTERNATIVE ROUTES
						routeVisionCoord(_coord_origin, _coord_destination, _coord_all_cities, _coord_diameter, start_point, _coord_restricted_diameter);

					}

				}else{ // WHEN COMING BACK FROM AN EDIT ROUTE

					_DomCtrl.setListOptionEmpty(); // EMPTY ALL THE LIST OPTION DATA
					_DomCtrl.setListOptionChange(start_point, 'ddlFrom'); // SET EVENT LIST TO CHANGE TO ACTIVE THE ONCHANGE FUNCTION

					setListRouteFixed(start_point, 0, data_temp_coords);

					data_temp_edit_coords = false;

				}

				//setBtnEditRoute('edit', 'edit-map-itinerary-btn');

				break;
			case 'multistop':

				// SET MAP
				setMap(dom, _coord_origin);

				if(data_temp_edit_coords == false){ // IF EVER NO EDIT ROUTE'S IS HAPPENING

					// INITIALIZE FIRST MAKER - SINCE THE MARKERS ARE DIFFERENT FROM THE ONE WAY - THAT WHY ITS NOT REFACTOR
					_coord_events.selected = true;
					setMarkers('multi', _coord_origin, false, false, _coord_events, false, true);
					if(!_AlterCtrl.getCityIDArray(_coord_temp_cities, _coord_origin.city_id)){
						_coord_temp_cities.push(_coord_origin);
					}

					// GET ALL THE MARKER OPTIONS THAT THE USER SUPPOSE TO CLICK
					setMarkerRoutesOptions([_coord_origin]);
					setMultiRouteCoord();
					_DomCtrl.setDomMultiAlter('multistop');

				}else{ // WHEN WANT TO EDIT ROUTE

					_coord_temp_allowed = 1;
					var data_coords = data_temp_coords; //getCleanMarkersArray('multi', data_temp_coords); //ITS ALREADY CLEAN DUE TO CHANGES OF REDIRECT

					// INITIALIZE FIRST MAKER - SINCE THE MARKERS ARE DIFFERENT FROM THE ONE WAY - THAT WHY ITS NOT REFACTOR
					_coord_events.selected = true;
					setMarkers('multi', data_coords[0], false, false, _coord_events, false, true);
					if(!_AlterCtrl.getCityIDArray(_coord_temp_cities, data_coords[0].city_id)){
						_coord_temp_cities.push(data_coords[0]);
					}

					// GET ALL THE MARKER OPTIONS THAT THE USER SUPPOSE TO CLICK
					setMarkerRoutesOptions([data_coords[0]]);

					for (var i = 1; i < data_coords.length; i++) {

						_coord_events.selected = false;

						setClearMarkers(_coord_marker_arr,'','multi');
						_coord_marker_arr.splice(0, _coord_marker_arr.length - 1);

						setMarkers('multi', data_coords[i], false, false, _coord_events, false, true);

						var marker = _AlterCtrl.getCityArray(_coord_marker_arr, data_coords[i].city_id, 'data');
						_ggl.event.trigger(marker, 'click');

					}

					//_DomCtrl.setRouteListSelected(data_coords);
					setMultiRouteCoord();
					_coord_temp_allowed = 0;
					
					_map.controls[_ggl.ControlPosition.RIGHT_TOP].clear();
					setBtnEditAction(false);
					_DomCtrl.setRouteListSelected(data_coords);
					_DomCtrl.setDomMultiAlter('oneway');

				}

				//_DomCtrl.setLoadingHide();

				break;
			case 'singlestop':

				// SET MAP
				setMap(dom, _coord_destination);

				setMarkers('single', _coord_destination, false, false, false, true, true);

				_DomCtrl.setDomMultiAlter('singlestop');
				_DomCtrl.setRouteMultistopDom([_coord_destination]);
				_DomCtrl.setLoadingHideQuick();

				break;

			case 'preview':

				setMap(dom, _coord_origin);

				for (var i = 0; i < _coord_all_cities.length; i++) {

					setMarkers('single', _coord_all_cities[i], false, false, false, true, true);

				}

				setMultiRouteCoord();

				break;

		}

		setBtnProposedScroll();

	}

	//SET FUNCTION COLLECTION

	function setMap(dom, data){

		// SET VARIABLES
		_map_dom = dom;
		_map_setting = {
			zoom: 6,
			center: setLatLng(data),
			mapTypeControlOptions: {
				mapTypeIds: [_ggl.MapTypeId.ROADMAP, 'eroam_map']
			}
		};

		// INITIALIZE MAP
		_map = new _ggl.Map(_map_dom, _map_setting);
		_map.mapTypes.set('eroam_map', setMapStyle());
		_map.setMapTypeId('eroam_map');
		_ggl_renderer.setMap(null);

	}

	function setMapStyle(){

		var style = _map_style[3].styles;

		return new _ggl.StyledMapType(style, {name: _map_style[3].name});

	}

	function setGeo(address, callback){

		_ggl_geocoder.geocode({ 'address': address }, function (response, status) {

			if (status == _ggl.GeocoderStatus.OK) {

				var data = { 
					name: response[0].address_components[0].long_name,
					lat: response[0].geometry.location.lat(), 
					lng: response[0].geometry.location.lng() 
				}; 

				callback(data);

			} else {

				alert('Geocode was not successful for the following reason: ' + status);

			}

		});

	}

	function setLatLng(coord){

		return (coord) ? new _ggl.LatLng(coord.lat, coord.lng) : new _ggl.LatLng(0, 0);

	}

	function setMarkers(type, data, auto_route, draggable, event_route, info_static, priority_route){

		var validate, marker, point;

		switch(type){

			case 'single':

				var marker = new _ggl.Marker({
					city_id: data.city_id,
					position: setLatLng(data),
					map: _map,
					title: data.name,
					draggable: draggable
				});

				if(priority_route){ // ROUTE MARKER SELECTED

					setMapMarkersIcons(true, true, marker); // IF WANT TO CHANGE OPTIONAL TO MUST-SEE
					//setMapMarkersIcons(data.priority, true, marker); // IF WANT TO RETAIN THE OPTINAL
					setMapMarkers(marker, 'add');

				}else{ // OPTIONAL ROUTE TO BE SELECTED

					setMapMarkersIcons(false, false, marker);
					setMapMarkersOptional(marker);

					_ggl.event.addListener(marker, 'click', function(event){

						setListOptionalEvent(marker);

					});

				}

				if(info_static){ // STATIC INFO BOX
					setMarkerInfoWindows('single', marker, priority_route);
				}else{ // HOVER IN AND OUT INFO BOX
					setMarkerInfoWindows('multi', marker, priority_route);
				}
				
			break;
			case 'multi':

				var marker = new _ggl.Marker({
					city_id: data.city_id,
					position: setLatLng(data),
					map: _map,
					title: data.name,
					draggable: draggable
				});

				//console.log("marker", marker);

				setMarkerMultiEvent(data, marker, event_route, info_static, priority_route);

			break;

		}

	}

	function setMapMarkersIcons(priority, type, marker){

		if(priority){

			//FINAL MARKER ICON
			marker.setIcon(_map_style[3].icon_active);

		}else{

			marker.setIcon(_map_style[3].icon_optional);

		}

	}

	function setMapMarkers(marker, type, key){

		_map.markers = _map.markers || [];

		switch(type){

			case 'add':

				_map.markers.push(marker);

				break;
			case 'cut':

				_map.markers.splice(key, 0, marker);

				break;

		}

	}

	function setMapRouted(coord, type, key){

		_map.routed = _map.routed || [];

		switch(type){

			case 'add':

				_map.routed.push(coord);

				break;
			case 'cut':

				_map.routed.splice(key, 0, coord);

				break;

		}

	}

	function setMapMarkersOptional(marker){

		_map.markers_optional = _map.markers_optional || [];
		_map.markers_optional.push(marker);

	}

	function setMapInfobox(infobox, type, key){

		_map.infoboxes = _map.infoboxes || [];

		switch(type){

			case 'add':

				_map.infoboxes.push(infobox);

				break;
			case 'cut':

				_map.infoboxes.splice(key, 0, infobox);

				break;
			case 'delete':

				for (var i = key; i < _map.infoboxes.length; i++) {

					_map.infoboxes[i].close();

				}

				_map.infoboxes.splice(key, _map.infoboxes.length - 1);

				break;
			case 'delete_single':

				_map.infoboxes[key].close();
				_map.infoboxes.splice(key, 1);

				break;

		}

	}

	function setMapControlPosition(obj, type, key){

		switch(type){

			case 'add':

				_map.controls[_ggl.ControlPosition.RIGHT_TOP].push(obj);

				break;
			case 'cut':

				_map.controls[_ggl.ControlPosition.RIGHT_TOP].splice(key, 1);
				_map.controls[_ggl.ControlPosition.RIGHT_TOP].splice(key, 0, obj);

				break;

		}

	}

	function setMarkerInfoWindows(type, coord_markers, priority){

		var infobox, infobox_index;

		switch(type){

			case 'single':

				infobox = setMarkerInfoWindowsData(coord_markers, priority);
				infobox = new InfoBox(infobox);
				infobox.city_id = coord_markers.city_id;
				infobox.open(_map);

				if(_map.infoboxes){

					infobox_index = _AlterCtrl.getCityArray(_map.infoboxes, coord_markers.city_id, 'key');

					if(infobox_index){
						setMapInfobox('', 'delete_single', infobox_index);
					}

				}

				setMapInfobox(infobox, 'add');

				break;
			case 'single_cut':

				infobox = setMarkerInfoWindowsData(coord_markers, priority);
				infobox = new InfoBox(infobox);
				infobox.city_id = coord_markers.city_id;
				infobox.open(_map);

				if(data_temp_edit_coords == false){

					if(_map.infoboxes){

						infobox_index = _AlterCtrl.getCityArray(_map.infoboxes, coord_markers.city_id, 'key');

						if(infobox_index){
							setMapInfobox('', 'delete_single', infobox_index);
						}

					}

					setMapInfobox(infobox, 'add');

				}else{

					setMarkerRouteProcess(coord_markers, infobox, 'infobox'); 

				}

				break;
			case 'multi':

				_ggl.event.addListener(coord_markers, 'mouseover', function(){

					infobox = setMarkerInfoWindowsData(coord_markers, priority);
					infobox = new InfoBox(infobox);
					infobox.city_id = coord_markers.city_id;
					infobox.open(_map);

				});

				_ggl.event.addListener(coord_markers, 'mouseout', function(){
					infobox.close();
				});

				break;

		}

	}

	function setMarkerInfoWindowsData(coord_markers, priority){

		var classname = (priority) ? "lbl-city-priority lbl-city-arrow" : "lbl-city lbl-city-arrow";

		return {
			content: "<div>" +  coord_markers.title + "</div>",
			boxStyle: {
				textAlign: "left",
				/*fontSize: "14px",*/
				whiteSpace: "nowrap",
				lineHeight: "16px",
				fontWeight: "bold",
				fontFamily: "Tahoma",
				margin: "-30px 0 0 18px" // RIGHT = -18px 0 0 24px, LEFT = 8px 0 0 -134px
			},
			disableAutoPan: true,
			position: new _ggl.LatLng(coord_markers.position.lat(), coord_markers.position.lng()),
			closeBoxURL: "",
			isHidden: false,
			pane: "floatPane",
			enableEventPropagation: true,
			boxClass: classname
		};

	}

	function setListOptionalEvent(marker){

		var data = getCleanMarkersArray('single', marker),
		data_info = [], data_opt, data_opt_index, data_opt_dom;

		data_opt = getListOptionalData(data).current;

		if(data_opt){

			data_opt_index = _AlterCtrl.getCityArray(_coord_obj, data_opt.city_id, 'key');
			data_opt_index = data_opt_index + 1;

			if(_coord_obj[data_opt_index]){

				data_opt_dom = _coord_obj[data_opt_index].dom_id;

				data_info.push({
					dom_id : data_opt_dom,
					city_id : data.city_id
				});

				data_opt_dom++;

				for (var i = data_opt_index; i < _coord_obj.length; i++) {

					data_info.push({
						dom_id : data_opt_dom,
						city_id : _coord_obj[i].city_id
					});

					data_opt_dom++;

				}

				_AjaxCtrl.promises(data_info, "GetAllTransportByCityID", {option:'places'}).done(function (response) {

					setCollectRoute(setListMultiRoute(data_info, response));

				});

			}else{

				data_opt_dom = _DomCtrl.setListLastSelected();
				data_opt_dom = data_opt_dom.dom_id + 1;

				_DomCtrl.setListOptionChangeSelect(data_opt_dom, data.city_id);
				setListRouteCoord();

			}
			
		}

	}

	function setListMultiRoute(data_info, data_coord){

		for (var i = 0; i < data_info.length; i++) { 

			var data = _DomCtrl.setSelectToArray(data_info[i].dom_id);

			if(data.indexOf(data_info[i].city_id) > -1){

				_DomCtrl.setListOptionChangeSelect(data_info[i].dom_id, data_info[i].city_id, data_coord);

			}

		}

	}

	function setMarkerMultiEvent(data, marker, event_route, info_static, priority_route){

		switch(event_route.selected){

			case true: // IF ITS SELECTED OR TRUE

				setMapMarkersIcons(true, true, marker); // IF WANT TO CHANGE OPTIONAL TO MUST-SEE
				setMarkerInfoWindows('single_cut', marker, priority_route); //setMarkerInfoWindows('multi', marker);

				if(_map.markers === undefined || _map.markers.length === 0){ // FOR ROUTING BACK TO ORIGIN

					setMapMarkers(marker, 'add');
					setMapRouted(data, 'add');
					//setMarkerInfoWindows('single', marker, priority_route);
					setMarkerMultiState('point', marker, data, event_route);

				}else{ // SINCE IT IS SELECTED, THE NEXT CLICK WILL BE SET FOR DELETION. FOR MARKER SELECTED DELETION

					setMarkerMultiProcess(marker, data);
					if(_coord_temp_allowed === 0){
						setMultiRouteCoord();
					}
					setMarkerMultiState('establish', marker, data, event_route);

				}

				break;
			case false: //SET MARKER WHICH IS NOT SELECTED OR AN OPTION TO BE SELECTED

				setMapMarkersIcons(false, false, marker);
				if(info_static){
					setMarkerInfoWindows('single', marker, priority_route);
				}else{
					setMarkerInfoWindows('multi', marker, priority_route);
				}
				setMarkerMultiState('prepare', marker, data, event_route);

				break;

		}

	}

	function setMarkerMultiState(type, marker, data, event_route){

		switch(type){

			case 'point':

				_ggl.event.addListener(marker, "click", function(event) {

					point = {
						lat : _map.markers[_map.markers.length - 1].position.lat(),
						lng : _map.markers[_map.markers.length - 1].position.lng()
					};

					if(_AlterCtrl.getCityArray(_map.markers, data.city_id, 'count') <= 1 && validCoordPointToPoint(data, point)){

						setMarkerMultiProcess(marker, data);
						setMultiRouteCoord();

					}

				});

				break;
			case 'prepare':

				_coord_marker_arr.push(marker);

				_ggl.event.addListener(marker, "click", function(event) {

					event_route.selected = true;

					setClearMarkers(_coord_marker_arr,'','multi');

					_coord_marker_arr.splice(0, _coord_marker_arr.length);

					setMarkers('multi', data, false, false, event_route, true, true);

					setMarkerRoutesOptions([data]); 

				});

				break;
			case 'establish':

				_ggl.event.addListener(marker, "click", function(event) {

					var marker_index = _AlterCtrl.getCityArray(_map.markers, marker.city_id, 'key'),
					marker_valid = validRouteBetweenCoord(_map.markers, marker_index);

					setClearMarkersOccurrence(_map.markers, _coord_origin.city_id,'multi');
					setClearMarkers(_coord_marker_arr,'','multi');

					setMarkerMultiClear(marker_index, marker_valid, marker.city_id);

					setMarkerRoutesOptions([_map.markers[_map.markers.length - 1]]);

					routeVisionMarkers(_map.markers);
					_DomCtrl.setRouteMultistopDom(_map.markers);

				});

				break;

		}

	}

	function setMarkerMultiClear(index, valid, city_id){

		var infobox_index = _AlterCtrl.getCityArray(_map.infoboxes, city_id, 'key');

		if(valid){

			setClearMarkers(_map.markers, index,'single');
			_map.markers.splice(index, 1);
			_map.routed.splice(index, 1);
			_coord_places_optional.splice(index, 1);
			_coord_marker_arr.splice(0, _coord_marker_arr.length);
			if(infobox_index){
				setMapInfobox('', 'delete_single', infobox_index);
			}

		}else{

			setClearMarkers(_map.markers, index,'multi');
			_map.markers.splice(index, _map.markers.length - 1);
			_map.routed.splice(index, _map.routed.length - 1);
			_coord_places_optional.splice(index, _coord_places_optional.length - 1);
			_coord_marker_arr.splice(0, _coord_marker_arr.length);
			setMapInfobox('', 'delete', index);

		}

	}

	function setMarkerMultiProcess(marker, data){

		//OLD - THIS IS OPTIONAL BY COORDINATES. MULTI-STOP FUNCTION THAT ONLY APPLY TO OPTIONAL AND NOT MUST-SEE 
		//setMarkerRouteProcess(marker, data); 
		//OLD - ROUTE ADD 
		// setMapMarkers(marker, 'add');
		// setMapRouted(data, 'add');

		//NEW - THIS IS OPTIONAL BY COLLECTION OF ARRAY. MULTI-STOP FUNCTION WILL TREAT ALL PLACES/MUST-SEE/OPTIONAL AS OPTIONAL
		//NEW - COMBINATION OF MERGING AND ADDING OF ROUTES
		if(data_temp_edit_coords == false){ // IF EVER NO EDIT ROUTE'S IS HAPPENING

			setMapMarkers(marker, 'add');
			setMapRouted(data, 'add');

		}else{

			setMarkerRouteProcess(marker, data, 'process'); 

		}

	}

	function setClearMarkers(coord_markers, index, type){

		if(coord_markers){

			switch(type){

				case 'multi':

					for (var i = (index) ? index : 0; i < coord_markers.length; i++ ) {
						coord_markers[i].setIcon(null);
						coord_markers[i].setMap(null);
						//_ggl.event.clearInstanceListeners(coord_markers[i]); - // NOT USED BUT DONT DELETE
					}

					break;
				case 'single':

					coord_markers[index].setIcon(null);
					coord_markers[index].setMap(null);

					break;

			}

		}

	}

	function setClearMarkersOccurrence(coord_markers, city_id){
		
		var count = 1;

		if(_AlterCtrl.getCityArray(coord_markers, city_id, 'count') > 1){

			for (var i = 0; i < coord_markers.length; i++) {

				if(coord_markers[i].city_id == city_id && count > 1){
					coord_markers.splice(i, 1);
				}else if(coord_markers[i].city_id == city_id){
					count++;
				}

			}

		}

		return coord_markers;

	}

	function setCoordCompass(origin, destination){

		var bearing = calcBearing(origin, destination),
		compass = setCompassPoint(bearing);
		destination.compass = compass;
		return compass;

	}

	function setCoordCompassLiberal(origin, destination){

		var bearing = calcBearing(origin, destination),
		compass = setCompassPointLiberal(bearing);
		destination.compass = compass;
		return compass;

	}

	function setCompassPoint(bearing){

		var compass;
		bearing = ((bearing%360)+360)%360; // normalise to 0..360

		position = Math.round(bearing*8/360)%8;

		switch (position) {
			case 0: compass = 'N';  break;
			case 1: compass = 'NE'; break;
			case 2: compass = 'E';  break;
			case 3: compass = 'SE'; break;
			case 4: compass = 'S';  break;
			case 5: compass = 'SW'; break;
			case 6: compass = 'W';  break;
			case 7: compass = 'NW'; break;
		}

		return compass;

	}

	function setCompassPointLiberal(bearing){

		var compass;
		bearing = ((bearing%360)+360)%360; // normalise to 0..360

		position = Math.round(bearing*4/360)%4;

		switch (position) {
			case 0: compass = 'N'; break;
			case 1: compass = 'E'; break;
			case 2: compass = 'S'; break;
			case 3: compass = 'W'; break;
		}

		return compass;

	}

	function setNearbyMarkerCompass(coord, destination, marker_nearby){

		if(marker_nearby){

			for (var i = 0; i < marker_nearby.length; i++) {

				setCoordCompass(coord, marker_nearby[i]);

				if(!validCompass(marker_nearby[i].compass, destination.compass)){
					marker_nearby.splice(i, 1);
					i--;
				}

			}

		}

		return marker_nearby;

	}

	function setNearbyMarkerCompassLiberal(coord, destination, marker_nearby){

		if(marker_nearby){

			for (var i = 0; i < marker_nearby.length; i++) {

				setCoordCompass(coord, marker_nearby[i]);

				if(!validCompassLiberal(marker_nearby[i].compass, destination.compass)){
					marker_nearby.splice(i, 1);
					i--;
				}

			}

		}

		return marker_nearby;

	}

	function setCollectRoute(fn){

		_AjaxCtrl.assortment(fn, function(response){

			setListRouteCoord();

		});

	}

	function setListRouteGeo(auto_route){

		_coord_origin = _DomCtrl.setListLastSelected();

		setGeo(_coord_origin.name+","+_coord_origin.country, function(origin_response) {

			origin_response.city_id = _coord_origin.city_id;
			origin_response.dom_id = _coord_origin.dom_id;
			_coord_origin = origin_response;

			setCollectRoute(setListRouteEvent(_coord_origin, auto_route));

		});

	}

	function setListRouteEvent(data_coord, auto_route){

		_coord_origin = data_coord;
		setCoordCompass(_coord_origin, _coord_destination);
		setCoordCompass(_coord_destination, _coord_origin);

		if(!_coord_places.length || !_AlterCtrl.getCityIDArray(_coord_places, _coord_origin.city_id)){
			_coord_places.push(_coord_origin);
		}

		var i = _coord_origin.dom_id;

		_AjaxCtrl.initialize("{city_id: "+_coord_origin.city_id+",pass_type_ids:''}", "GetAllTransportByCityID", false, {option:'places'}, true, function(response){

			if(response){

				_coord_cities = response;
				_coord_list_cities = _coord_cities;
				setListTempCollectCities(_coord_cities);
				setIsolateCitiesFiltered(_coord_cities, _coord_origin.city_id); // SEPARATE THE PRIORITY FOR MUST-SEE AND OPTIONAL

				if(_coord_cities.length){

					var nearest_city = calcNearbyDistance(_coord_origin, _coord_destination, _coord_cities);
					var exist_destination = _AlterCtrl.getCityIDArray(_coord_places, _coord_destination.city_id);
					var exist_nearest = _AlterCtrl.getCityIDArray(_coord_places, nearest_city.city_id);

					if(nearest_city && !exist_destination && !exist_nearest){

						// ADD NEW DROPDOWN LIST
						i++;
						_DomCtrl.setNewListOption(i, true);
						_DomCtrl.setListOptionDom(_coord_list_cities, nearest_city, i);

						nearest_city.dom_id = i;

						setClearMarkers(_map.markers,'','multi');
						setClearMarkers(_map.markers_optional,'','multi');
						_map.markers = [];
						_map.markers_optional = [];
						_coord_obj.push({ dom_id : i , city_id : nearest_city.city_id });
						_coord_places.push(nearest_city);
						_coord_origin = nearest_city;

						if(auto_route){
							setListRouteEvent(_coord_origin, auto_route);
						}

					}

				}

			}

		});

	}

	function setListRoute(dom_id, city_id, route_segment, data_coord){

		var data = {
			city_id : city_id,
		},
		count = parseInt(dom_id.split('_')[1]);

		if(_AlterCtrl.getCityIDArray(data_coord, data.city_id)){

			_coord_cities = _AlterCtrl.getCityArray(data_coord, data.city_id, 'data').response;
			_coord_cities = setListCities(_coord_cities, city_id);
			_coord_list_cities = _coord_cities;
			setListTempCollectCities(_coord_cities);

			var routed_city = _AlterCtrl.getObjArray(_coord_obj, count, 'data');
			_coord_origin = _AlterCtrl.getCityArray(_coord_temp_cities, city_id, 'data');

			if(route_segment && routed_city){

				setListRouteClear(routed_city.index);

			}

			setIsolateCitiesFiltered(_coord_cities, city_id); // SEPARATE THE PRIORITY FOR MUST-SEE AND OPTIONAL

			if(_coord_origin){

				_coord_obj.push({ dom_id : count , city_id : _coord_origin.city_id });
				_coord_places.push(_coord_origin);

			}

			_DomCtrl.setNewListOption(count + 1, true);
			_DomCtrl.setListOptionDom(_coord_list_cities, data, count + 1);

		}

	}

	function setListRouteClear(index){

		setClearMarkers(_map.markers,'','multi');
		setClearMarkers(_map.markers_optional,'','multi');
		_map.markers = [];
		_map.markers_optional = [];
		_coord_places_optional.splice(index, _coord_places_optional.length - 1);
		_coord_places.splice(index, _coord_places.length - 1);
		_coord_obj.splice(index, _coord_obj.length - 1);

	}

	function setListRouteCoord(){

		_DomCtrl.setLoadingShow('init');
		setClearMarkers(_map.markers,'','multi');
		setClearMarkers(_map.markers_optional,'','multi');
		_map.markers = [];
		_map.markers_optional = [];

		// SET MARKER FOR MUST SEE PLACES
		setListRouteCoordPriority(_coord_places);

		// ROUTE ALL THE PRIORITY/MUST-SEE PLACES TO ANOTHER PAGE
		_DomCtrl.setRouteOnewayhdDom(_map.markers);
		_DomCtrl.setPageManualOverride('TailTabMulti'); // CAUSE IT NEED TO BE REDIRECT TO THE OTHER PAGE SO THEY CAN EDIT DIRECTLY

	}

	function setMultiRouteCoord(marker, data){

		_DomCtrl.setLoadingShow('init');
		routeVisionMarkers(_map.markers);
		_DomCtrl.setRouteMultistopDom(_map.markers);
		//_DomCtrl.setLoadingHideQuick();
		//_DomCtrl.setLoadingHide();

	}

	function setMarkerRouteProcess(marker, data, type){ // EVERY CLICK SA MARKER

		var temp_data, ref_data, ref_area, ref_next_data, data_index, resource, action, data_list_option, 
		data_opt_origin, data_opt_destination, data_opt, data_validate = false, data_opt_info = [];

		data_list_option = getListOptionalData(marker);
		ref_data = (data_list_option.current) ? data_list_option.current : null;
		ref_next_data = (data_list_option.next) ? data_list_option.next : null;
		ref_array = (data_list_option.array.length) ? data_list_option.array : null;
		//validIsolateCitiesFilteredTransport('marker_validate', _coord_places_optional, _coord_temp_cities_transport, marker);
		//
		switch(type){

			case 'process':

				temp_data = getCleanMarkersArray('single', marker);
				resource = _map.markers;
				action = function(condition, index){

					if(condition == 'add'){

						setMapMarkers(marker, 'add');
						setMapRouted(data, 'add');

					}else{

						setMapMarkers(marker, 'cut', index + 1);
						setMapRouted(data, 'cut', index + 1);

					}

				};

				break;
			case 'optional':

				temp_data = marker;
				resource = _coord_places_optional;
				action = function(condition, index){

					if(condition == 'add'){

						_coord_places_optional.push(data);

					}else{

						_coord_places_optional.splice(index + 1, 0, data);

					}

				};

				break;
			case 'infobox':

				temp_data = getCleanMarkersArray('single', marker);
				resource = _map.markers;
				action = function(condition, index){

					if(_map.infoboxes){

						var infobox_index = _AlterCtrl.getCityArray(_map.infoboxes, temp_data.city_id, 'key');

						if(infobox_index){
							setMapInfobox('', 'delete_single', infobox_index);
						}

					}

					if(condition == 'add'){

						setMapInfobox(data, 'add');

					}else{

						setMapInfobox(data, 'cut', index + 1);

					}

				};

				break;

		}

		if(ref_array){

			data_opt_origin = _map.routed[0];
			data_opt_destination = _map.routed[_map.routed.length - 1];

			if(data_opt_origin.city_id !== data_opt_destination.city_id){

				setCoordCompass(temp_data, data_opt_origin);
				setCoordCompass(temp_data, data_opt_destination);

				data_opt_info = validCompassMarkerRouteProcess(data_opt_origin, data_opt_destination, temp_data, ref_array);

				if(data_opt_info.length){

					//GETTING THE ACCURATE NEAREST CITY COMING FROM THE CURRENT ONE
					data_opt = validSortMarkerRouteProcess(data_opt_origin, data_opt_destination, temp_data, data_opt_info);

					//SETTING UP WHAT IS NEXT STOP WILL BE
					data_list_option = getListOptionalData(data_opt, 'fixed');
					ref_data = (data_list_option.current) ? data_list_option.current : null;
					ref_next_data = (data_list_option.next) ? data_list_option.next : null;

					if(ref_data && ref_next_data){

						data_validate = validMarkerRouteProcess(temp_data, ref_data, ref_next_data, data_opt_origin, data_opt_destination, _coord_diameter);

						if(data_validate){

							data_index = _AlterCtrl.getCityArray(resource, ref_data.city_id, 'key');

							if(data_index !== false){

								action('cut', data_index);

							}else{

								action('add');

							}

						}else{

							action('add');

						}

					}else{

						action('add');

					}


				}else{

					action('add');

				}

			}else{

				action('add');

			}

			
		}else{

			action('add');

		}

	}

	function setSortMarkerRouteProcess(ref_coord, cur_coord, opt_coord){

		var result = [], 
		coord = calcDistanceRearrange(cur_coord, opt_coord);

		setCoordCompass(ref_coord, cur_coord);

		for (var i = 0; i < coord.length; i++) {
			
			setCoordCompass(cur_coord, coord[i]);

			if(validCompassLiberal(coord[i].compass, cur_coord.compass)){

				result.push(coord[i]);

			}

		}

		if(!result.length){

			for (var i = 0; i < coord.length; i++) {
			
				setCoordCompass(cur_coord, coord[i]);

				if(validCompassLiberalWide(coord[i].compass, cur_coord.compass)){

					result.push(coord[i]);

				}

			}

		}

		return result;

	}

	function setListRouteCoordPriority(coord_cities){

		if(coord_cities){

			for (var i = 0; i < coord_cities.length; i++) {

				setMarkers('single', coord_cities[i], false, false, false, true, true);

			}

		}

	}

	function setListRouteCoordOptional(coord_cities, coord_cities_optional){

		if(coord_cities_optional){

			for (var i = 0; i < coord_cities_optional.length; i++) {

				for (var j = 0; j < coord_cities_optional[i].cities.length; j++) {

					if(!_AlterCtrl.getCityIDArray(coord_cities, coord_cities_optional[i].cities[j].city_id)){
						setMarkers('single', coord_cities_optional[i].cities[j], false, false, false, false, false);
					}

				}

			}

		}

	}

	function setListOption(origin, destination, charted){

		var dom_count = 2; // ITS BECAUSE '1' IS ALREADY IMPLEMENTED(THE ORIGIN OF TRAVEL)

		charted.unshift(origin);
		charted.push(destination);

		for (var i = 0; i < charted.length; i++) {

			var expected_data = charted[i + 1];

			var additional = {
				option : 'options',
				charted : charted[i],
				expect_charted : charted[i + 1],
				dom_count : dom_count
			};

			if(charted[i + 1])
				_AjaxCtrl.initialize("{city_id: "+charted[i].city_id+",pass_type_ids:''}", "GetAllTransportByCityID", true, additional);

			dom_count++;

		}

	}

	function setListTempCollectCities(coord_cities){

		for (var i = 0; i < coord_cities.length; i++) { // THE COLLECTOR OF ALL CITIES
			
			if(!_AlterCtrl.getCityIDArray(_coord_temp_cities, coord_cities[i].city_id)){
				_coord_temp_cities.push(coord_cities[i]);
			}

		}

	}

	function setListTempCollectTransport(coord, coord_cities){

		if(!_AlterCtrl.getCityIDArray(_coord_temp_cities_transport, coord.city_id)){

			var data = {
				city_id : coord.city_id,
				name : coord.name,
				cities : coord_cities
			};

			_coord_temp_cities_transport.push(data);

		}

	}

	function setIsolateCities(coord_cities){

		var optional = [],
			cities = [];

		for (var i = 0; i < coord_cities.length; i++) { // SEPARATE THE MUST SEE AND OPTIONAL

			if(coord_cities[i].priority){
				cities.push(coord_cities[i]);
			}else{
				optional.push(coord_cities[i]);
			}

		}

		return { cities : cities , optional : optional };

	}

	function setIsolateCitiesFiltered(coord_cities, city_id){

		var filtered_cities, data, origin;

		origin = _AlterCtrl.getCityArray(_coord_temp_cities, city_id, 'data');

		if(origin){

			filtered_cities = setIsolateCities(coord_cities);
			_coord_cities = filtered_cities.cities;
			filtered_optional = validIsolateCitiesFilteredCoord(_coord_places_optional, filtered_cities.optional, origin);

			data = {
				city_id : origin.city_id,
				lat : origin.lat,
				lng : origin.lng,
				name : origin.name,
				priority : origin.priority,
				cities : calcDistanceRearrange(origin, filtered_optional)
			};

			if(!_AlterCtrl.getCityIDArray(_coord_places_optional, data.city_id)){
				_coord_places_optional.push(data);
			}

		}  

	}

	function setListCities(coord_cities, city_id){

		for (var i = 0; i < coord_cities.length; i++) { 
			
			if(coord_cities[i].city_id == city_id){
				coord_cities.splice(i, 1);
			}

		}

		return coord_cities;

	}

	function setListRouteFixed(start_point, start_index, route_coord){

		var dom_id = start_point;
		var result = [];

		for (var i = start_index; i < route_coord.length; i++) {

			result.push({
				dom_id : dom_id,
				city_id : route_coord[i].city_id.toString()
			});

			dom_id++;

		}

		_AjaxCtrl.promises(result, "GetAllTransportByCityID", {option:'places'}).done(function (response) {

			setCollectRoute(setListMultiRoute(result, response));

		});

	}

	function setListRouteCharted(origin, destination, coord_charted, callback){

		var result = [],
		blacklist_coord = [],
		origin_instance = origin,
		start_pair_instance = origin,
		end_pair_instance = destination,
		destination_instance = destination,
		check_route, check_route_exist, check_route_blacklist;

		for (var i = 0; i < coord_charted.length; i++) {

			if(coord_charted[i + 1]){

				setCoordCompass(start_pair_instance, coord_charted[i + 1]);

				if(validCompass(coord_charted[i + 1].compass, start_pair_instance.compass)){
					blacklist_coord.push(coord_charted[i + 1]);
				}

				if(validCompass(start_pair_instance.compass, coord_charted[i + 1].compass)){
					blacklist_coord.push(coord_charted[i + 1]);
				}

				setCoordCompass(end_pair_instance, coord_charted[i + 1]);

				if(end_pair_instance.city_id !== coord_charted[i + 1].city_id){

					if(validCompass(end_pair_instance.compass, coord_charted[i + 1].compass)){
						blacklist_coord.push(coord_charted[i + 1]);
					}

				}

			}

		}

		_AjaxCtrl.promises(coord_charted, "GetAllTransportByCityID", {option:'places'}).done(function (response) {

			for (var i = 0; i < coord_charted.length; i++) {

				if(coord_charted[i + 1]){

					if(_AlterCtrl.getCityIDArray(response, origin_instance.city_id)){

						destination_instance = coord_charted[i + 1];

						check_route = _AlterCtrl.getCityArray(response, origin_instance.city_id, 'data').response;
						check_route_exist = _AlterCtrl.getCityIDArray(check_route, destination_instance.city_id);
						check_route_blacklist = _AlterCtrl.getCityIDArray(blacklist_coord, destination_instance.city_id);

						setCoordCompass(origin, destination_instance);

						if(destination_instance.priority && check_route_exist && !check_route_blacklist){

							origin_instance = destination_instance;
							result.push(destination_instance);

						}

					}

				}

			}

			if(result.length && destination.city_id == result[result.length - 1].city_id){
				result.pop();
			}

			callback(result);

		});

	}

	function setListRouteChartedFiltered(coord_charted, callback){

		var origin, destination, check_route, check_route_exist;

		_AjaxCtrl.promises(coord_charted, "GetAllTransportByCityID", {option:'places'}).done(function (response) {

			for (var i = 0; i < coord_charted.length; i++) {
			
				if(coord_charted[i + 1]){

					origin = coord_charted[i];
					destination = coord_charted[i + 1];

					setCoordCompass(origin, destination);
					setCoordCompass(destination, origin);

					if(_AlterCtrl.getCityIDArray(response, origin.city_id)){

						check_route = _AlterCtrl.getCityArray(response, origin.city_id, 'data').response;
						check_route_exist = _AlterCtrl.getCityIDArray(check_route, destination.city_id);

						if(!check_route_exist){

							coord_charted.splice(i, 1);
							i--;

						}

					}

				}

			}

			callback(coord_charted);

		});

	}

	function setMarkersDistance(origin, coord_cities){

		var mlat, mlng, dlat, dlng,
		radius = 6371,
		angular_dest, //angular distance in radians
		sqr_half, //square of half the chord length between the points
		diameter, //diameter
		distances = [],
		result = [],
		closest = -1;

		for(i = 0; i < coord_cities.length; i++) {

			if(validCoordPointToPoint(origin, coord_cities[i])){

				mlat = coord_cities[i].lat;
				mlng = coord_cities[i].lng;
				dlat  = calcRadius(mlat - origin.lat);
				dlng = calcRadius(mlng - origin.lng);

				sqr_half = Math.sin(dlat/2) * Math.sin(dlat/2) +
					Math.cos(calcRadius(origin.lat)) * Math.cos(calcRadius(origin.lat)) * Math.sin(dlng/2) * Math.sin(dlng/2);
				angular_dest = 2 * Math.atan2(Math.sqrt(sqr_half), Math.sqrt(1-sqr_half));
				diameter = radius * angular_dest;
				distances[i] = diameter;

				closest = i;
				coord_cities[i].distance = diameter;
				result.push(coord_cities[i]);

			}

		}

		return result;

	}

	function setAlterCoord(origin, destination, id, start_point){

		var alter_coord;

		_AjaxCtrl.promises([{origin : origin, destination : destination}], "GetAlterCitiesByCityID", false).done(function (response) {

			alter_coord = response[0].response;
			alter_coord = (alter_coord.length) ? _AlterCtrl.setStripAlterCoord(alter_coord) : null;
			alter_coord = (alter_coord) ? _AlterCtrl.setAlterIDCoord(alter_coord) : null;
			alter_coord = (alter_coord) ?
							(!id || isNaN(id)) ? alter_coord[alter_coord.length - 1].data
												: _AlterCtrl.getAlterArray(alter_coord, _coord_alter_id, 'data').data 
							: null;

			if(!alter_coord){

				_DomCtrl.setPageRedirectType('multistop');

			}else{

				setListRouteFixed(start_point, 0, alter_coord);

			}

		}); 

	}

	function setAutoCoord(origin, destination, route_markers, route_diameter, start_point, route_restricted_diameter){

		var route_legs, route_legs_coord, route_nearby, result, route_valid, distance_allowed,
		route_charted = [],
		request = {
			origin: setLatLng(origin),
			destination: setLatLng(destination),
			travelMode: _ggl.TravelMode.DRIVING,
			provideRouteAlternatives: true
		};

		_ggl_direction.route(request, function (response, status) {

			if (status == _ggl.DirectionsStatus.OK) {

				route_legs = response.routes[response.routes.length - 1].legs[0].steps;

				for (var i = 0; i < route_legs.length; i++) {

					route_legs_coord = {
						lat : route_legs[i].end_location.lat(),
						lng : route_legs[i].end_location.lng()
					};

					distance_allowed = getDistanceCoord(origin, route_legs_coord);

					if(distance_allowed > route_restricted_diameter){

						route_nearby = calcNearbyMarker(origin, destination, route_markers, route_legs_coord, route_diameter);

						if(route_nearby.length){

							for (var j = 0; j < route_nearby.length; j++) {

								if(!_AlterCtrl.getCityIDArray(route_charted, route_nearby[j].city_id)){
									route_charted.push(route_nearby[j]);
								}

							}

						}

					}

				}

				route_charted = calcDistanceRearrange(origin, route_charted);
				route_charted.unshift(origin);
				route_charted.push(destination);

				setListRouteCharted(origin, destination, route_charted, function(result){ // 1 PHASE VALIDATION - CHECK IF THERE IS TRANSPORT COMING FROM ORIGIN

					result.unshift(origin);
					result.push(destination);

					setListRouteChartedFiltered(result, function(response){ // 2 PHASE VALIDATION - CHECK EACH PAIR IF THEY HAVE PROPER TRANSPORT

						response.unshift(origin);
						response.push(destination);

						setListRouteChartedFiltered(response, function(response){ // 3 PHASE VALIDATION (TO STRICT) - CHECK EACH PAIR IF THEY HAVE PROPER TRANSPORT

							route_valid = validRouteChartedEndToEnd(response);
							setRouteCoord(route_valid, response, start_point, 0);

						});

					});

				});

				
			}else{

				alert('Routing coordinates was not successful for the following reason: ' + status);
				_DomCtrl.setLoadingHideQuick();
				_DomCtrl.setPageRedirectType('multistop');

			}

		});

	}
	// Manaul Selected Drive
	function setRouteCoord(valid, coord, start_point, index){
		if(valid){
   			//OLD - SETTING UP DATA ON MAP
			//setListRouteFixed(start_point, index, coord);        
			//NEW - SETTING UP DATA ON MAP
			_DomCtrl.setRouteOnewayhdDom(coord);
			_DomCtrl.setPageManualOverride('TailTabMulti'); // CAUSE IT NEED TO BE REDIRECT TO THE OTHER PAGE SO THEY CAN EDIT DIRECTLY
		}else{
			_DomCtrl.setPageRedirectType('multistop');
		}
	}

		/*
		function setMarkerRoutesOptions(data){

		if(data){

			_AjaxCtrl.promises(data, "GetAllTransportByCityID", {option:'places'}).done(function (response) {

				var route_markers, filtered_optional;

				for (var j = 0; j < data.length; j++) {

					if(_AlterCtrl.getCityIDArray(response, data[j].city_id)){

						route_markers = _AlterCtrl.getCityArray(response, data[j].city_id, 'data').response;

						//NEW - WILL BE USED FOR THE OPTIONAL COORDINATES. SETTING MARKER UP FOR MULTI-STOP SINCE ALL MARKERS NOW ARE TREAT AS OPTIONAL
						setListTempCollectCities(route_markers);
						setListTempCollectTransport(data[j], route_markers);
						
						filtered_optional = validIsolateCitiesFilteredLiberal(_coord_places_optional, route_markers, data[j]);
						data[j].cities = calcDistanceRearrange(data[j], filtered_optional);

						if(!_AlterCtrl.getCityIDArray(_coord_places_optional, data[j].city_id)){

							if(data_temp_edit_coords == false){

								_coord_places_optional.push(data[j]);

							}else{

								setMarkerRouteProcess(data[j], data[j], 'optional');

							}
							
						}

						validIsolateCitiesFilteredTransport('reset', _coord_places_optional, null);
						validIsolateCitiesFilteredTransport('validate', _coord_places_optional, _coord_temp_cities_transport);

						for (var x = 0; x < _coord_places_optional.length; x++) {

							for (var y = 0; y < _coord_places_optional[x].cities.length; y++) {

								var route_markers_events = {
									option : true,
									selected : false
								};

								if(_coord_places_optional[x].cities[y].include){

									if(!_AlterCtrl.getCityIDArray(_map.markers, _coord_places_optional[x].cities[y].city_id) && !_AlterCtrl.getCityIDArray(_coord_marker_arr, _coord_places_optional[x].cities[y].city_id)){
										setMarkers('multi', _coord_places_optional[x].cities[y], false, false, route_markers_events, true, false);
									}

								}

							}

						}

					}

				}
				validMapInfobox();
				validMarkerRouteVisibility();
			});

		}

	}
*/

	
	function setMarkerRoutesOptions(data){
		if(data){
			_AjaxCtrl.promises(data, "GetAllTransportByCityID", {option:'places'}).done(function (response) {
				// setMarkerRoutesOptionsViaCity(data);
				var route_markers, filtered_optional;
				for (var j = 0; j < data.length; j++) {
					if(_AlterCtrl.getCityIDArray(response, data[j].city_id)){
						// run get stored procedures with city id param
				  	  	_AjaxCtrl.initialize("{city_id:"+ data[j].city_id +"}", "GetCityByViaCityID", false); // added
						route_markers = _AlterCtrl.getCityArray(response, data[j].city_id, 'data').response;
						// //console.log(route_markers);
						//NEW - WILL BE USED FOR THE OPTIONAL COORDINATES. SETTING MARKER UP FOR MULTI-STOP SINCE ALL MARKERS NOW ARE TREAT AS OPTIONAL
						setListTempCollectCities(route_markers);
						// //console.log(setListTempCollectCities);
						setListTempCollectTransport(data[j], route_markers);				
						filtered_optional = validIsolateCitiesFilteredLiberal(_coord_places_optional, route_markers, data[j]);
						// //console.log(filtered_optional);
						data[j].cities = calcDistanceRearrange(data[j], filtered_optional);
						// //console.log(data[j].cities);
						if(!_AlterCtrl.getCityIDArray(_coord_places_optional, data[j].city_id)){
							if(data_temp_edit_coords == false){
								_coord_places_optional.push(data[j]);
							}else{
								setMarkerRouteProcess(data[j], data[j], 'optional');
							}						
						}
						validIsolateCitiesFilteredTransport('reset', _coord_places_optional, null);
						validIsolateCitiesFilteredTransport('validate', _coord_places_optional, _coord_temp_cities_transport);
						for (var x = 0; x < _coord_places_optional.length; x++) {
							for (var y = 0; y < _coord_places_optional[x].cities.length; y++) {
								var route_markers_events = {
									option : true,
									selected : false
								};
								if(_coord_places_optional[x].cities[y].include){
									if(!_AlterCtrl.getCityIDArray(_map.markers, _coord_places_optional[x].cities[y].city_id) && !_AlterCtrl.getCityIDArray(_coord_marker_arr, _coord_places_optional[x].cities[y].city_id)){
										setMarkers('multi', _coord_places_optional[x].cities[y], false, false, route_markers_events, true, false);
									}
								}
							}
						}
					}
				}
				validMapInfobox();
				validMarkerRouteVisibility();
				/*//console.log("CITIES TEMP TRANSPORT : ", _coord_temp_cities_transport);
				//console.log("OPTIONAL CITIES : ", _coord_places_optional);
				//console.log("INFOBOX : ", _map.infoboxes);
				//console.log("MARKERS : ", _map.routed); */
			});
		}
	} 

	function setMarkerRouteVisibility(type, category){

		var coord_label;

		switch(category){

			case 'selected':

				for (var i = 0; i < _map.markers.length; i++) {

					coord_label = _AlterCtrl.getCityArray(_map.infoboxes, _map.markers[i].city_id, 'data');

					if(coord_label){

						switch(type){
							case 'hide':

									coord_label.close();

								break;
							case 'show':

									coord_label.open(_map);

								break;
						}

					}

				}


				break;
			case 'optional':

				for (var i = 0; i < _coord_marker_arr.length; i++) {

					coord_label = _AlterCtrl.getCityArray(_map.infoboxes, _coord_marker_arr[i].city_id, 'data');

					if(coord_label){

						switch(type){
							case 'hide':
									
									_coord_marker_arr[i].setVisible(false);
									coord_label.close();

								break;
							case 'show':

									_coord_marker_arr[i].setVisible(true);
									coord_label.open(_map);

								break;
						}

					}

				}

				break;

		}

	}

	function setBtnEditRoute(type, id){

		_DomCtrl.setDomEditBtn(type, id);

		var btn = _DomCtrl.getDomObject(id), check_btn_arr;
		_DomCtrl.setDomShow(id);

		if(_map.controls[_ggl.ControlPosition.RIGHT_TOP].getAt(0)){

			_map.controls[_ggl.ControlPosition.RIGHT_TOP].removeAt(0);
			_map.controls[_ggl.ControlPosition.RIGHT_TOP].insertAt(0, btn);

		}else{

			_map.controls[_ggl.ControlPosition.RIGHT_TOP].setAt(0, btn);

		}
		
		if(_map.controls[_ggl.ControlPosition.RIGHT_TOP].getAt(1)){

			_map.controls[_ggl.ControlPosition.RIGHT_TOP].removeAt(1);
			_map.controls[_ggl.ControlPosition.RIGHT_TOP].insertAt(1, _DomCtrl.getDomObject('spacer-div'));

		}else{

			_map.controls[_ggl.ControlPosition.RIGHT_TOP].setAt(1, _DomCtrl.getDomObject('spacer-div'));

		}

		_ggl.event.addDomListener(btn, 'click', function() {

			if(type == 'edit'){

				data_temp_marker_visibility = true;
				setMarkerRouteVisibility('show', 'optional');
				//setMarkerRouteVisibility('hide', 'selected');
				_DomCtrl.setDomMultiAlter('edit_instruction');

			}else{

				data_temp_marker_visibility = false;
				setMarkerRouteVisibility('hide', 'optional');
				//setMarkerRouteVisibility('show', 'selected');
				_DomCtrl.setDomMultiAlter('oneway');

			}

			setBtnEditAction(data_temp_marker_visibility);
			setBtnProposedScroll();

		});

	}

	function setBtnEditAction(type){

		if(type === false){

			setBtnEditRoute('edit', 'edit-map-itinerary-btn');

		}else{

			setBtnEditRoute('submit', 'submit-map-itinerary-btn');

		}

	}

	function setBtnProposedScroll(){

		_DomCtrl.setDomViewProposedBtn();

		var btn = _DomCtrl.getDomObject('view-itinerary-btn');
		_DomCtrl.setDomShow('view-itinerary-btn');

		if(_map.controls[_ggl.ControlPosition.RIGHT_TOP].getAt(2)){

			_map.controls[_ggl.ControlPosition.RIGHT_TOP].removeAt(2);
			_map.controls[_ggl.ControlPosition.RIGHT_TOP].insertAt(2, btn);

		}else{

			_map.controls[_ggl.ControlPosition.RIGHT_TOP].setAt(2, btn);

		}

		_ggl.event.addDomListener(btn, 'click', function() {

			_DomCtrl.setDomSlide('proposed-itinerary-main-container');

		});

	}

	//GET FUNCTION COLLECTION

	function getCleanMarkersArray(type, array){

		var result;

		switch(type){
			case 'single':

				result = {
					city_id: array.city_id, 
					name: array.title,
					lat: array.position.lat(),
					lng: array.position.lng()
				};

				break;
			case 'multi':

				result = [];

				for (var i = 0; i < array.length; i++) {

					var data = {
						city_id: array[i].city_id, 
						name: array[i].title,
						lat: array[i].position.lat(),
						lng: array[i].position.lng()
					};

					result.push(data);

				}
				
				break;
		}

		return result;

	}

	function getNearbyMarker(data) {

		var nearby, result = false;

		if(data.length){

			nearby = data[0].distance;

			for (var i = 0; i < data.length; i++) {

				if (nearby >= data[i].distance) {
					nearby = data[i].distance;
					result = data[i];
				}

			}

		}

		return result;

	}

	function getListOptionalData(data, type){

		var result = {
			current : null,
			next : null,
			array : []
		}, check_route;

		for (var i = 0; i < _coord_places_optional.length; i++) {
			
			if(type == 'fixed'){

				if(_coord_places_optional[i].city_id === data.city_id){

					result.current = _coord_places_optional[i];
					result.next = (_coord_places_optional[i + 1]) ? _coord_places_optional[i + 1] : null;
					result.array.push(_coord_places_optional[i]);

				}

			}else{

				check_route = _AlterCtrl.getCityArray(_coord_places_optional[i].cities, data.city_id, 'data');

				if(check_route){

					result.current = _coord_places_optional[i];
					result.next = (_coord_places_optional[i + 1]) ? _coord_places_optional[i + 1] : null;
					result.array.push(_coord_places_optional[i]);

				}

			}

		}

		return result;

	}

	function getDistanceCoord(coord, coord_opt){

		var result, mlat, mlng, dlat, dlng, 
		angular_dest, //angular distance in radians
		sqr_half, //square of half the chord length between the points
		radius = 6371, // radius of earth in km

		mlat = coord.lat;
		mlng = coord.lng;
		dlat = calcRadius(mlat - coord_opt.lat);
		dlng = calcRadius(mlng - coord_opt.lng);

		sqr_half = Math.sin(dlat / 2) * Math.sin(dlat / 2) +
		Math.cos(calcRadius(coord_opt.lat)) * Math.cos(calcRadius(coord_opt.lat)) * Math.sin(dlng / 2) * Math.sin(dlng / 2);
		angular_dest = 2 * Math.atan2(Math.sqrt(sqr_half), Math.sqrt(1 - sqr_half));
		result = radius * angular_dest;

		return result;

	}

	//VALIDATE FUNCTION COLLECTION

	function validMapInfobox(){

		var check_array = [], check_infobox, check_selected, check_optional;

		for (var i = 0; i < _map.infoboxes.length; i++) {

			check_selected = _AlterCtrl.getCityArray(_map.markers, _map.infoboxes[i].city_id, 'data');
			check_optional = _AlterCtrl.getCityArray(_coord_marker_arr, _map.infoboxes[i].city_id, 'data');

			if(!check_selected){

				if(!check_optional){

					check_array.push({index : i , city_id : _map.infoboxes[i].city_id});

				}

			}

		}

		if(check_array.length){

			for (var i = 0; i < check_array.length; i++) {

				check_infobox = _AlterCtrl.getCityArray(_map.infoboxes, check_array[i].city_id, 'key');

				if(check_infobox){

					setMapInfobox('', 'delete_single', check_infobox);

				}

			}

		}

	}

	function validMarkerRouteProcess(coord, prev_coord, next_coord, origin_coord, destination_coord, diameter){

		var result = true;

		if(!calcWithinRadiusMarker(prev_coord, coord, diameter)){

			setCoordCompass(prev_coord, coord);

			if(!validCompassLiberal(coord.compass, destination_coord.compass)){

				setCoordCompass(prev_coord, next_coord);

				if(!validCompassLiberal(coord.compass, next_coord.compass)){

					result = false;

				}else{

					result = true;

				}

			}else{

				result = true;

			}

		}else{

			result = true;

		}

		return result;

	}

	function validSortMarkerRouteProcess(origin, destination, cur_coord, opt_coord){

		var result, result_ref, result_arr = [], result_info = [], data_ref_origin, data_ref_destination, data_ref,
		data_ref_opt, data_ref_arr = [], data_ref_info = [];

		data_ref_origin = setSortMarkerRouteProcess(origin, cur_coord, opt_coord);
		data_ref_destination = setSortMarkerRouteProcess(destination, cur_coord, opt_coord);

		if(!data_ref_origin && !data_ref_destination){

			result = calcDistanceRearrange(cur_coord, opt_coord)[0];

		}else{

			if(data_ref_origin.length){

				data_ref = calcDistanceRearrange(cur_coord, data_ref_origin)[0];

			}

			if(data_ref_destination.length){

				data_ref_opt = (data_ref) ? data_ref : null;
				data_ref = calcDistanceRearrange(cur_coord, data_ref_destination)[0];

				if(data_ref_opt){

					data_ref_info = [data_ref_opt, data_ref];

					for (var i = 0; i < data_ref_info.length; i++) {
			
						setCoordCompass(cur_coord, data_ref_info[i]);

						if(validCompassLiberal(data_ref_info[i].compass, cur_coord.compass)){

							data_ref_arr.push(data_ref_info[i]);

						}

					}

					if(!data_ref_arr.length){

						data_ref = calcDistanceRearrange(cur_coord, data_ref_info)[0];

					}else{

						data_ref = calcDistanceRearrange(cur_coord, data_ref_arr)[0];

					}

				}

			}

			if(!data_ref){

				result = calcDistanceRearrange(cur_coord, opt_coord)[0];

			}else{
				
				result_ref = calcDistanceRearrange(cur_coord, opt_coord)[0];
				result_info = [data_ref, result_ref];

				/*PROTO*/
				result = calcDistanceRearrange(cur_coord, result_info)[0];

				/*WORKING ON-PROGRESS*/
				/*//console.log("RES REF : ", result_ref);
				//console.log("RES INF : ", result_info);

				for (var i = 0; i < result_info.length; i++) {
			
					setCoordCompass(cur_coord, result_info[i]);

					if(validCompassLiberal(result_info[i].compass, cur_coord.compass)){

						result_arr.push(result_info[i]);

					}

				}

				//console.log("RESULT ARR : ", result_arr);

				if(!result_arr.length){

					result = calcDistanceRearrange(cur_coord, result_info)[0];

				}else{

					result = calcDistanceRearrange(cur_coord, result_arr)[0];

				}*/

			}

		}

		return result;

	}

	function validCompassMarkerRouteProcess(origin, destination, cur_coord, ref_coord){

		var result = [];

		//PHASE 1 VALIDATION FOR COMPASS - STRICTLY COMING FROM ORIGIN ONLY
		for (var i = 0; i < ref_coord.length; i++) {
					
			setCoordCompass(cur_coord, ref_coord[i]);

			if(validCompassLiberal(ref_coord[i].compass, origin.compass)){

				result.push(ref_coord[i]);

			}

		}

		//PHASE 2 VALIDATION FOR COMPASS - STRICTLY COMING FROM ORIGIN AND DESTINATION ONLY
		if(!result.length){

			for (var i = 0; i < ref_coord.length; i++) {
					
				setCoordCompass(cur_coord, ref_coord[i]);

				if(validCompassLiberal(ref_coord[i].compass, destination.compass)){

					result.push(ref_coord[i]);

				}

			}

			//PHASE 3 VALIDATION FOR COMPASS - LESS STRICT AND ALLOW ALL - JUST TO MAKE/GET THE POSSIBLE ROUTE
			if(!result.length){

				for (var i = 0; i < ref_coord.length; i++) {

					result.push(ref_coord[i]);

				}

			}

		}

		return result;

	}

	function validCompass(compass_origin, compass_destination){

		var result;

		switch(compass_destination){
			case 'N':
			result = (compass_origin == 'N' || compass_origin == 'NE' || compass_origin == 'NW') ? true : false;
			break;
			case 'E':
			result = (compass_origin == 'E' || compass_origin == 'NE' || compass_origin == 'SE') ? true : false;
			break;
			case 'S':
			result = (compass_origin == 'S' || compass_origin == 'SE' || compass_origin == 'SW') ? true : false;
			break;
			case 'W':
			result = (compass_origin == 'W' || compass_origin == 'NW' || compass_origin == 'SW') ? true : false;
			break;
			case 'NE':
			result = (compass_origin == 'N' || compass_origin == 'NE' || compass_origin == 'E') ? true : false;
			break;
			case 'NW':
			result = (compass_origin == 'N' || compass_origin == 'NW' || compass_origin == 'W') ? true : false;
			break;
			case 'SE':
			result = (compass_origin == 'E' || compass_origin == 'SE' || compass_origin == 'S') ? true : false;
			break;
			case 'SW':
			result = (compass_origin == 'S' || compass_origin == 'SW' || compass_origin == 'W') ? true : false;
			break;
		}

		return result;

	}

	function validCompassLiberal(compass_origin, compass_destination){

		var result;

		switch(compass_destination){
			case 'N':
			result = (compass_origin == 'N' || compass_origin == 'NE' || compass_origin == 'NW' || compass_origin == 'E' || compass_origin == 'W') ? true : false;
			break;
			case 'E':
			result = (compass_origin == 'E' || compass_origin == 'NE' || compass_origin == 'SE' || compass_origin == 'N' || compass_origin == 'S') ? true : false;
			break;
			case 'S':
			result = (compass_origin == 'S' || compass_origin == 'SE' || compass_origin == 'SW' || compass_origin == 'E' || compass_origin == 'W') ? true : false;
			break;
			case 'W':
			result = (compass_origin == 'W' || compass_origin == 'NW' || compass_origin == 'SW' || compass_origin == 'N' || compass_origin == 'S') ? true : false;
			break;
			case 'NE':
			result = (compass_origin == 'N' || compass_origin == 'NE' || compass_origin == 'E') ? true : false;
			break;
			case 'NW':
			result = (compass_origin == 'N' || compass_origin == 'NW' || compass_origin == 'W') ? true : false;
			break;
			case 'SE':
			result = (compass_origin == 'E' || compass_origin == 'SE' || compass_origin == 'S') ? true : false;
			break;
			case 'SW':
			result = (compass_origin == 'S' || compass_origin == 'SW' || compass_origin == 'W') ? true : false;
			break;

		}

		return result;

	}

	function validCompassLiberalWide(compass_origin, compass_destination){

		var result;

		switch(compass_destination){
			case 'N':
			result = (compass_origin == 'N' || compass_origin == 'NE' || compass_origin == 'NW' || compass_origin == 'E' || compass_origin == 'W') ? true : false;
			break;
			case 'E':
			result = (compass_origin == 'E' || compass_origin == 'NE' || compass_origin == 'SE' || compass_origin == 'N' || compass_origin == 'S') ? true : false;
			break;
			case 'S':
			result = (compass_origin == 'S' || compass_origin == 'SE' || compass_origin == 'SW' || compass_origin == 'E' || compass_origin == 'W') ? true : false;
			break;
			case 'W':
			result = (compass_origin == 'W' || compass_origin == 'NW' || compass_origin == 'SW' || compass_origin == 'N' || compass_origin == 'S') ? true : false;
			break;
			case 'NE':
			result = (compass_origin == 'N' || compass_origin == 'NE' || compass_origin == 'E' || compass_origin == 'SE' || compass_origin == 'NW') ? true : false;
			break;
			case 'NW':
			result = (compass_origin == 'N' || compass_origin == 'NW' || compass_origin == 'W' || compass_origin == 'SW' || compass_origin == 'NE') ? true : false;
			break;
			case 'SE':
			result = (compass_origin == 'E' || compass_origin == 'SE' || compass_origin == 'S' || compass_origin == 'NE' || compass_origin == 'SW') ? true : false;
			break;
			case 'SW':
			result = (compass_origin == 'S' || compass_origin == 'SW' || compass_origin == 'W' || compass_origin == 'NW' || compass_origin == 'SE') ? true : false;
			break;
		}

		return result;

	}

	function validCompassLiberalLight(compass_origin, compass_destination){

		var result;

		switch(compass_destination){
			case 'N':
			result = (compass_origin == 'S') ? false : true;
			break;
			case 'E':
			result = (compass_origin == 'W') ? false : true;
			break;
			case 'S':
			result = (compass_origin == 'N') ? false : true;
			break;
			case 'W':
			result = (compass_origin == 'E') ? false : true;
			break;
		}

		return result;

	}

	function validEndToEnd(coord_validate, coord){

		if((coord.lat != coord_validate.origin.lat && coord.lng != coord_validate.origin.lng) &&
		(coord.lat != coord_validate.destination.lat && coord.lng != coord_validate.destination.lng)){
			return true;
		}

		return false;

	}

	function validCoordPointToPoint(coord_first, coord_second){

		if(coord_first.lat != coord_second.lat && coord_first.lng != coord_second.lng){
			return true;
		}

		return false;

	}

	function validNearbyMarker(coord_validate, coord){

		if((coord_validate.markers.lat != coord.lat && coord_validate.markers.lng != coord.lng) &&
		(coord_validate.markers.lat != coord_validate.origin.lat && coord_validate.markers.lng != coord_validate.origin.lng) &&
		(coord_validate.markers.lat != coord_validate.destination.lat && coord_validate.markers.lng != coord_validate.destination.lng)){
			return true;
		}

		return false;

	}

	function validNearbyMarkerCollection(charted, nearby){

		var valid = false;

		if(charted.length){

			for (var i = 0; i < charted.length; i++) {

				if(nearby.lat == charted[i].lat && nearby.lng == charted[i].lng){
					valid = true;
				}

			}

		}

		return (valid) ? false : true;

	}

	function validIsolateCitiesFilteredCoord(coord_cities_optional, filtered_optional, coord){

		var result = [];

		if(coord_cities_optional && filtered_optional){

			if(coord_cities_optional.length && filtered_optional.length){

				for (var i = 0; i < coord_cities_optional.length; i++) {

					setCoordCompass(coord_cities_optional[i], coord);

					for (var j = 0; j < filtered_optional.length; j++) {

						setCoordCompass(coord, filtered_optional[j]);

						if(validCompass(coord.compass, filtered_optional[j].compass)){

							// FOR RE-POSITIONING THE OPTIONAL
							if(!_AlterCtrl.getCityIDArray(result, filtered_optional[j].city_id)){
								result.push(filtered_optional[j]);
							}

							// FOR REMOVING IF THE OPTIONAL IS ON THE OTHER ROUTE
							var coord_index = _AlterCtrl.getCityArray(coord_cities_optional[i].cities, filtered_optional[j].city_id, 'key');
							if(coord_index){
								coord_cities_optional[i].cities.splice(coord_index, 1);
							}

						}

					}

				}

			}else{

				result = filtered_optional;

			}

		}

		return result;

	}

	function validIsolateCitiesFilteredTransport(type, coord_cities_optional, coord_cities_transport, coord_data){

		var check_route, check_route_ref, coord_ref, coord_current, check_route_exist, coord_list_option, coord_index,
		coord_key, coord_include, coord_length;

		if(coord_cities_optional && coord_cities_optional.length){

			switch(type){

				case 'reset':

					coord_include = true;
					coord_length = coord_cities_optional.length;

					if(data_temp_edit_coords == false){

						coord_key = coord_length - 1;

						for (var i = 1; i < coord_length - 1; i++) {

							coord_current = coord_cities_optional[i];

	                        for (var j = 0; j < coord_current.cities.length; j++) {
	                        
	                                coord_current.cities[j].include = false;

	                        }

						}

					}else{

						coord_key = 0;

					}

					for (var i = coord_key; i < coord_length; i++) {

                        coord_current = coord_cities_optional[i];

                        for (var j = 0; j < coord_current.cities.length; j++) {
                        
                                coord_current.cities[j].include = coord_include;

                        }

                    }

					break;
				case 'validate':

					for (var i = 1; i < coord_cities_optional.length; i++) {

						coord_ref = coord_cities_optional[i - 1];
						coord_current = coord_cities_optional[i];
						check_route = _AlterCtrl.getCityArray(coord_cities_transport, coord_current.city_id, 'data').cities;
						check_route_ref = _AlterCtrl.getCityArray(coord_cities_transport, coord_ref.city_id, 'data').cities;

						for (var j = 0; j < coord_ref.cities.length; j++) {

							check_route_exist = _AlterCtrl.getCityIDArray(check_route, coord_ref.cities[j].city_id);

							if(coord_ref.cities[j].include){
								coord_ref.cities[j].include = check_route_exist;
							}

						}

					}

				break;
				case 'marker_validate':

					/*coord_current = coord_data;
					check_route = _AlterCtrl.getCityArray(coord_cities_transport, coord_current.city_id, 'data');
					
					_AjaxCtrl.promises([coord_current], "GetAllTransportByCityID", {option:'places'}).done(function (response) {

						//console.log("01 ; ", response);
						coord_ref = _AlterCtrl.getCityArray(response, coord_current.city_id, 'data').response;
						coord_list_option = getListOptionalData(coord_data);
						coord_list_option = (coord_list_option) ? coord_list_option.current : null;
						coord_index = _AlterCtrl.getCityArray(coord_cities_optional, coord_list_option.city_id, 'key');

						//NEXT PROGRAM SHOULD REMOVE DATA WITHOUT PROPER TRANSPORT

					});*/


					break;

			}

		}

	}

	function validIsolateCitiesFilteredLiberal(coord_cities_optional, filtered_optional, coord){

		var result = [];

		if(coord_cities_optional && filtered_optional){

			if(coord_cities_optional.length && filtered_optional.length){

				for (var i = 0; i < coord_cities_optional.length; i++) {

					for (var j = 0; j < filtered_optional.length; j++) {

						// FOR RE-POSITIONING THE OPTIONAL
						if(!_AlterCtrl.getCityIDArray(result, filtered_optional[j].city_id)){
							result.push(filtered_optional[j]);
						}

						// FOR REMOVING IF THE OPTIONAL IS ON THE OTHER ROUTE
						var coord_index = _AlterCtrl.getCityArray(coord_cities_optional[i].cities, filtered_optional[j].city_id, 'key');

						if(coord_index){
							coord_cities_optional[i].cities[coord_index].include = false;
						}

					}

				}

			}else{

				result = filtered_optional;

			}

		}

		return result;

	}

	function validRouteBetweenCoord(coord_charted, marker_index){

		var result = false;

		for (var i = 0; i < coord_charted.length; i++) {

			if(marker_index == i && coord_charted[i + 1]){

				origin = coord_charted[i - 1];
				destination = coord_charted[i + 1];

				check_route = _AjaxCtrl.initialize("{city_id: "+origin.city_id+",pass_type_ids:''}", "GetAllTransportByCityID", false, {option:'places'});
				result = _AlterCtrl.getCityIDArray(check_route, destination.city_id);

			}

		}

		return result;

	}

	function validRouteChartedEndToEnd(coord_charted){

		var origin, destination, result = true;

		if(coord_charted.length <= 2){

			if(coord_charted[0] && coord_charted[coord_charted.length - 1]){

				origin = coord_charted[0];
				destination = coord_charted[coord_charted.length - 1];

				check_route = _AjaxCtrl.initialize("{city_id: "+origin.city_id+",pass_type_ids:''}", "GetAllTransportByCityID", false, {option:'places'});
				check_route_exist = _AlterCtrl.getCityIDArray(check_route, destination.city_id);

				if(!check_route_exist){

					result = false;

				}

			}else{

				result = false;

			}

		}

		return result;

	}

	function validMarkerRouteVisibility(){

		if(data_temp_edit_coords === true){

			if(data_temp_marker_visibility === false){

				setMarkerRouteVisibility('hide', 'optional');

			}else{

				setMarkerRouteVisibility('show', 'optional');

			}
			
		}else{

			setMarkerRouteVisibility('show', 'optional');

		}

	}

	//CALCULATION FUNCTION COLLECTION

	function calcRadius(data) { // rad

		return data * Math.PI / 180; 

	}

	function calcDegree(data){ // degree

		return data * 180 / Math.PI;

	}

	function calcBearing(origin, destination) { //find_bearing

		var dest_lng = calcRadius(destination.lng - origin.lng),
		y = Math.sin(dest_lng) * Math.cos(calcRadius(destination.lat)),
		x = Math.cos(calcRadius(origin.lat))*Math.sin(calcRadius(destination.lat)) - Math.sin(calcRadius(origin.lat))*Math.cos(calcRadius(destination.lat))*Math.cos(dest_lng),
		bearing = calcDegree(Math.atan2(y, x));

		return ((bearing + 360) % 360);

	}

	function calcDistanceRearrange(origin, coord_cities){

		var result = setMarkersDistance(origin, coord_cities);

		_AlterCtrl.setSortNearbyDistance(result);

		return result;

	}

	function calcNearbyDistance(origin, destination, coord_cities){

		var result = setMarkersDistance(origin, coord_cities);

		_AlterCtrl.setCleanArray(result);
		setNearbyMarkerCompass(origin, destination, result);

		if(!result.length)
			result = false;
		else
			result = getNearbyMarker(result);

		return result;

	}

	function calcWithinRadiusMarker(coord, ref_coord, route_diameter){

		var validate, mlat, mlng, dlat, dlng, 
		angular_dest, //angular distance in radians
		sqr_half, //square of half the chord length between the points
		diameter, //diameter
		radius = 6371, // radius of earth in km
		result = true;

		mlat = coord.lat;
		mlng = coord.lng;
		dlat = calcRadius(mlat - ref_coord.lat);
		dlng = calcRadius(mlng - ref_coord.lng);

		sqr_half = Math.sin(dlat / 2) * Math.sin(dlat / 2) +
		Math.cos(calcRadius(ref_coord.lat)) * Math.cos(calcRadius(ref_coord.lat)) * Math.sin(dlng / 2) * Math.sin(dlng / 2);
		angular_dest = 2 * Math.atan2(Math.sqrt(sqr_half), Math.sqrt(1 - sqr_half));
		diameter = radius * angular_dest;

		if ( diameter <= route_diameter ) { 

			result = true;

		}else{

			result = false;

		}


		return result;

	}

	function calcNearbyMarker(origin, destination, route_markers, route_coord, route_diameter){

		var validate, mlat, mlng, dlat, dlng, 
		angular_dest, //angular distance in radians
		sqr_half, //square of half the chord length between the points
		diameter, //diameter
		radius = 6371, // radius of earth in km
		result = [];

		for (i = 0; i < route_markers.length; i++) {

			coord_validate = {
				markers : {lat: route_markers[i].lat, lng: route_markers[i].lng},
				origin : origin,
				destination : destination
			};  

			if(validNearbyMarker(coord_validate, route_coord)){

				mlat = route_markers[i].lat;
				mlng = route_markers[i].lng;
				dlat = calcRadius(mlat - route_coord.lat);
				dlng = calcRadius(mlng - route_coord.lng);

				sqr_half = Math.sin(dlat / 2) * Math.sin(dlat / 2) +
				Math.cos(calcRadius(route_coord.lat)) * Math.cos(calcRadius(route_coord.lat)) * Math.sin(dlng / 2) * Math.sin(dlng / 2);
				angular_dest = 2 * Math.atan2(Math.sqrt(sqr_half), Math.sqrt(1 - sqr_half));
				diameter = radius * angular_dest;

				if ( diameter <= route_diameter ) { 

					result[i] = {
						city_id: route_markers[i].city_id,
						distance: diameter, 
						name: route_markers[i].name,
						lat: route_markers[i].lat,
						lng: route_markers[i].lng,
						priority: route_markers[i].priority
					};

				}

			}

		}

		_AlterCtrl.setCleanArray(result);

		return result;

	}

	//SETTING ROUTE FUNCTION COLLECTION

	function routeVisionCoord(origin, destination, route_markers, route_diameter, start_point, route_restricted_diameter, route_alter_id, route_alter_type){

		var alter_coord, fn, alter_coord_default;

		fn = (route_alter_type) 
				? (route_alter_type == 'group') 
					? 'GetAlterItineraryByCityID'
					: 'GetAlterCitiesByCityID'
				: 'GetAlterCitiesByCityID';

		_AjaxCtrl.promises([{origin : origin, destination : destination}], fn, false).done(function (response) {

			alter_coord = response[0].response;
			alter_coord = (alter_coord.length) ? _AlterCtrl.setStripAlterCoord(alter_coord) : null;
			alter_coord = (alter_coord) ? _AlterCtrl.setAlterIDCoord(alter_coord) : null;
			alter_coord_default = (alter_coord) ? alter_coord.filter(function(val){
				return val.data[0].priority == true
			}) : false;

			alter_coord = (alter_coord) 
							? (!route_alter_id || isNaN(route_alter_id)) 
								? (alter_coord_default && alter_coord_default.length > 0) ? alter_coord_default[alter_coord_default.length - 1].data : alter_coord[alter_coord.length - 1].data
								: (_AlterCtrl.getAlterIDArray(alter_coord, route_alter_id)) ? _AlterCtrl.getAlterArray(alter_coord, route_alter_id, 'data').data 
									: null
							: null;

			data_itinerary_data = (route_alter_type && route_alter_type == 'group') ? alter_coord : [];
			// //console.log("aaa", data_itinerary_data);

			if(!alter_coord){

				setAutoCoord(origin, destination, route_markers, route_diameter, start_point, route_restricted_diameter);

			}else{

				setRouteCoord(true, alter_coord, start_point, 0);

			}

		});

		
	}

	function routeFitBounds(coord_markers){

		_ggl_bounds = new _ggl.LatLngBounds();

		for (var i = 0; i < coord_markers.length; i++) {

			data = {
				city_id: coord_markers[i].city_id,
				name: coord_markers[i].title,
				lat: coord_markers[i].position.lat(),
				lng: coord_markers[i].position.lng()
			};

			_ggl_bounds.extend(setLatLng(data));
		}

		_ggl.event.addListener(_map, 'zoom_changed', function() {

			listener = _ggl.event.addListener(_map, 'bounds_changed', function(event) {
				
				if (this.getZoom() > 6 && this.initialZoom === true) {
					this.setZoom(6);
					this.initialZoom = false;
				}

				_ggl.event.removeListener(listener);

			});
		});

		_map.initialZoom = true;
		_map.fitBounds(_ggl_bounds);

	}

/* 
	function routeVisionMarkers(coord_markers){

		_DomCtrl.setLoadingShow('init', true);

		//OLD ROUTING ONLY WITH ROADS
		//routeFitBounds(coord_markers);
		//routeVisionMarkersIdentifier(coord_markers);
		//routeVisionMarkersAutoPolylines(coord_markers);

		//NEW ROUTING WITH FLIGHT
		routeFitBounds(coord_markers);
		routeVisionMarkersIdentifier(coord_markers);
		routeVisionMarkersManualPolylines(coord_markers);
	}
 */

	function routeVisionMarkers(coord_markers){
		_DomCtrl.setLoadingShow('init', true);
		////console.log("ROUTE PROGRAM : ", coord_markers);

		var temp_coord_markers = routeVisionMarkersIntervene(coord_markers);
		////console.log("Temp Coord Markers", temp_coord_markers); // 
		//FOR VIA CITIES WE NEED TO CHANGE THE MARKERS ARRAY VALUES
		routeFitBounds(coord_markers);
		//routeVisionMarkersIdentifier(coord_markers);
		routeVisionMarkersManualPolylines(temp_coord_markers);
	} 

	/* ------------------------------ new updated code here may 17 2016 ------------------------------- */
	function routeVisionMarkersIntervene(coord_markers){

		var result = [], data, sub_data, sub_data_branch, sub_data_detail, count, ctr = 0, temp = {};
		for (var i = 0; i < coord_markers.length; i++) {
			_AjaxCtrl.initialize("{city_id:"+ coord_markers[i].city_id +"}", "GetCityByViaCityID", false); // MOVE BY CALOY
			temp[coord_markers[i].city_id] = {};
		}

		for (var i = 0; i < data_all_via_cities.length; i++) {

			if(_AlterCtrl.getCityArray(coord_markers, data_all_via_cities[i].city_id, 'data')){
				data_all_via_cities[i].city_id;
				temp[data_all_via_cities[i].city_id];
				if(!temp[data_all_via_cities[i].city_id].data){
					temp[data_all_via_cities[i].city_id].data = [];
				}
				data_all_via_cities[i];
				temp[data_all_via_cities[i].city_id].data.push(data_all_via_cities[i]);
			}
		}

		coord_markers = coord_markers.map(function(value, index){
			value.allow_plot = true;
			return	value;
		});
	
		for (var i = 0; i < coord_markers.length; i++) {
			var temp_val = coord_markers[i].city_id;

			if(temp[temp_val] && temp[temp_val].data && temp[temp_val].data.length > 0){
				data = _AlterCtrl.getCityArray(coord_markers, temp_val, 'data'); 
				result.push(data);

				sub_data = _AlterCtrl.getCityArray(coord_markers, temp_val, 'data');
				//console.log("sub data", sub_data);

				sub_data_branch = temp[temp_val].data.reverse();
				//_AlterCtrl.setSortArrayOrder(sub_data_branch);
				count = sub_data.index + ctr || null;

				//console.log("Via Cities : ", sub_data_branch);

				for (var j = 0; j < sub_data_branch.length; j++) { 
					if(count){
						sub_data_detail = {
							city_id: sub_data_branch[j].city_id,
				    	    name: sub_data_branch[j].city_via_name,
				    	    position: setLatLng(sub_data_branch[j]),
				    	    allow_plot: false
						};
						console.log("Sub Details Name", sub_data_detail );
						result.splice(count, 0, sub_data_detail);
						ctr++;
					}
				}

			}else{
				data = _AlterCtrl.getCityArray(coord_markers, temp_val, 'data');
				result.push(data);
			}
		} 
		// //console.log("result lol : ", result);
		return result ? result : coord_markers;
	}
	/* -------------------------------------------- #end --------------------------------------------- */

	/* ------------------------------ new updated code here may 17 2016 ------------------------------- */
	function routeVisionMarkersAutoPolylines(coord_markers){
		//console.log("routeVisionMarkersAutoPolylines");
		var route_charted = [], data, origin, destination;

		origin = getCleanMarkersArray('single', coord_markers[0]);
		//console.log("Origin", origin);

		for (var i = 1; i < coord_markers.length - 1; i++) {
			if(coord_markers[i]){
				data = getCleanMarkersArray('single', coord_markers[i]);
				route_charted.push(data);
				//console.log("Route Chrated", route_charted);
			}
		}

		destination = getCleanMarkersArray('single', coord_markers[coord_markers.length - 1]);
		routeCoord(origin, destination, route_charted);
	}
	/* -------------------------------------------- #end --------------------------------------------- */

	function routeVisionMarkersManualPolylines(coord_markers){

		////console.log("routeVisionMarkersManualPolylines");
		////console.log("CHECKON : ", coord_markers);

		var coord_data = [], origin, destination, coord_history,
		coord_markers_clean = getCleanMarkersArray('multi', coord_markers),
		coord_max = coord_markers_clean.length,
		count = 0;

		_ggl_renderer_globally_border.setMap(null);
		_ggl_renderer_globally_border.setMap(_map);

		_ggl_renderer_globally.setMap(null);
		_ggl_renderer_globally.setMap(_map);
		routeVisionMarkersLoadingChecker(coord_markers, coord_data);

		////console.log("WHOA WOHA : ", _coord_route_history);

		for (var i = 0; i < coord_max; i++) {
			if(coord_markers_clean[i + 1]){
				//console.log("PRE ORIGIN : ", coord_markers_clean[i]);
				//console.log("PRE DESTINASTION : ", coord_markers_clean[i + 1]);

				(function(i){
					coord_history = _AlterCtrl.getCoordRouteArray(_coord_route_history, coord_markers_clean[i], coord_markers_clean[i + 1], 'data');
					//console.log("COORD HISTORY", coord_history);

					if(coord_history){
						//console.log("BACK TO THE FUTURE");
						coord_history.index = i;
						coord_data.push(coord_history);
						count++;
					}else{
						//console.log("CURRENTLY IN FUTURE");
						setTimeout(function(){
							origin = coord_markers_clean[i];
							destination = coord_markers_clean[i + 1];
							//console.log("CHECK ORIGIN : ", origin);
							//console.log("CHECK DESTINASTION : ", destination);
							routeCoordManual(origin, destination, i).then(function(index, request, response, status) {
								return {
									index : index,
									request : request,
									response : response,
									status : status
								};
								console.log("status", status );
							}).done(function(result){
								coord_data.push(result);
								count++;
							});
						}, 800 * i);
					}
				}(i));
			}
		};

		var coord_time = setInterval(function(){
			//console.log('WORKING TEST SET '+count+' == '+coord_max+' : ', coord_data);
			routeVisionMarkersLoadingChecker(coord_markers, coord_data);
			routeCoordManualStruct(coord_max - 1, count, coord_time, coord_data, coord_markers);
		}, 500);

	}


	function routeVisionMarkersLoadingChecker(coord_markers, coord_data){

		var coord_extra;

		//FOR INVIDUAL LOADING PURPOSES
		if(data_temp_edit_coords === false && !coord_data.length){

			//console.log('ajax function noh : ', coord_markers);
			// origin-p
			coord_extra = {
				request : {
					origin : {
						city_id : coord_markers[0].city_id,
						title : coord_markers[0].title,
						lat : coord_markers[0].position.lat(),
						lng : coord_markers[0].position.lng()
					}					
				}
			};

			_DomCtrl.setLoadingShow('data', false, coord_markers, [coord_extra]);
			
		}else{

			_DomCtrl.setLoadingShow('data', false, coord_markers, coord_data);

		}

		_DomCtrl.setLoadingShow('finalize', false);

	}

	function routeVisionMarkersIdentifier(coord_markers){

		if(_coord_marked_identifier){

			for (var i = 0; i < _coord_marked_identifier.length; i++) {
				_coord_marked_identifier[i].setMap(null);
			};

		}

		for (var i = 0; i < coord_markers.length; i++) {

			var data = new _ggl.Circle({
				strokeColor: '#FF0000',
				strokeOpacity: 0.5,
				strokeWeight: 5,
				fillColor: '#FFFFFF',
				fillOpacity: 0.5,
				radius: 1000,
				map: _map
			});

			data.bindTo('center', coord_markers[i], 'position');
			_coord_marked_identifier.push(data);

		};

	}

	function routeCoord(origin, destination, charted) {

		var way_points = [],
		request = {
			origin: setLatLng(origin),
			destination: setLatLng(destination),
			travelMode: _ggl.TravelMode.DRIVING,
			optimizeWaypoints: true
		};

		if(charted.length){

			for (var i = 0; i < charted.length; i++) {
				way_points.push({
					location: setLatLng(charted[i]),
					stopover: true
				});
			}

			request.waypoints = way_points;
			request.optimizeWaypoints = false;

		}

		_DomCtrl.setLoadingShow('finalize', false);

		_ggl_direction.route(request, function (response, status) {

			if (status == _ggl.DirectionsStatus.OK) {

				_ggl_renderer.setMap(_map);
				_ggl_renderer.setDirections(response);

				setTimeout(function(){

					_DomCtrl.setLoadingShow('finalize', true);
					_DomCtrl.setLoadingHideQuick();

				}, 2000);

			}else{

				alert('Routing coordinates was not successful for the following reason: ' + status);

			}

		});

	}

	function routeCoordManual(origin, destination, index) {

		var dfd = $.Deferred();

		_ggl_direction.route({
			origin: setLatLng(origin),
			destination: setLatLng(destination),
			travelMode: _ggl.DirectionsTravelMode.WALKING,
			optimizeWaypoints: false

		}, (function(i, o, d){ return function (response, status) {

			if (status == _ggl.DirectionsStatus.OK) {

				dfd.resolve(index, {origin : o, destination : d}, response, true);

			}else{

				dfd.resolve(index, {origin : o, destination : d}, null, false);

			} 

		}}(index,origin,destination)));

		return dfd.promise();
	}

	function routeCoordManualStruct(coord_max, count, coord_time, coord_data, coord_markers){

		var max, coord_exist, validate, _ggl_path = new _ggl.MVCArray;

		//console.log("THNEORY : ", coord_data, " - ", coord_max);

		validate = _AlterCtrl.validIndexArray(coord_data, coord_max);

		//console.log("MAG TEST PAQKO KUNG MO GANA NAVBA : ", validate,' && ',count,' == ',coord_max);

		if(validate && count == coord_max){

			_DomCtrl.setLoadingShow('finalize', false);
			clearInterval(coord_time);
			_AlterCtrl.setSortArray(coord_data);

			for (var i = 0; i < coord_data.length; i++) {

				if(coord_data[i].status){

					max = coord_data[i].response.routes[0].overview_path.length;

					for (var j = 0; j < max; j++) {
						_ggl_path.push(coord_data[i].response.routes[0].overview_path[j]);
					}

				}else{

					coord_exist = _ggl_path.getArray();

					if(!_AlterCtrl.getLatLngArray(coord_exist, coord_data[i].request.origin.lat, coord_data[i].request.origin.lng)){
						_ggl_path.push(setLatLng(coord_data[i].request.origin));
					}

					_ggl_path.push(setLatLng(coord_data[i].request.destination));

				}

			};

			_coord_route_history = coord_data;
			_ggl_renderer_globally_border.setPath(_ggl_path);
			_ggl_renderer_globally.setPath(_ggl_path);

			//console.log("WUT ISS THE FINAL HISTORY : ", _coord_route_history);

			setTimeout(function(){

				_DomCtrl.setLoadingShow('finalize', true);
				_DomCtrl.setLoadingHideQuick();

			}, 2000);

			
		}

	}

	function routeVisionAlternatives(alter_array){

		var alter_renderer = [], alter_data, alter_marker, alter_stroke,
		alter_stroke_alter = _map_style[3].route_stroke_alter;
		alter_stroke_group = _map_style[3].route_stroke_group;
		alter_stroke_border = _map_style[3].route_stroke_alter_border;

		for (var i = 0; i < alter_array.length; i++) {

			alter_stroke = (alter_array[i].type == 'group') ? alter_stroke_group : alter_stroke_alter;

			alter_data = {
				type : alter_array[i].type,
				renderer : new _ggl.Polyline(alter_stroke),
				renderer_border : new _ggl.Polyline(alter_stroke_border),
				path : new _ggl.MVCArray,
				coord : alter_array[i].coord,
				origin : alter_array[i].coord[0],
				destination : alter_array[i].coord[alter_array[i].coord.length - 1]
			};

			alter_renderer.push(alter_data);

		}

		routeVisionAlternativesRenderer(alter_renderer, 0, alter_renderer.length); // LOOP EVERY ALTERNATIVE WITH DELAYS

	}

	function routeVisionAlternativesRenderer(alter_renderer, count, max){

		if(alter_renderer[count]){

			routeVisionAlternativesManualPolylines(alter_renderer[count], alter_renderer[count].type, function(result){

				if(result === true && count < max){

					count++;
					routeVisionAlternativesRenderer(alter_renderer, count, max);

				}

			});

		}

	}

	function routeVisionAlternativesManualPolylines(alter_renderer_data, alter_type, callback){
		//console.log("routeVisionAlternativesManualPolylines");	
		var alter_markers = alter_renderer_data.coord,
		alter_renderer = alter_renderer_data.renderer,
		alter_renderer_border = alter_renderer_data.renderer_border,
		alter_path = alter_renderer_data.path,
		coord_data = [], origin, destination,
		coord_max = alter_markers.length,
		count = 0,
		result = false;

		alter_renderer_border.setMap(null);
		alter_renderer_border.setMap(_map);

		alter_renderer.setMap(null);
		alter_renderer.setMap(_map);

		for (var j = 0; j < coord_max; j++) {

			if(alter_markers[j + 1]){

				(function(j){

					setTimeout(function(){

						origin = alter_markers[j];
						destination = alter_markers[j + 1];

						routeCoordManual(origin, destination, j).then(function(index, request, response, status) {

							return {
								index : index,
								request : request,
								response : response,
								status : status
							};
							console.log("status", status)
							 //console.log("index", index);
						}).done(function(result){
							coord_data.push(result);
							count++;
						});

							/*coord_data = 0;
								function iSuspectToBeLoopingInfititely() {
								  //console.log("infinite loop");
								  coord_data += 1;
								  if (coord_data > 100) { debugger; }

								}*/

					}, 1000 * j);

				}(j));

			}

		};

		var coord_time = setInterval(function(){

			result = routeCoordAlternativesStruct(coord_max - 1, count, coord_time, coord_data, alter_markers, alter_renderer, alter_renderer_border, alter_path, alter_type)

			callback(result);

		}, 1000);

	}
	

	function routeCoordAlternativesStruct(coord_max, count, coord_time, coord_data, alter_markers, alter_renderer, alter_renderer_border, alter_path, alter_type){

		var max, coord_exist, validate, alter_id,
		alter_renderer = alter_renderer,
		alter_renderer_border = alter_renderer_border,
		alter_path = alter_path,
		result = false;

		//console.log("WUT THE F : ", coord_data);

		validate = _AlterCtrl.validIndexArray(coord_data, coord_max);

		if(validate && count == coord_max){

			clearInterval(coord_time);
			_AlterCtrl.setSortArray(coord_data);

			for (var i = 0; i < coord_data.length; i++) {

				if(coord_data[i].status){

					max = coord_data[i].response.routes[0].overview_path.length;

					for (var j = 0; j < max; j++) {
						alter_path.push(coord_data[i].response.routes[0].overview_path[j]);
					}

				}else{

					coord_exist = alter_path.getArray();

					if(!_AlterCtrl.getLatLngArray(coord_exist, coord_data[i].request.origin.lat, coord_data[i].request.origin.lng)){
						alter_path.push(setLatLng(coord_data[i].request.origin));
					}

					alter_path.push(setLatLng(coord_data[i].request.destination));

				}

			};

			alter_renderer_border.setPath(alter_path);
			alter_renderer_border.alter_id = alter_markers[0].other_id;

			alter_renderer.setPath(alter_path);
			alter_renderer.alter_id = alter_markers[0].other_id;

			_ggl.event.addListener(alter_renderer, 'click', function(event){

				if(alter_type == 'group'){

					itineraryCredentials('update', 'alter', '');
					itineraryCredentials('update', 'group', alter_renderer.alter_id);

				}else{

					itineraryCredentials('update', 'alter', alter_renderer.alter_id);
					itineraryCredentials('update', 'group', '');

				}
				
				dom_form_submit.click();

			});

			_ggl.event.addListener(alter_renderer, 'mouseover', function(event){

				alter_renderer.setOptions({
					strokeOpacity: 0.5
				});

			});

			_ggl.event.addListener(alter_renderer, 'mouseout', function(event){

				alter_renderer.setOptions({
					strokeOpacity: 1
				});

			});

			alter_id = alter_markers[0].other_id;
			_DomCtrl.setLoadingHideAlter(alter_type, alter_id);

			result = true;

		}

		return result;

	}

	return {
		initialize : initialize, //USED
		setMap : setMap, //USED
		setMapStyle : setMapStyle, //USED
		setGeo : setGeo, //NOT USE
		setLatLng : setLatLng, //USED
		setMarkers : setMarkers, //USED
		setMapMarkersIcons : setMapMarkersIcons, //USED
		setMapMarkers : setMapMarkers, //USED
		setMapRouted : setMapRouted, //USED
		setMapMarkersOptional : setMapMarkersOptional, //NOT USE
		setMapInfobox : setMapInfobox, //USED
		setMapControlPosition : setMapControlPosition, //NOT USE
		setMarkerInfoWindows : setMarkerInfoWindows, //USED
		setMarkerInfoWindowsData : setMarkerInfoWindowsData, //USED
		setListOptionalEvent : setListOptionalEvent, //NOT USE
		setListMultiRoute : setListMultiRoute, //NOT USE
		setMarkerMultiEvent : setMarkerMultiEvent, //USED
		setMarkerMultiState : setMarkerMultiState, //USED
		setMarkerMultiClear : setMarkerMultiClear, //USED
		setMarkerMultiProcess : setMarkerMultiProcess, //USED
		setClearMarkers : setClearMarkers, //USED
		setClearMarkersOccurrence : setClearMarkersOccurrence, //USED
		setCoordCompass : setCoordCompass, //USED
		setCoordCompassLiberal : setCoordCompassLiberal, //NOT USE
		setCompassPoint : setCompassPoint, //USED
		setCompassPointLiberal : setCompassPointLiberal, //NOT USE
		setNearbyMarkerCompass : setNearbyMarkerCompass, //NOT USE
		setNearbyMarkerCompassLiberal : setNearbyMarkerCompassLiberal, //NOT USE
		setCollectRoute : setCollectRoute, //NOT USE
		setListRouteGeo : setListRouteGeo, //NOT USE
		setListRouteEvent : setListRouteEvent, //NOT USE
		setListRoute : setListRoute, //NOT USE
		setListRouteClear : setListRouteClear, //NOT USE
		setListRouteCoord : setListRouteCoord, //NOT USE
		setMultiRouteCoord : setMultiRouteCoord, //USED
		setMarkerRouteProcess : setMarkerRouteProcess, //USED
		setListRouteCoordPriority : setListRouteCoordPriority, //NOT USE
		setListRouteCoordOptional : setListRouteCoordOptional, //NOT USE
		setListOption : setListOption, //NOT USE
		setListTempCollectCities : setListTempCollectCities, //USED
		setListTempCollectTransport : setListTempCollectTransport, //USED
		setIsolateCities : setIsolateCities, //NOT USE
		setIsolateCitiesFiltered : setIsolateCitiesFiltered, //NOT USE
		setListCities : setListCities, //NOT USE
		setListRouteFixed : setListRouteFixed, //NOT USE
		setListRouteCharted : setListRouteCharted, //USED
		setListRouteChartedFiltered : setListRouteChartedFiltered, //USED
		setMarkersDistance : setMarkersDistance, //USED
		setAlterCoord : setAlterCoord, //NOT USE
		setAutoCoord : setAutoCoord, //USED
		setRouteCoord : setRouteCoord, //USED
		setMarkerRoutesOptions : setMarkerRoutesOptions, //USED murag main
		setMarkerRouteVisibility : setMarkerRouteVisibility, //USED
		setBtnEditRoute : setBtnEditRoute, //USED
		setBtnEditAction : setBtnEditAction, //USED
		setBtnProposedScroll : setBtnProposedScroll, //USED
		getCleanMarkersArray : getCleanMarkersArray, //USED
		getNearbyMarker : getNearbyMarker, //NOT USE
		getListOptionalData : getListOptionalData, //USED
		getDistanceCoord : getDistanceCoord, //USED
		validMapInfobox : validMapInfobox, //USED
		validMarkerRouteProcess : validMarkerRouteProcess, //USED
		validCompass : validCompass, //NOT USE
		validCompassLiberal : validCompassLiberal, //USED
		validCompassLiberalWide : validCompassLiberalWide, //USED
		validCompassLiberalLight : validCompassLiberalLight, //NOT USE
		validEndToEnd : validEndToEnd, //NOT USE
		validCoordPointToPoint : validCoordPointToPoint, //USED
		validNearbyMarker : validNearbyMarker, //NOT USE
		validNearbyMarkerCollection : validNearbyMarkerCollection, //NOT USE
		validIsolateCitiesFilteredCoord : validIsolateCitiesFilteredCoord, //NOT USE
		validIsolateCitiesFilteredTransport : validIsolateCitiesFilteredTransport, //USED
		validIsolateCitiesFilteredLiberal : validIsolateCitiesFilteredLiberal, //USED
		validRouteBetweenCoord : validRouteBetweenCoord, //USED
		validRouteChartedEndToEnd : validRouteChartedEndToEnd, //USED
		validMarkerRouteVisibility : validMarkerRouteVisibility, //USED
		calcRadius : calcRadius, //USED
		calcDegree : calcDegree, //USED
		calcBearing : calcBearing, //USED
		calcDistanceRearrange : calcDistanceRearrange, //USED
		calcNearbyDistance : calcNearbyDistance, //NOT USE
		calcWithinRadiusMarker : calcWithinRadiusMarker, //USED
		calcNearbyMarker : calcNearbyMarker, //USED
		routeVisionCoord : routeVisionCoord, //USED
		routeFitBounds : routeFitBounds, //USED
		routeVisionMarkers : routeVisionMarkers, //USED
		routeVisionMarkersAutoPolylines : routeVisionMarkersAutoPolylines, //USED
		routeVisionMarkersManualPolylines : routeVisionMarkersManualPolylines, //USED
		routeVisionMarkersLoadingChecker : routeVisionMarkersLoadingChecker, //USED
		routeVisionMarkersIdentifier : routeVisionMarkersIdentifier, //USED
		routeCoord : routeCoord, //USED
		routeCoordManual : routeCoordManual, //USED
		routeCoordManualStruct : routeCoordManualStruct, //USED
		routeVisionAlternatives : routeVisionAlternatives, //USED
		routeVisionAlternativesRenderer : routeVisionAlternativesRenderer, //USED
		routeVisionAlternativesManualPolylines : routeVisionAlternativesManualPolylines, //USED
		routeCoordAlternativesStruct : routeCoordAlternativesStruct //USED
	};

})();

$(document).ready(init());

