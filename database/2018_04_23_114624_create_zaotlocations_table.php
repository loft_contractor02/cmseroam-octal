<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZaotlocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('zaotlocations'))
        {
        Schema::create('zaotlocations', function (Blueprint $table) {
            $table->increments('id');
            $table->text('LocationType')->nullable();
            $table->text('LocationCode')->nullable();
            $table->text('CountryCode')->nullable();
            $table->text('StateCode')->nullable();
            $table->text('LocationName')->nullable();
            $table->timestamps();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zaotlocations');
    }
}
