<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZaecitiesMappedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('zaecitiesmapped'))
        {
            Schema::create('zaecitiesmapped', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('ae_city_id');
                $table->integer('eroam_city_id');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zaecitiesmapped');
    }
}
