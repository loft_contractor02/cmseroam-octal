<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZviatorlocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('zviatorlocations'))
        {
        Schema::create('zviatorlocations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('destinationId',255)->nullable();
            $table->string('destinationName',255)->nullable();
            $table->string('destinationType',255)->nullable();
            $table->string('iataCode',255)->nullable();
            $table->string('latitude',255)->nullable();
            $table->string('longitude',255)->nullable();
            $table->string('timeZone',255)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zviatorlocations');
    }
}
