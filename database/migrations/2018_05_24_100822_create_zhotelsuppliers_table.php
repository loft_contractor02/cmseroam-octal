<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZhotelsuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('zhotelsuppliers'))
        {
        Schema::create('zhotelsuppliers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('name');
            $table->string('abbreviation');
            $table->text('logo')->nullable();
            $table->text('description')->nullable();
            $table->string('product_contact_name')->nullable();
            $table->string('product_contact_email')->nullable();
            $table->string('product_contact_title')->nullable();
            $table->string('product_contact_phone')->nullable();
            $table->string('reservation_contact_name')->nullable();
            $table->string('reservation_contact_landline')->nullable();
            $table->string('reservation_contact_email')->nullable();
            $table->string('reservation_contact_free_phone')->nullable();
            $table->string('accounts_contact_name')->nullable();
            $table->string('accounts_contact_email')->nullable();
            $table->string('accounts_contact_title')->nullable();
            $table->string('accounts_contact_phone')->nullable();
            $table->string('special_notes')->nullable();
            $table->string('remarks')->nullable();
            $table->string('percentage')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->text('cancellation_formula')->nullable();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zhotelsuppliers');
    }
}
