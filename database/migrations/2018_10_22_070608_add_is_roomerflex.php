<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsRoomerflex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('itenaryorders', function (Blueprint $table) {
            $table->tinyInteger('is_roomerflex')->after('invoice_no')->nullable();
            $table->double('roomerflex_amount',8, 2)->after('total_amount')->nullable();
        });
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('itenaryorders', function (Blueprint $table) {
            $table->dropColumn('is_roomerflex');
            $table->dropColumn('roomerflex_amount');
        });
    }
}
