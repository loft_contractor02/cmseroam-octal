<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZhotelseasonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('zhotelseason'))
        {
        Schema::create('zhotelseason', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
			$table->date('[from]')->nullable();
            $table->date('[to]')->nullable();
			$table->integer('hotel_id');
			$table->integer('hotel_supplire_id');
			$table->timestamps();
			$table->softDeletes();
			$table->text('cancellation_policy')->nullable();
            $table->text('cancellation_formula')->nullable();
            $table->integer('currency_id')->nullable();
        });
        }   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zhotelseason');
    }
}
