<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblflightpaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('tblflightpayment'))
        {
        Schema::create('tblflightpayment', function (Blueprint $table) {
            $table->increments('flight_id');
            $table->integer('tour_id')->nullable();
            $table->integer('season_id')->nullable();
            $table->string('flight_currency_id')->nullable();
            $table->decimal('flightPrice',16,4)->nullable();
            $table->text('flightDescription')->nullable();
            $table->integer('flight_depart_city')->nullable();
            $table->tinyInteger('isMandatory')->nullable();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblflightpayment');
    }
}
