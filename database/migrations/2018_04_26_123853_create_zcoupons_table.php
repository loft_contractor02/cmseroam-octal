<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZcouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('zcoupons')){  
            Schema::create('zcoupons', function (Blueprint $table) {
                $table->increments('id');
                $table->string('title');
                $table->string('code',50);
                $table->date('start_date');
                $table->date('end_date');
                $table->string('coupon_currency')->nullable();
                $table->string('coupon_type');
                $table->decimal('amount', 8, 2);
                $table->tinyInteger('is_active');
                $table->tinyInteger('is_deleted');
                $table->string('deleted_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zcoupons');
    }
}
