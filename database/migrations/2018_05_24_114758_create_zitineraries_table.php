<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZitinerariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('zitineraries'))
        {
        Schema::create('zitineraries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('is_booked')->nullable();
            $table->integer('customer_id')->nullable();
            $table->integer('agent_id')->nullable();
            $table->string('reference_no')->nullable();
            $table->string('title')->nullable();
            $table->integer('from_city_id');
            $table->integer('to_city_id');
            $table->integer('num_of_travellers');
            $table->date('travel_date');
            $table->tinyInteger('is_deleted');
            $table->integer('total_itinerary_legs');
            $table->decimal('total_amount',8,2);
            $table->string('currency');
            $table->timestamps();
            $table->integer('total_days');
            $table->decimal('total_per_person',8,2);
            $table->decimal('total_refundable',8,2)->nullable();
            $table->softDeletes();
            $table->tinyInteger('is_saved_itinerary');
            $table->text('ae_api_response')->nullable();
            $table->text('search_session')->nullable();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zitineraries');
    }
}
