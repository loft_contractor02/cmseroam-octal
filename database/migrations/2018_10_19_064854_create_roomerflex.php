<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomerflex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roomerflex', function (Blueprint $table) {
            $table->increments('id');
            $table->string('request_token');
            $table->double('request_refund_rate',8,2);
            $table->double('request_fee',8,2);
            $table->double('request_fee_per_night',8,2);
            $table->string('request_currency');
            $table->boolean('protection_success');
            $table->boolean('protection_registered');
            $table->string('protection_token');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roomerflex');
    }
}
