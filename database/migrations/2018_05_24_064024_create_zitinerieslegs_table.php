<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZitinerieslegsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('zitinerieslegs'))
        {
        Schema::create('zitinerieslegs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('itinerary_id');
            $table->integer('from_city_id');
            $table->integer('to_city_id');
            $table->integer('travel_sequence');
            $table->string('transport_type')->nullable();
            $table->string('hotel_type')->nullable();
            $table->date('from_date')->nullable();
            $table->date('to_date')->nullable();
            $table->string('activity_type')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zitinerieslegs');
    }
}
